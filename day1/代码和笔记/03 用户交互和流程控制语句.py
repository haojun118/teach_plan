# 用户交互：（输入/输出）
# input(提示语句!)  -- 输入

# msg = input("请输入您内容:")
# int("5") ---> 5
# print(int(msg) + 5)


# int() -- 将字符串类型强制转换成整型
# type -- 查看数据类型
# python3中的input获取到内容都是字符串
# python2中的input获取的就是数据本身

# 流程控制语句:
# 单 if
# if -- 如果
# 缩进 官方推荐4个空格,Tab  空格和tab混合使用

# if 条件:
# 缩进 结果

# money = 10
# print("从学校出发")
# if money >= 10:
#     print("买个炸鸡")
#     print("买个啤酒")
# print("走啊走")
# print("到家了")

# if else 二选一
# if 条件:
# 缩进 结果
# else:
# 缩进 结果

# print(123)
# if 3>2:
#     print("这是如果执行了")
#     print("123")
#     print("234")
# else:
#     print("这是否则执行了")
# print(345)

# if elif elif elif  多选一或不选
# if -- 如果
# elif -- 在如果

# if 条件:
#     结果
# elif 条件:
#     结果
# elif 条件:
#     结果
# elif 条件:
#     结果

# if 3>2:
#     print("这是A")
# elif 4>3:
#     print("这是B")
# elif 5>3:
#     print("这是C")
# elif 6>3:
#     print("这是D")

# if 1>2:
#     print("这是A")
# elif 4>3:
#     print("这是B")
# elif 5>3:
#     print("这是C")
# elif 6>3:
#     print("这是D")

# if 1>2:
#     print("这是A")
# elif 2>3:
#     print("这是B")
# elif 2>3:
#     print("这是C")
# elif 2>3:
#     print("这是D")

# if elif elif else 多个选一个

# if 条件:
#     结果
# elif 条件:
#     结果
# elif 条件:
#     结果
# else:
#     结果

# if 1>2:
#     print("A")
# elif 2>3:
#     print("B")
# elif 5>6:
#     print("c")
# else:
#     print("D")


# if if if # 多个条件选多个

# if 条件:
#     结果
# if 条件:
#     结果
# if 条件:
#     结果

# if 3>2:
#     print("A")
# if 4>2:
#     print("B")
# if 6>3:
#     print("C")

# if 嵌套：

# if 条件：
#     if 条件：
#         结果

# sex = "女"
# age = 30
# if sex == "女":
#     if age == 30:
#         print("进来坐一坐")
#     else:
#         print("隔壁找太白")
# else:
#     print("滚犊子")
