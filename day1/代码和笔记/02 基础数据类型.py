# 基础数据类型初识
# 字符串
# 数字
# 布尔值
# 列表
# 元祖
# 字典
# 集合

# 字符串 -- str
# 字符串用于存储一些数据
# 在python中只要是用引号引起来就是字符串
"abc"  'abc' """absc"""

# a = """absc"""
# print(a)

# 数字 -- int
# 用于计算,用于比较

# a = 10 + 5
# print(a)

# b = 10 - 2
# print(b)

# b = 10 * 2
# print(b)

# b = 10 / 2
# print(b)

# python3: 除法的时候返回的是小数(浮点数)
# pyhton2: 除法的时候返回的是整数(向下取整)

# a = "alex"
# b = "dsb"
# c = a + b   # 字符串拼接
# print(c)

# a = "alex" + "Dsb" * 5
# print(a)

#字符串+: 字符串和字符串相加
# 字符串*: 字符串和数字相乘

# 布尔值 -- bool
# 用于判断:
# True  -- 真
# False -- 假

# print(3 > 2)
# print(3 < 2)

# 在赋值的时候先执行等号右边的内容
