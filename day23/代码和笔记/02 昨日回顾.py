# 1.继承

# 子类 : 派生类
# 父类 : 基类,超类

# A(B) A类继承了B类
# 当A类继承了B类,A类中没有__init__方法时执行B类的__init__方法

# 通过子类的类名使用父类的属性和方法
# 查找顺序: 当前类 --> 当前类的父类 ---> 当前类父类的父类

# 通过子类的对象使用父类的属性和方法
# 查找顺序: 当前类实例化的对象 --> 当前类 --> 当前类的父类 ---> 当前类父类的父类

# 既要执行子类又要执行父类
# 方法一:
# class A:
#     def __init__(self,name,age,sex):
#         self.name = name
#         self.age = age
#         self.sex = sex
#
# class B(A):
#     def __init__(self,name,age,sex,hobby):  # 构造方法(初始化方法)
#         super().__init__(name,age,sex)      # 重构方法
#         self.hobby = hobby
#
# b = B("瑞瑞",18,"男","睡")

# 方法二:
# class A:
#     def __init__(self,name,age,sex):
#         self.name = name
#         self.age = age
#         self.sex = sex
#
# class B:
#     def __init__(self,name,age,sex,hobby):
#         A.__init__(self,name,age,sex)
#         self.hobby = hobby
#
# b = B("瑞瑞",18,"男","睡")

# 继承顺序: 不可逆

# 继承的优点:
# 1.结构清晰,可读性高
# 2.增加耦合性
# 3.节省重复代码

# python2.2 之前都是经典类,python2.2之后经典类和新式类共存
# python2.2之后继承了object的就是新式类,不继承就是经典类
# python3 不管继不继承都是新式类

