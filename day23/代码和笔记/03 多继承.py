# encoding:utf-8

# 多继承是继承多个父类

# 经典类: 多继承时从左向右执行
# class Immortal(object):
#
#     def fly(self):
#         print("会飞")
#
#     # def eat_tao(self):
#     #     print("会遁地")
#
# class Monkey:
#
#     def eat_tao(self):
#         print("吃桃")
#
#     def climb_tree(self):
#         print("会爬树")
#
# class SunWuKong(Immortal,Monkey):
#     pass
#
# sxz = SunWuKong()
# sxz.eat_tao()



# class A:
#     # name = "宝元"
#     pass
#
# class B(A):
#     pass
#     # name = "太正博"
#
# class C(A):
#     # name = "alex"
#     pass
#
# class D(B, C):
#     # name = "日魔22"
#     pass
#
# class E:
#     pass
#     # name = "日魔11"
#
# class F(E):
#     # name = "日魔"
#     pass
#
# class G(F, D):
#     # name = "bb"
#     pass
# class H:
#     # name = "aaa"
#     pass
#
# class Foo(H, G):
#     pass
#
# f = Foo()
# print(f.name)


# 经典类: (深度优先) 左侧优点,一条路走到头,找不到会到起点向右查询


# class O(object):
#     # name = "宝元"
#     pass
#
# class D(O):
#     # name = "日魔"
#     pass
#
# class E(O):
#     # name = "太正博"
#     pass
#
# class F(O):
#     # name = "alex"
#     pass
#
# class B(D,E):
#     # name = "三哥"
#     pass
#
# class C(E,F):
#     # name = "文刚"
#     pass
#
# class A(B,C):
#     # name = "春生"
#     pass
#
# a = A()
# print a.name

# 新式类: (c3)
# mro(Child(Base1，Base2)) = [ Child ] + merge( mro(Base1), mro(Base2), [ Base1, Base2] )
# mro(A(B,C)) = [ A ] + merge( mro(B), mro(C), [ B, C])
# mro(A(B,C)) = [ A ] + merge([B,D,E,O], [C,E,F,O], [ B, C])
# mro(B(D,E)) = [B,D,E,O]
# mro(C(E,F)) = [C,E,F,O]
#
# mro(A(B,C)) = [A,B,D,C,E,F,O]
# print A.mro()




# class A(object):
#     pass
#
# class B(A):
#     pass
#
# class C(A):
#     pass
#
# class F:
#     pass
#
# class D(B,F):
#     pass
#
# class E(D,C):
#     pass

# mro(Child(Base1，Base2)) = [ Child ] + merge( mro(Base1), mro(Base2), [ Base1, Base2] )
# mro(E(D,C)) = [ E ] + merge( mro(D), mro(C), [ D, C] )
# [E,D,B,C,A,F]

# 经典类不能使用mro  新式类才能使用mro
# print(E.mro())

# class B:
#     pass
#
# class V(B):
#     pass
#
# class F(B):
#     pass
#
# class A(F,B):
#     pass
#
# class D(A):
#     pass
#
# class R(D,A):
#     pass


# [R,D,A,F,B]
"""
mro(Child(Base1，Base2)) = [ Child ] + merge( mro(Base1), mro(Base2), [ Base1, Base2] )
mro(R(D，A)) = [ R,D,A,F,B ]
"""
# print(R.mro())