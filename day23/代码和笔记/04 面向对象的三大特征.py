# 1.继承   √
# 2.封装 : 将一些代码或数据存储到某个空间中   √
#          函数,对象,类

# 3.多态 : Python默认就是多态               √

# a = "alex"
# a = 100
# int a = 100

# 4.print带色

# 开头格式: \033[
# 结尾格式: \033[0m
# print('\033[0;35m字体变色，但无背景色 \033[0m')  # 有高亮
# print('\033[1;35m字体变色，但无背景色 \033[0m')  # 有高亮
# print('\033[4;35m字体变色，但无背景色 \033[0m')  # 有高亮
# print('\033[5;35m字体变色，但无背景色 \033[0m')  # 有高亮  # 不好使
# print('\033[7;35m字体变色，但无背景色 \033[0m')  # 有高亮
# print('\033[8;35m字体变色，但无背景色 \033[0m')  # 有高亮  # 不好使

# print('\033[1;32m 字体变色，但无背景色 \033[0m')  # 有高亮
# print('\033[1;33m 字体变色，但无背景色 \033[0m')  # 有高亮
# print('\033[1;36m 字体变色，但无背景色 \033[0m')  # 有高亮

# print('\033[1;32m 字体变色，但无背景色 \033[0m')  # 有高亮
# print('\033[1;33m 字体变色，但无背景色 \033[0m')  # 有高亮


# print('\033[1;36;40m 字体变色，但无背景色 \033[0m')  # 有高亮
# print('\033[1;32;40m 字体变色，但无背景色 \033[0m')  # 有高亮
# print('\033[1;30;43m 字体变色，但无背景色 \033[0m')  # 有高亮
# print('\033[1;30;43m 字体变色，但无背景色 \033[0m')  # 有高亮

# print('\033[0;36m骑牛远远过前村，')
# print('短笛横吹隔陇闻。')
# print('多少长安名利客，')
# print('机关用尽不如君。\033[0m')