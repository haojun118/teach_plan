from django.db import models


class Book(models.Model):
    title = models.CharField(max_length=32, unique=True)


class Author(models.Model):
    name = models.CharField(max_length=32)
    books = models.ManyToManyField('Book',through='AuthorBook',through_fields=['author','book'])  # 只用django提供的查询方法  不用创建表 用用户创建的表AuthorBook


class AuthorBook(models.Model):
    book = models.ForeignKey('Book',on_delete=models.CASCADE,null=True)
    author = models.ForeignKey('Author',on_delete=models.CASCADE,related_name='x1',null=True)
    tuiianren = models.ForeignKey('Author',on_delete=models.CASCADE,related_name='x2',null=True)
    date = models.CharField(max_length=32)


