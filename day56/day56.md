## 内容回顾

### 1.django处理请求的流程：

1. 在地址栏中输入地址，回车发送一个请求；
2. wsgi服务器接收到http请求，封装request对象；
3. django根据请求的地址找到对应的函数，执行；
4. 函数执行的结果，django将结果封装成http响应的格式返回给浏览器。

### 2.发请求的途径

	1. 地址栏中输入地址 回车   GET请求
 	2. a标签  GET请求
 	3. form表单  GET/POST

### 3.get和post的区别

​	get   获取页面  

​    ?k1=v1&k1=v2       request.GET.get()   url 上携带的参数  

​	get请求没有请求体

​    post  提交数据

​	提交的数据 在请求体中

### 4.函数

```
def  func(request):
	# 逻辑
	return HttpResponse对象


request.GET    url上携带的参数   {} 
request.POST   POST请求提交的数据    {} 
request.method  请求方法  GET  POST 

HttpResponse('字符串')     返回字符串
render(request,'模板文件的名字',{k1:v1})     返回一个HTML页面
redirect('重定向的地址')    重定向
```

### 5.外键

描述  一对多

```python
class Publisher(models.Model):
	name = models.CharField(max_length=32)


class Book(models.Model):
	title = models.CharField(max_length=32)
    pub = models.ForeignKey('Publisher',on_delete=models.CASCADE)    # pub_id
```

展示

```python
from app01 import models
all_books = models.Book.objects.all()

for book in all_books:
    print(book.id)  print(book.pk)
    print(book)   # 对象  __str__
    print(book.title)   
	print(book.pub)      # 外键关联的对象  print(book.pub.pk)  print(book.pub.name)
    print(book.pub_id)   # 外键关联的对象id
```

新增

```
models.Book.objects.create(title='xxxx',pub=关联的对象)
models.Book.objects.create(title='xxxx',pub_id=id)

obj =models.Book(title='xxxx',pub=关联的对象)
obj.save()
```

删除

```
models.Book.objects.filter(id=id).delete()
models.Book.objects.filter(id=id).first().delete()
```

编辑

```
obj = models.Book.objects.filter(id=id).first()
obj.title = 'xxxxx'
# obj.pub = pub_obj
obj.pub_id = pub_obj.id
obj.save()
```

### 6.模板的语法

```
{{ 变量  }}

for循环
{% for i in list  %}
	{{ forloop.counter  }}
	{{ i }}
{% endfor %}


{%  if 条件 %}
	内容
{% endif %}

{%  if 条件 %}
	内容
{% else %}
	内容2
{% endif %}


{%  if 条件 %}
	内容
{%  elif 条件1 %}
	内容	
{% else %}
	内容2
{% endif %}

```

### 今日内容

作者的管理

作者  书籍  多对多的关系

```python
class Author(models.Model):
    name = models.CharField(max_length=32)
    books = models.ManyToManyField('Book')  # 不会生成字段  生成第三张表
```

展示

```python
all_authors = models.Author.objects.all()
for author in all_authors:
        print(author)
        print(author.name)
        print(author.books,type(author.books))  # 多对多的关系管理对象
        print(author.books.all(),type(author.books.all()))  # 关系对象列表
        

all_books = models.Book.objects.all()
for book in all_books:
	book  # 书籍对象
	book.title
	book.author_set         # 多对多的关系管理对象
	book.author_set.all()   # 书籍关联的所有的作者对象  对象列表  

```

新增

```
author_obj = models.Author.objects.create(name=author_name)  # 插入作者信息
author_obj.books.set(book_id) # 设置作者和书籍的多对多的关系
```

多对多关系表的创建方式：

1. django自己创建

   ```python
   class Book(models.Model):
       title = models.CharField(max_length=32, unique=True)
       pub = models.ForeignKey('Publisher', on_delete=models.CASCADE)
    
   
   class Author(models.Model):
       name = models.CharField(max_length=32)
       books = models.ManyToManyField('Book')  # 不会生成字段  生成第三张表
   ```

2. 自己手动创建

   ```python
   class Book(models.Model):
       title = models.CharField(max_length=32, unique=True)
   
   
   class Author(models.Model):
       name = models.CharField(max_length=32)
   
   
   class AuthorBook(models.Model):
       book = models.ForeignKey('Book',on_delete=models.CASCADE)
       author = models.ForeignKey('Author',on_delete=models.CASCADE)
       date = models.CharField(max_length=32)
   
   ```

3. 半自动创建(自己手动创建表  + ManyToManyField  可以利用查询方法  不能用set)

   ```python
   class Book(models.Model):
       title = models.CharField(max_length=32, unique=True)
   
   class Author(models.Model):
       name = models.CharField(max_length=32)
       books = models.ManyToManyField('Book',through='AuthorBook')  # 只用django提供的查询方法  不用创建表 用用户创建的表AuthorBook
   
   class AuthorBook(models.Model):
       book = models.ForeignKey('Book',on_delete=models.CASCADE)
       author = models.ForeignKey('Author',on_delete=models.CASCADE)
       date = models.CharField(max_length=32)
   
   ```

   ```python
   class Book(models.Model):
       title = models.CharField(max_length=32, unique=True)
   
   
   class Author(models.Model):
       name = models.CharField(max_length=32)
       books = models.ManyToManyField('Book',through='AuthorBook',through_fields=['author','book'])  # 只用django提供的查询方法  不用创建表 用用户创建的表AuthorBook
   
   
   class AuthorBook(models.Model):
       book = models.ForeignKey('Book',on_delete=models.CASCADE,null=True)
       author = models.ForeignKey('Author',on_delete=models.CASCADE,related_name='x1',null=True)
       tuiianren = models.ForeignKey('Author',on_delete=models.CASCADE,related_name='x2',null=True)
       date = models.CharField(max_length=32)
   
   ```

周末

学员管理系统

学生表  班级表  教师表 





