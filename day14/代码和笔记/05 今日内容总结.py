# 1.装饰器

# 开发封闭原则:
# 1.对扩展开放
# 2.对修改源代码及调用方式封闭

# 装饰器:不修改源代码及原调用方式的前提下,额外增加新功能

# 语法糖:写在被装饰的函数正上方

# def warpper(f):
#     def inner(*args,**kwargs):
#         print("被装饰函数执行前")
#         ret = f(*args,**kwargs)
#         print("被装饰函数执行后")
#         return ret
#     return inner
#
# @warpper
# def func(*args,**kwargs):
#     print(f"被装饰的{args,kwargs}")
#     return "我是func函数"
# print(func(1,2,3,4,5,6,7,8,a=1))


# def warpper(f):
#     def inner(*args,**kwargs):
#         f(*args,**kwargs)
#     return inner
#
# @warpper
# def func():
#     print(111)

# 一定要自己多敲