# 装饰器: 面试笔试必问

# 开放封闭原则:
# 1.代码扩展进行开放
# 2.修改源代码是封闭

# 在不修改源代码及调用方式,对功能进行额外添加就是开发封闭原则

# def index():
#     print("这是一个主页")
# index()

# 装饰(额外功能)   器:工具(函数)

# import time

# def index():
#     time.sleep(2)  #
#     print("这是小明写的功能")
#
# def func():
#     time.sleep(1)  #
#     print("这是小刚写的功能")
#
# def red():
#     time.sleep(2)  #
#     print("这是小红写的功能")

# start_time = time.time()  # 时间戳
# index()
# print(time.time() - start_time)
#
# start_time = time.time()  # 时间戳
# func()
# print(time.time() - start_time)
#
# start_time = time.time()  # 时间戳
# red()
# print(time.time() - start_time)



# import time
#
# def index():
#     time.sleep(2)  #
#     print("这是小明写的功能")
#
# def func():
#     time.sleep(1)  #
#     print("这是小刚写的功能")
#
# def red():
#     time.sleep(2)  #
#     print("这是小红写的功能")
#
# def times(func):
#     start_time = time.time()  # 时间戳
#     func()
#     print(time.time() - start_time)
#
# times(index)
# times(func)


# import time
#
# def index():
#     time.sleep(2)  #
#     print("这是小明写的功能")
#
# def func():
#     time.sleep(1)  #
#     print("这是小刚写的功能")
#
# def red():
#     time.sleep(2)  #
#     print("这是小红写的功能")
#
# def times(func):
#     start_time = time.time()  # 时间戳
#     func()
#     print(time.time() - start_time)
#
# f = index
# index = times
# index(f)
#
# f1 = func
# func = times
# func(f1)

# 第一版装饰器

# import time
# def func():
#     time.sleep(1)  #
#     print("这是小刚写的功能")
#
# def red():
#     time.sleep(2)  #
#     print("这是小红写的功能")
#
#
# def index():
#     time.sleep(2)  #
#     print("这是小明写的功能")
#
# def times(func):
#     def foo():
#         start_time = time.time()  # 时间戳  被装饰函数执行前干的事
#         func()
#         print(time.time() - start_time) #   被装饰函数执行后干的事
#     return foo
#
# index = times(index)
# index()
# func = times(func)
# func()



# def foo():
#     print("被装饰的函数")
#
# def warpper(func):  # func == None
#     def inner():
#         print("日魔最色")
#         func()      # None()
#         print("日魔最骚")
#     return inner()   # 不能加括号
#
# foo = warpper(foo())  # 不能加括号  foo = warpper(None)
# foo()

# def foo():
#     print("被装饰的函数")
#
# def warpper(func):  # func == foo函数的内存地址
#     def inner():
#         print("日魔最色")
#         func()      # foo()
#         print("日魔最骚")
#     return inner()   # 不能加括号   # return None
#
# foo = warpper(foo)  # 不能加括号  foo = warpper(None)
# foo()


# def func():
#     print("被装饰的函数")
#
# def warpper(f):
#     def inner():
#         print("111")
#         f()
#         print("222")
#     return inner
# func = warpper(func)
# func()



# def warpper(f):
#     def inner():
#         print("111")
#         f()
#         print("222")
#     return inner
#
# # python帮咱们做的一个东西,语法糖
# @warpper  # func = warpper(func)
# def func():
#     print("被装饰的函数1")
#
# @warpper  # index = warpper(index)
# def index():
#     print("被装饰的函数2")
#
# func()



# def warpper(f):
#     def inner():
#         print("111")
#         f()
#         print("222")
#     return inner
#
# @warpper  # func = warpper(func)
# def func():
#     print("被装饰的函数1")
#
# @warpper  # index = warpper(index)
# def index():
#     print("被装饰的函数2")
#
# # python帮咱们做的一个东西,语法糖
# func()


# def foo():
#     print("被装饰的函数")
#
# def warpper(func):  # func == foo函数的内存地址
#     def inner():
#         print("日魔最色")
#         func()      # foo()
#         print("日魔最骚")
#     return inner # 不能加括号   # return None
#
# foo = warpper(foo)  # 不能加括号  foo = warpper(None)
# foo()

# def warpper(f):
#     def inner():
#         print("111")
#         f()
#         print("222")
#     return inner
#
# @warpper  # func = warpper(func)
# def func():
#     print("被装饰的函数1")
#
# @warpper  # index = warpper(index)
# def index():
#     print("被装饰的函数2")
#
# func()
# index()

# python帮咱们做的一个东西,语法糖
# 要将语法糖放在被装饰的函数正上方

# def warpper(f):
#     def inner():
#         print("111")
#         f()
#         print("222")
#     return inner
#
# @warpper  # func = warpper(func)
# def func():
#     print("被装饰的函数1")
#
# @warpper  # index = warpper(index)
# def index():
#     print("被装饰的函数2")
#
# func()
# index()

# def warpper(f):
#     def inner(*args,**kwargs):
#         print("被装饰函数执行前")
#         ret = f(*args,**kwargs)
#         print("被装饰函数执行后")
#         return ret
#     return inner
#
# @warpper
# def func(*args,**kwargs):
#     print(f"被装饰的{args,kwargs}")
#     return "我是func函数"
#
# @warpper
# def index(*args,**kwargs):
#     print(11111)
#
# print(func(1,2,3,4,5,6,7,8,a=1))