# l1 = [1, 2, 3, 4, 5, 6]
# l2 = ['oldboy', 'alex', 'wusir', '太白', '日天']
# tu = ('**', '***', '****', '*******')
# print(list(filter(lambda x:x[0]>2 and len(x[-1])>3, zip(l1,l2,tu))))

# 3.有如下数据结构,通过过滤掉年龄大于16岁的字典
# lst = [{'id':1,'name':'alex','age':18},
#         {'id':1,'name':'wusir','age':17},
#         {'id':1,'name':'taibai','age':16},]

# print(list(filter(lambda x:x["age"] < 17,lst)))

# map(str,[1,2,3,4,5,6,7,8,9])输出是什么? (面试题)
# print(map(str,[1,2,3,4,5,6,7,8,9]))


# 10.有一个数组[34,1,2,5,6,6,5,4,3,3]请写一个函数，找出该数组中没有重复的数
# 的总和（上面数据的么有重复的总和为...)(面试题)

# lst = [34,1,2,5,6,6,5,4,3,3]
# print(sum(filter(lambda x:lst.count(x) == 1,lst)))

# def num():
#     return [lambda x:x**i for i in range(4)]
# print([m(2)for m in num()])


# def num():
#     lst = []  #[lambda x:x**i,lambda x:x**i,lambda x:x**i,lambda x:x**i]
#     for i in range(4):
#         lst.append(lambda x:x**i)
#     return lst


# for m in num(): #num() == [lambda x:x**i,lambda x:x**i,lambda x:x**i,lambda x:x**i]
#     print(m(2))