import socket
import time
# 创建一个socket对象（tcp）
sk = socket.socket()
# 绑定IP和端口
sk.bind(('127.0.0.1', 8848))

sk.listen()


# 定义函数
def guochan(url):
    ret = '<h1>yanzhaomen</h1> - {}'.format(url)
    return ret


def rihan(url):
    ret = '<h1>qiaobiluo</h1> - {}'.format(url)
    return ret


def oumei(url):
    ret = '<h1>alex</h1> - {}'.format(url)
    return ret

def zhifu(url):
    ret = '<h1>alexyouhuo</h1> - {}'.format(url)
    return ret


def luoli(url):
    ret = '<h1>luoli</h1> - {}'.format(url)
    return ret

def index(url):
    with open('index.html','r',encoding='utf-8') as f:
        return f.read()

def  timer(url):
    with open('time.html','r',encoding='utf-8') as f:
        now = time.strftime('%Y/%m/%d %H:%M:%S')
        ret = f.read().replace('@@time@@',now)
        return ret




list1 = [
    ('/guochan/',guochan),
    ('/rihan/',rihan),
    ('/oumei/',oumei),
    ('/zhifu/',zhifu),
    ('/luoli/',luoli),
    ('/',index),
    ('/index/',index),
    ('/time/',timer),

]


while True:
    # 等待连接
    conn, addr = sk.accept()
    # 接受数据
    data = conn.recv(1024)
    data = data.decode('utf-8')
    url = data.split()[1]
    print(url)
    # 返回数据
    func = None

    for i in list1:
       if i[0] == url:
            func= i[1]
            break
    if func:
        ret = func(url)
    else:
        ret = '404 not found'

    conn.send(b'HTTP/1.1 200 OK\r\n\r\n')
    conn.send(ret.encode('utf-8'))
    # 关闭连接
    conn.close()
