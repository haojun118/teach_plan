import socket

# 创建一个socket对象（tcp）
sk = socket.socket()
# 绑定IP和端口
sk.bind(('127.0.0.1', 8848))

sk.listen()


# 定义函数
def guochan(url):
    ret = '<h1>yanzhaomen</h1> - {}'.format(url)
    return ret


def rihan(url):
    ret = '<h1>qiaobiluo</h1> - {}'.format(url)
    return ret


def oumei(url):
    ret = '<h1>alex</h1> - {}'.format(url)
    return ret

def zhifu(url):
    ret = '<h1>alexyouhuo</h1> - {}'.format(url)
    return ret



while True:
    # 等待连接
    conn, addr = sk.accept()
    # 接受数据
    data = conn.recv(1024)
    data = data.decode('utf-8')
    url = data.split()[1]
    print(url)
    # 返回数据
    if url == '/guochan/':
        ret = guochan(url)
    elif url == '/rihan/':
        ret = rihan(url)
    elif url == '/oumei/':
        ret = oumei(url)
    elif url == '/zhifu/':
        ret = zhifu(url)
    else:
        ret = '404 not found'

    conn.send(b'HTTP/1.1 200 OK\r\n\r\n')
    conn.send(ret.encode('utf-8'))
    # 关闭连接
    conn.close()
