import socket

# 创建一个socket对象（tcp）
sk = socket.socket()
# 绑定IP和端口
sk.bind(('127.0.0.1', 8848))

sk.listen()

while True:
    # 等待连接
    conn, addr = sk.accept()
    # 接受数据
    data = conn.recv(1024)
    print(data)
    # 返回数据
    conn.send(b'HTTP/1.1 200 OK\r\n\r\n<h1>ok</h1>')
    # 关闭连接
    conn.close()
