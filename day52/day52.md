## 内容规格

Python的基础

面向对象

网络

并发

数据库

前端



## 今日内容

1. web框架  原理
2. Django的下载安装使用



百度 socket服务端

 	1. socket服务端启动
   	2. 绑定ip和端口
   	3. 监听等待连接
 	4. 接受数据
 	5. 返回数据
 	6. 断开连接



浏览器  socket客户端

	4. socket客户端启动
 	5. 连接（ip和端口）
 	6. 发送数据
 	7. 接受数据
 	8. 断开连接

https://www.cnblogs.com/maple-shaw/articles/9060408.html

HTTP协议：

http是请求和应答的标准

请求方式：8种   主要是GET  POST

状态码  1xx   2xx 3xx 4xx 5xx

请求格式：

​	‘请求方式 路径 协议版本

​	请求头： 值

​    请求头： 值

​	

​	请求数据’

响应格式：

​	'协议版本 状态码 状态描述

​    请求头： 值

​    请求头： 值

​	

​	响应数据'



web框架的实现的功能：

1. socket收发消息

2. 根据不同的路径返回不同的内容

3. 返回HTML页面  

4. 返回动态的页面 （模板的渲染   字符串的替换）

web框架

Django   2 3 4    

flask   2 3    

tornado 1 2 3 4 

Django   大而全 

flask  tornado  轻量级的框架 

### Django

#### 下载：

1. ##### 命令行

 pip install -i django==1.11.25 -i  https://pypi.tuna.tsinghua.edu.cn/simple

2. ##### pycharm

   在项目的解释器中找对应的Django版本下载

#### 创建项目

1. ##### 命令行

   django-admin 项目名 

2. ##### pycharm

   file ——》  new project ——》 填写项目名称  ——》 选择解释器 ——》 create

### 启动项目

 1. #### 命令行

    python  manage.py runserver   #  127.0.0.1:8000

    python  manage.py runserver 80   #  127.0.0.1:80

    python  manage.py runserver   0.0.0.0:80  #   0.0.0.0:80  

	2. #### pycharm

    

### 简单使用

1. urls.py中写路径和函数的对应关系

   ```
   urlpatterns = [
       url(r'^index/', index),
   ]
   ```

2. 写函数

   ```
   from django.shortcuts import HttpResponse ,render
   def index(request):
       # 逻辑
       # return HttpResponse('欢迎进入day5项目')   # 返回字符串
       return render(request,'index.html')  # 返回一个HTML页面    页面写在templates文件夹中
   ```

   

作业：

​	用bootstrap写登录页面，使用Django返回给浏览器



