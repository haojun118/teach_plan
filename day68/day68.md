git

版本控制

git init   初始化仓库

git init  文件  创建文件并初始化仓库

git  status 查看状态

git  add  文件名 /.    将变化的内容添加到暂存区

git commit -m '描述信息'   提交到版本库 

git log  查看版本记录

git reflog  查看版本变更记录

git reset --hard  版本号    版本回退



### 分支

默认 master

git branch  查看所有的分支

git branch  分支   新建分支

git checkout 分支 切换分支

git merge new分支    把new分支合并到当前分支

合并中可能出现冲突  需要手动解决冲突



git stash  把当前的操作藏在某个地方了 





### 个人开发 

master  dev   debug

dev 的分支上开发功能

合并到master分支上   (master分支放可以上线的代码)

master分支的代码有bug

1. 新建一个debug分支
2. 切换到debug分支修改bug，提交版本
3. 切换回master分支，合并debug分支

dev开发，开发到一半：

	1. git stash 隐藏当前的操作
 	2. 修复完线上的bug，合并debug分支，删除debug分支
 	3. git stash pop 拿回之前的操作，继续开发





码云  GitHub  gitlab 



### 个人 公司  家

公司下班：

git  add . 

git  commit - m  '未完成' 

git push origin dev 

到家接着开发：

下载安装git  配置

git clone https://gitee.com/maple-shaw/day68.git

git branch dev 

git checkout  dev 

git pull origin dev 

接着开发功能 

开发完成 

git  add . 

git  commit - m  '功能' 

git push origin dev 

第二天上班：

git pull origin dev 

有冲突解决冲突

继续开发



### 协同开发

master  dev 每个人创建自己的分支

每个人在自己的分支开发功能

开发完成后 add  commit   推送到远程仓库自己的分支

提交pull request  合并到dev分支上 





























