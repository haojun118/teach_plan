# 并发编程
# io操作 : input/output
# 阻塞 非阻塞(get_nowait) 同步 异步(start terminate submit map) 并发 并行
# 进程的三状态图 : 阻塞 运行 就绪
# 进程  计算机中最小的资源分配单位
    # 进程之间数据隔离\资源不共享
    # 可以利用多个CPU
    # 开启和销毁的开销大
    # 由操作系统负责调度
    # multiprocessing模块
        # Process类 : 开启进程
            # 各种操作进程的方法
            # 守护进程 : 守护到主进程的代码结束
        # IPC : 进程之间通信
            # 基于文件Queue Pipe\基于网络 socket 第三方工具
        # 进程中的互斥锁 Lock :性能好
        # 进程中的递归锁 RLock : 效率低
            # 递归锁可以锁多次不会发生死锁
# 线程
    # 计算机中能够被操作系统调度的最小单位
    # 线程之间资源共享
    # 可以利用多核
    # 开启和销毁的开销小
    # 由操作系统负责调度
    # GIL锁 : 全局解释器锁 互斥锁
        # 导致了Cpython解释器下  同一个进程下的多个线程 不能利用多核
        # 由于垃圾回收机制gc不能在多线程环境下正常进行引用计数
    # threading模块
        # Thread类
            # 开启线程
            # 守护线程 : 守护整个主线程的
            # 子线程先结束
            # 主线程结束
            # 主进程结束(意味着主进程就结束了,一旦主进程结束,所有的属于这个进程的资源都被回收了)
            # 主进程结束了 如果还存在守护线程,那么守护线程会随着进程的结束被回收
        # 锁:
            # 死锁现象  交叉使用互斥(递归锁)锁
            # 线程互斥锁
            # 线程递归锁
            # 单例模式 要加锁
            # 线程之间数据安全
                # 但凡是 带着判断 判断之后要做xxxx 都需要加锁
                # if while += -= *= /= 都需要加锁
                # n = 0;tmp=n;n = tmp+1
            # append pop extend 队列 logging都是线程安全的
    # 线程队列
        # queue模块
        # Queue 先进先出
            # 生产者消费者模型
        # LifoQueue 后进先出栈
            # from queue import LifoQueue
            # lq = LifoQueue()
            # lq.put(1)
            # lq.put(2)
            # lq.put(3)
            # print(lq.get())
        # PriorityQueue 优先级队列
            # from queue import PriorityQueue
            # pq = PriorityQueue()
            # pq.put((10,'alex'))
            # pq.put((5,'egon'))
            # pq.put((15,'yuan'))
            # print(pq.get())
            # print(pq.get())
            # print(pq.get())
    # 池 concurrent.futrues
        # 进程池 ProcessPoolExecutor
        # 线程池 ThreadPoolExecutor
        # 回调函数
# import requests
# from concurrent.futures import ThreadPoolExecutor
#
# def get_url(name,url):
#     ret = requests.get(url)
#     return name,ret.content
#
# def dump(res):
#     with open(res.result()[0],mode='wb') as f:
#         f.write(res.result()[1])
#
# tp = ThreadPoolExecutor(4)
# dic = {
#     'baidu':'http://www.baidu.com',
#     'taobao':'http://www.taobao.com',
#     'jd':'http://www.jd.com',
#     'sina':'http://www.sina.com',
#     'sogou':'http://www.sogou.com',
#     'cnblog':'https://www.cnblogs.com/Eva-J/articles/9676220.html',
# }
# for k in dic:
#     ret = tp.submit(get_url,k,dic[k])
#     ret.add_done_callback(dump)

# 协程
    # 资源开销小
    # 协程是微线程
    # 本质是一条线程 所以不能利用多核
    # 由程序控制的,不是由操作系统调度的
    # gevent 第三方模块
    # 协程能够识别的IO操作 : 网络操作 sleep
    # 协程和cpython解释器下的线程有啥区别 :资源开销小 能够把单线程的效率提高 协程能够识别的io操作不如线程多

# 学了什么
    # 基础数据类型
    # 流程控制 if while for
    # 文件操作
    # 函数 (定义 调用 参数 返回值 命名空间和作用域 闭包 装饰器 生成器 迭代器 匿名函数 内置函数 递归)
    # 模块 (常用模块 自定义模块 模块的导入 开发规范 包)
    # 面向对象(基础 三大特性 三个装饰器 内置方法 反射)
    # 网络编程(osi协议 socket模块 tcp的粘包 socketserver)
    # 并发编程(操作系统概念 进程 线程 协程)
# 回顾作业
    # 三次登录 购物车 atm 博客园 选课系统 ftp