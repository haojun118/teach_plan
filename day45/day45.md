## 内容回顾

### 选择器

### 基本选择器

标签  div p span 

id     #id

类  class=‘xxx x2’     .xxx 

通用选择器 * {}

### 高级选择器

后代  div p  找div标签下所有的p标签

子代  div>p  找div标签下子代的p标签

毗邻 div+p    找div标签下一个p标签

弟弟 div~p    找div标签后面所有的p标签

属性 [href]   [href='www.baidu.com']  找有属性为href的标签   找有属性href=www.baidu.com的标签   

并集 div,p,span  找逗号左右所有的标签

交集 div.x1  找class=‘x1‘的div的标签

伪类  a:link   没有访问过的样式  a:visited 访问过后的样式 a:active  鼠标按下时的样式

​		div:hover 鼠标悬浮上的样式

伪元素  p:before  p标签内部之前 content 内容

​			p:after  p标签内部之后 content 内容

​			p:fistt-letter  一个元素的样式

### 选择器的优先级

```
行内（1000）  > id(100) > 类（10） > 标签（1） >继承（0）
优先级会累加但是不进位
!important 最高级别
```

### 颜色的表示

```
rgb  光学三原色  红色 绿色 蓝色
rgb(0,0,0)   黑色  rgb(255,255,255)   白色
十六进制  #000000 - #ffffff    #000-#fff
单词  green red  
rgba(0,0,0,0)   0 完全透明 1 完全不透明
```

### 字体 

```
font-family：’宋体‘,'微软雅黑'；
font-size:   字体大小  默认16px;
font-weight:  粗细 sold;  100-900   400
color:  字体颜色
```

## 文本

```
text-aligin:  文字水平排列  left  center right 
line-hight : 行高
text-indent:  缩进  2em
text-decoration: underline  overline line-through none 
```

### 背景图片

```
background-color : 背景颜色
background-imge : 背景图片  URL('')
background-repeat: repeat  repeat-x  repeat-y no-repeat 
background-position : 12px  12px  \   left right center   top center bottom
background-size:  图片大小的设置  px  %  cover 整个覆盖 contain 包含在内
background: 颜色 图片 重复 位置；
```

### 边框

```
border-style: solid 实心  double 双线  dotted 实心圆  dashed虚线
border-color：
border-size：
border-color： red green;   red green gray yellow; 
border-botom-color
border : 1px green solid ;
```

### 行内和块的元素

```
display： inline 基本不用
display：block  块    
display：inline-block  行内块  
display：none   无  不显示 不占位
```

## 盒模型

```
width:   内容的宽
height： 内容的高
border：  边框
padding： 内边距  
margin： 外边距

塌陷现象 ：上下时只取最大的margin
```

# 今日内容



### overflow

```  
overflow: visible;  可见  默认
overflow: hidden;  隐藏
overflow: auto;    超出时出现滚动条
overflow: scroll;  显示滚动条
```

### 浮动

```
float: right; left ： 脱离文档流 先浮动的先占位，后面的紧贴着
清除浮动：
.clear{
            clear: both;
        }
<div id="father" style="">
    <div id="d1" class="box"></div>
    <div class="box"></div>
    <div class="clear"></div>
</div>
伪元素清除法：
.clearfix:after{
      content: '';
      display: block;
      clear: both;
   }
```

### 定位

```
相对定位 relative  相对于原位置进行定位，还占原来的位置
绝对定位 absolute  相对于已经有相对定位的父标签的定位\相对于body的定位 不占原来的位置
固定定位：fixed  相对于窗口的位置  
  top: ;
  left: ;
  right: ;
  bottom: ;
```

### z-index

- z-index 值表示谁压着谁，数值大的压盖住数值小的，
- 只有定位了的元素，才能有z-index,也就是说，不管相对定位，绝对定位，固定定位，都可以使用z-index，而浮动元素不能使用z-index
- z-index值没有单位，就是一个正整数，默认的z-index值为0如果大家都没有z-index值，或者z-index值一样，那么谁写在HTML后面，谁在上面压着别人，定位了元素，永远压住没有定位的元素。
- 从父现象：父亲怂了，儿子再牛逼也没用



### javascript 编程语言

### ECMAScript  标准化的规范

DOM  document object model  文档对象模型 

BOM  bowser object model  浏览器对象模型 

### JS引入

```
写在html script标签内部
<script>
    alert('hello,world!')
</script>
```

### 变量

```
变量名  数字 字母 下划线 $ 
使用var 定义变量  var a =1
var a  ;  表示声明了一个变量  undefined
```

### 注释 

```
当行注释  //
多行注释  /*  
*/
```

### 输出和输入

```js
alert()  弹窗
console.log()  控制台输出

prompt('请输入')
"1111111"
var a = prompt('请输入')

```

### 基本的数据类型

#### number  数字

```
表示整数 浮点数   NaN  not a number

a.toFixed(2)   保留小数位数 四舍五入
```

#### string 字符串

```
var  a  = '单引号'
var  a  = “双引号“
```

#### 布尔值 boolean

```
true
false
```

#### 空元素 null   未定义undefined

```
var a = null;
var a ;  undefined
```

#### 数组 

```
var a = [1,2,3]
var a = new Array()  
```

#### 对象

```
var a = {name:'alex',age:87}
var a = {'name':'alex','age':87}
取值  a['name']
赋值  a['name'] = 'alexdsb'
```



























