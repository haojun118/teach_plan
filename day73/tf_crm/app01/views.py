from django.shortcuts import render, HttpResponse, redirect, reverse
from app01 import models
import hashlib
from app01.forms import RegForm, CustomerForm


# Create your views here.
def login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        md5 = hashlib.md5()
        md5.update(password.encode('utf-8'))
        password = md5.hexdigest()
        user_obj = models.UserProfile.objects.filter(username=username, password=password, is_active=True).first()
        if user_obj:
            request.session['pk'] = user_obj.pk
            request.session['is_login'] = True
            return redirect(reverse('customer'))
        return render(request, 'login.html', {'error': '用户名或密码错误'})

    return render(request, 'login.html')


def register(request):
    form_obj = RegForm()
    if request.method == 'POST':
        form_obj = RegForm(request.POST)
        if form_obj.is_valid():
            # 插入数据库
            # print(form_obj.cleaned_data)
            # models.UserProfile.objects.create(**form_obj.cleaned_data)
            form_obj.save()
            return redirect(reverse('login'))

    return render(request, 'register.html', {'form_obj': form_obj})


def customer(request):
    if request.path_info == reverse('customer'):
        all_customer = models.Customer.objects.filter(consultant__isnull=True)
    else:
        all_customer = models.Customer.objects.filter(consultant=request.user_obj)

    return render(request, 'customer.html', {'all_customer': all_customer})


from django.views import View
from django.db.models import Q


class Customer(View):

    def get(self, request, *args, **kwargs):
        if request.path_info == reverse('customer'):
            all_customer = models.Customer.objects.filter(consultant__isnull=True)
        else:
            all_customer = models.Customer.objects.filter(consultant=request.user_obj)
        q = self.query(['name', 'phone', 'qq'])
        all_customer = all_customer.filter(q)
        page_obj = Pagination(request.GET.get('page', 1), all_customer.count(), request.GET.copy(), 2)

        return render(request, 'customer.html',
                      {
                          'all_customer': all_customer[page_obj.start:page_obj.end],
                          'page_html': page_obj.page_html,
                          'url': reverse('customer')
                      })

    def post(self, request, *args, **kwargs):
        action = request.POST.get('action')
        func = getattr(self, action)
        if not func:
            return HttpResponse('没有的此方法')
        ret = func()  # 可以有返回值 必须是HttpResponse
        if ret:
            return ret
        # return redirect(request.path_info)
        return self.get(request, *args, **kwargs)

    def query(self, field_names):
        query = self.request.GET.get('query', '')
        # query_field = request.GET.get('query_field','')
        # if query_field:
        #     all_customer = all_customer.filter(Q(('{}__contains'.format(query_field),query)))     #   Q(qq__contains=query)  Q(('qq__contains',query))

        # all_customer = all_customer.filter(Q(Q(qq__contains=query) | Q(name__contains=query) | Q(phone__contains=query)))

        q = Q()
        q.connector = 'OR'
        if not query:
            return q

        for field in field_names:
            q.children.append(Q(('{}__contains'.format(field), query)))

        return q

    def multi_apply(self):
        # 公户转私户
        # 获取客户的id
        pk_list = self.request.POST.getlist('pk')
        # orm

        # for pk in pk_list:
        #     obj = models.Customer.objects.filter(pk=pk).first()
        #     # obj.consultant = self.request.user_obj
        #     obj.consultant_id = self.request.user_obj.pk
        #     obj.save()

        # models.Customer.objects.filter(pk__in=pk_list).update(consultant=self.request.user_obj)

        self.request.user_obj.customers.add(*models.Customer.objects.filter(pk__in=pk_list))

    def multi_pub(self):
        # 私户转公户
        # 获取客户的id
        pk_list = self.request.POST.getlist('pk')
        # models.Customer.objects.filter(pk__in=pk_list).update(consultant=None)

        self.request.user_obj.customers.remove(*models.Customer.objects.filter(pk__in=pk_list))


users = [{'name': 'alex-{}'.format(i), 'pwd': 'alexdsb{}'.format(i)} for i in range(1, 355)]

from utils.pagination import Pagination


def customer_add(request):
    form_obj = CustomerForm()
    if request.method == 'POST':
        form_obj = CustomerForm(request.POST)
        if form_obj.is_valid():
            form_obj.save()  # 新增
            return redirect(reverse('customer'))

    return render(request, 'customer_add.html', {'form_obj': form_obj})


def customer_edit(request, pk):
    obj = models.Customer.objects.filter(pk=pk).first()
    form_obj = CustomerForm(instance=obj)  # 包含原始数据
    if request.method == 'POST':
        form_obj = CustomerForm(request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()  # 修改
            return redirect(reverse('customer'))

    return render(request, 'customer_edit.html', {'form_obj': form_obj})


def customer_change(request, pk=None):
    obj = models.Customer.objects.filter(pk=pk).first()
    form_obj = CustomerForm(instance=obj)  # 包含原始数据
    if request.method == 'POST':
        form_obj = CustomerForm(request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            path = request.GET.get('path')
            print(path)

            return redirect(path)
    title = '编辑客户' if pk else '新增客户'
    return render(request, 'customer_change.html', {'form_obj': form_obj, 'title': title})


def user_list(request):
    page_obj = Pagination(request.GET.get('page', 1), len(users))

    return render(request, 'user_list.html',
                  {'users': users[page_obj.start:page_obj.end], 'page_html': page_obj.page_html})
