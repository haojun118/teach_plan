```
Q(name__contains=xxx)
Q(('name__contains',xxx))

q = Q()
q.connector =  'OR'
q.children.append(Q(name__contains=xxx))
q.children.append(Q(('name__contains',xxx))

Q( Q(name__contains=xxx)|Q('name__contains',xxx) )

```



```
from django.http.request import QueryDict
dic = request.GET
dic._mutable = True
dic['page'] = 1    
print(dic)   # {'query': ['111'], 'page': [1]}
dic.urlencode()   # query=111&page=1

QueryDict(mutable=True)
request.GET.copy() # 深拷贝 可编辑
```



