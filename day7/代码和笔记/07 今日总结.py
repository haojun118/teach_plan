# 1.基础数据类型补充
# str:
    # 首字母大写
    # 每个单词首字母大写
    # 大小写转换
    # 居中
    # 查找 find     -1
    # 查找 index    报错
    # 拼接 将列表转换成字符串

# list:
    # 排序 - 默认是升序
    # 反转:
    #     原地转
    #     开辟新的空间进行转换
    #
    # 面试题:
    #     lst = [1,[]]
    #     lst * 5

# tuple:
    # (1,) 元组
    # (1)
    # ()  元组

    # 面试题:
    #     lst = (1,[])
    #     lst * 5

# dict:
    # popitem 随机删除
    # formkeys 值是可变且共用就是坑
    # 面试题


# 2.坑

# list添加:
# list删除:1.从后向前删除  2.创建新的列表,循环新的删除旧的

# dict and set:
    # 遍历时不能改变源数据的大小,可以改变源数据的值


# 3.二次编码
#     python3:unicode
#     python2:ascii
#     encode() 编码
#     decode() 解码
#     用什么编码就要用什么解码

