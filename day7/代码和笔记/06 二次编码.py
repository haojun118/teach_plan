# 1.编码:
# ascii码:
#     支持:英文,数字,符号           1字节
#     不支持:中文

# GBK(国标):
#     支持:英文,数字,符号 -- ascii  1字节
#     支持:中文                    2字节

# Unicode(万国码):
#     支持:英文,数字,符号 -- ascii 4字节
#     支持:欧洲                   4字节
#     支持:亚洲                   4字节

# utf-8:
#     支持:英文,数字,符号 -- ascii  1字节
#     支持:欧洲                    2字节
#     支持:亚洲                    3字节

# 00001101 =  1字节

# 字节:存储和传输
# x\a11\12a\
# 010101010

# 今天是个好日子
# s = "今天"   # b'\xe4\xbb\x8a\xe5\xa4\xa9'
# s1 = s.encode("utf-8") # 编码
# print(s1)


# a = "meet"
# s1 = a.encode("utf-8") # 编码
# print(s1)

# a = "meet"
# a = "今天好"
# s1 = a.encode("gbk") # 编码
# print(s1)

# print(b"meet")  # 只有字母才能这么搞
# s2 = s1.decode("utf-8") # 解码
# print(s2)

# 必会
    # python3: 默认编码unicode
    # pyhton2:默认编码ascii
    # python2不支持中文

# 重要:
    # encode()  # 编码
    # decode()  # 解码
    # 用什么编码就要用什么解码
    # 网络传输一定是字节