# 1.小数据池
#     1.1 is == id
#          is 判断两边的内存地址是否相同
#          == 判断两边的值是否相同
#          id 查看内存地址

#     1.2 小数据池 - int:
#            -5 ~ 256
#        小数据池 - str:
#            字符串乘法的时候总长度不超过20
#            特殊符号是乘 0
#        bool

#     1.3 代码块 - int: py文件,一个函数,一个模块,终端中每一行
#            -5 ~ 正无穷
#         代码块 - str:
#            特殊符号是乘 0 或 1
#         bool

#     优先级:先执行代码块,在执行小数据池
#     作用:节省内存,提高效率

# 2.集合
#     没有值得字典,元素是不可变,唯一(自带去重)
#     无序,可变
#     空集合 == set()
#     增:add(),update()
#     删:remove,pop,clear
#     改:先删后加
#     查:for循环
#     其他操作:
#         - 差集
#         & 交集
#         | 并集
#         ^ 反交集
#         > 子集
#         < 超集
#         冻结集合


# 3.深浅拷贝
#     3.1 赋值
#         将多个变量名指向同一个内存地址
#     3.2 浅拷贝
#           只拷贝第一层,修改拷贝后数据的第一层时不影响源数据
#           (可变数据类型)添加第一层数据时源数据受影响
#     3.3 深拷贝
#         不可变数据类型内存地址共用,可变数据类型重新开辟一个新的空间
