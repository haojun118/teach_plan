# 1.str
    # a = "alex Wusir"
    # print(a.capitalize())   # 首字母大写
    # print(a.title())        # 每个单词首字母大写
    # print(a.swapcase())     # 大小写转换
    # print(a.center(20,"*")) # 居中 - 填充
    # print(a.find("c"))        # 查找 通过元素查找索引,查找不到时返回-1
    # print(a.index("c"))       # 查找 通过元素查找索引,查找不到时就报错
    # print(a.join("_"))
    # print("_".join(["1","2","4"]))  # 拼接,将列表转换成字符串
    # str + str
    # str * 5
    # 字符串进行加操作,乘操作都是开辟新的空间


# 2.list
# 列表的定义
# print(list('123445'))
# 列表的方法:
# lst = [1,23,4,5,7,8,9]
# print(lst.index(4))        # 通过元素查找索引
# lst.sort()                 # 排序 默认是升序
# lst.sort(reverse=True)     # 降序
# print(lst)

# lst = [1,23,4,5,7,8,9]
# lst.sort()
# lst.reverse()             # 人工降序
# print(lst)

# lst = [1,23,4,5,7,8,9]
# lst.reverse()
# print(lst)               # 将源数据进行反转


# lst = [1,23,4,5,7,8,9]
# lst1 = lst[::-1]
# print(lst)
# print(lst1)              # 不修改源数据进行反转


# lst = [1,2,3,4]
# lst = lst + [1,2,3]
# print(lst)  # [1,2,3,4,[1,2,3],]   [1,2,3,4,1,2,3]

# lst = [1,2,3] * 5
# print(lst)
# print(id(lst[0]),id(lst[3]))

# 面试题:
    # lst = [1,[]] * 5
    # print(lst)
    # lst[1].append(6)
    # print(lst)

# 列表在进行乘法的时候,元素是共用的

# 3.tuple
# 面试:
    # tu = (1)
    # tu1 = ("alex")
    # tu2 = (1,)          #元组


    # tu = (12,3,4) + (4,5,3,4)
    # print(tu)

    # tu = (1,[]) * 3
    # print(tu)
    # tu[-1].append(10)
    # print(tu)


# 4.dict

    # dic = {"key":1,"key1":2,"key2":4,"key3":1}
    # print(dic.popitem())   # 随机删除  python3.6版删除最后一个键值对
    # # popitem返回的是被删除的键值对
    # print(dic)

    # 面试题:
    #     dic = {}
    #     dic.fromkeys("abc",[])   # 批量创建键值对 "a":[],"b":[],"c":[]
    #     print(dic)

        # dic = {}
        # dic = dic.fromkeys("abc",[])
        # print(dic)
        # dic["b"] = 11
        # dic["a"].append(10)
        # print(dic)

        # fromkeys 第一个参数必须是可迭代对象,会将可迭代对象进行迭代,成为字典的键.第二个参数是值(这个值是共用的)
        # fromkeys 共用的值是可变数据类型就会有坑,不可变数据类型就没事

# 基础数据类型总结:
# 可变,不可变,有序,无序
    # 1.可变:
    #     list
    #     dict
    #     set
    # 2.不可变:
    #     int
    #     str
    #     bool
    #     tuple
    # 3.有序:
    #     list
    #     tuple
    #     str
    # 4.无序:
    #     dict
    #     set

# 取值方式:

    # 1.索引
    #     list
    #     tuple
    #     str
    #
    # 2.键
    #     dict
    #
    # 3.直接
    #     int
    #     bool
    #     set

# 数据类型转换
# str -- int
# int -- str
# str -- bool
# bool -- str
# int  -- bool
# bool -- int

# list -- tuple
# lst = [1,23,5,4]
# print(tuple(lst))

# tuple -- list
# tu = (1,23,5,4)
# print(list(tu))

# list -- set
# lst = [1,23,12,31,23]
# print(set(lst))

# set -- list

# tuple -- set
# tu = (1,2,3,4,5)
# print(set(tu))

# set -- tuple


# 重要: *****

    # list -- str
    # lst = ["1","2","3"]
    # print("".join(lst))

    # str -- list
    # s = "alex wusir 太白"
    # print(s.split())

#  目前字典转换,自己实现方法

# 重点:

    # find
    # join
    # 列表乘法
    # 元组(1,)
    # 元组乘法
    # list -- str
    # str -- list