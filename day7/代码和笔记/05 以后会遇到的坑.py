# 死循环
# lst = [1,2,3]
# for i in lst:
#     lst.append(4)
# print(lst)

# lst = [11,22,33,44]
# for i in lst:
#     lst.remove(i)
# print(lst)

# for i in lst:
#     del lst[-1]
# print(lst)

# lst = [11,22,33,44]
# for i in lst:

# lst = [11,22,33,44]
# for i in range(len(lst)):
#     del lst[-1]
# print(lst)

# for i in range(len(lst)):
#     lst.pop()
# print(lst)

# lst = [11,22,33,44]
# lst1 = lst[:]
# for i in lst1:
#     lst.remove(i)
# print(lst)

# 使用for循环清空列表元素内容
# 1.从后向前删除, 2.创建一个新的容器,循环新的容器删除旧的容器内容

# 面试题:

    # lst = [1,[2]]
    # lst[1] = lst
    # print(lst)
    # 答案: [1, [...]]

# 字典和集合:
# dic = {"key":1,"key1":2}
#
# for i in dic:
#     if i == "key1":
#         dic[i] = dic[i] + 8  # dic[i] = 10
#     else:
#         print("没有这个键!")
# print(dic)

# 字典和集合在遍历(循环)时不能修改原来的大小(字典的长度),可以进行修改值
# s = {1,2,3,4,5,6}
# for i in s:
#     s.pop()
# print(s)

