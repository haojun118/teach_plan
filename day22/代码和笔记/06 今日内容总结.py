# 1.继承

# 单继承,多继承
# 经典类,新式类

# 1.子类的类名使用父类的属性和方法
# 2.子类的对象使用父类的属性和方法
# 3.既要使用子类的属性又使用父类的属性

# 方法一:不依赖(需要)继承
    # 在本类的__init__方法中,使用另一个类名的__init__方法进行初始化
    # def __init__(self,name,age,sex):
    #     A.__init__(self,name,age)
    #     self.sex = sex

# 方法二:(依赖继承) super只能继承之后使用
#     def __init__(self,name,age,sex): # 初始化方法
#         super(本类,self).__init__(name,age)
#         super().__init__(name,age)   # 重构方法
#         self.sex = sex