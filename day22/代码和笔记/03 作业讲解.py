# 暴力摩托程序（完成下列需求）：

# 创建三个游戏人物，分别是：

# 苍井井，女，18，攻击力ad为20，血量200
# 东尼木木，男，20，攻击力ad为30，血量150
# 波多多，女，19，攻击力ad为50，血量80


# class Role:
#
#     def __init__(self, name, sex, age, ad, hp):
#         self.name = name
#         self.sex = sex
#         self.age = age
#         self.ad = ad
#         self.hp = hp
#
#     def attack(self, obj, wea=None):
#         """
#         攻击
#         :return:
#         """
#         if wea:
#             obj.ph = obj.hp - self.ad - wea.ad
#             print(f"{self.name}利用{wea.name}打了{obj.name}一{wea.name},{obj.name}还剩{obj.ph}")
#         else:
#             obj.hp -= self.ad
#             print(f"{self.name}赤手空拳打了{obj.name}{self.ad}滴血,{obj.name}还剩{obj.hp}")
#
#
#     def run_moto(self, moto):
#         """
#         骑摩托
#         :return:
#         """
#         moto.run(self)
#
#     def run_moto_wea(self, obj, moto1, moto2, wea):  # 依赖关系
#
#         self.obj = obj   # 组合关系
#         self.wea = wea
#
#         self.obj.hp = self.obj.hp - self.ad - self.wea.ad
#         print(f"{self.name}骑着{moto1.name}打了骑着{moto2.name}的{obj.name}一{wea.name},{self.obj.name}哭了,还剩{self.obj.hp}血")

# 创建三个游戏武器，分别是：

# 平底锅，ad为20
# 斧子，  ad为50
# 双节棍，ad为65

# class Weapon:
#
#     def __init__(self, name, ad):
#         self.name = name
#         self.ad = ad

# 创建三个游戏摩托车，分别是：

# 小踏板，速度60迈
# 雅马哈，速度80迈
# 宝马，  速度120迈。

# class Motorcycle:
#
#     def __init__(self, name, speed):
#         self.name = name
#         self.speed = speed
#
#     def run(self,obj):
#         print(f"{obj.name}骑着{self.name}开着{self.speed}迈的车行驶在赛道上")



# 苍井井，女，18，攻击力ad为20，血量200
# 东尼木木，男，20，攻击力ad为30，血量150
# 波多多，女，19，攻击力ad为50，血量80

# 完成下列需求（利用武器打人掉的血量为武器的ad + 人的ad）：
# （1）苍井井骑着小踏板开着60迈的车行驶在赛道上。

# cang = Role("苍井井", '女', 18, 20 ,200)
# dong = Role("东尼木木", '男', 20, 30 ,150)
# bo = Role("波多多", '女', 19, 50 ,80)
#
# wea_guo = Weapon("平底锅",20)
# wea_fuzi = Weapon("斧子",50)
# wea_gun = Weapon("双节棍",65)
#
# moto_tb = Motorcycle("小踏板",60)
# moto_ymh = Motorcycle("雅马哈",80)
# moto_bmw = Motorcycle("宝马",120)

# moto_tb = Motorcycle("小踏板",60)
# cang.run_moto(moto_tb)
# （2）东尼木木骑着宝马开着120迈的车行驶在赛道上。
# （3）波多多骑着雅马哈开着80迈的车行驶在赛道上。

# （4）苍井井赤手空拳打了波多多20滴血，波多多还剩xx血。
# cang.attack(bo)
# （5）东尼木木赤手空拳打了波多多30滴血，波多多还剩xx血。

# （6）波多多利用平底锅打了苍井井一平底锅，苍井井还剩xx血。
# bo.attack(cang,wea_guo)
# （7）波多多利用斧子打了东尼木木一斧子，东尼木木还剩xx血。

# （8）苍井井骑着宝马打了骑着小踏板的东尼木木一双节棍，东尼木木哭了，还剩xx血。（选做）
# cang.run_moto_wea(dong,moto_bmw,moto_tb,wea_gun)
# （9）波多多骑着小踏板打了骑着雅马哈的东尼木木一斧子，东尼木木哭了，还剩xx血。（选做）


"""
定义一个学校类，一个老师类。

学校类要求：
学校类封装学校名称，学校地址，以及相关老师（以列表形式存放老师对象）的属性。
name: 学校名称。
address: 具体地址。
teacher_list: []。
学校类设置添加老师对象的方法。
"""

# class School:
#
#     def __init__(self, name, address):
#         self.name = name
#         self.address = address
#         self.teacher_list = []
#
#     def add_teacher(self,teacher_obj):
#         self.teacher_list.append(teacher_obj)

"""
老师类封装姓名，教授学科，以及所属学校的具体对象。
name: 老师名。
course: 学科。
school: 具体学校对象。
"""
# class Teacher:
#
#     def __init__(self, name, course, school_obj):
#
#         self.name = name
#         self.course = course
#         self.school_obj = school_obj

# school1 = School("北京校区","美丽的沙河")
# school2 = School("深圳校区","南山区")
#
# taibai = Teacher("太白","Python",school1)
# wuchao = Teacher("吴超","Linux",school1)
# baoyuan = Teacher("宝元","Python",school1)
# yuanhao = Teacher("苑昊","Python",school2)
# xiaohu = Teacher("小虎","Linux",school2)
# xiaowang = Teacher("小王","Python",school2)
"""
完成以下具体需求：
获取宝元所属学校名称。
获取宝元所属学校的学校地址。
将所有属于北京校区的所有老师名展示出来，并添加到一个列表中。
将所有属于深圳校区的所有老师以及所负责的学科展示出来。
将两个校区的负责Python学科的所有老师对象添加到一个列表中。
将两个校区的负责linux学科的所有老师对象添加到一个列表中并循环展示出来。
将北京校区这个对象利用pickle写入文件中，然后读取出来，并展示出所属于北京校区的老师姓名
"""
# print(baoyuan.school_obj.name)
# print(baoyuan.school_obj.address)

# school1.add_teacher(taibai)
# school1.add_teacher(baoyuan)
# school1.add_teacher(wuchao)

# school1_list = []
# for i in school1.teacher_list:
#     school1_list.append(i.name)
# print(school1_list)

# school2.add_teacher(yuanhao)
# school2.add_teacher(xiaohu)
# school2.add_teacher(xiaowang)

# for i in school2.teacher_list:
#     print(f"老师姓名：{i.name} 学科:{i.course}")

# python_list = []
# for i in school1.teacher_list + school2.teacher_list:
#     if i.course == "Python":
#         print(i.name)
#         python_list.append(i)
# print(python_list)


# import pickle
# with open("a","wb")as f:
#     pickle.dump(school1,f)
#
# with open("a","rb")as f1:
#     obj = pickle.load(f1)
#     print(obj)
#
# for i in obj.teacher_list:
#     print(i.name)

