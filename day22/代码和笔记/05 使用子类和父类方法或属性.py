class Animal: # 父类
    """
    动物类
    """
    live = "活的"

    def __init__(self, name, age, sex):
        # self = p的内存地址
        self.name = name
        self.sex = sex
        self.age = age

    def eat(self):  # self 是函数的位置参数
        print("吃")

# 方法一: 不依赖(不需要)继承
# class Human: # 子类
#
#     def __init__(self, name, age, sex, hobby):
#         # print(Animal.live)
#         # self = p的内存地址
#         Animal.__init__(self,name,age,sex)
#         self.hobby = hobby
#
# class Dog:
#
#     def __init__(self, name, age, sex, attitude):
#         # self = p的内存地址
#         self.name = name
#         self.sex = sex
#         self.age = age
#
#
# p = Human("日魔",18,"男","健身")
# print(p.__dict__)


# 方法二: 依赖(需要)继承

# class Dog(Animal):
#
#     def __init__(self, name, age, sex, attitude):
#         # self = p的内存地址
#         # super(Dog,self).__init__(name,age,sex)   # 完整写法
#         super().__init__(name,age,sex)   # 正常写法
#         self.attitude = attitude
#
# d = Dog("日魔",18,"男","忠诚")
# print(d.__dict__)


# def func(self):
#     self = 3
#     print(self)
#
# self = 3
# func(self)

# 1

# class Base:
#     def __init__(self, num):
#         self.num = num
#
#     def func1(self):
#         print(self.num)
#
# class Foo(Base):
#     pass
#
# obj = Foo(123)
# obj.func1()

# class Base:
#     def __init__(self, num):
#         self.num = num
#
#     def func1(self):
#         print(self.num)
#
#
# class Foo(Base):
#     def func1(self):
#         print("Foo. func1", self.num)
#
#
# obj = Foo(123)
# obj.func1()


# class Base:
#     def __init__(self, num):
#         self.num = num
#
#     def func1(self):
#         print(self.num)
#         self.func2()
#
#     def func2(self):
#         print("Base.func2")
#
#
# class Foo(Base):
#     def func2(self):
#         print("Foo.func2")
#
#
# obj = Foo(123)
# obj.func1()


# class Base:
#     def __init__(self, num):
#         self.num = num
#
#     def func1(self):
#         print(self.num)
#         self.func2()
#
#     def func2(self):
#         print(111, self.num)
#
#
# class Foo(Base):
#     def func2(self):
#         print(222, self.num)
# a = Base(1)
# b = Base(2)
# c = Foo(3)
# lst = [a, b, c]
# print(lst)
# for obj in lst:
#     obj.func2()


# class Base:
#     def __init__(self, num):
#         self.num = num
#
#     def func1(self):
#         print(self.num)
#         self.func2()
#
#     def func2(self):
#         print(111, self.num)
#
#
# class Foo(Base):
#     def func2(self):
#         print(222, self.num)
#
# 
# lst = [Base(1), Base(2), Foo(3)]
# for obj in lst:
#     obj.func1()

# 1
# 111 1
# 2
# 111 2
# 3
# 222 3