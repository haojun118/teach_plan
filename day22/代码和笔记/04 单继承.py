# 继承  - 子承父业
# 程序中 A(B)

# A -- 子类,派生类
# B -- 父类,基类,超类

# class Human:
#
#     def __init__(self,name,age,sex):
#
#         self.name = name
#         self.sex = sex
#         self.age = age
#
#     def eat(self):
#         print("吃")
#
# class Dog:
#
#     def __init__(self, name, age, sex):
#         self.name = name
#         self.sex = sex
#         self.age = age
#
#     def eat(self):
#         print("吃")
#
# class Cat:
#
#     def __init__(self, name, age, sex):
#         self.name = name
#         self.sex = sex
#         self.age = age
#
#     def eat(self):
#         print("吃")
#
# class Pig:
#
#     def __init__(self, name, age, sex):
#         self.name = name
#         self.sex = sex
#         self.age = age
#
#     def eat(self):
#         print("吃")



class Animal: # 父类
    """
    动物类
    """
    live = "活的"

    def __init__(self, name, age, sex):
        print("is __init__")
        self.name = name
        self.sex = sex
        self.age = age

    def eat(self):  # self 是函数的位置参数
        print("吃")

class Human(Animal): # 子类
    pass

class Dog(Animal):  # 子类
    pass

# 通过子类的类名使用父类的属性和方法
# Human.eat(12)
# Human.__init__(Human,"日魔",18,"男")

# print(Human.live)
# print(Human.__dict__)

# 通过子类的对象使用父类的属性和方法
# p = Human("日魔",18,"男")
# d = Dog("rimo",1,'母')
# print(d.__dict__)
# print(p.__dict__)

# p = Human("日魔",18,"男")
# print(p.live)

# 查找顺序:
# 不可逆(就近原则)
# 通过子类,类名使用父类的属性或方法(查找顺序):当前类, 当前类的父类,当前类的父类的父类 --->
# 通过子类对象使用父类的属性或方法(查找顺序):先找对象,实例化这个对象的类,当前类的父类, --->


### 重要
# 继承: 单继承,多继承
# Python2: python2.2 之前都是经典类,python2.2之后出现了新式类,继承object就是新式类
# Python3: 只有新式类,不管你继不继承object都是新式类


# 继承的优点:
# 1.减少重复代码
# 2.结构清晰,规范
# 3.增加耦合性(不在多,在精)