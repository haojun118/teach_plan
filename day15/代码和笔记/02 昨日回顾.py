# 1.开放封闭原则:
# 对扩展开放
# 对修改源代码和调用方式是封闭

# 装饰器:

# def func(f):
#     def foo():
#         print("被装饰之前")
#         f()
#         print("别装饰之后")
#     return foo
#
# def func1():
#     print("被装饰函数")
#
# func1 = func(func1)
# func1()


# def f1():
#     print(11)
#
# def func(f):
#     def foo():
#         f1()
#         f()
#         print("被装饰之后")
#     return foo
#
# @func
# def func1():
#     print("被装饰函数")
#
# func1()


# def func(f):
#     def foo(*args,**kwargs):
#         ret = f(*args,**kwargs)
#         print("被装饰之后")
#         return ret
#     return foo
#
# @func
# def func1(*args,**kwargs):
#     print("被装饰函数")
#     return args,kwargs
# print(func1(1,2,3))


# 语法糖:写在被装饰函数的正上方
# 语法糖和被装饰的函数之间可以有空行但是不建议这样写
# 装饰器一定要写在被装饰函数的前面

# 装饰器可以装饰多个函数
# def func(f):
#     def foo(*args,**kwargs):
#         ret = f(*args,**kwargs)
#         print("被装饰之后")
#         return ret
#     return foo

# @func
# def func1(*args,**kwargs):
#     print("被装饰函数1")


# @func
# def func2(*args,**kwargs):
#     print("被装饰函数2")


# @func
# def func3(*args,**kwargs):
#     print("被装饰函数3")

# func3()
# func2()
# func1()


# import time
# time.time()  #时间戳 -- 浮点数
# time.sleep(1)  # 秒  -- 睡眠 -- 阻塞

# 7  --  1
# 3  -- 0.5
# 4  -- 0.5
# 6  -- 0.3