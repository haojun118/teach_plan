# 1.有参装饰器

# def warpper(func):
#     def inner(*args,**kwargs):
#         user = input("user:")
#         pwd = input("pwd:")
#         if user == 'alex' and pwd == "dsb":
#             func(*args,**kwargs)
#     return inner
#
# @warpper
# def foo():
#     print("被装饰的函数")
#
# foo()

# def auth(argv):
#     def warpper(func):
#         def inner(*args,**kwargs):
#             if argv == "博客园":
#                 print("欢迎登录博客园")
#                 user = input("user:")
#                 pwd = input("pwd:")
#                 if user == 'alex' and pwd == "dsb":
#                     func(*args,**kwargs)
#             elif argv == "码云":
#                 print("欢迎登录码云")
#                 user = input("user:")
#                 pwd = input("pwd:")
#                 if user == 'alex' and pwd == "jsdsb":
#                     func(*args, **kwargs)
#
#         return inner
#     return warpper
#
# def foo():
#     print("被装饰的函数")
#
# msg = input("请输入您要登录的名字:")
# a = auth(msg)
# foo = a(foo)
# foo()



# def wrapper(func):
#     def inner(*args,**kwargs):
#         print("走了,走了,走了")
#         func()
#     return inner
#
#
#
# @wrapper
# def foo():
#     print("这是一个点燃")
#
# foo()
# foo()

# def auth(argv):
#     def wrapper(func):
#         def inner(*args,**kwargs):
#             if argv:
#                 print("我加上功能了!")
#                 func(*args,**kwargs)
#             else:
#                 func(*args,**kwargs)
#         return inner
#     return wrapper
#
# def foo():
#     print("这是一个点燃")
#
# wrapper = auth(False)
# foo = wrapper(foo)
# foo()


# def auth(argv):
#     def wrapper(func):
#         def inner(*args,**kwargs):
#             if argv:
#                 print("我加上功能了!")
#                 func(*args,**kwargs)
#             else:
#                 func(*args,**kwargs)
#         return inner
#     return wrapper

# https://www.cnblogs.com/

# @auth("guobaoyuan")   # @auth == foo = wrapper(foo) = auth(True)   flask框架
# def foo():
#     print("这是一个点燃")
# foo()


# 多个装饰器装饰一个函数
# 多个装饰器装饰一个函数时,先执行离被装饰函数最近的装饰器

# def auth(func): # wrapper1装饰器里的 inner
#     def inner(*args,**kwargs):
#         print("额外增加了一道 锅包肉")
#         func(*args,**kwargs)
#         print("锅包肉 38元")
#     return inner
#
# def wrapper1(func): # warpper2装饰器里的 inner
#     def inner(*args,**kwargs):
#         print("额外增加了一道 日魔刺生")
#         func(*args,**kwargs)
#         print("日魔刺生 白吃")
#     return inner
#
# def wrapper2(func):  # 被装饰的函数foo
#     def inner(*args,**kwargs):
#         print("额外增加了一道 麻辣三哥")
#         func(*args,**kwargs)
#         print("难以下嘴")
#     return inner
#
# @auth        # 1           7
# @wrapper1    #   2       6
# @wrapper2    #    3    5
# def foo():   #      4
#     print("这是一个元宝虾饭店")


# foo = wrapper2(foo) # inner = wrapper2(foo)
# foo = wrapper1(foo) # inner = wrapper1(inner)
# foo = auth(foo)     # inner = auth(inner)
# foo()               # auth里边的inner()

# def auth(func): # wrapper1装饰器里的 inner
#     def inner(*args,**kwargs):
#         print(123)
#         func(*args,**kwargs)
#         print(321)
#     return inner
#
#
# def wrapper1(func): # warpper2装饰器里的 inner
#     def inner(*args,**kwargs):
#         print(111)
#         func(*args,**kwargs)
#     return inner
#
#
# def wrapper2(func):  # 被装饰的函数foo
#     def inner(*args,**kwargs):
#         print(222)
#         func(*args,**kwargs)
#         print(567)
#     return inner
#
#
# @auth
# @wrapper1
# @wrapper2
# def foo():
#     print("www.baidu.com")
# foo()

