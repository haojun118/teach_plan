# msg = """
# 1.登录
# 2.注册
# 3.主页
# 4.评论
# 5.注销
# 6.退出
# >>>
# """
# login_dic = {
#     "username":None,
#     "flag":False
# }
#
# def auth(func):
#     def inner(*args,**kwargs):
#         if login_dic['flag'] == False:  # 没有登录过
#             login(func)
#         else:
#             func()
#     return inner
#
# def login(func):
#     """
#     登录
#     :return:
#     """
#     user = input("username:")
#     pwd = input("password:")
#     if user == "meet" and pwd == "棒棒的":
#         login_dic["flag"] = True
#         login_dic["username"] = user
#         print("登录成功!")
#         func()  # index()
#     else:
#         print("请重新选择功能!")
#
# def register():
#     """
#     注册
#     :return:
#     """
#     print("这是一个注册")
#
# @auth
# def index():
#     """
#     主页
#     :return:
#     """
#     print("这是一个主页")
#
# @auth
# def comment():
#     """
#     评论
#     :return:
#     """
#     print("这是一个评论")
#
# @auth
# def login_out():
#     """
#     注销
#     :return:
#     """
#     print("这是一个注销")
#
#
# func_dic = {
#     "1":login,
#     "2":register,
#     "3":index,
#     "4":comment,
#     "5":login_out,
#     "6":exit
# }
#
#
# while True:
#     choose = input(msg)  # 3
#     if choose in func_dic:
#         func_dic[choose]()
#     else:
#         print("输入有误,请重新输入!")


# 请实现一个装饰器，每次调用函数时，将被装饰的函数名以及调用被装饰函数的时间节点写入文件中。
# 可用代码：
# import time
#
# struct_time = time.localtime()
# print(time.strftime("%Y-%m-%d %H:%M:%S", struct_time))  # 获取当前时间节点
#
#
# def func():
#     print(func.__name__)


# import time
# struct_time = time.localtime()
# time_str = time.strftime("%Y-%m-%d %H:%M:%S", struct_time)
# print(time_str)

# def warpper(func):
#     def inner(*args,**kwargs):
#         struct_time = time.localtime()
#         time_str = time.strftime("%Y-%m-%d %H:%M:%S", struct_time)
#         print(time_str)
#         with open("a.txt","a",encoding="utf-8")as f:
#             f.write(f"函数名:{func.__name__}  执行时间:{time_str}\n")
#         func(*args,**kwargs)
#     return inner
#
# @warpper
# def foo():
#     print("这是一个被装饰的函数!")
#
# time.sleep(5)
# foo()
# 函数名通过： 函数名.__name__获取