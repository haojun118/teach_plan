# 递归
# 递: 一直传参
# 归: 返回

# 1.不断调用自己本身   (无效递归 -- 死递归)

    # def func():
    #     print(1)
    #     func()
    # func()

# 2.有明确的终止条件
# 递归的最大深度(层次) 官方说明1000  实际测试 998/997


# def func(n): # n = 3
#     if n == 3:
#         return "alexdsb"
#     return func(n+1)
#
# def func1(n): # n = 2
#     if n == 3:
#         return
#     return func(n+1)
#
# def func2(n): # n = 1
#     if n == 3:
#         return
#     return func1(n+1)
#
# print(func2(1))


#1 宝元 wusir -2
#2 wusir alex -2
#3 alex 38

# def age(n):
#     if n == 3:
#         return 38
#     return age(n+1)-2
#
# def age1(n):
#     if n == 3:
#         return 38
#     return age(n+1)-2
#
# def age2(n):
#     if n == 3:
#         return 38
#     return age1(n+1)-2
#
# age2(1)


# 递归的应用场景:
# meet = [["北京",["alex","wusir","太白金星","闫龙","景女神",["邢姨",["肖帮主","吴老板",["张晓波","王胜辉"]]]]],
#         ["上海",["林海峰"],],
#         ["深圳",["日天"]]]

# for i in meet:
#     if type(i) == list:
#         for em in i:
#             if type(em) == list:
#                 for j in em:


# def func(m):
#     for i in m:
#         if type(i) == list:
#             func(i)
#         else:
#             print(i)
# func(meet)

