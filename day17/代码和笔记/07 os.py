# 工作路径:
import os   # os是和操作系统做交互,给操作发指令
# print(os.getcwd())  # 获取当前文件工作的路径     ***
# os.chdir("D:\Python_s25\day16")  # 路径切换     **
# print(os.getcwd())
# print(os.curdir)
# print(os.pardir)


# 文件夹   ***
# os.mkdir("a2")  # 创建文件夹
# os.rmdir("a2")  # 删除文件夹
# os.makedirs('a1/a2/a3')  # 递归创建文件夹
# os.removedirs("a1/a2/a3") # 递归删除文件夹
# print(os.listdir(r"D:\Python_s25\day17"))  # 查看当前文件下所有的内容


# 文件 ***
# os.remove(r"D:\Python_s25\day17\a")      #删除文件,彻底删除 找不回来
# os.rename()                              # 重命名


# 路径
# print(os.path.abspath("test"))   # 返回的是绝对路径     ***
# print(os.path.split(r"D:\Python_s25\day17\test"))  #将路径分割成一个路径和一个文件名 **
# print(os.path.dirname(r"D:\Python_s25\day17\test"))  #获取到父目录                   ***
# print(os.path.basename(r"D:\Python_s25\day17\test")) #获取文件名  **
# print(os.path.join("D:\Python","day17","test"))        # 路径拼接   ***(非常重要)

# 判断
# print(os.path.exists(r"D:\Python_s25\day17\blog"))  # 判断当前路劲是否存在  **
# print(os.path.isabs(r"D:\Python_s26\day17\blog"))     # 判断是不是绝对路径  **
# print(os.path.isdir(r"D:\Python_s25\day17\blog"))        # 判断是不是文件夹 **
# print(os.path.isfile(r"D:\Python_s25\day17\blog"))           # 判断是不是文件  **

# print(os.path.getsize(r"D:\Python_s25\day17\01 今日内容.py"))  # 获取文件大小
# print(os.path.getsize(r"D:\Python_s25"))  # 获取文件大小                        ***