# sys python解释器做交互

import sys
# print(sys.path)   #模块查找的顺序  ***
# print(sys.argv)     # 只能在终端执行   **
# print(sys.modules)    # 查看加载到内存的模块
# print(sys.platform)      # 查看当前操作系统平台 mac - darwin  win - win32  **
# mac -- linux  查看 ls
# win               dir
# print(sys.version)              # 查看当前解释器的版本