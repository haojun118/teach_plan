# 1.自定义模块
# 作用:
# 1.文件化管理,代码重复,节省时间
# 2.拿来就用

# 导入时做的事情:
# 1.将模块中的代码全部读取到当前文件
# 2.开辟独立空间
# 3.等待被调用

# import 导入同一个模块名是只会执行一次
# import 模块名 将模块整个调用使用
# as 起别名
# as 支持 import和from
# import 后边不能有点操作
# 导入模块时,模块名不能加后缀

# import 只能导入当前文件夹下的模块

# import sys
# sys.path.append("被导入的模块路劲")
# sys.path.insert(0,"被导入的模块路劲")

import os,sys   # 不推荐
import os
import sys      # 推荐

from os import path,getcwd,makedirs  # 推荐

# from 模块 import * 导入所有的功能
# __all__ = ["a","func"]  控制 import*

# .py文件被当做模块导入时会保留.pyc文件(字节码)

# pip install 模块名 安装第三方模块

# 查找顺序:
# 内存 > 内置 > [第三方 > 自定义 ] # sys.path
# 内存 > 自定义 > 内置 > 第三方   # sys.path

# 模块的两种用法:
# 1.当做模块导入  __name__ 返回的是当前模块名
# 2.当做脚本被执行 __name__ 返回 '__main__'

# if __name__ == '__main__':
#     pass

# import 和 from 的区别
# 1.import 是将模块中所有的功能导入
# 2.from 是导入指定的工能
# 3.import 和 from 都是相对路径
# 4.from 比 import 灵活 (可以从别的文件中导入模块,也可以从模块中指定导入某个功能)
# 5.from 容易覆盖已经定义好的内容


# 两个注意:
# 1.自定义的模块命名不要和内置的冲突
# 2.循环导入,交叉导入(不能互相查找内容)

# 2.time模块
import time
# time.time() 时间戳
# time.sleep() 睡眠
# time.localtime()
# time.strftime()
# time.strptime()
# time.mktime()
# %Y %m %d %H %M %S
# 结构化时间 -- 命名元组 支持索引和.操作

# 3.datetime

# from datetime import datetime,timedelta
# datetime.now() # 获取当前时间
# datetime(2018,10,11,1,1,1)
# datetime(2018,10,11,1,1,1) - datetime.now()
# datetime.timestamp()
# datetime.fromtimestamp()
# datetime.strftime()
# datetime.strptime()
# datetime.now() - timedelta(days=1)

# 4.random
import random
# random.random() 获取到是0-1之间随机小数
# random.randint()  获取到是随机整数  可以取到右侧
# random.uniform()  获取指定范围小数
# random.randrange()  获取随机的范围数字 起始值 终止值 步长
# random.choice()       从容器中随机选择一个
# random.choices([1111],k=2)    从容器中指定随机选择几个,会有重复
# random.sample([111,222],k=3)  从容器中指定随机选择几个,不会有重复 能够支持集合  返回的是列表结构
# random.shuffle()       # 洗牌 在原地打乱顺序