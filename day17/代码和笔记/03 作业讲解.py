# 3.用户输入一个"2019-7-26 20:30:30"和当前时间相比,一共过去了多少年多少月多少天到少小时多少分钟
# import time
# s = "2019-5-23 9:30:30"
# time_j = time.strptime(s,"%Y-%m-%d %H:%M:%S")
# time_f = time.time() - time.mktime(time_j)
# t = time.localtime(time_f)
# print(f"过去了{t[0]-1970}年,{t[1]-1}月,{t[2]-1}日,{t[3]-8}时,{t[4]}分,{t[5]}秒")

# 4.写函数，生成一个4位随机验证码（包含数字大小写字母)

# def func():
#     lst = []
#     import random
#     lst.append(chr(random.randrange(65, 91)))
#     lst.append(chr(random.randrange(97, 123)))
#     lst.append(str(random.randrange(0, 10)))
#     lst.append(str(random.randrange(0, 10)))
#     random.shuffle(lst)
#     l = random.choices(lst,k=4)
#     return ''.join(l)
#
# print(func())