# a = 1
# b = 2
# def func():
#    return a + b
# print(func())

# def yue(app1,app2,app3,app4):  # 形参
#
#     # 函数帮咱们做了一个赋值 app = "陌陌"
#     print("打开手机")
#     print(f"打开{app1} {app2}")
#     print("聊一聊")
#     print("见一见")
#     print("......")
#
# yue("微信","探探","陌陌","soul")        # 实参


# def yue(a,b,c,app1="微信"):  # 形参
#
#     # 函数帮咱们做了一个赋值 app = "陌陌"
#     print("打开手机")
#     print(f"打开{a} {app1}")
#     print("聊一聊")
#     print("见一见")
#     print("......")
#
# yue("探探","陌陌","微信") # 实参


# def yue(a,b,c,a1="微信"):  # 形参
#
#     # 函数帮咱们做了一个赋值 app = "陌陌"
#     print("打开手机")
#     print(f"打开{a} {b} {c} {a1}")
#     print("聊一聊")
#     print("见一见")
#     print("......")
#
# yue(11,c=5,b=1) # 实参

# def func(a,b):
#     return a + b
#
# print(func(1,3))

# def func(a,b):
#     if a > b:
#         return a
#     else:
#         return b
# print(func(6,9))

# 三元运算 (三目运算)
# a = 6
# b = 9
# c = a if a > b else b
# print(c)
# 条件成立的结果(a) 条件(if a > b else) 条件不成立的结果(b)

# def func(a,b):
#     return a if a > b else b
# print(func(6,9))

# def func(n=5):
#     num = input("请输入数字:")
#     if num.isdecimal():
#        return int(num) + n
# print(func())


"""
姓名
年龄
性别
岗位
薪资
"""

# def info(name,age,job,moeny,sex="男"):
#     print(f"姓名:{name} 年龄:{age} 性别:{sex} 岗位:{job} 薪资:{moeny}")
#
# while True:
#     name = input("name:")     # rimo
#     age = input("age:")       # 89
#     sex = input("sex(男性回车):") # 女
#     job = input("job:")          #  wc
#     money = input("moeny:")      # 10
#     if sex == "女":
#         info(name,age,job,money,sex)
#     else:
#         info(name, age, job, money)


# 函数的参数:

    # 形参:
    #     位置参数:
    #         一一对应
    #     默认参数: 函数定义的时括号中写好的就是默认参数
    #         不进行传参使用默认参数,进行传参时使用传递的参数
    # 实参:
    #     位置参数:
    #         一一对应
    #     关键字参数:
    #         按照名字进行传参
    #      混合参数:
    #          位置参数和关键字参数一起使用

# 位置参数 > 默认参数(关键字参数)

# 形参: 函数定义阶段括号中的参数叫做形参
# 实参: 函数调用阶段括号中的参数叫做实参
# 传参: 将实参传递给形参的过程叫传参
