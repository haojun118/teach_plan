# 1.什么函数?
# 1,将某个功能封装到一个空间中就是一个函数
# 2,减少重复代码

# s = "alexdsb"
# n = 0
# for i in s:
#     n += 1
# print(n)
#
# lst = [1,2,3,4,5]
# n = 0
# for i in lst:
#     n += 1
# print(n)
#
# tu = (1,2,3,4,5,6,7,8)
# n = 0
# for i in tu:
#     n += 1
# print(n)
#
# dic = {1:12,4:115,6:7}
# n = 0
# for i in dic:
#     n += 1
# print(n)

# 定义函数:
# def python中关键字
# len 函数名 -- 变量名一模一样
# ()  必须要写的 格式规定
# :   语句结束

# def len():
#     函数体

# dic = {1:12,4:115,6:7}
# dic = "alexdsb"

# def my_len():
#     n = 0
#     for i in dic:
#         n += 1
#     print(n)

# 函数的调用
# 函数名 + () 就是在调用函数
# my_len()


# def yue():
#     print("掏出手机")
#     print("打开微信")
#     print("摇一摇")
#     print("聊一聊")
#     print("约吗?")
#     print("....")
# yue()
# yue()
# yue()
# yue()

# while True:
#     print("掏出手机")
#     print("打开微信")
#     print("摇一摇")
#     print("聊一聊")
#     print("约吗?")
#     print("....")




# 面向过程:
# print("掏出手机")
# print("打开微信")
# print("摇一摇")
# print("聊一聊")
# print("约吗?")
# print("....")
# print("上班")
# print("掏出手机")
# print("打开微信")
# print("摇一摇")
# print("聊一聊")
# print("约吗?")
# print("....")
# print("检查一下")


# 面向函数编程:
# def work():
#     print("打开电脑")
#     print("查看邮件")
#     print("打开找到微信")
#     print("进行群聊")
#     print("开始撸代码")
#     print("撸完")
#     print("下班")
#
# def yue():
#     print("掏出手机")
#     print("打开微信")
#     print("摇一摇")
#     print("聊一聊")
#     print("约吗?")
#     print("....")
#
# yue()
# work()
# yue()
# print("检查一下")
# yue()
# print("6块钱的麻辣烫")
# yue()
# print("歇会,上个班")

# 函数的返回值:
# def yue():
#     print("打开手机")
#     print("打开微信")
#     print("打开附近的人")
#     print("聊一聊")
#     print("见一见")
#     print("......")
#     return "大妈"
# girl = yue()
# print(girl)

# def yue():
#     print("打开手机")
#     print("打开微信")
#     print("打开附近的人")
#     print("聊一聊")
#     print("见一见")
#     print("......")
#     return 1,2,3,4,5
#     print(1111)
# girl = yue()
# print(girl)

# a = 1,2,3,4,5
# print(a)


# def func():
#     msg = input("请输入内容:")
#     if msg == "1":
#         while True:
#             print("111")
#             return 3
#
# func()


# def func():
#     msg = input("请输入内容:")  # 2
#     if msg == "1":
#         while True:
#             print("111")
#     return 1
# print(func())


# 函数的返回值,返回给函数的调用者
# return 值 == 返回值

# return:
# 1.可以返回任意类型数据
# 2.return 返回多个内容是元组的形式
# 3.return 下方不执行,并且会终止当前这个函数
# 4.return 不写或写了return后面不写值都返回None


# 函数名+():
# 1.启动函数
# 2.接受返回值