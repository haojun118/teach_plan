# 1.文件操作: 目的 -- 持久化,永久存储数据
# 1.读: r
    # f = open("r",mode="r",encoding="utf-8")
    # print(f.read())  # 全部读取
    # print(f.read(3)) # 读字符
    # print(f.readline()) # 读一行
    # print(f.readlines()) # 一行一行的读
    # print(f.readline().strip()) # 去除换行符,制表符,空格

# rb:读字节
# 读字节不能写encoding
# read(3) #按照字节读取

# 路径:
# 1.绝对路径:从磁盘根部查找
# 2.相对路径:相对于某个文件进行查找
# 转义: 1.  "C:\\users\\..."    2. r"C:\users\..."


# w模式和a模式会自动创建文件

# 2.写: w
    # 1.打开文件的时候清空
    # 2.写入内容
    # 写的内容必须是字符串

# wb:写字节
    # 写字节不能写encoding

# 3.追加:a
    # 写在文件的末尾

# ab:
    # 写字节不能写encoding


# 4.+操作:
    # r+ 读写:
    # w+ 先写后读
    # a+ 加在最后面


# 5.其他操作:
    # seek() 移动光标
    # seek(0) 文件的头部
    # seek(0,1) 光标当前位置
    # seek(0,2) 文件末尾
    # seek(3) 按照字节移动

# tell() 查看光标
    # tell 返回的是字节

# 6.修改文件:
    # 1.先读在创建一个新的文件,将内容写入到新文件中

    # import os
    # f = open("旧文件位置","r",encoding="utf-8")
    # f1 = open("新文件位置","w",encoding="utf-8")
    # for i in f:
    #     i = i.replace("旧的值","新的值")
    #     f1.write(i)
    #
    # f.close()
    # f1.close()
    #
    # os.rename('旧文件',"备份文件")
    # os.rename("新文件","旧文件")


# with open()
    # 1.自动关闭文件
    # 2.同时操作多个文件