"""
3.文件a.txt内容：每一行内容分别为商品名字，价钱，个数。
apple 10 3
tesla 100000 1
mac 3000 2
lenovo 30000 3
chicken 10 3
# 通过代码，将其构建成这种数据类型：[{'name':'apple','price':10,'amount':3},{'name':'tesla','price':1000000,'amount':1}......] 并计算出总价钱。
"""

# lst = []
# f = open("a.txt","r",encoding="utf-8")
# for i in f:
#     a,b,c = i.split()
#     dic = {"name":a,"price":int(b),"amount":int(c)}
#     lst.append(dic)
# print(lst)


"""
4.有如下文件：
alex是老男孩python发起人，创建人。
alex其实是人妖。
谁说alex是sb？
你们真逗，alex再牛逼，也掩饰不住资深屌丝的气质。
将文件中所有的alex都替换成大写的SB（文件的改的操作）。
"""
# f = open("a.txt","r",encoding='utf-8')
# f1 = open("b.txt",'w',encoding="utf-8")
# for i in f:
#     i = i.replace("alex","SB")
#     f1.write(i)
# f.close()
# f1.close()
# import os
# os.rename("a.txt","c.txt")
# os.rename("b.txt","a.txt")


"""
5.文件a1.txt内容(选做题)

name:apple price:10 amount:3 year:2012
name:tesla price:100000 amount:1 year:2013
.......

通过代码，将其构建成这种数据类型：
[{'name':'apple','price':10,'amount':3,year:2012},
{'name':'tesla','price':1000000,'amount':1}......]
并计算出总价钱。
"""

# f = open("a.txt","r",encoding="utf-8")
# lst = []
# for i in f:
#     dic = {}
#     for em in  i.split():
#         k,v = em.split(":")
#         if v.isdecimal():
#             v = int(v)
#         dic[k] = v
#     lst.append(dic)
# print(lst)

"""
6.文件a1.txt内容(选做题)
序号 部门 人数 平均年龄 备注
1 python 30 26 单身狗
2 Linux 26 30 没对象
3 运营部 20 24 女生多
.......

通过代码，将其构建成这种数据类型：
[{'序号':'1','部门':Python,'人数':30,'平均年龄':26,'备注':'单身狗'},
{'序号':'2','部门':Linux,'人数':26,'平均年龄':30,'备注':'没对象'},
......]
"""

# f = open("a.txt","r",encoding="utf-8")
# ka,kb,kc,kd,ke = f.readline().split()
# lst = []
# for i in f:
#     va,vb,vc,vd,ve = i.split()
#     dic = {ka:va,kb:vb,kc:vc,kd:vd,ke:ve}
#     lst.append(dic)
# print(lst)