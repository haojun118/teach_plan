## 内容回顾

### 文本操作

```
节点.text()  //  获取标签文本
节点.text('文本内容')  //  设置标签文本内容 
节点.html('<a>xxxx</a>')  //  设置标签html的内容  (dom对象、jq对象)
```

### 标签操作

#### 增加

```
父子关系：  给父标签内部添加子标签
	父标签.append（子标签）     子标签.appendTo（父标签）   添加到后面
	父标签.prepend（子标签）    子标签.prependTo（父标签）  添加到前面
兄弟关系：  给兄弟前后添加兄弟标签
    兄标签.after(弟标签)       弟标签.insertAfter（兄标签）  
    兄标签.before(弟标签)      弟标签.insertBefore（兄标签）  
```

#### 删除

```
remove     删除标签和事件 返回当前删除的标签
detach	   删除标签  不删除事件 返回当前删除的标签
empty      清空内部的内容 保留当前的标签
```

### 替换

```
a.replaceWith(b)   //  用b去替换a
b.replaceAll(a)    //  用b去替换a
```

### 克隆

```
.clone()   //  仅复制标签  0 false
.clone(true)   //  复制标签和事件
```

## 属性操作

### 通用属性

```
.attr('属性')  // 获取某个属性的值
.attr('属性'，‘值’)  // 设置单个属性的值
.attr({'属性'：‘值’，'属性'：‘值’})   // 设置多个属性的值

.removeAttr('属性')  //  删除某个属性
```

### 表单的值

```
input   select  textarea    （value）
.val()   // 获取标签的值
.val(‘值’)   // 获取标签的值
$('#s1').val(['1','2'])   // 设置select的多选的值

radio checkbox select // 查看是否选中

```

#### 类

```
.addClass('类名')     //  classList.add()
.removeClass('类名')  //  classList.remove()
.toggleClass('类名')  //  classList.toggle()  切换  有就删除 没有就添加

```

#### css

```
.css（'css样式名'）   // 获取对应样式的值
.css（'css样式名','值'）   // 设置对应样式的值
.css（{'css样式名':'值','css样式名2':'值'}）   // 设置对应样式的值
```

#### 盒子模型

```
width()   //  内容的宽
height()  //  内容的高

innerHeight()    // 内容 + 内边距  高
innerWidth()     // 内容 + 内边距  宽

outerWidth()    //  内容 + 内边距  + 边框  宽
outerHeight()   //  内容 + 内边距  + 边框  高

outerHeight(true)    //  内容 + 内边距  + 边框 + 外边距 高
outerWidth(true)   //  内容 + 内边距  + 边框 + 外边距 宽

设置宽高的时候 只是设置内容的宽高
```

## 今日内容

### 动画

```
滑动系列
slideDown     向下划入
slideUp       向上划出
slideToggle   切换 
slideDown（毫秒数，回调函数）

显示系列
show   hide  toggle 

渐入渐出
fadeIn   渐入
fadeOut  渐出
fadeToggle  切换


stop()  停止之前的动画
```

### 事件

#### 绑定事件

```
// bind  
 $('button').bind('click',{'a':'bb'},fn);  //   事件类型 参数 函数

    function fn(e) {    //   e 事件的对象 
        console.log(e);    
        console.log(e.data);   //  传的参数
        // alert(123)
  }
  
 $('button').bind('click',fn);

    function fn(e) {
        console.log(e);
    } 

// 事件

$('button').click({'a': 'b'}, fn)  //  参数  函数

    function fn(e) {
        console.log(e.data);
    }
    
$('button').click( fn)

    function fn(e) {
        console.log(e.data);
    }

```

#### 解除事件

```
$('button').unbind('click') 
```

#### 常用的事件

```
click(function(){...})    // 点击事件

focus(function(){...})   //  获取焦点
blur(function(){...})    // 失去焦点


change(function(){...})  //内容发生变化，input（鼠标移出），select等

keyup(function(){...})  

mouseover/mouseout   
mouseenter/mouseleave  =   hover(function(){...})
```

### 文档的加载

```js
js
window.onload = function () {  //  页面 图片 视频 音频 都加载好执行
            $('#b1').click(
                function () {
                    $('.mask').show()
                }
            );
        }

//  window.onload  只执行一次  多次的划  后面的覆盖前面的

jquery
 $(window).ready(function () {  //  页面 图片 视频 音频 都加载好执行
            $('#b1').click(
                function () {
                    $('.mask').show()
                }
           );

  })
  // $(window).ready()  可执行多次  不会覆盖
  

  $(document).ready(function () {  //  页面  都加载好执行
            $('#b1').click(
                function () {
                    $('.mask').show()
                }
            );
            })
        })

//  简写
    $(
           function () {  //  页面  都加载好执行
                $('#b1').click(
                    function () {
                        $('.mask').show()
                    }
               );


        )


```

### each

```
$('li').each(function (i,dom) {
        console.log(i,dom)
        console.log(i,dom.innerText)
})

```

作业：

1. 小米购物车
2. input框单独校验（不能为空）手机号 
3. 模态框

