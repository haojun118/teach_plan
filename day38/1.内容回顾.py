# select distinct * from 表 where 条件 group by 字段
# having 过滤 order by 字段 limit n offset m;

# 书名	作者	出版社	价格	出版日期
# create table books(bname char(20),
# author char(12),
# press char(20),
# price float(6,2),
# pub_date date);

# insert into books values
# ('倚天屠龙记','egon','北京工业地雷出版社',70,'2019-7-1'),
# ('九阳神功','alex','人民音乐不好听出版社',5,'2018-7-4'),
# ('九阴真经','yuan','北京工业地雷出版社',62,'2017-7-12'),
# ('九阴白骨爪','jinxin','人民音乐不好听出版社',40,'2019–8-7'),
# ('独孤九剑','alex','北京工业地雷出版社',12,'2017-9-1'),
# ('降龙十巴掌','egon','知识产权没有用出版社',20,'2019-7-5'),
# ('葵花宝典','yuan','知识产权没有用出版社',33,'2019–8-2');

# 查询egon写的所有书和价格
# select bname,price from books where author='egon';

# 找出最贵的图书的价格
# select max(price) from books;

# 求所有图书的均价
# select avg(price) from books;

# 将所有图书按照出版日期排序
# select * from books order by pub_date;

# 查询alex写的所有书的平均价格
# select avg(price) from books where author = 'alex'

# 查询人民音乐不好听出版社出版的所有图书
# select * from books where press = '人民音乐不好听出版社';

# 查询人民音乐出版社出版的alex写的所有图书和价格
# select bname,price from books where press = '人民音乐不好听出版社' and author = 'alex'

# 找出出版图书均价最高的作者
# select author from books group by author order by avg(price) desc limit 1;
# select author,avg(price) as avg_price from books group by author order by avg_price desc limit 1;

# 找出最新出版的图书的作者和出版社
# select author,press from books order by pub_date desc limit 1

# 显示各出版社出版的所有图书
# select press,group_concat(bname) from books group by press;

# 查找价格最高的图书,并将它的价格修改为50元
# select max(price) from books;    # 70
# update books set price=50 where price = 70;

# update books set price=50 order by price desc limit 1;
# update books set price=50 where price = (select * from (select max(price) from books)as t);

# 删除价格最低的那本书对应的数据
# select min(price) from books;  # 5
# delete from books where price = 5;

# delete from books order by price limit 1;
# delete from books where price = (select * from (select min(price) from books)as t);

# 将所有alex写的书作者修改成alexsb
# update books set author = 'alexsb' where author = 'alex';

# select year(publish_date) from book
# month(publish_date)
# day(publish_date)
# delete from books where year(publish_date) = 2017;

