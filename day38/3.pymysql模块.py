import pymysql
# conn = pymysql.connect(host='localhost', user='root', password="123",
#                  database='day38')
# cur = conn.cursor(pymysql.cursors.DictCursor)  # cur游标 cur数据库操作符
# 对数据的增删改查
# ret = cur.execute('select * from books')
# ret 影响行数 在这里表示查到7行数据
# for i in range(ret):
#     row1 = cur.fetchone()   # 每次取一条数据
#     print(row1)
# row2 = cur.fetchmany(3) # 按照指定参数取n条
# print(row2)
# row3 = cur.fetchall()   # 取所有，过于浪费内存
# print(row3)

# insert update delete  conn.commit()
# cur = conn.cursor()  # cur游标 cur数据库操作符
# cur.execute('insert into books values("学python从开始到放弃","alex","人民大学出版社",50,"2018-7-1")')
# conn.commit()

# cur = conn.cursor()
# sql = 'insert into books values(%s,%s,%s,%s,%s)'
# # 操作文件
# with open('file',encoding='utf-8') as f:
#     for line in f:
#         try:
#             lst = line.strip().split('|')
#             cur.execute(sql,lst)
#             conn.commit()
#         except:
#             conn.rollback()
# cur.close()
# conn.close()

# sql注入
# 登陆注册 + 数据库
# 表 userinfo
# cur = conn.cursor()
# name = input('user:')
# passwd = input('password:')
# sql = "select * from userinfo where username = %s and password = %s;"
# print(sql)
# ret = cur.execute(sql,(name,passwd))
# if ret:
#     print('登陆成功')
# else:
#     print('登陆失败')

# sql注入
# select * from userinfo where username = 'xxx' or 1=1 ; -- ' and password =













