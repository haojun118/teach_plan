# 怎么让两张表之间产生关系？

# 连表
    # 内连接（所有不在条件匹配内的数据，都会被剔出连表）
    # 方式一 ：select * from employee,department where dep_id = department.id;
    # 方式二 ：select * from employee inner join department on dep_id = department.id;

    # 外连接
        # 左外连接 left join
        # select * from employee left join department on dep_id = department.id;
        # 右外连接 right join
        # select * from employee right join department on dep_id = department.id;
        # 全外连接 full join
        # select * from employee left join department on dep_id = department.id
        # union
        # select * from employee right join department on dep_id = department.id


    #示例1：以内连接的方式查询employee和department表，并且employee表中的age字段值必须大于25,即找出年龄大于25岁的员工以及员工所在的部门
    # select employee.name,department.name from employee inner join department on dep_id = department.id where age>25;
    # select e.name ename,d.name dname from employee e inner join department d on dep_id = d.id where age>25;
    # select e.name,d.name from employee e inner join department d on dep_id = d.id where age>25;

    #示例2：以内连接的方式查询employee和department表，并且以age字段的升序方式显示。
    # select * from employee inner join department on dep_id = department.id order by age;

# 子查询
    # 查询平均年龄在25岁以上的部门名
    # select name from department where id in (select dep_id from employee group by dep_id having avg(age)>25);

    # 查看技术部员工姓名
        # 先查询技术部的id
            # select id from department where name = '技术';
        # 根据技术部id查询employee表 找到技术部id对应的人名
            # select * from employee where dep_id = 200;
        # 结果
            # select name from employee where dep_id = (select id from department where name = '技术');

    # 查看不足1人的部门名(子查询得到的是有人的部门id)
        # 先从employee中查有多少个部门有人
            # select distinct dep_id from employee;
        # 从department表中把不在上述部门中的那些项找出来
            # select * from department where id not in (200,201,202,204);
        # 结果
            # select * from department where id not in (select distinct dep_id from employee);

    # 查询大于所有人平均年龄的员工名与年龄
        # 所有人的平均年龄
            # select avg(age) from employee;   # 28
        # 查大于上述平均年龄的人
            # select name,age from employee where age>28;
        # 结果
            # select name,age from employee where age>(select avg(age) from employee);
    # 查询大于部门内平均年龄的员工名、年龄
        # 查询各部门平均年龄
            # select dep_id,avg(age) from employee group by dep_id;
        # 查大于部门平均年龄的人
            # select * from employee where dep_id = 200 and age>18
            # select * from employee where dep_id = 201 and age>43
            # select * from employee where dep_id = 202 and age>28
            # select * from employee where dep_id = 204 and age>18
        # 结果
            # select * from employee inner join (select dep_id,avg(age) as avg_age from employee group by dep_id) as t
            # on employee.dep_id = t.dep_id;
            #
            # select * from employee inner join (select dep_id,avg(age) as avg_age  from employee group by dep_id) as t
            # on employee.dep_id = t.dep_id where age>avg_age;

#





