"""
1.判断下列逻辑语句的True,False.
1 > 1 or 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6
1 > 1 or 3 < 4 or F  and 9 > 8 or 7 < 6
1 > 1 or 3 < 4 or F  or 7 < 6
F or 3 < 4 or F  or 7 < 6
T or F  or 7 < 6

2）not 2 > 1 and 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6
F and 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6
F or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6
F or F and 2 > 1 and 9 > 8 or 7 < 6
F or F and 9 > 8 or 7 < 6
F or F or F
F
"""


"""
1),8 or 3 and 4 or 2 and 0 or 9 and 7   8
8 or 3 and 4 or 2 and 0 or 9 and 7
8



2),0 or 2 and 3 and 4 or 6 and 0 or 3  4
"""

"""
5.利用while语句写出猜大小的游戏：













设定一个理想数字比如：66，让用户输入数字，如果比66大，
则显示猜测的结果大了；如果比66小，
则显示猜测的结果小了;只有等于66，
显示猜测结果正确，然后退出循环。
"""
# num = 66
# while True:
#     msg = int(input(">>>"))
#     if msg == num:
#         print("等于")
#         break
#     elif msg > num:
#         print("大于")
#     elif msg < num:
#         print("小于")

"""
6.在5题的基础上进行升级：
给用户三次猜测机会，如果三次之内猜测对了，则显示猜测正确，退出循环，
如果三次之内没有猜测正确，则自动退出循环，并显示‘太笨了你....’。
"""
# num = 66
# count = 3
# while count > 0:
#     msg = int(input(">>>"))
#     if msg == num:
#         print("等于")
#         break
#     elif msg > num:
#         print("大于")
#     elif msg < num:
#         print("小于")
#     count = count - 1
# else:
#     print("太笨了你")

# 7.使用while循环输出 1 2 3 4 5 6 8 9 10
# num = 1
# while num < 11:
#     if num == 7:
#         num += 1
#     print(num)
#     num += 1

"""
求1-100的所有数的和
"""
# num_sum = 0
# count = 1
# while count < 101:
#     num_sum = num_sum + count
#     count += 1
# print(num_sum)

"""
输出 1-100 内的所有奇数
"""
# count = 1
# while count < 101:
#     if count % 2 == 1:
#         print(count)
#     count += 1

# count = 1
# while count < 101:
#     print(count)
#     count += 2

"""
求+1-2+3-4+5 ... 99的所有数的和
"""
# j_sum = 0  # 奇数的和
# o_sum = 0  # 偶数的和
# num = 1
# while num < 100:
#     if num % 2 == 1:
#         j_sum = j_sum + num
#     else:
#         o_sum = o_sum - num
#     num += 1
#
# print(j_sum + o_sum)


# num_sum = 0
# num = 1
# while num < 100:
#     if num % 2 == 1:
#         num_sum = num_sum + num
#     else:
#         num_sum = num_sum - num
#     num += 1
# print(num_sum)

"""
12.⽤户登陆（三次输错机会）且每次输错误时显示剩余错误次数
（提示：使⽤字符串格式化）
"""
# count = 3
# while count > 0:
#     user = input("username:")
#     pwd = input("password:")
#     if user == "alex" and pwd == "alex123":
#         print("登录成功!")
#         break
#     else:
#         # print("账号或密码错误,剩余次数%s"%(count - 1))
#         print(f"账号或密码错误,剩余次数{count - 1}")
#
#     count = count - 1