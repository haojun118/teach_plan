# user = input("username:")
# pwd = input("password:")
# yzm = input("请输入验证码(A0oE):")
#
# if yzm.upper() == "A0oE".upper():
#     if user == 'alex' and pwd == "alexdsb":
#         print("登录成功!")
#     else:
#         print("账号或密码错误!")
# else:
#     print("验证码错误!")


# 字符串是不可变数据类型,字符串是有序的

# name = "alex"
# a = name.upper()  # 全部大写
# print(a)
# print(name)

# name = "ALEX"
# a = name.lower()   # 全部小写
# print(name)
# print(a)

# name = "alex"
# print(name.startswith('e',2,3))  # 以什么开头 -- 返回的是布尔值
# print(name.endswith('l',0,2))    # 以什么结尾 -- 返回的是布尔值

# name = "alexwusirtaibIa"
# print(name.count("i"))      # 统计,计数

# user = input("username:").strip()
# pwd = input("password:").strip()
# # alex   alexdsb
# if user == 'alex' and pwd == "alexdsb":
#     print("登录成功!")
# else:
#     print("账号或密码错误!")


# pwd = " alexdsb   "
# a = pwd.strip()   # 脱 默认脱(脱头尾两端的空格,换行符\n,制表符\t)
# print(a)

# pwd = "alexasdsbal"
# a = pwd.strip("al")  # 去除头尾两端指定的内容
# print(a)


# name = "alex_wu_si_r"
# a = name.split("_")        # 分割(默认空格,换行符\n,制表符\t)
# print(a)                   # ['alex', 'wusir']

# print(name.split("_",2))   # 可以指定分割的次数



# name = "alex,wusir,ta,i,b,a,i"
# a = name.replace(",",".")               # 全部替换
# print(a)

# a = name.replace(",",".",4)              # 可以指定替换的次数
# print(a)


# 字符串格式化(字符串的方法)
# name = "{}今年:{}".format("宝元",18)    # 按照位置顺序进行填充
# print(name)

# name = "{1}今年:{0}".format("宝元",18)    # 按照索引进行填充
# print(name)

# name = "{name}今年:{age}".format(name="宝元",age=18)    # 按照名字进行填充
# print(name)

#  %s  f  format

# is 系列 是进行判断  返回的是布尔值

msg = "alex"
# print(msg.isdigit())      # 判断字符串中的内容是不是全都是数字(阿拉伯数字)
# print(msg.isdecimal())    # 判断是不是十进制数
# print(msg.isalnum())      # 判断是不是数字,字母,中文
# print(msg.isalpha())      # 判断是不是字母,中文


# 总结:
#     upper        *****
#     lower        ****
#     startswith   ***
#     endswith     ***
#     count        ****
#     strip        ******
#     split        ******
#     replace      ******
#     format       ***
#     isdecimal()  ***
#     isalnum()    ***
#     isalpha()    ***


msg = "今天是个好日子,日魔又要去找曹洋"

# 公用的方法: len
# print(len(msg))

# print(msg)
# 今
# 天
# 是
# 个
# 好
# 日
# 子
# ,

# 使用while循环打印字符串中每个元素

# i = 0
# while i < len(msg):  # while 0 < 16:
#     print(msg[i])          # print(msg[0])
#     i += 1

# for 循环

# for i in msg:
#     print(i)

# for 关键字
# i   变量名(可以任意修改)
# in  关键字
# msg 可迭代对象

# 可迭代对象: Python数据类型中 除了int,bool其余都可以迭代

# msg = "今天是个好日子,日魔又要去找曹洋"
# for a in msg:
#     print(a)
# print(a)



# 面试题
# for a in "abcds":
#     pass  # 过  占位
# print(a)


# for i in "dsb":
#     i = i+"sb"
#     print(i)

