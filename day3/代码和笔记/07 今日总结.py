# 1.整型及布尔值转换
    # Python3: 都是整型
    # Python2: 整型, 长整型

    # 10  -   2 bin()
    # 2   -   10 int("1110",2)

    # 数字:只要不为零全都是True
    # 字符串:只要有内容就是True

# 2.索引,切片,步长

    # 索引:
    #     从左向右
    #     从右向左
    #     索引不能超出索引最大值
    #
    # 切片:
    #     [起始位置:终止位置]
    #     顾头不顾尾
    #     终止位置可以超出
    #     [:]  [从头到尾]

    # 步长:
    #     [起始位置:终止位置:步长]
    #     步长决定查找方向
    #     决定查找时迈的步子
    #     [::-1] 将字符串进行反转

# 3.字符串方法
    #     upper        *****
    #     lower        ****
    #     startswith   ***
    #     endswith     ***
    #     count        ****
    #     strip        ******
    #     split        ******
    #     replace      ******
    #     format       ***
    #     isdecimal()  ***
    #     isalnum()    ***
    #     isalpha()    ***

# 4.for循环

# for 关键字
# i   变量(可以任意修改)
# in  关键字
# 可跌代对象(int,bool)除外

# for i in "abc":
#     pass  # 占一行的位置 pass下方的代码会进行执行
# print(i)


# for 循环在循环的时候就已经进行了赋值

# for i in "abc":
#     ...  #  ...和pass功能一样


for i in "abc":
    print(i)
