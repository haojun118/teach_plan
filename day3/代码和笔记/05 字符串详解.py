# str -- 字符串:
# python中只要是引号引起来的就是字符串
""" """
''' '''
" "
' '
# 字符串:用于数据存储,存储少量数据

# a = "meet"
# 字符串中每一个字母或者字符都称为元素

# 索引(下标)
# "meet"
#  0123
# 从左向右排
# " m e e t"
#  -4-3-2-1
# 从左向右排

# a = "meet"
# print(a[4])

# a = "meet"
# print(a[-5])

# 索引的时候不能超出索引最大值

# a = "alex_wu_sir,_tai_bai_日魔"
# print(a[5])
# print(a[6])
# print(a[11])
# print(a[-2])
# print(a[-1])

# a1 = a[5]
# a2 = a[6]
# a3 = a[7]
# a4 = a[8]
# a5 = a[9]
# a6 = a[10]
# print(a1+a2+a3+a4+a5+a6)

# print(a[5:11])  # [索引值]  [起始位置(包含):终止位置(不包含)]
# 顾头不顾尾

# print(a[12:21])

a = "alex_wu_sir,_tai_bai_日魔"
# print(a[21:100])
# print(a[21:])  [21(起始位置):(默认到最后)]
# print(a[:])    [(默认从最开始):(默认到最后)]

# print(a[0:-2])

# 切片的时候可以超出索引值

# print(a[-6:-2:1])
# a = "alex_wu_sir,_tai_bai_日魔"
# print(a[1:8:2]) -- lxw_
# print(a[1:8:4]) -- lw

# print(a[-12:-3:2])  --  ,tibi
# print(a[10:-5:1])
# print(a[-5:-10:-1])

#步长决定查找的方向

# name = "alex,wusir,太白金星,女神,吴超"
# 1.太金
# print(name[11:15:2])
# 2.神女
# print(name[-4:-6:-1])
# 3.星白,
# print(name[-7:-13:-2])
# 4."alex,wusir,太白金星,女神,吴超" 整体反转
# print(name[::-1])
# ***** 面试题的答案
# print(name[10:10000:200000])