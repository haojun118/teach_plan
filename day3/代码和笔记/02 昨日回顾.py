# 1.while循环
#     break:终止当前循环,只能在循环体中使用
#     continue:跳出本次循环,继续下次循环
#     while else:当while的条件为假的时候,执行else.当while条案为真的时候,执行while循环体
#     当循环体中出现了break终止循环,不执行else
#
# 2.格式化
#     %s:占位符,字符串
#     %d:占位符,整型
#     %i:占位符,整型
#     %%:转义%
#
#     # f-string
#     f"{name},{'alex'}"

# 3.运算符
#     算数运算符
#         + - * / % // **
#     逻辑运算符
#         and or not
#         真:x and y  == y
#         假:x and y  == x
#         一真一假 0 and True
#
#         or
#         真:x or y == x
#         假:x or y == y
#         一真一假:x or y == x

           # not(非)
           # not True  -- False
           # not False -- True

        # 优先级:
        #     () > not > and > or

    # 比较运算符
    #     > < >= <= == !=
    # 赋值运算符
    #     = += -= *= /= //= **= %=
    # 成员运算符
    #     in not in


# 4.编码初识
#     unicode(万国码)
#         中文/英文 4个字节
#     gbk(国标)
#         英文 -- 1个字节
#         中文 -- 2个字节
#     ascii
#         不支持中文
#     utf-8
#         英文 1
#         欧洲 2
#         亚洲 3

# 1B(字节) = 8b(位)
