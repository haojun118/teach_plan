# int -- 整型
# python3: 全部都是整型
# python2: 整型,长整型long
# 用于计算和比较

# 123  -- 10进制

# 10进制 - 2进制
# 30   0
# 15   1
# 7    1
# 3    1
# 1    1

# 11110
# print(bin(30))
# 111110


# 2进制 - 10进制
# 0 * 2**0 + 1 * 2 ** 1 + 1 * 2**2 + 1* 2**3 + 1 * 2 ** 4 + 1 * 2 ** 5
# 0 + 2 + 4 + 8 + 16 + 32 = 62
# print(int("111110",2))

# 布尔值
# 转换

# int,str,bool

# a = bool(0)
# print(a)

# a = int(True)
# print(a)

# a = int(False)
# print(a)
#
# a = bool("a")
# print(a)
#
# a = bool("啊")
# print(a)
#
# a = bool(" ")
# print(a)
#
# a = bool("")
# print(a)

# a = str(True)
# print(type(a),a)

# a = str(False)
# print(type(a),a)

# 整型中只要是非零的就是True
# 布尔值 0 -- False  1 -- True
# 字符串中只要没有内容就是False
