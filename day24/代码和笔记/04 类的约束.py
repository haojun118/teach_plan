# 约束:类(子类)

# 版一:
# class WechatPay:
#
#     def pay(self):
#         print("微信支付")
#
#
# class AliPay:
#
#     def pay(self):
#         print("支付宝支付")
#
#
# class QQpay:
#
#     def fuqian(self):
#         print("QQ支付")

# qq = QQpay()
# qq.fuqian()

# wei = WechatPay()
# ali = AliPay()
# qq = QQpay()
#
#
# wei.pay()
# ali.pay()
# qq.fuqian()


# def pay(object):
#     object().pay()


# 版二
# 方式一:
# class PayClass:
#     def pay(self):
#         pass
#
# class WechatPay(PayClass):
#
#     def pay(self):
#         print("微信支付")
#
#
# class AliPay(PayClass):
#
#     def pay(self):
#         print("支付宝支付")
#
#
# class QQpay(PayClass):
#
#     def fuqian(self):
#         print("QQ支付")
#
#
# def pay(object):
#     object().pay()
#
# pay(QQpay)

# 版三:
# 方式一:  (推荐并且常用的方式)
# raise 主动抛出异常(主动报错)
# class PayClass:
#     def pay(self):
#         raise Exception("你子类必须要写一个pay方法")
#
# class WechatPay(PayClass):
#
#     def pay(self):
#         print("微信支付")
#
#
# class AliPay(PayClass):
#
#     def pay(self):
#         print("支付宝支付")
#
#
# class QQpay(PayClass):
#
#     def pay(self):
#         print("QQ支付")
#
#     # def pay(self):
#     #     pass
#
# def pay(object):
#     object().pay()

# pay(QQpay)


# 方法二
# 抽象类,接口类: 制定一些规则
# from abc import ABCMeta,abstractmethod   # 抽象类,接口类
# class PayClass(metaclass=ABCMeta):  # 元类
#     @abstractmethod
#     def pay(self):
#         raise Exception("你子类必须要写一个pay方法")
#
# class WechatPay(PayClass):
#
#     def pay(self):
#         print("微信支付")
#
# class AliPay(PayClass):
#
#     def pay(self):
#         print("支付宝支付")
#
# class QQpay(PayClass):
#
#     def pay(self):
#         print("QQ支付")

# def pay(object):
#     object().pay()

# pay(WechatPay)
# pay(AliPay)
# pay(QQpay)

# qq = QQpay()
# qq.pay()
