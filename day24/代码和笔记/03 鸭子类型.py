# 鸭子类型(编程思想)

# 鸭子类型: 当看到一只鸟走起来像鸭子、游泳起来像鸭子、叫起来也像鸭子，那么这只鸟就可以被称为鸭子。

# class Str:
#
#     def index(self):
#         print("啊啊啊")
#
#     def walk(self):
#         print("一步两步")
#
#     def do(self):
#         print("左手右手一个慢动作")
#
# class List:
#
#     def index(self):
#         print("嗯嗯嗯")
#
#     def walk(self):
#         print("一步两步")
#
#     def do(self):
#         print("左手右手一个慢动作")

# a = A()
# b = B()
# a.call()
# b.call()

# 统一接口 ,归一化(规范)

# def call(object):
#     object().call()
#
# call(B)
# call(A)


# python中 str,list,tuple中很多使用鸭子类型
# str.index()
# list.index()
# tuple.index()

