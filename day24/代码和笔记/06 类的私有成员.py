# 私有 : 只能自己有
# 以__开头就是私有内容
# 私有内容的目的就是保护数据的安全性

class Human:

    live = "有思想"        # 类公有的属性
    __desires = "有欲望"   # (程序级别)类私有的属性
    _desires = "有欲望"    # (程序员之间约定俗成)类私有的属性

    def __init__(self,name,age,sex,hobby):
        self.name = name
        self.age = age
        self.sex = sex        # 对象的公有属性
        self.__hobby = hobby  # 对象的私有属性

    def func(self):
        # 类内部可以查看对象的私有属性
        print(self.__hobby)

    def foo(self):            # 公有方法
        # 类内部可以查看类的私有属性
        print(self.__desires)

    def __abc(self):          # 私有方法
        print("is abc")

# print(Human.__dict__)
# print(Human._desires)


# class B(Human):
#     pass
    # def run(self):
        # print(self.__desires)
        # self._Human__abc()    # 强制继承(非常不推荐使用)


# b = B("日魔",28,"男","女孩子")
# print(b.__desires)
# b.run()
# b._Human__abc()
# _类名私有的属性和方法
# 子类不能继承父类中私有的方法和属性


# 类的外部查看
# print(obj.name)
# print(Human.live)

# Human.live = "无脑"
# print(Human.live)
# obj.__abc()
# print(Human.__desires)  # 保证数据的安全性