# encoding:utf-8
# class D(object):
#
#     def f1(self):
#         print("这是D父类",self)
#
# class B1(D):
#     pass
#     # def f1(self):
#     #     print("这是B1父类",self)
#
# class A(D):
#     pass
#     # def f1(self):
#     #     print("这是A父类",self)
#
#
# class B(B1,A):
#
#     def f1(self):
#         print ("这是子类",self)
#         super(B,self).f1()   # super 按照mro进行查找
#
# b = B()
# b.f1()
# print(B.mro())


# class A:
#     def f1(self):
#         print('in A f1')
#
#     def f2(self):
#         print(self)
#         print('in A f2')
#
#
# class Foo(A):
#     def f1(self):
#         super(Foo,self).f2()   # 查找当前类(Foo)的下一个类
#         # A.f2()
#         # [Foo --> A  ---> object]
#         print('in A Foo')
#
#
# obj = Foo()
# obj.f1()


# class A:
#     def f1(self):
#         print('in A')
#
# class Foo(A):
#     def f1(self):
#         super().f1()
#         print('in Foo')
#
# class Bar(A):
#     def f1(self):
#         print('in Bar')
#
# class Info(Foo,Bar):
#     def f1(self):
#         super(Foo,self).f1()
#         print('in Info f1')
#
# # [Info -- Foo -- Bar -- A]
# obj = Info()
# obj.f1()


# class A:
#     def f1(self):
#         print('in A')
#
# class Foo(A):
#     def f1(self):
#         super().f1()
#         print('in Foo')
#
# class Bar(A):
#     def f1(self):
#         print('in Bar')
#
# class Info(Foo,Bar):
#     def f1(self):
#         # super里的类名是指定查找mro中类名的下一个类, self是指定查找时使用的mro顺序
#         super(Info,self).f1()   # Foo() 对象的内存地址  # super(子类名,子类的mro列表)
#         print('in Info f1')


# aa = Info()  # 对象的内存地址
# aa.f1()

# [Info', Foo', Bar', A', 'object']

# a = Foo()
# b = a
# print(a)
# print(b)

# print(Info.mro())
# obj = Info()
# obj.f1()
# 总结:
