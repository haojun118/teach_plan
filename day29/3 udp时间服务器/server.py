import time
import socket
sk = socket.socket(type=socket.SOCK_DGRAM)
sk.bind(('127.0.0.1',9001))
while True:
    msg,addr = sk.recvfrom(1024)
    date_fmt = msg.decode('utf-8')
    tim = time.strftime(date_fmt)
    sk.sendto(tim.encode('utf-8'),addr)