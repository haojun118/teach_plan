import struct
import socket

def proto_send(msg):
    msg = msg.encode('utf-8')
    len_msg = len(msg)
    proto_len = struct.pack('i', len_msg)  # 把字节的长度编程4字节,i代表int
    conn.send(proto_len)
    conn.send(msg)

sk = socket.socket()
sk.bind(('192.168.12.26',9001))
sk.listen()

conn,addr = sk.accept()
msg1 = 'hello'
msg2 = 'world'
proto_send(msg1)
proto_send(msg2)

# 计算要发送的数据字节长度
# 把字节的长度编程4字节
# 发送4字节
# 发送数据

# 通过socket的tcp协议上传文件