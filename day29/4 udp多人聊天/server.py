import json
import socket
sk = socket.socket(type=socket.SOCK_DGRAM)
sk.bind(('127.0.0.1',9001))
# 同时多个人发消息
# 区别每个人的标识符和昵称
dic = {'10000':('alex','32'),'10001':('wusir','31'),
       '10002':('宝哥哥','33'),'10003':('景俊可','34')}
while True:
    msg,addr = sk.recvfrom(1024)
    info = msg.decode()
    msg_dic = json.loads(info)
    uid = msg_dic['id']
    print('\033[1;%sm%s: %s\033[0m'%(dic[uid][1],dic[uid][0],msg_dic['message']))
    msg = input('>>>').strip().encode()
    sk.sendto(msg,addr)