import json
import socket

sk = socket.socket(type=socket.SOCK_DGRAM)
server = ('127.0.0.1',9001)

while True:
    inp = input('>>>')
    dic = {'id':'10002','message':inp}
    msg = json.dumps(dic)
    sk.sendto(msg.encode('utf-8'),server)
    ret = sk.recv(1024).decode()
    print(ret)
