import struct

ret = struct.pack('i',197274000)
print(ret)

# 能够把一个任意大小的数据 转换成固定的 4个字节

res = struct.unpack('i',b'\x90)\xc2\x0b')
print(res[0])

# -2147483648 ~ +2147483647  # 2g
