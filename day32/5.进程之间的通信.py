# 进程之间通信 IPC
# Inter Process Communication
from multiprocessing import Queue,Process

# def son(q):
#     print(q.get())
#
# if __name__ == '__main__':
#     q = Queue()
#     p = Process(target=son,args=(q,))
#     p.start()
#     q.put(123)
# 在进程之间维护数据的安全 -- 进程安全
# 队列是进程安全的(进程队列保证了进程的数据安全)
# 队列都是先进先出的
# 队列是基于文件 + 锁实现的
# 队列一共提供两个方法:get put

# q = Queue()
# q.put({1,2,3})
# num = q.get()  # get是一个同步阻塞方法,会阻塞直到数据来
# print(num)

# q = Queue(2)
# q.put({1,2,3})
# q.put({1,2,3})
# q.put({1,2,3})  # put是一个同步阻塞方法,会阻塞直到队列不满
import queue
# q = Queue(2)
# try:
#     for i in range(4):
#         q.put_nowait(i)  # put_nowait 同步非阻塞方法
# except queue.Full:
#     print(i)

# q2 = Queue(2)
# try:
#     print(q2.get_nowait())
# except queue.Empty:pass

# q = Queue(5)
# ret = q.qsize()  # 查看当前队列有多少值
# print(q.empty())
# print(q.full())

# 生产者消费者模型

# 队列Queue = 管道Pipe + 锁
# Pipe  基于文件实现的(socket+pickle)  =  数据不安全
# Queue 基于文件(socket+pickle)+锁(lock)实现的  =  数据安全
#      基于pipe+锁(lock)实现的

# IPC:
    # 内置的模块实现的机制 :队列\管道
    # 第三方工具 : redis rabbitMQ memcache







