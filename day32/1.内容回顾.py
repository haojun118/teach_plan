# 1.重视你的每一节课
# 2.重视复习 重视自己的努力

# 1.把概念背一遍 和你的同桌说一遍
# 2.如果概念\作业今天没有完成 不要走
# 3.至少每天提一个有质量的问题
# 4.每天至少敲300行不重复的代码(写之前的作业)

# 多道操作系统 : 遇到IO就切换,支持多个进程同时被计算机执行
# 分时操作系统 : 时间片的概念 时间片到了就切换,更好的支持多个进程同时被计算机执行
# IO操作的概念 :
    # time.sleep
    # input和print
    # 网络操作 : connect accept recv recvfrom send sendto close
    # 文件的操作 : read write dump load

    # 会发生人类能感知到的阻塞:
        # sleep
        # input
        # accept
        # connect
        # recv/recvfrom

# 计算 : 除了文件操作和sleep之外,所有的python操作内存中数据的代码都是计算.都需要CPU参与
    # + - * / ** %
    # 各种书据类型的方法 :strip len split

# 进程的三状态图
    # 就绪 运行 阻塞
    # 阻塞,需要重新排队

import sys
# username = input()  阻塞
# username,password = sys.argv[1],sys.argv[2]

# ftp 命令行方式
# python ftp.py alex alex3714 upload d://xxx.mp4
# python ftp.py alex alex3714 download xxxx.mov
# python ftp.py alex alex3714 ls xxxx.mov

# 阻塞 : 你写的一段代码 在这段代码的执行期间CPU不工作
# 反过来就是非阻塞
# 同步 : 之前写的左右程序都是同步
# 异步 : 进程start()

# 同步阻塞  : input() sleep()
# 同步非阻塞 :  len()

# from multiprocessing import Process
#
# def func(a):
#     print('我是一个子进程',a)
#
# if __name__ == '__main__':
#     # 为了避免在win操作系统中递归开启子进程而必须要写的
#     # 主要是因为不同的操作系统开启进程的方式不同
#     p = Process(target=func,args=(1,))
#     p.start()












