# 数据安全(锁) :用来保证数据安全
# 如果多个进程同时对一个文件进行操作会出现什么问题
    # 1.读数据 : 可以同时读
    # 2.写数据 : 但不能同时写
from multiprocessing import Process,Lock
def change(lock):
    print('一部分并发的代码,多个进程之间互相不干扰的执行着')
    lock.acquire()  # 给这段代码上锁
    with open('file','r') as f:
        content = f.read()
    num = int(content)
    num += 1
    for i in range(1000000):i+=1
    with open('file','w') as f:
        f.write(str(num))
    lock.release()  # 给这段代码解锁
    print('另一部分并发的代码,多个进程之间互相不干扰的执行着')

if __name__ == '__main__':
    lock = Lock()
    for i in range(10):
        Process(target=change,args=(lock,)).start()
# 当多个进程同时操作文件/共享的一些数据的时候就会出现数据不安全

# 开启多进程 同时执行100000行代码
# 其中20行涉及到了操作同一个文件
# 只给这20行代码枷锁,来保证数据的安全












