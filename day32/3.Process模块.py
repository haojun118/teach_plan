import os
import time
from multiprocessing import Process
#
# def son_process():
#     print('strat son',os.getpid())
#     time.sleep(50)
#     print('end son')
#
# if __name__ == '__main__':
#     print(os.getpid())
#     Process(target=son_process).start()

# 主进程是在子进程执行完毕之后才结束
# 主进程回收子进程的资源

# 多个子进程
# def son_process():
#     print('strat son',os.getpid())
#     time.sleep(1)
#     print('end son')
#
# if __name__ == '__main__':
#     print(os.getpid())
#     Process(target=son_process).start()
#     Process(target=son_process).start()
#     Process(target=son_process).start()

# def son_process():
#     print('strat son',os.getpid())
#     time.sleep(1)
#     print('end son')
#
# if __name__ == '__main__':
#     for i in range(5):
#         Process(target=son_process).start()

# join
# import time
# def son_process():
#     time.sleep(4)
#     print('通知xxx用户,机器挂掉了')
#
# if __name__ == '__main__':
#     p = Process(target=son_process)
#     p.start()
#     p.join() # 阻塞,直到p对应的子进程对象执行结束
#     print('所有任务结束')

# import time
# def son_process(n):
#     print('start', n)
#     time.sleep(2)
#     print('end',n)
#
# if __name__ == '__main__':
#     p_l = []
#     for i in range(10):
#         p = Process(target=son_process,args=(i,))
#         p.start()   # start相当于告诉操作系统要开启一个子进程,而子进程的调度是由操作系统控制的
#         p_l.append(p)
#     for p in p_l:p.join()  # join 如果执行这句话的时候子进程已经结束了,那么join就不阻塞了
#     print('所有任务结束')

# 守护进程
# 守护进程会随着父进程的代码结束而结束
# import time
# def son():
#     while True:
#         time.sleep(1)
#         print('in son')
#
#
# if __name__ == '__main__':
#     p = Process(target=son)
#     p.daemon = True   # 将当前的子进程设置为守护进程
#     p.start()
#     time.sleep(5)

# 正常情况下 父进程永远会等着子进程结束
# 如果设置了守护进程 父进程的代码结束之后 守护进程也跟着结束

# 子进程结束之后,父进程才会结束

# 代码结束和进程结束是两回事儿
# 没设置守护进程
    # 1.子进程的代码和主进程的代码自己执行自己的,互相之间没关系
    # 2.如果主进程的代码先结束,主进程不结束,等子进程代码结束,回收子进程的资源,主进程才结束
    # 3.如果子进程的代码先结束,主进程边回收子进程的资源边执行自己的代码,当代码和资源都回收结束,主进程才结束
# 设置了守护进程
    # 1.子进程的代码和主进程的代码自己执行自己的,互相之间没关系
    # 2.一旦主进程的代码先结束,主进程会先结束掉子进程,然后回收资源,然后主进程才结束

# import time
# def son():
#     while True:
#         time.sleep(1)
#         print('in son')
#
# def son2():
#     print('start son2')
#     time.sleep(10)
#     print('end son2')
#
# if __name__ == '__main__':
#     p = Process(target=son)
#     p.daemon = True
#     p.start()
#     Process(target=son2).start()
#     time.sleep(5)
# 守护进程不会守护除了主进程代码之外的其他子进程

# 偏要求守护进程在子进程结束之后才结束
# import time
# def son():
#     while True:
#         time.sleep(1)
#         print('in son')
#
# def son2():
#     print('start son2')
#     time.sleep(10)
#     print('end son2')
#
# if __name__ == '__main__':
#     p = Process(target=son)
#     p.daemon = True
#     p.start()
#     p = Process(target=son2)
#     p.start()
#     time.sleep(5)
#     p.join()

# Process对象的方法
    # start()
    # join()
    # terminate()
    # isalive()

# import time
# def son():
#     while True:
#         time.sleep(1)
#         print('in son')
#
# if __name__ == '__main__':
#     p = Process(target=son)
#     p.start()
#     time.sleep(5)
#     print(p.is_alive())
#     p.terminate()   # 异步非阻塞操作
#     time.sleep(0.1)
#     print(p.is_alive())
#     print('我还可以继续做其他的事情,主进程的代码并不结束')

# 面向对象的方式开启子进程
# class MyProcess(Process):
#     def run(self):
#         print(os.getpid())
#
# if __name__ == '__main__':
#     print('主 :',os.getpid())
#     MyProcess().start()

# 传参数
# class MyProcess(Process):
#     def __init__(self,arg1,arg2):
#         super().__init__()   # 执行父类的init
#         self.a1 = arg1
#         self.a2 = arg2
#     def run(self):
#         print(os.getpid(),self.a1,self.a2)
#
# if __name__ == '__main__':
#     print('主 :',os.getpid())
#     MyProcess(1,2).start()

# 进程之间的数据是否隔离
# n = 0
# def son():
#     global n
#     n += 1
# for i in range(20):
#     son()
# print(n)

n = 0
def son():
    global n
    n += 1

if __name__ == '__main__':
    p_l = []
    for i in range(20):
        p = Process(target=son)
        p.start()
        p_l.append(p)
    for p in p_l:p.join()
    print(n)