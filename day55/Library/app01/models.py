from django.db import models


# Create your models here.


class Publisher(models.Model):
    pid = models.AutoField(primary_key=True)  # pid 主键
    name = models.CharField(max_length=32, unique=True)
    addr = models.CharField(max_length=32, )

    def __str__(self):
        # return "《 {} - {} 》 ".format(self.pid, self.name)
        return "{}".format(self.name)


class Book(models.Model):
    title = models.CharField(max_length=32, unique=True)
    pub = models.ForeignKey('Publisher',on_delete=models.CASCADE)

