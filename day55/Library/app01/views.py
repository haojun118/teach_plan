from django.shortcuts import render, redirect, HttpResponse

from app01 import models


# Create your views here.
def publisher_list(request):
    # 从数据库中获取所有的出版社的信息
    all_publisher = models.Publisher.objects.all()  # 对象列表

    for i in all_publisher:
        print(i)

    # 将数据展示到页面中
    return render(request, 'publisher_list.html', {'k1': all_publisher})


def publisher_add(request):
    #  get请求 返回一个页面
    if request.method == 'GET':
        return render(request, 'publisher_add.html')
    elif request.method == 'POST':
        # 获取用户提交的出版社的信息
        pub_name = request.POST.get('pub_name')
        pub_addr = request.POST.get('pub_addr')
        # 对用户提交的数据进行校验
        if not pub_name:
            # 出版社名称为空
            return render(request, 'publisher_add.html', {'error': '名称不能为空'})
        if models.Publisher.objects.filter(name=pub_name):
            # 出版社名称重复
            return render(request, 'publisher_add.html', {'error': '名称已存在'})

        # 直接插入到数据库中
        ret = models.Publisher.objects.create(name=pub_name, addr=pub_addr)
        # pub_obj = models.Publisher(name=pub_name,addr=pub_addr)  # 内存中的对象 和数据库没关系
        # pub_obj.save()  # 插入到数据库中
        # 跳转到展示页面(重定向)
        return redirect('/publisher_list/')


def publisher_add(request):
    error = ''
    if request.method == 'POST':
        # 获取用户提交的出版社的信息
        pub_name = request.POST.get('pub_name')
        pub_addr = request.POST.get('pub_addr')
        # 对用户提交的数据进行校验
        if not pub_name:
            # 出版社名称为空
            error = '出版社名称不能为空'
        elif models.Publisher.objects.filter(name=pub_name):
            # 出版社名称重复
            error = '名称已存在'
        else:
            # 直接插入到数据库中
            ret = models.Publisher.objects.create(name=pub_name, addr=pub_addr)
            # 跳转到展示页面
            return redirect('/publisher_list/')
    return render(request, 'publisher_add.html', {'error': error})


def publisher_del(request):
    # 获取要删除数据的id
    pid = request.GET.get('id')
    ret = models.Publisher.objects.filter(pid=pid)
    if not ret:
        return HttpResponse('数据不存在')

    # 从数据库删除数据
    ret.delete()  # 对象列表 删除
    # models.Publisher.objects.get(pid=pid).delete()  # 对象 删除

    # 跳转到展示的页面
    return redirect('/publisher_list/')
    # all_publisher= models.Publisher.objects.all()
    # return render(request,'publisher_list.html',{'k1':all_publisher})


def publisher_edit(request):
    error = ''
    # get 请求
    # 获取要编辑对象的ID
    pid = request.GET.get('id')
    pub = models.Publisher.objects.filter(pid=pid)  # 对象列表
    if not pub:
        return HttpResponse('数据不存在')
    # 获取对象
    pub_obj = pub[0]
    if request.method == 'POST':
        # 获取新提交的数据
        pub_name = request.POST.get('pub_name')
        pub_addr = request.POST.get('pub_addr')
        # 对用户提交的数据进行校验
        if not pub_name:
            # 出版社名称为空
            error = '出版社名称不能为空'
        elif pub_obj.name != pub_name and models.Publisher.objects.filter(name=pub_name):
            # 出版社名称重复
            error = '名称已存在'
        elif pub_obj.name == pub_name and pub_obj.addr == pub_addr:
            error = '数据未修改'
        else:
            # 修改数据
            pub_obj.name = pub_name
            pub_obj.addr = pub_addr
            pub_obj.save()  # 将修改提交的数据库
            # 跳转到展示页面
            return redirect('/publisher_list/')

    # 返回一个页面进行展示
    return render(request, 'publisher_edit.html', {'pub_obj': pub_obj, 'error': error})


def book_list(request):
    # 获取所有的数据
    all_books = models.Book.objects.all()  # 对象列表
    # for book in all_books:
    #     print(book)
    #     print(book.title,type(book.title))
    #     print(book.pub,type(book.pub))   # 外键关联的对象
    #     print(book.pub.pid)
    #     print(book.pub_id)

    # 返回页面
    return render(request, 'book_list.html', {'all_books': all_books})


def book_add(request):
    if request.method == 'POST':
        # 获取用户提交的数据
        book_name = request.POST.get('book_name')
        pub_id = request.POST.get('pub_id')
        # 校验数据
        # 校验成功 添加到数据库中
        # models.Book.objects.create(title=book_name,pub=models.Publisher.objects.get(pk=pub_id))
        models.Book.objects.create(title=book_name, pub_id=pub_id)
        # 跳转到展示页面
        return redirect('/book_list/')

    # 获取所有的出版社信息
    all_publisher = models.Publisher.objects.all()

    return render(request, 'book_add.html', {'all_publisher': all_publisher})


def book_del(request):
    # 获取要删除对象的id
    pk = request.GET.get('id')
    # 获取对象进行删除
    models.Book.objects.filter(pk=pk).delete()
    # 跳转到展示页面
    return redirect('/book_list/')


def book_edit(request):
    # 获取要编辑的对象
    pk = request.GET.get('id')
    book_obj = models.Book.objects.filter(pk=pk).first()  # 取对象列表中的第一个  如果是空列表 取不到就是None
    if not book_obj:
        return HttpResponse('所编辑的对象不存在')
    if request.method == 'POST':
        # 用户提交的数据
        book_name = request.POST.get('book_name')
        pub_id = request.POST.get('pub_id')
        # 校验
        book_obj.title = book_name
        book_obj.pub_id = pub_id  # book_obj.pub = pub_obj
        book_obj.save()
        # 跳转到展示页面
        return redirect('/book_list/')

    all_publisher = models.Publisher.objects.all()
    return render(request, 'book_edit.html', {'book_obj': book_obj, 'all_publisher': all_publisher})
