## 内容回顾

### 1.django中所有的命令

1. 下载django

   pip  install  django==1.11.25  -i 源

2. 创建django项目

   django-admin startproject 项目名

3. 启动项目

   切换到项目目录下

   python manage.py runserver   #  127.0.0.1:8000 

   python manage.py runserver  80   #  127.0.0.1:80 

   python manage.py runserver  0.0.0.0::80   #  0.0.0.0::80 

4. 创建app

   python  manage.py  startapp  app名称

5. 数据库迁移的命令

   python manage.py makemigrations   # 检测已经注册app下的models变更记录

   python manage.py  migrate   # 将变更记录同步到数据库中

### 2.django的配置

1. 静态文件

   STATIC_URL = '/static/'    # 静态文件的别名

   STATICFILES_DIRS = [      #    静态文件存放的路径 

   ​		os.path.join(BASE_DIR ,'static')

   ]

2. 数据库

   ENGINE   mysql  

   NAME  数据库名称

   HOST    ip  

   PORT    端口号  

   USER   用户名

   PASSWORD  密码

3. 中间件

   注释掉 一个  csrf     (可以提交POST请求)

4. app

   INSTALLED_APPS = [

   ​		'app名称'

   ​		‘app名称.apps.app名称Config’

   ]

5. 模板  

   TEMPLATES   DIRS =[ 	os.path.join(BASE_DIR ,'templates')]  

### 3.django使用mysql数据库的流程

1. 创建一个mysql数据库

2. 配置数据库连接信息

   ENGINE   mysql  

   NAME  数据库名称

   HOST    ip  

   PORT    端口号  

   USER   用户名

   PASSWORD  密码

3. 告诉django使用pymysql替换Mysqldb

   在与settings同级目录下的init文件中写：

   ```
   import pymysql
   pymysql.install_as_MySQLdb()
   ```

4. 在models中写类（继承models.Model）

   ```python 
   class Publisher(models.MOdel):
       pid = models.AutoField(primry_key=True)
       name = models.CharField(max_length=32,unique=True)
   ```

5. 执行数据库迁移的命令

   python manage.py makemigrations   # 检测已经注册app下的models变更记录

   python manage.py  migrate   # 将变更记录同步到数据库中

### 4.get和post的区别

发送get的方式：

1. 地址栏中输入地址 回车
2. a 标签
3. from表单

?k1=v1&k2=v2      request.GET.get(key)  #  url携带的参数 



发送post请求：

​	form表单  method ='post'

form表单

	1. action=''   向当前地址进行提交  method ='post'
 2. 所有的input需要有name属性  value
 3. 需要有button或者一个type=‘submit’的input框

request.POST.get(key)

### 5.ORM

对象关系映射  

#### 对应关系

​	类     ——》   表

​    对象  ——》  数据行（记录）

​	属性   ——》  字段

#### ORM操作

##### 查询

```python
from app01 import  models
models.Publisher.objects.all()   #  查询所有的数据    QuerySet 【对象】    对象列表
models.Publisher.objects.get(name='xxx')  #  查询有且唯一的对象  没有或者是多个就报错
models.Publisher.objects.filter(name='xxx')   # 查询所有符合条件的对象   QuerySet 【对象】    对象列表

for  i in models.Publisher.objects.all() ：
	print(i)    #  Publisher object   __str__()
    print(i.name)
```

##### 新增

```
models.Publisher.objects.create(name='xxxx',addr='xxxx')

obj = models.Publisher(name='xxxx',addr='xxxx')
obj.save()
```

##### 删除

```
models.Publisher.objects.filter(pid=pid).delete()
models.Publisher.objects.get(pip=pid).delete()
```

##### 编辑

```
pub_obj.name = 'xxxx'
pub_obj.addr = 'xxxx'
pub_obj.save()
```

### 6.模板语法

render（request，‘模板的文件名’，{ key:value }）

```
{{  key }}  ——》 value的值

for循环
{% for i in list  %}
	{{ forloop.counter  }}
	{{ i }}

{% endfor  %}
```

## 今日内容

书籍的管理

### 外键   描述一对多的关系

```python
class Book(models.Model):
    title = models.CharField(max_length=32, unique=True)
    pub = models.ForeignKey('Publisher',on_delete=models.CASCADE)
  
on_delete在2.0版本中是必填的
on_delete=models.CASCADE(级联删除)
		  models.SET()
    	  models.SET_DEFAULT   default=值
		  models.SET_NULL      null=True
          models.DO_NOTHING

```

### 展示

```
    all_books = models.Book.objects.all()  # 对象列表
    for book in all_books:
        print(book)
        print(book.title,type(book.title))
        print(book.pub)     # 外键关联的对象
        print(book.pub_id)  # 关联对象的id  直接从数据库查询
```

### 新增

```
models.Book.objects.create(title=book_name,pub=models.Publisher.objects.get(pk=pub_id))
models.Book.objects.create(title=book_name,pub_id=pub_id)
```

### 模板语法

```
{% if  条件  %}
	满足条件显示的值
{% endif  %}


{% if  条件  %}
	满足条件显示的值
{% else %}
	不满足条件显示的值
{% endif  %}


{% if  条件  %}
	满足条件显示的值
{% elif  条件1  %}
	满足条件显示的值
{% else %}
	不满足条件显示的值
{% endif  %}

```







