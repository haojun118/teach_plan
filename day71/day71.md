1. 注册

2. 客户表的展示

   1. 普通字段

      对象.字段名   ——》  数据库的数据

   2. 有choices参数的字段

      对象.字段 ——》  数据库的数据

      `对象.get_字段名_display（）` ——》  显示的结果

   3. 自定义方法

      ```python
       def show_class(self):
           return ','.join([str(i) for i in self.class_list.all()])
      ```

   from django.utils.safestring import mark_safe

   

3. 分页