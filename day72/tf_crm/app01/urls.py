from django.conf.urls import url
from app01 import views

urlpatterns = [
    url(r'^login/$', views.login, name='login'),
    url(r'^register/$', views.register, name='register'),
    url(r'^customer/$', views.customer, name='customer'),
    url(r'^my_customer/$', views.customer, name='my_customer'),
    # url(r'^customer_add/$', views.customer_add, name='customer_add'),
    # url(r'^customer_edit/(\d+)/$', views.customer_edit, name='customer_edit'),
    url(r'^customer_add/$', views.customer_change, name='customer_add'),
    url(r'^customer_edit/(\d+)/$', views.customer_change, name='customer_edit'),

    url(r'^user_list', views.user_list, name='user_list'),
]
