from django.shortcuts import render, HttpResponse, redirect, reverse
from app01 import models
import hashlib
from app01.forms import RegForm, CustomerForm


# Create your views here.
def login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        md5 = hashlib.md5()
        md5.update(password.encode('utf-8'))
        password = md5.hexdigest()
        user_obj = models.UserProfile.objects.filter(username=username, password=password, is_active=True).first()
        if user_obj:
            request.session['pk'] = user_obj.pk
            request.session['is_login'] = True
            return redirect(reverse('customer'))
        return render(request, 'login.html', {'error': '用户名或密码错误'})

    return render(request, 'login.html')


def register(request):
    form_obj = RegForm()
    if request.method == 'POST':
        form_obj = RegForm(request.POST)
        if form_obj.is_valid():
            # 插入数据库
            # print(form_obj.cleaned_data)
            # models.UserProfile.objects.create(**form_obj.cleaned_data)
            form_obj.save()
            return redirect(reverse('login'))

    return render(request, 'register.html', {'form_obj': form_obj})


def customer(request):
    if request.path_info == reverse('customer'):
        all_customer = models.Customer.objects.filter(consultant__isnull=True)
    else:
        all_customer = models.Customer.objects.filter(consultant=request.user_obj)

    return render(request, 'customer.html', {'all_customer': all_customer})


users = [{'name': 'alex-{}'.format(i), 'pwd': 'alexdsb{}'.format(i)} for i in range(1, 355)]

from utils.pagination import Pagination


def customer_add(request):
    form_obj = CustomerForm()
    if request.method == 'POST':
        form_obj = CustomerForm(request.POST)
        if form_obj.is_valid():
            form_obj.save()  # 新增
            return redirect(reverse('customer'))

    return render(request, 'customer_add.html', {'form_obj': form_obj})


def customer_edit(request, pk):
    obj = models.Customer.objects.filter(pk=pk).first()
    form_obj = CustomerForm(instance=obj)  # 包含原始数据
    if request.method == 'POST':
        form_obj = CustomerForm(request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()  # 修改
            return redirect(reverse('customer'))

    return render(request, 'customer_edit.html', {'form_obj': form_obj})


def customer_change(request, pk=None):
    obj = models.Customer.objects.filter(pk=pk).first()
    form_obj = CustomerForm(instance=obj)  # 包含原始数据
    if request.method == 'POST':
        form_obj = CustomerForm(request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            return redirect(reverse('customer'))
    title = '编辑客户' if pk else '新增客户'
    return render(request, 'customer_change.html', {'form_obj': form_obj, 'title': title})


def user_list(request):
    page_obj = Pagination(request.GET.get('page', 1), len(users))

    return render(request, 'user_list.html',
                  {'users': users[page_obj.start:page_obj.end], 'page_html': page_obj.page_html})
