1.modelform

```python
from django import forms
from app01 import models
class RegForm(forms.ModelForm):
     password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': '密码'}))
	 re_password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': '确认密码'}))
    
    class Meta:
        model = models.User
        fileds = '__all__'  # ['name','']
        exclude = ['is_active']
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
    def clean(self):
        self._validate_unique = True  # 在数据库校验唯一性
        return self.cleaned_data
    
 使用：
form_obj = RegForm()

{{ form_obj.username }}   _>  input 
{{ form_obj.username.errors }}   _> 所有的错误
{{ form_obj.username.label }}   _>  提示 
{{ form_obj.username.id_for_label }}   _>  input的id
{{ form_obj.errors }}   _>  所有的错误


form_obj = RegForm(request.POST)

form_obj.is_valid()
form_obj.save() # 保存数据

```

2.展示数据

```python
[对象，对象]
1. 普通字段
	对象.字段名  ——》  数据库的值
2. 有choices参数的字段
	（（1,'男’），）
	对象.字段名  ——》  数据库的值  1 
	对象.get_字段名_display()  ——》  显示的内容 
	
3. 自定义方法
	def show_class(self):
		return  ','.join([  str(i) for i in self.class_list.all()])
	
    from django.utils.safestring import mark_safe
    
```

3.分页



今天的需求：

1. 新增客户
2. 编辑客户
3. 公户 私户 

