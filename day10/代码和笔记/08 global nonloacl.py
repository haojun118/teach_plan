# global : 只修改全局
# nonlocal : 只修改局部,修改离nonlocal最近的一层,上一层没有继续向上上层查找.只限在局部

# a = 10
# def func():
#     global a
#     a = a - 6
#     print(a)
#
# print(a)
# func()
# print(a)

# a = 100
# def func():
#     b = 10
#     def foo():
#         b = a
#         def f1():
#             nonlocal b
#             b = b + 5
#             print(b)  # 105
#         f1()
#         print(b)  # 105
#     foo()
#     print(b) # 10
# func()
# print(a) # 100