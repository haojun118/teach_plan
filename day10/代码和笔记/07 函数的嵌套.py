# 1.不管在什么位置,只要是函数名()就是在调用一个函数

# 混合嵌套:
# def f1():
#     print(11)
#
# def f2():
#     print(22)
#     f1()
#
# def f3():
#     print(33)
#     f1()
#
# def run():
#     f3()
#     f2()
#     f1()
# run()

# def func(a):
#     print(a)
#     return f1(foo(a))
#
# def foo(b):
#     print(b)
#     return b + 5
#
# def f1(args):
#     return args + 10
#
# print(func(5))

# def foo(a):
#     a = 10
#     def f1(b):
#         c = b
#         def foo(c):
#             print(c)
#             print(foo.__doc__)
#         foo(c)
#         print(b)
#     f1(a)
#     print(a)
# foo(25)


# def foo():
#     a = 10
#     func(a)
#
# def func(a):
#     print(a)
#
# foo()