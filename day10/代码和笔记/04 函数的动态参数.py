# def func(a,b,c):
#     print(a,b,c)
#
# func("小米","华为","苹果")

# def eat(a,b,c,d,e,f):
#     print(a,b,c,d,e,f)
#
# eat("麻辣烫","大烧饼","大煎饼","大腰子","韭菜","羊蝎子")

# 动态位置参数
# def eat(*args):  # 函数的定义阶段 *聚合(打包)
#     print(args) # tuple
#     print(*args) # 函数体中的 *    打散(解包)

    # a,b,*c = args
    # print(a,b,c)
# eat("麻辣烫","大烧饼","大煎饼","大腰子","韭菜","羊蝎子")
# eat("麻辣烫","大烧饼")

# eat("麻辣烫","大烧饼","大煎饼","大腰子","韭菜","羊蝎子","黄焖鸡","大盘鸡")


# def eat(a,b,*c):
#     print(a)
#     print(b)
#     print(c)     # tuple
#
# eat("面条","米饭","馒头","大饼")


# def eat(a,b,*c):  #位置参数 > 动态位置参数
#     print(a)
#     print(b)
#     print(c)     # tuple
#
# eat("面条","米饭","馒头","大饼")

# def eat(a,b,d=2,*c):  #位置参数 > 动态位置参数
#     print(a)
#     print(b)
#     print(c)     # tuple
#     print(d)
#
# eat("面条","米饭","馒头","大饼")


# def eat(a,b,*c,d=2):  #位置参数 > 动态位置参数 > 默认参数
#     print(a)
#     print(b)
#     print(c)     # tuple
#     print(d)
#
# eat("面条","米饭","馒头","大饼","鞍山市大",d="花卷")


# def eat(*args,d=2):  # 位置参数 > 动态位置参数 > 默认参数
#     print(*args)     # tuple
#     print(d)
# eat("面条","米饭")


# def eat(a,b,*args,d=2,**c):  # 位置参数 > 动态位置参数 > 默认参数 > 动态默认参数
#     print(a)
#     print(b)
#     print(d)
#     print(args)     # tuple
#     print(c)        # dict

# eat("面条","米饭","大烧饼","大煎饼",a1=1,b1=2)        # 位置 > 关键字


# def eat(*args,**kwargs):  # (万能传参)
#     print(args) # tulpe
#     print(kwargs) #

# lst = [1,23,4,6,7]
# dic = {"key1":1,"key2":3}

# 应用场景
# eat(lst,dic)
# eat(*lst,**dic) # eat(1,23,4,6,7,"key1"=1,"key2"=2)

# json = {
#
#     "101":{1:{"日魔":"对象"},
#          2:{"隔壁老王":"王炸"},
#          3:{"乔碧萝":("日魔","炮手","宝元")},
#          },
#     "102":{1:{"汪峰":{"国际章":["小苹果","大鸭梨"]}},
#          2:{"邓紫棋":["泡沫","信仰","天堂","光年之外"]},
#          3:{"腾格尔":["隐形的翅膀","卡路里","日不落"]}
#          },
#     "103":{1:{"蔡徐坤":{"唱":["鸡你太美"],
#                    "跳":["钢管舞"],
#                    "rap":["大碗面"],
#                    "篮球":("NBA形象大使")}},
#
#          2:{"JJ":{"行走的CD":["江南","曹操","背对背拥抱","小酒窝","不潮不花钱"]}},
#          3:{"Jay":{"周董":["菊花台","双节棍","霍元甲"]}}},
#
#     "201":{
#         1:{"韦小宝":{"双儿":"刺客","建宁":{"公主":{"吴三桂":"熊"}},"龙儿":{"教主老婆":"教主"}}}
#     }
# }

# 数据库 -- **

# def func(*args,**kwargs):
    # print(kwargs)
# func("key"=1,"ke1"=1)


# print(a1=1,b1=2)a1=1,b1=2


# 总结:

    # *args(聚合位置参数)      大家伙都用的名字, 可以进行修改但是不建议修改
    # **kwargs(聚合关键字参数) 大家伙都用的名字, 可以进行修改但是不建议修改

    # 函数的定义阶段 * 和 ** 都是聚合
    # 函数体中 * 就是打散, *args将元组中的元组进行打散  *kwargs将字典的键获取

    # 形参:
    #     位置参数:
    #     动态位置参数: 先执行位置参数,位置参数接受后额外的参数动态位置参数进行接受 获取的是一个元组
    #     默认参数:
    #     动态关键字参数(默认): 先执行默认参数,默认参数接受后额外的默认参数动态默认参数进行接受,获取的是一个字典

    # 实参和函数体:
    #     * 打散
    #     ** 实参时能够使用

    