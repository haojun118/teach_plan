"""
7.写函数，检查传入字典的每一个value的长度,如果大于2，那么仅保留前两个长度的内容，并将新内容返回给调用者。
dic = {"k1": "v1v1", "k2": [11,22,33,44]}
PS:字典中的value只能是字符串或列表
"""
# dic = {"k1": "v1v1", "k2": [11,22,33,44]}
# def func(dic):
#     new_dic = {}
#     for k,v in dic.items():
#         if len(v) > 2:
#             v = v[:2]
#         new_dic[k] = v
#     return new_dic
# print(func(dic))

"""
8.写函数，此函数只接收一个参数且此参数必须是列表数据类型，此函数完成的功能是返回给调用者一个字典，
此字典的键值对为此列表的索引及对应的元素。例如传入的列表为：[11,22,33] 返回的字典为 {0:11,1:22,2:33}。
"""
# def func(lst:list):
#     dic = {}
#     for i in range(len(lst)):
#         dic[i] = lst[i]
#     return dic
# print(func([11,22,33]))

# def func(lst:list):
#     dic = {}
#     for k,v in enumerate(lst):  # 枚举 默认是从0开始数  第一个参数:可迭代对象 第二个参数:数数的起始值
#         dic[k] = v
#     return dic
# print(func([11,22,33]))

"""
9.写函数，函数接收四个参数分别是：姓名，性别，年龄，学历。用户通过输入这四个内容，
然后将这四个内容传入到函数中，此函数接收到这四个内容，将内容追加到一个student_msg文件中。
"""
# def func(name,sex,age,edu):
#     with open("student_msg","a",encoding="utf-8")as f:
#         f.write(f"{name} {sex} {age} {edu}\n")
# func(input("name"),input("sex"),input("age"),input("edu"))


# name = input("name")
# sex = input("sex")
# age = input("age")
# edu = input("edu")
# func(name,sex,age,edu)

"""
10.对第9题升级：支持用户持续输入，Q或者q退出，性别默认为男，如果遇到女学生，则把性别输入女。
"""
# def func(name,age,edu,sex="男"):
#     with open("student_msg","a",encoding="utf-8")as f:
#         f.write(f"{name} {sex} {age} {edu}\n")
#
# while True:
#     name = input("name")
#     sex = input("sex")
#     age = input("age")
#     edu = input("edu")
#     if name.upper() == "Q" or sex.upper() == "Q" or age.upper() == "Q" or edu.upper() == "Q":
#         break
#     elif sex == "女":
#         func(name,age,edu,sex)
#     else:
#         func(name, age, edu)

"""
11. 写函数，用户传入修改的文件名，与要修改的内容，执行函数，完成整个文件的批量修改操作（选做题）。
"""

# def func(file_name,old,new):
#     with open(file_name,'r',encoding="utf-8")as f, \
#         open("a.txt",'w',encoding='utf-8')as f1:
#         for i in f:
#             i = i.replace(old,new)
#             f1.write(i)
#     import os
#     os.rename(file_name,"b.txt")
#     os.rename("a.txt",file_name)
# func("student_msg","q","QQ")

