# 整数
# create table t1(i1 tinyint,i2 int);
# 默认创建的所有数据都是有符号的
# create table t2(i1 tinyint unsigned,i2 int unsigned);
# unsigned表示无符号

# 小数
# create table t3(f1 float,f2 double)
# create table t4(f1 float(7,2))

# 字符串
# char(25)
    # 'abc            ' 浪费空间、节省时间
# varchar(25)
    # '3abc'            节省空间、浪费时间
# create table t5(c1 char(5),c2 varchar(5));
