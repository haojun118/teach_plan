# 1.dbms 数据库管理系统
#   dba 数据库管理员
#   db 数据库/文件夹
#   table 表/文件
#   data 表中具体数据
#
# 2.关系型 mysql oracle sqlserver sqllite
#   非关系型 redis mongodb hbase

# mysqld install 安装sql服务
# net start mysql 启动服务
# net stop mysql 关闭服务

# mysql -uroot -h192.168.12.45 -p
# Enter password：
# mysql>

# 你启动的mysql.exe是客户端 S 你安装好的服务是server
# 在客户端需要指定 要登录的数据库所在的ip 以及用户名和密码
# mysql -uroot -p123 -h192.168.12.45
# mysql> set password = password('123');  设置密码
    # 需要注意要在sql的结尾输入;表示整个sql语句的结束
    # 如果忘记了可以在换行之后补上。
# \c表示放弃当前行要执行的sql语句
# exit 表示退出客户端

# 用户
# create user '用户名'@'允许的网段' identified by '密码'
# grant usage on 数据库.表 to 'eva'@'%';
# grant select on 数据库.* to 'eva'@'%';
# grant update on *.* to 'eva'@'%';
# grant insert on *.* to 'eva'@'%';
# grant delete on *.* to 'eva'@'%';
# grant all on ftp.* to 'eva'@'%';

# show databases；  查看当前的所有库
# create database ftp；

# mysql -ueva -h192.168.12.22 -p123
# mysql> select user();













