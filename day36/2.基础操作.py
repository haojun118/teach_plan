# 数据库（文件夹）的操作
# create database   数据库名;
# show databases;   查看当前所有的库
# use 数据库名       切换到对应的库中/实际上就是进入对应的文件夹
# select database(); 查看当前所在的库

# 表（文件）的操作
# create table demo(num int,username char(12),password char(32));
# show tables;      查看有哪些表
# desc demo;        查看表结构
# describe demo;

# 数据的操作
# insert into demo values(2,'alex','sb123')写数据
# select * from demo;
# update demo set password='alex3714' where num=1;
# delete from demo where num=1;