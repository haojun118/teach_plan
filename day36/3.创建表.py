# 数据类型
    # 数字类型
        # 整数 tinyint int
        # 小数 float double
    # 字符串
        # 定长、节省时间、浪费空间 char(255)
        # 变长、节省空间、浪费时间 varchar(65535)
    # 时间类型
        # now()函数表示当前时间
        # datetime 年月日时分秒   0000-9999
        # date     年月日
        # time     时分秒
        # year     年
        # timestamp 年月日时分秒  1970-2038
    # enum和set
        # enum 单选
        # set  多选


