# 函数名的第一类对象
# 1.当做值,赋值给变量

# def func():
#     print(1)
#
# print(func)  # 函数的内存地址
# a = func
# print(a)
# a()

# 2.可以当做容器中的元素

# lst = []
# dic = {}

# def func():
#     print(1)
#
# def foo():
#     print(2)

# lst.append(func)
# lst.append(foo)
# print(lst)

# lst = [func,foo]
# print(lst)
# for i in lst:
#     i()

# def login():
#     print("这是登录")
#
# def register():
#     print("这是注册")
#
# def index():
#     print("这是博客园的主页")

# msg = """
# 1 登录
# 2 注册
# 3 主页
# """
# choose = input(msg)   # 1
# if choose.isdecimal():
#     if choose == "1":
#         login()
#     elif choose == "2":
#         register()
#     elif choose == "3":
#         index()
#     elif choose == "3":
#         index()
#     elif choose == "3":
#         index()
#     elif choose == "3":
#         index()
#     elif choose == "3":
#         index()
#     elif choose == "3":
#         index()


# dic = {"1":login,"2":register,"3":index}
#
# msg = """
# 1 登录
# 2 注册
# 3 主页
# """
# choose = input(msg)   # 1
# if choose.isdecimal():
#     if dic.get(choose):
#         dic[choose]()
#     else:
#         print("请正确输入!")


# 3.函数名可以当做函数的参数

# def func(a):
#     print(111)
#     a()
#
# def foo():
#     print(222)
#
# func(foo)

# 4.函数名可以当函数的返回值

# def func():
#     def foo():
#         print(111)
#     return foo
#
# a = func()
# a()
# func()()  # foo()


# 函数名的第一类对象的使用方式如下:
# 1.可以当做值,赋值给变量
# 2.可以当做容器中的元素
# 3.可以当做函数的参数
# 4.可以当做函数的返回值
# str,int,list,tuple,dict

# def foo(a):
#     def func(a):
#         def f1(a):
#             print(a)
#             return "aelx"
#         return f1(a)
#     return func(a)
# print(foo(5))


# def func(a):
#     a()
#
# def foo(b):
#     return b()

# def f1(c):
#     def a():
#         def f3():
#             print(3333)
#             return [f3,a,f1]
#         print(11)
#         return f3()
#     return c(a())
#
# def aa(b):
#     print(111)
#     return b
# print(f1(aa))


def f1(c):
    def a():
        def f3():
            print(3333)
            return [f3,a,f1]
        print(11)
        return f3()
    ret = a()
    return c(ret)

def aa(b):
    print(111)
    return b
print(f1(aa))