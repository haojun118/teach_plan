"""8.写函数,接收一个参数(此参数类型必须是可迭代对象),将可迭代对象的每个元素以’_’相连接,
形成新的字符串,并返回.
例如 传入的可迭代对象为[1,'老男孩','宝元']返回的结果为’1_老男孩_宝元’"""

# def func(*args):
#     s = ""
#     for i in args:
#         s = s + str(i) + "_"
#     print(s)
#
# func({"key1":1})

# def func(args):
#     s = ""
#     if type(args) == dict:
#         for i in args.values():
#             s = s + str(i) + "_"
#         print(s.strip("_"))
#     else:
#         for em in args:
#             s = s + str(em) + "_"
#         print(s.strip("_"))
# func({"key1":1,"key2":2,"key3":"alex"})
# func([1,2,3,4,5])

