# f""  f-strings

# name =
# age =

# msg = "姓名:xx 年龄:xx"
# msg = "姓名:%s 年龄:%s"%(name,age)

# f"姓名:{name} 年龄:{age}"
# f"姓名:{'alex'}"
# f"姓名:{34}"

# print(F"姓名:{input('name:')} 年龄:{input('age:')}")

# def foo():
#     print("is foo")

# lst = [1,2,3,4]
# dic = {"key1":23,"key2":56}
# print(f"""{dic['key1']}""")

# print(f"{3+5}")
# print(f"{3 if 3>2 else 2}")

# print(f"""{':'}""")

# msg = f"""{{{{'alex'}}}}"""
# print(msg)

# name = "alex"
# print(f"{name.upper()}")

# print(f"{':'}")