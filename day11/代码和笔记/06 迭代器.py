# 可迭代对象

# 可迭代对象
# list,tuple,str,set,dict 取值方式只能直接看
# 只要具有__iter__() 方法就是一个可迭代对象


# lst = [1,23,4,5]
# for i in lst:
#     print(i)

# lst = [1,2,3,4]
# lst.__iter__()
# dict.__iter__()

# 迭代器:工具
# 具有__iter__()和__next__()两个方法才是迭代器

# lst = [1,2,3,4,5]
# l = lst.__iter__()  # 将可迭代对象转换成迭代器

# l.__iter__()  # 迭代器指定__iter__()还是原来的迭代器
# print(l.__next__())   # 1
# print(l.__next__())   # 2

# for i in lst:
#     print(i)



# for循环的本质   *****
# while True:
#     try:
#         print(l.__next__())
#     except StopIteration:
#         break

# lst = [1,2,3,4,5]
# l = iter(lst)
# print(next(l))
# print(next(l))
# print(next(l))
# print(next(l))
# print(next(l))



# lst = [1,2,3,4,5]
# print(next(iter(lst)))               # 买了个水杯  喝了一口水
# print(next(iter(lst)))
# print(next(iter(lst)))
# print(next(iter(lst)))
# print(next(iter(lst)))


# iter() 和 __iter__() 是一样的 推荐使用iter()

# pyhton3:
    # iter()和 __iter__() 都有
    # next()和__next__()都有

# python2:
    # iter()
    # next()

# lst = []
# for i in range(10000):
#     lst.append(i)
#
# l = lst.__iter__()
#
# for i in range(16):
#     print(l.__next__())
#
# print(''.center(50,"*"))
#
# for i in range(16):
#     print(l.__next__())
#
# print(''.center(50,"*"))
# for i in range(16):
#     print(l.__next__())



# l = lst.__iter__()
# print(l)


# 迭代器的优点:
#     1.惰性机制 -- 节省空间

# 迭代器的缺点：
#     1.不能直接查看值  迭代器查看到的是一个迭代器的内存地址
#     2.一次性，用完就没有了
#     3.不能逆行（后退）


# 空间换时间： 容器存储大量的元素，取值时 取值时间短，但是容器占用空间较大
# 时间换空间： 迭代器就是节省了空间，但是取值时间较长

# 迭代器是基于上一次停留的位置，进行取值
# 可迭代对象：具有iter()方法就是一个可迭代对象
# 迭代器：具有iter()和next()方法就是一个迭代器


# for循环的本质   *****
# while True:
#     try:
#         print(l.__next__())
#     except StopIteration:
#         break

# python3和python2的区别:

# pyhton3:
    # iter()和 __iter__() 都有
    # next()和__next__()都有

# python2:
    # iter()
    # next()