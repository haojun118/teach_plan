# 1.动态参数
    # 在函数的定义阶段 * and ** 都是聚合
    # 在函数体中 * 就是打散, kwargs获取的是字典的键
    # 万能传参 *args,**kwargs
    # args 获取的是一个元组
    # kwargs 获取是一个字典
    # args和kwargs可以改变,但是不建议修改

    # 实参中: * 和 **都是打散 字典的键不能使用数字

    # 1.*args 聚合位置参数
    # 2.**kwargs 聚合关键字参数
    # 位置参数 > 动态位置参数 > 默认参数 > 动态默认参数

# 2.函数的注释
#     定义函数的时候要声明函数参数的数据类型
#     注释是 """"""
#     函数名.__doc__  查看函数的注释
#     函数名.__name__ 查看函数的名字

# 3. 名称空间:
#     1.内置空间: python解释器自带的空间
#     2.全局空间: 在py文件中顶格写的内容
#     3.局部空间: 函数体中的空间

    # 1.加载顺序:
    # 内置 > 全局 > 局部

    #2.取值顺序:
    # 局部 >  全局 > 内置

    # 作用域:
    # 作用域的作用就是保护数据的安全性
    # 1.全局作用域: 全局 + 内置
    # 2.局部作用域: 局部

# 4.函数的嵌套
#     不管在什么地方只要是函数名()就是在调用函数
#     函数和函数之间空间是独立的

# 枚举:

# 5.global nonlocal
#     global: 只修给全局变量,全局没有时会创建
#     nonlocal: 只修改局部变量,修改离nonlocal最近的一层,如果此层没有就向上一层查找,只限在局部,nonlocal没找到时报错