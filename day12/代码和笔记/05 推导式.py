# 推导式

# lst = []
# for i in range(10):
#     lst.append(i)
# print(lst)

# 列表推导式
    # 普通循环
    # print([i for i in range(10)])
    # print([变量 for循环])

    # 筛选

    # lst = []
    # for i in range(10):
    #     if i % 2 == 0:
    #         lst.append(i)
    # print(lst)

    # 筛选模式
    # print([i for i in range(10) if i % 2 ==0])
    # print([i for i in range(10) if i > 2])
    # [加工后的变量 for循环 加工条件]

# 集合推导式:
    # 普通循环
    # print({i for i in range(10)})
    # {变量 for循环}
    # 筛选模式
    # print({i for i in range(10) if i % 2 == 1})
    # {加工后的变量 for循环 加工条件}

# 字典推导式:
    # 普通循环
    # print({i: i+1 for i in range(10)})
    # {键:值 for循环}
    # 筛选模式
    # print({i: i+1 for i in range(10) if i % 2 == 0})
    # {加工后的键:值 for循环 加工条件}

# 生成器推导式:
    # 普通模式
    # tu = (i for i in range(10))
#  （ 变量 for循环）
    # 筛选模式
    # tu = (i for i in range(10) if i > 5)
#   (加工后的变量 for循环 加工条件)
    # for i in tu:
    #     print(i)