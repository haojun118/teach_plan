# def func(arg):
#     return arg.replace('苍老师', '***')
#
# def run():
#     msg = "Alex的女朋友苍老师和大家都是好朋友"
#     result = func(msg)
#     print(result)
#
# run()
# data = run()
# print(data)


# data_list = []
# s = "alex"
# def func(arg):
#     data_list.append(10)
#     print(s.upper())
#
# data = func('绕不死你')
# print(data)
# print(data_list)


# item = '老男孩'
# def func():
#     item = 'alex'
#     def inner():
#         print(item)
#     for inner in range(10):
#         pass
#     inner()
# func()


# name = '宝元'
# def func():
#     name = 'alex'
#     print(name)
# func()

# def func():
#     print(name)
# func()

# def func():
#     if 3:
#         pass

# 词法分析:
# 语法分析:  if else
# 语义分析:

# def extendList(val,list=[]):  #函数的默认参数陷阱
#     list.append(val)
#     return list
#
# list1 = extendList(10)
# list2 = extendList(123,[])
# list3 = extendList('a')
#
# print('list1=%s'%list1) [10,"a"]
# print('list2=%s'%list2) [123]
# print('list3=%s'%list3) [10,"a"]

# def extendList(val,list=[]):
#     list.append(val)
#     return list
# print('list1=%s'% extendList(10)) [10]
# print('list2=%s'% extendList(123,[])) [123]
# print('list3=%s'% extendList('a')) [123,"a"]

# def extendList(val,list=[]):
#     list.append(val)
#     return list
# print('list1=%s'% extendList(10))
# lst = []
# print('list2=%s'% extendList(123,lst))
# print('list3=%s'% extendList('a'))
# print('list2=%s'% extendList(123,lst))

