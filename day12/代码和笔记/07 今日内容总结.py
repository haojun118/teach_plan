# 1.生成器

    # 生成器的本质就是一个迭代器
    # 生成器和迭代器的区别:
    # 1.迭代器python自带的
    # 2.生成器程序员自己写的
    # 生成器和迭代器通过send和内存地址进行区分

    # 可迭代对象:
    #   优点: list,tuple,str 节省时间,取值方便,使用灵活(具有自己私有方法)
    #   缺点: 大量消耗内存

    # 迭代器:
    #   优点:节省空间
    #   缺点:不能直接查看值,使用不灵活,消耗时间,一次性,不可逆行

    # 生成器:
    #   优点:节省空间,人为定义
    #   缺点:不能直接查看值,消耗时间,一次性,不可逆行

    # 生成器的应用场景:当数据量较大时,推荐使用生成器

    # yield 和 return 的区别
    # 相同点:
    #   1.都是返回内容
    #   2.都可以返回对个,但是return写多个只会执行一个

    # 不同点:
    #     1.return 终止函数 yield是暂停生成器
    #     2.yield能够记录当前执行位置
    # 一个yield 对应一个 next


    # yield from 和 yield 的区别
    # yield from 将可迭代对象中的元素逐个返回
    # yield 一次性返回

    # for i in 迭代器:

# 2. 推导式

    # 普通循环 及 筛选
    # 列表
    # 字典
    # 集合
    # 生成器


# 3.内置函数一

"""
all() any() bytes() callable() chr() complex() divmod() eval()
exec()  frozenset() globals() hash() help() id() input()
int() iter() locals() next() oct() ord() pow() repr() round()
"""