# 1.函数名的使用
# 1.1 函数名可以当做值 赋值给变量,将函数的内存地址赋值给变量名
# 1.2 函数名可以当做容器中的元素
# 1.3 函数名可以当做函数的返回值
# 1.4 函数名可以当做函数的参数

# 2.格式化 f-strings
    # f"{变量名}"
    # f"{列表}"
    # F"{列表}"
    # F"{字典}"
    # f"{字符串.upper()}"
    # f"{'alex'}"
    # f"{3 if 3> 2 else 2}"
    # f"{{}}" 转义成一个大括号


    # 不能放:, 转成字符串存放


# 3.迭代器
#     可迭代对象: 具有iter()方法
#     迭代器: 具有iter()和next()方法
#     __iter__() 和 __next__()
#     iter(可迭代对象)  转换成迭代器


# 迭代器取值时不能取超,不然后报错
# lst = [1,2,3,4,5]
# print(next(iter(lst)))
# print(next(iter(lst)))
# print(next(iter(lst)))
# print(next(iter(lst)))


# l = iter(lst)
# print(next(l))
# print(next(l))
# print(next(l))
# print(next(l))
# 迭代器取值的时候是一个一个取

# 迭代器的优点:
#     1.因为惰性机制导致节省空间

# 迭代器的缺点:
#     1.不能后退
#     2.一次性
#     3.不能直接查看元素,直接查看获取的是一个迭代器的内存地址

# 迭代器在执行__iter__() 方法还是一个迭代器
# 迭代器是基于上一层执行的位置,继续执行的

# for循环的本质:
# while True:
#     try:
#         print(next(迭代器))
#     except StopIteration:
#         break

# 时间换空间:迭代器
# 空间换时间:容器

# python2和python3的区别:
    # python3可以使用__iter__() __next__() iter() next()
    # python2可以使用__iter__() iter() next()
    # 推荐使用iter()和next()
