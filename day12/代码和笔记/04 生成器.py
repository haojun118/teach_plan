# 生成器
# 什么是生成器? 核心: 生成器的本质就是一个迭代器
# 1,迭代器python自带的
# 2,生成器程序员自己写的一种迭代器

# 生成器编写方式:
# 1.基于函数编写
# 2.推导式方式编写

# def func():
#     print("这是一个函数")
#     return "函数"
# func()

# def func():
#     print("这是一个生成器")
#     yield "生成器"

# 坑
# func() # 生成一个生成器
# print(func().__next__())  # 启动生成器
# print(func().__next__())


# 函数体中出现yield代表要声明一个生成器
# generator -- 生成器

# 获取到的是一个生成器的内存地址
# <generator object func at 0x00000087C2A10CA8>

# lst = [1,2,3,4,5]

# def func():
#     msg = input("请输入内容")
#     yield msg
#     print("这是第二次启动")
#     yield "生成器2"
#     yield "生成器3"
#     yield "生成器4"
#
# g = func()
# print(next(g))
# print(next(g))
# print(next(g))
# print(next(g))


# yield 和 return 的区别
# 相同点:
#   1.都是返回内容
#   2.都可以返回对个,但是return写多个只会执行一个

# 不同点:
#     1.return 终止函数 yield是暂停生成器
#     2.yield能够记录当前执行位置

# 一个yield 对应一个 next


# def func():
#     lst = []
#     for i in range(100000):
#         lst.append(i)
#     return lst
#
# for i in range(100):
#     print(func()[i])

# def func():
#     for i in range(100000):
#         yield i
# g = func()
#
# for i in range(100):
#     print(g.__next__())



# 生成器的作用是节省空间

# 可迭代对象:
#   优点: list,tuple,str 节省时间,取值方便,使用灵活(具有自己私有方法)
#   缺点: 大量消耗内存

# 迭代器:
#   优点:节省空间
#   缺点:不能直接查看值,使用不灵活,消耗时间,一次性,不可逆行

# 生成器:
#   优点:节省空间,人为定义
#   缺点:不能直接查看值,消耗时间,一次性,不可逆行

# 使用场景:
# 1.当文件或容器中数据量较大时,建议使用生成器

# 数据类型 (pyhton3: range() | python2 :xrange()) 都是可迭代对象 __iter__()
# 文件句柄是迭代器  __iter__() __next__()

# with open("a.txt","w",encoding="utf-8")as f

# lst = [1,2,3,4]
# l = lst.__iter__()
# print(l)

# 区别什么是迭代器,什么是生成器
    #迭代器的地址 <list_iterator object at 0x000000987B6E97F0>
    #生成器的地址 <generator object func at 0x00000087C2A10CA8>

# send() 不讲

# lst = [1,2,3,4]
# l = lst.__iter__()
# print(l.send())

# def func():
#     a = yield 1
#     print(a)
#     b = yield 2
#     print(b)
#
# g = func()
# print(g.__next__())   # send  -- 发送
# print(g.send("alex"))   # send  -- 发送
# print(g.send("宝元"))   # send  -- 发送

# 没有send方法就是一个迭代器,具有send方法的就是一个生成器

# def func():
#     def foo():
#         print(11)
#     lst = {"key":1,"key1":2}
#     yield foo
#
# print(func().__next__())

# def func():
#     lst = [1,2,3,45,6]
#     lst1 = ["alex","wusir","taibi","baoyuan"]
#     yield from lst
#     yield from lst1
#
# g = func()

# for i in g:
#     print(i)

# lst = [1,2,3,45,6]
# lst1 = ["alex","wusir","taibi","baoyuan"]

# for i in lst,lst1:
#     print(i)

# yield 将可迭代对象一次性返回
# yield from 将可迭代对象逐个返回