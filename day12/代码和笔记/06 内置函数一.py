# a = "88 + 99"
# a = """
# def func():
#     print(111)
# func()
# """
# print(type(a))
# print(eval(a))  # 神器一
# exec(a)         # 神器二
# 注意:千万记住 禁止使用
# exec(input("请输入内容:"))


# print(hash("123"))
# print(hash(12))
# print(hash(-1))
# print(hash(-10))
# print(hash((2,1)))

# dic = {[1,2,3]:2}
# print(hash([1,2,3]))

# hash() 作用就是区分可变数据类型和不可变数据类型
# lst = [1,2,3]

# help(list)  查看帮助信息

# def func():
#     print(1)
# lst = [1,23,4,]
# print(callable(lst))   # 查看对象是否可调用

# print(int("010101",16))
# print(float(3))
# print(int(3))

# print(complex(20))  # 复数

# print(bin(100))  # 十进制转二进制
# print(oct(10))   # 十进制转八进制
# print(hex(17))   # 十进制转十六进制

# print(divmod(5,2))  # (商,余)

# print(round(3.534232,2))   # 保留小数位

# print(pow(2,2))  #幂 pow 两个参数是求幂
# print(pow(2,2,3))  #幂 pow 两个参数是求幂后的余


# s = "你好"
# s1 = bytes(s,encoding="utf-8")  # 将字符串进行编码
# print(str(s1,encoding="utf-8"))

# print(s.encode("utf-8"))

# print(ord("你"))   # 通过元素获取当前(unicode)表位的序号
# print(chr(20320))      # 通过表位序号查找元素

# a = 'alex'
# print(repr(a))   #查看你数据的原生态  -- 给程序员使用的
# print(a)         # 给用户使用的

# lst = [1,2,0,4,5]
# print(all(lst))     # 判断容器中的元素是否都位真  and

# lst = [1,2,3,0,1,23]  # 判断容器中的元素是否有一个为真
# print(any(lst))

# a = 10
# def func():
#     a = 1
#     print(locals())     #  查看当前空间变量
#     print(1)

# func()
# print(globals())        #  查看全局空间变量

