# 什么是闭包?
# 1.在嵌套函数内,使用(非本层变量)和非全局变量就是闭包

# def func():
#     a = 1
#     def foo():
#         print(a)
#     print(foo.__closure__)  # 判断是不是闭包
# func()

# 10w
# lst = []
# def h6(money):
#     lst.append(money)
#     if len(lst) == 7:
#         print("第一周平均价格:",sum(lst) / 7)
#
# h6(200000)
# h6(100000)
# h6(1400000)
# h6(100000)
# lst[0] = 10
# h6(1100000)
# h6(2500000)
# h6(2500000)

# 1128571.4285714286
# 1100001.4285714286



# def h6(money):
#     lst = []
#     lst.append(money)
#     if len(lst) == 7:
#         print("第一周平均价格:",sum(lst) / 7)
#
# h6(200000)
# h6(100000)
# h6(1400000)
# h6(100000)
# h6(1100000)
# h6(2500000)
# h6(2500000)


# def h6():
#     lst = []
#     def foo(money):
#         lst.append(money)
#         if len(lst) == 7:
#             print("第一周平均价格:",sum(lst) / 7)
#     return foo
# foo = h6()
# foo(111)
# foo(1500000)
# foo(1100000)
# foo(100000)
# foo(200000)
# foo(300000)
# foo(1200000)
# foo(1100000)


# 闭包的作用:
# 1.保护数据的安全性
# 2.装饰器


# 例一：
# def wrapper():
#     a = 1
#     def inner():
#         print(a)
#     return inner
# ret = wrapper()

# a = 2
# def wrapper():
#     def inner():
#         print(a)
#     return inner
# ret = wrapper()

# def wrapper(a,b):
#     def inner():
#         print(a)
#         print(b)
#     inner()
#     print(inner.__closure__)
# a = 1
# b = 2
# wrapper(11,22)