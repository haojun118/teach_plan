# print(dict([(1,2),(3,4)]))
# print(dict(k=1,v=2,c=3))
# dic1 = {"key1":1,"key2":2}
# dic2 = {"a":1,"b":2}
# dic2.update(dic1)
# print(dic2)
# print(dict(**dic1,**dic2))

# print(dict(((1,2),(3,33))))
# for i in ((1,(2,3)),(3,33)):
#     k,v = i

# dic = {}
# dict(k=1)
# dict([(1,2)])

# sep : 每一个元素之间分割的方法 默认 " "    ****
# print(1,2,3,sep="|")

# end :print执行完后的结束语句 默认\n        ****
# print(1,2,3,end="")
# print(4,5,6,end="")

# file : 文件句柄 默认是显示到屏幕
# print(1,2,3,4,file=open("test","w",encoding="utf-8"))

# print()  # flush 刷新

# print(sum([1,2,2,1]))
# print(sum([10,20,30,40],100))

# print(abs(9)) # 绝对值

# print(dir(list))  # 查看当前对象所有方法  返回的是列表
# print(dir(str))  # 查看当前对象所有方法

# lst1 = [1,2,34,5]
# lst2 = ["alex","wusir","宝元"]
# print(list(zip(lst1,lst2))) # 拉链

# 面试题:
# print(dict(zip(lst1,lst2)))

# print(format("alex",">20"))  # 右对齐
# print(format("alex","<20"))  # 左对齐
# print(format("alex","^20"))  # 居中

# print(format(10,"b"))      # bin  二进制
# print(format(10,"08b"))
# print(format(10,"08o"))    # oct  八进制
# print(format(10,"08x"))    # hex  十六进制
# print(format(0b1010,"d"))  # digit 十进制

# print(list(reversed("alex")))

# lst = [1,2,3,4,5]
# print(list(reversed(lst)))  反转
# print(lst)

# filter -- 过滤

# lst = [1,2,3,4,5,6]
# def func(a):
#     return a>1
#
# print(list(filter(func,lst)))
# print(list(filter(lambda x:x>1,lst)))

# lst = [1,2,3,4,5,6]
# def f(func,args):
#     new_lst = []
#     for i in args:
#         if func(i):
#             new_lst.append(i)
#     return new_lst
#
# def func(a):
#     return a>1
#
# print(f(func,lst))

# print(list(filter(lambda x:x>2,[1,2,3,4,5])))
# lst = [{'id':1,'name':'alex','age':18},
#         {'id':1,'name':'wusir','age':17},
#         {'id':1,'name':'taibai','age':16},]
#
# print(list(filter(lambda x:x['age']>16,lst)))


# map 映射函数 (将每个元素都执行了执行的方法)
# print(list(map(lambda x,y:x+y,[1,2,3,4],[11,2,3,411,22])))
# print([i*8 for i in [1,2,3,4]])
# print(sorted([1,-22,3,4,5,6],key=abs))  # key指定排序规则


# lst = []
# for i in [1,-22,3,4,5,6]:
#     lst.append(abs(i))
# lst.sort()
# print(lst)

# filter()  -- 过滤

# def func(a):
#     return a == 1
#
# print(list(filter(func,[1,2,3,4,6])))
# 1,指定过滤规则(函数名[函数的内存地址])  2,要过滤的数据

# 自己模拟

# def filter(func,argv):
#     lst = []
#     for i in argv:
#         ret = func(i)
#         if ret:
#             lst.append(i)
#     return lst
#
# def foo(a):
#     return a>2
#
# print(filter(foo,[1,2,3,4])) # filter(foo,[1,2,3,4])

# def func(a):
#     return a>1
# print(list(filter(func,[1,2,3,4,5])))

# print(list(filter(lambda a:a>1,[1,2,3,4,5])))

# map()  -- 映射 (将可迭代对象中的每个元素执行指定的函数)

# def func(a,b):
#     return a+b
# print(list(map(func,[1,2,3,4,5],[33,22,44,55])))


# def map(argv,args):
#     lst = []
#     num = len(args) if len(args) < len(argv) else len(argv)
#     for i in range(num):
#         lst.append(argv[i] + args[i])
#     return lst
# print(map([1,2,3,4],[3,4,5,6,7,8,9,0]))

# print(list(map(lambda x,y:x+y,[1,2,3,4,5],[33,22,44,55])))

# sorted()  -- 排序
# print(sorted([1,2,3,4,5,6],reverse=True))
# print(sorted([1,2,3,4,5,-6],reverse=True,key=abs))

# lst = ["三国演义","红楼梦","铁道游击队","西游记","水浒传","活着"]
# print(sorted(lst,key=len))


# lst = [{"age":19},{"age1":20},{"age2":80},{"age3":10}]
# print(sorted(lst,key=lambda x:list(x.values())))
# print(sorted(lst,key=lambda x:list(x.keys()),reverse=True))
# help(sorted)

# lst = [{'id':1,'name':'alex','age':18},
#     {'id':2,'name':'wusir','age':17},
#     {'id':3,'name':'taibai','age':16},]
#
# print(sorted(lst,key=lambda x:x['age']))

# max()  -- 最大值
# print(max(10,12,13,15,16))
# print(max([10,12,13,15,-16],key=abs))

# min()  -- 最小值


# from functools import reduce   # 累计算
# 从 functools工具箱中拿来了reduce工具
# def func(x,y):
#     return x+y
# print(reduce(func,[1,2,3,4,5]))
# print(reduce(lambda x,y:x+y,[1,2,3,4,5]))