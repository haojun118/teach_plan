# 1.生成器
# 编写方式:
    # 1.基于函数
    # 2.推导式方式

# 当函数体中出现yield时函数名() 产生生成器
# 函数名().__next__() 是启动生成器
# 生成器可以使用for进行遍历取值

# 坑

# def func():
#     print(1)
#     yield 2
#
# print(func().__next__())
# print(func().__next__())
# print(func().__next__())

# g = func()
# print(next(g))

# 生成器本质就是迭代器
# 生成器和迭代器的区别
# 生成器的自己写的
# 迭代器是python自带的
# 生成器和迭代器通过send和内存地址进行区分
# 生成器的使用场景:
# 当文件或数据比较大时,建议使用生成器
# yield 和 next 要一一对应

# 生成器的优点:
#     节省空间
#     人为定义

# 生成器的缺点:
#     不能直接查看元素
#     消耗时间
#     一次性
#     不能逆行

# 迭代器的优点:
#       节省空间

# 迭代器的缺点:
#     不能直接查看元素
#     消耗时间
#     一次性
#     不能逆行

# 可迭代对象:
#     优点:
#       节省时间
#       取值方便
#       使用灵活
#     缺点:
#         占用大量内存资源

# yield 和 return 的区别
# yield可以记录执行位置
# return 只会执行一次


# yield from 将可迭代对象的元素逐个返回
# yield 是一次性返回

# yield和return相同之处
#     都可以返回多个内容,并且写多次
#     yield可以暂停生成器但是不能终止循环

# 不重要的send
# 第一次使用send时括号中写的内容必须是None

# 可迭代对象: 具有__iter__()就是可迭代对象 range()

# 迭代器: 文件句柄 具有__iter__() 和 next()

# python2和python3的区别:
# python2中的xrange 和 python3中的range相同
# python2中的range返回的是一个列表,一个可迭代对象

# 推导式:
# 列表推导式:
# 普通循环
#   [i for i in range(10)]
# 筛选模式
#     [i for i in range(10) if i > 2]

# 生成器推导式:

# 普通循环
#   (i for i in range(10))
# 筛选模式
#     print((i for i in range(10) if i > 2))
# 返回的是一个生成器的地址

# 集合推导式:
# 普通循环
#   {i for i in range(10)}
# 筛选模式
#     {i for i in range(10) if i > 2}

# 字典推导式:
# 普通循环
#   {i:i+1 for i in range(10)}
# 筛选模式
#     {i:i+1 for i in range(10) if i > 2}

# 内置函数一: