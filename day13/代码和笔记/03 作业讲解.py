# 求[{x:y}]其中x是0-5之间的偶数以元组的形式，y是0-5之间的奇数元组形式组成字典
# [{(0,2,4):(1,3,5)}]

# lst = []
# key = []
# value = []
# for i in range(6):
#     if i % 2 == 0:
#         key.append(i)
#     else:
#         value.append(i)
# dic = {tuple(key):tuple(value)}
# lst.append(dic)
# print(lst)

# print([{tuple([i for i in range(0,6,2)]):tuple([j for j in range(1,6,2)])}])

"""
3.有以下数据类型：

将上面的数据通过列表推导式转换成下面的类型：
[[1517991992.94, 100], [1517992000.94, 200], [1517992014.94, 300],
[1517992744.94, 350], [1517992800.94, 280]]
"""

# x = {
# 'name':'alex',
# 'Values':[{'timestamp':1517991992.94,
# 'values':100,},
# {'timestamp': 1517992000.94,
# 'values': 200,},
# {'timestamp': 1517992014.94,
# 'values': 300,},
# {'timestamp': 1517992744.94,
# 'values': 350},
# {'timestamp': 1517992800.94,
# 'values': 280}
# ],}
#
# print([list(i.values()) for i in x["Values"]])


"""
构建一个列表,列表里面的元素是扑克牌除去大小王以后，所有的牌类（列表里面的元素为元组类型）。
l1 = [('A','spades'),('A','diamonds'), ('A','clubs'), ('A','hearts')......
('K','spades'),('K','diamonds'), ('K','clubs'), ('K','hearts') ]
"""
lst = [i for i in range(2,11)] + ["J","Q","K","A"]
l = ['spades','diamonds','clubs','hearts']

# new_lst = []
# for i in lst:
#     for j in l:
#         new_lst.append((i,j))
# print(new_lst)

# print([(i,j) for i in lst for j in l])

# v = [i % 2 for i in range(10)]
# print(v) [0,1,0,1,0,1]