# 匿名函数  == 一行函数
# 匿名函数的名字叫做 lambda

# def func():
#     return 6
# print(func.__name__)

# lambda == def == 关键字
# x  是普通函数的形参 (位置,关键字 ...) 可以不接收参数 (x:可以不写)
# :后边x 是普通函数的函数值(只能返回一个数据类型) (:x返回值必须写)

# print((lambda x:x+6)(5))

# f = lambda x:x+6
# print(f.__name__)

# f = lambda x,y,z,b=1:x,y,z,b
# print(f(1,2,3))

# print([lambda :5][0]())
# print((lambda :5)())
# a = lambda :5
# a()

# lst = [lambda :i for i in range(5)]
# print(lst[0]())

# 面试题拆解:
# lst = []
# for i in range(5):
#     def func():
#         return i
#     lst.append(func)


# lst = [] # [lambda x:x+1,lambda x:x+1]
# for i in range(2):
#     lst.append(lambda x:x+1)
# print(lst[-1](5))

# lst = [lambda x:x+1 for i in range(5)]
# print(lst[0](5))
#
# tu = (lambda :i for i in range(3))
# print(next(tu)())
# print(next(tu)())
# print(next(tu)())

# def func():
#     for i in range(3):  # i = 0 1 2
#         def foo():      # foo1  foo2  foo3
#             return i
#         yield foo       # foo1  foo2  foo3
# g = func()
# print(next(g)())  # foo1
# print(next(g))  # foo2
# print(next(g))  # foo3

# lst = [lambda :i for i in range(3)]
# print(lst[0]())

# tu = (lambda :i for i in range(3))
# print(next(tu)())

# 函数体中存放的是代码
# 生成器体中存放的也是代码
# 就是yield导致函数和生成器的执行结果不统一

# lst = [lambda x:x+5 for i in range(2)]
# print([i(2) for i in lst])

# lst = [] # [lambda x:x+5,lambda x:x+5]
# for i in range(2):
#     lst.append(lambda x:x+5)
# new_lst = []
# for i in lst:
#     new_lst.append(i(2))
# print(new_lst)


# lst = (lambda x:x+5 for i in range(2))
# print([i(2) for i in lst])

# def func():
#     for i in range(2):
#         f = lambda x: x + 5
#         yield f
# g = func()
# lst = []
# for i in g:
#     lst.append(i(2))
# print(lst)

# lst = [lambda x:x*i for i in range(2)]
# print([i(2) for i in lst])  #[2,2]

# lst = [] # [lambda x:x*i,lambda x:x*i]
# for i in range(2):
#     lst.append(lambda x:x*i)
# # print(i)
# new_lst = []
# for em in lst:
#     new_lst.append(em(2))
# print(new_lst)



# lst = (lambda x:x*i for i in range(2))
# print([i(2) for i in lst])  #[0,2]
#
# def func():
#     for i in range(2):
#         f = lambda x:x*i
#         yield f
# g = func()
# lst = []
# for i in g:
#     lst.append(i(2))
# print(lst)

# func = lambda x:[i for i in x]
# print(func('afafasd'))

# print(list('afafasd'))