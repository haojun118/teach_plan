from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import HttpResponse


class MD1(MiddlewareMixin):
    def process_request(self, request):
        # print(1,id(request))
        # request.jia = 'geshuxing'
        print('MD1 process_request ')

        # return HttpResponse('hehehe')

    def process_response(self, request, response):
        # print(response.content)
        # response.status_code = 302
        print('MD1 process_response ')
        # response['Location'] = 'http://www.baidu.com'

        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        # print(view_func)
        # print(view_args)
        # print(view_kwargs)
        # view_kwargs['index'] = 'index'

        print('MD1 process_view ')

        # return HttpResponse('MD1 process_view')

    def process_exception(self, request, exception):
        print('MD1 process_exception ')


    def process_template_response(self,request,response):

        print('MD1 process_template_response ')

        # self.template_name = template
        # self.context_data = context
        # print(response.template_name)
        # print(response.context_data)
        response.template_name = 'index2.html'
        response.context_data['name'] = 'alexdsb'
        return response


class MD2(MiddlewareMixin):
    def process_request(self, request):
        print('MD2 process_request ')

    def process_response(self, request, response):
        print('MD2 process_response ')

        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        print('MD2 process_view ')

    def process_exception(self, request, exception):
        print('MD2 process_exception ')
        return HttpResponse('错误处理完成')
