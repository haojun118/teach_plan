## cookie和session

### cookie

1. #### 定义

   保存在浏览器上的一组组键值对

2. #### 为什么要有？

   HTTP协议是无状态，每次请求之间都相互独立的，之间没有关系，没办法保存状态。

3. #### 特性：

   1. 服务器让浏览器进行设置，浏览器有权利不保存
   2. 下次访问时，自动携带响应的cookie
   3. 保存在浏览本地的

4. #### django的操作：

   1. 设置  set-cookie 

      response.set_cookie(key,value,max_age=5,path='/')

      response.set_signed_cookie(key,value,max_age=5,path='/',salt='xxxxx')

   2. 获取  cookie

      request.COOKIES   {} 

      request.COOKIES[key]   request.COOKIES.get(key)

      request.get_signed_cookie(key,salt='xxxxx'，default=‘’)

   3. 删除  set-cookie  

      response.delete-cookie(key')

      

### session

1. #### 定义：

   ​	保存在服务器上的一组组键值对，必须依赖cookie

2. #### 为什么要有？

   1. cookie保存在浏览器上，不太安全
   2. 浏览器对cookie的大小有一定的限制

3. #### session的流程：

   1. 浏览器发送请求，没有cookie也没有session
   2. 要设置session时，先根据浏览器生成一个唯一标识（session_key），存键值对，并且有超时时间
   3. 返回cookie  session_id = 唯一标识

4. #### 在django中的操作：

   1. 设置

      request.session[key]  = value 

   2. 获取 

      request.session[key]   request.session.get()

   3. 删除

      del request.session[key]

      request.session.pop(key)

      request.session.delete()   删除所有的session  不删除cookie

      request.session.flush()     删除所有的session  删除cookie

   4. 其他

      request.session.clear_expired()     删除已经失效的session数据	

      request.session.set_expiry(value)

   5. 配置

      from django.conf  import global_settings

   

   ​		SESSION_SAVE_EVERY_REQUEST = True   每次请求都更新session数据   

   ​		SESSION_EXPIRE_AT_BROWSER_CLOSE = True

   ​		SESSION_ENGINE = 'django.contrib.sessions.backends.db'     默认存在数据库

   ​		缓存    文件   缓存+数据库   加密cookie

   ​		Json序列化的过程



## 中间件

中间件  ：中间件就是一个类，在全局范围内处理django的请求和响应。

类    5个方法     4个特点

### process_request(self,request)

执行时间：

​		在视图函数之前

执行顺序： 

​		按照注册的顺序 顺序执行

参数 ：  

​			request：  请求的对象，和视图函数是同一个

返回值：

​		None :   正常流程 

​		HttpResponse:之后中间件的process_request、路由、process_view、视图都不执行，执行执行当前中间件对应process_response方法，接着倒序执行之前的中间件中的process_response方法。



### process_response(self, request, response)

执行时间：

​		在视图函数之后

执行顺序： 

​		按照注册的顺序 倒序执行

参数 ：  

​			request：  请求的对象，和视图函数是同一个

​			response： 响应对象

返回值：

​		HttpResponse: 必须返回



### process_view(self, request, view_func, view_args, view_kwargs)

执行时间：

​		在路由匹配之后，在视图函数之前

执行顺序： 

​		按照注册的顺序 顺序执行

参数 ：  

​			request：  请求的对象，和视图函数是同一个

​			view_func：视图函数

​			view_args:   给视图传递的位置参数

​			view_kwargs：  给视图传递的关键字参数

返回值：

​			None：  正常流程

​		    HttpResponse: 之后中间件的process_view、视图都不执行，直接执行最后一个中间件process_response，倒序执行之前中间件的process_response方法

### process_exception(self, request, exception)

执行时间：

​		在视图函数出错之后执行

执行顺序： 

​		按照注册的顺序 倒序执行

参数 ：  

​			request：  请求的对象，和视图函数是同一个

​			exception：报错的对象

返回值：

​			None：  自己没有处理，交给下一个中间件处理，所有的中间件都没有处理，django处理错误。

​		    HttpResponse: 之后中间件的process_exception，直接执行最后一个中间件process_response，倒序执行之前中间件的process_response方法

### process_template_response(self,request,response)

执行时间：

​		当视图函数返回一个TemplateResponse对象

执行顺序： 

​		按照注册的顺序 倒序执行

参数 ：  

​			request：  请求的对象，和视图函数是同一个

​			response：响应的对象

返回值：

​		    HttpResponse: 必须返回