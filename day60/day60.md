##  内容回顾

视图

### FBV  CBV

定义CBV

```python
from django.views import View
class PUblisherAdd(View):
    # http_method_names = ['get',]
    
    def get(self,request,*args,**kwargs):
        # 处理GET请求的具体的逻辑
        self.request
        return response
    
    def post(self,request,*args,**kwargs):
        # 处理GET请求的具体的逻辑
        return response

```

ulrs.py中写对应关系

```python
url(r'add_publisher/',views.PublisherAdd.as_view())
```

### as_view的流程

1. 程序启动的时候，类.as_view()方法要执行  ——》  view函数

2. 请求到来的时候要执行view函数

   1. 实例化类  成对象 ——》  self

   2. self.request = request

   3. 执行dispatch的方法

      1. 判断请求方式是否被允许；  http_method_names = []

         1. 允许

            通过反射获取对应请求方式所对应的方法  ——》 handler

         2. 不允许

            http_methid_not_allowed  ——》 handler

      2. 执行handler 返回它的执行结果

### 加装饰器的方法

```python
form functools import wraps

def timer(func):
	@wraps(func)
	def inner(*args,**kwargs):
		# 之前的操作
		ret = func(*args,**kwargs)
		# 之后的操作
		return ret
	return inner
```

FBV  直接加

CBV 

from django.utils.decorators import method_decorator

```python
1. 加在方法上
@method_decorator(timer)
def get(....)

2. 加在类上
@method_decorator(timer，name='get')
class PUblisherAdd(View):

3. 加在dispatch方法上
@method_decorator(wrapper)
def dispatch(self, request, *args, **kwargs):
    ret = super().dispatch(request, *args, **kwargs)
    return ret

@method_decorator(timer，name='dispatch')
class PUblisherAdd(View):
```

### request对象

```python
request.GET   #  url上携带的参数  {} 
request.POST  # POST请求提交的数据   编码类型urlencode
request.method  # 请求方式 POST GET 
request.FILES  # 上传的文件
	1. enctype="multipart/form-data"
    2. input type = 'file'  name='f1'
request.path_info   # 路径信息  不包含ip和端口  也不包含查询的参数
request.body   # 请求体  浏览器提交的原始数据
request.META   # 请求头的信息  小写 ——》  大写  - ——》 _  HTTP_
request.COOKIES  # COOKIE 
request.session  # session 

request.get_full_path()  # 完整的路径信息  不包含ip和端口  包含查询的参数 
request.is_ajax()     # 是否是ajax请求
```

### response对象

```python
HttpRsponse('字符串')     # 返回字符串   Content-Type: text/html; charset=utf-8
render(request,'模板的文件名'，{})   # 返回一个HTML页面
redirect(地址)  # 重定向到地址    本质：响应头 Location:地址
JsonResponse({})   # 返回json数据   返回非字典类型safe=False  Content-Type: application/json

```

## 路由

### URLconf

```

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^blog/$', views.blogs),  # /blog/
    url(r'^blog/[0-9]{4}/\d{2}/$', views.blog), # /blog/2019/10/
    
]

url(正则表达式的字符串, 视图函数，)
```

### 正则表达式

​	r''     ^ 开通  $ 结尾 [0-9a-zA-Z]{4}    \d  \w   .   ? 0个或1个     *0个或无数个  + 至少一个 

### 分组

```
urlpatterns = [

    url(r'^blog/([0-9]{4})/(\d{2})/$', views.blog),

]
```

从URL上捕获参数，将参数按照位置传参传递给视图函数。

### 命名分组

```
urlpatterns = [

    url(r'^blog/(?P<year>[0-9]{4})/(?<month>\d{2})/$', views.blog),

]
```

从URL上捕获参数，将参数按照关键字传参传递给视图函数。

### include

```
from django.conf.urls import url, include
from django.contrib import admin

from app01 import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^app01/', include('app01.urls')),
    url(r'^app02/', include('app02.urls')),

    url(r'^file_upload/', views.file_upload),
    url(r'^get_data/', views.get_data),

]

```

app01 urls,py

```
from django.conf.urls import url


from app01 import views

urlpatterns = [

    url(r'^publisher_list/', views.publisher_list),
    # url(r'^publisher_add/', views.publisher_add),
    url(r'^publisher_add/', views.PublisherAdd.as_view()),
    # url(r'^publisher_add/', view),
    url(r'^publisher_del/(\d+)/', views.publisher_del),  
    url(r'^publisher_edit/', views.publisher_edit),
    
]
```

### URL的命名和反向解析

#### 静态路由

URL的命名

```
url(r'^publisher_list/', views.publisher_list, name='publisher_list'),
```

URL反向解析

模板

```
{% url 'publisher_list' %}    ——》  解析生成完整的URL路径  '/app01/publisher_list/'
```

py文件

```
from django.shortcuts import render, redirect, HttpResponse, reverse
from django.urls import reverse

reverse('publisher_list')   ——》  '/app01/publisher_list/'
```

#### 分组

URL的命名

```
 url(r'^publisher_del/(\d+)/', views.publisher_del,name='publisher_del'),
```

URL反向解析

模板

```
{% url 'publisher_del' 1  %}    ——》  解析生成完整的URL路径  '/app01/publisher_del/1/'
```

py文件

```
from django.shortcuts import render, redirect, HttpResponse, reverse
from django.urls import reverse

reverse('pub_del',args=(1,))   ——》  '/app01/publisher_del/1/'
```

#### 命名分组

URL的命名

```
 url(r'^publisher_del/(\d+)/', views.publisher_del,name='publisher_del'),
```

URL反向解析

模板

```
{% url 'publisher_del' 1  %}    ——》  解析生成完整的URL路径  '/app01/publisher_del/1/'
{% url 'publisher_del' pk=1  %}    ——》  解析生成完整的URL路径  '/app01/publisher_del/1/'
```

py文件

```
from django.shortcuts import render, redirect, HttpResponse, reverse
from django.urls import reverse

reverse('pub_del',args=(1,))   ——》  '/app01/publisher_del/1/'
reverse('pub_del',kwargs={'pk':1})   ——》  '/app01/publisher_del/1/'
```

### namespace

```
urlpatterns = [

    url(r'^app01/', include('app01.urls', namespace='app01')),  # home   name=home
    url(r'^app02/', include('app02.urls', namespace='app02')),  # home   name=home

]

```

反向解析生成URL

{%  url  'namespace:name'  % }

reverse( 'namespace:name'  )















