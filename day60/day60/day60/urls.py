"""day60 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from app01 import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^blog/$', views.blogs, {'k1': 'v1'},name='blog'),  # /blog/2019/11/
    url(r'^blog/(?P<year>[0-9]{4})/(?P<month>\d{2})/$', views.blog, {'year': 1000}),

    url(r'^app01/', include('app01.urls', namespace='app01')),  # home   name=home
    url(r'^app02/', include('app02.urls', namespace='app02')),  # home   name=home

]
