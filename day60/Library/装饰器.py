from functools import wraps


def wrapper(func):
    @wraps(func)
    def inner(*args, **kwargs):
        # 之前
        ret = func(*args, **kwargs)
        # 之后
        return ret

    return inner


@wrapper  # a = wrapper(a)  inner
def a():
    """
    这是a
    :return:
    """
    pass


@wrapper  # b = wrapper(b)  inner
def b():
    """
      这是b
      :return:
      """
    pass


print(a.__name__)
print(a.__doc__)
print(b.__name__)
print(b.__doc__)
