from django.db import models


# Create your models here.
class Publisher(models.Model):
    pid = models.AutoField(primary_key=True)  # pid 主键
    name = models.CharField(max_length=32,unique=True)
    addr = models.CharField(max_length=32,)