## 内容回顾

1. 下载

​	命令行：

​		pip install django==1.11.25  -i   源

​	pycharm：

​		file _>   settings  _>  项目  ——》 解释器  ——》  点+号  ——》  输入django  ——》 选择版本 ——》 下载

2.创建项目

​	命令行：

​		django-admin  startproject  项目名

​	pycharm：

​		file  _>   new project ——》 选择django  ——》 输入项目的路径 ——》 选择解释器  ——》 templates  还有app名称 

3.启动项目

​	命令行：

​			切换项目的根目录下   manage.py

​			python manage.py  runserver     #  127.0.0.1:8000 

​			python manage.py  runserver  80   #  127.0.0.1:80

​			python manage.py  runserver  0.0.0.0:80   # 0.0.0.0:80

​	pycharm：

​			找绿三角  点击  （前面是django的项目）  

​			配置ip和端口

4.settings的配置

​	模板文件     TEMPLATES    DIRS    [ os.path.join(BASE_DIR,'templates') ]	

​	静态文件

​				STATIC_URL  ='/static/'    #  静态文件的别名

​				STATICFILES_DIRS  = [

​					os.path.join(BASE_DIR,'static')	,

​					os.path.join(BASE_DIR,'static1')	

​			]

​	数据库配置

​	APP

​	中间件

​			注释一个  csrf     效果： 可以提交post请求

5. APP

   创建APP

   ​	python manage.py  startapp app名称   

   注册APP

   ​	INSTALLED_APPS = [

   ​		'app01',

   ​		'app01.apps.App01Config'   推荐写法

   ]	

6. urls.py

   ```
   from app01 import views
   
   urlpatterns = [
       url(r'^admin/', admin.site.urls),
       url(r'^login/', views.login),
       url(r'^index/', views.index),
   ]
   
   ```

7. views.py

   ```
   from django.shortcuts import render,HttpResponse,redirect
   
   def login(request):
   	# 逻辑
   	renturn HttpResponse('xxx')
   ```

   HttpResponse()     返回的字符串

   render(request,'模板的文件名')       返回HTML页面

   redirect('地址')    重定向 

8. get和post 

   get     获取页面 资源

   /sssss/？k1=v1&k2=v2

   request.GET   {}  request.GET.get('key')

   

   post   提交数据

   request.POST    {}  request.POST.get('key')

9. form的表单

   1. form标签的属性  action=''  提交的地址    method='post'  请求方式    novalidate  不在前端做校验
   2. input标签要有name属性  value的值
   3. 有个type=‘submit’ 的input或者button按钮

10. django使用mysql的流程：

    1. 创建一个mysql数据库

    2. 在settings中配置数据库的信息

       EGNIGE   引擎  mysql

       NAME    数据库名称

       HOST    ip  

       PORT    端口 3306

       USER   用户名

       PASSWORD  密码

    3. 告诉使用pymysql替换mysqldb的模块

       写在与settings同级目录下的`__init__`中：

       ```
       import  pymysql
       pymysql.install_as_MySQLdb()
       ```

    4. 在已经注册的app下的models.py中写类（继承models.Model）

       ```python
       class User(models.Model):
           username = models.CharField(max_length=32)  #  username  varchar(32)
       ```

    5. 执行命令

       ```
       python manage.py  makemigrations  # 记录了一个models的变更记录
       python manage.py  migrate    # 将models的变更记录同步到数据库中
       ```

       

11. ORM

    对象关系映射  

    对应关系

    类       —— 》    表

    对象   ——》     数据行（记录）

    属性   ——》    字段

​       ORM能做的事情：

  1. 对数据库中表操作

  2. 对表中的数据操作

     orm具体的操作：

     ```
     from app01.models import User
     
     User.objects.all()   # 获取到所有的数据 QuerySet  [User对象]  对象列表
     User.objects.get（） # 获取一个对象  只能获取唯一存在的对象   如果不存在或者是多个结果就报错
     User.objects.filter（）   # 获取到所有满足条件的对象  QuerySet  [User对象]  对象列表
     ```

     

## 今日内容

图书管理系统

出版社     书籍     作者 

出版社的管理

展示

1. 设计URL地址

   ```
   from app01 import views
   
   urlpatterns = [
       url(r'^admin/', admin.site.urls),
       url(r'^publisher_list/', views.publisher_list),
   ]
   ```

2. 写函数

   ```
   def publisher_list(request):
       # 从数据库中获取所有的出版社的信息
       all_publisher = models.Publisher.objects.all()  # 对象列表
   
       # 将数据展示到页面中
       return render(request,'publisher_list.html',{'k1':all_publisher})
   ```

3. 模板语法

   ```
   {{ 变量  }}
   
   for循环
   {% for i in 变量  %}
   	循环体 {{ i }}
   {% endfor  %}
   ```

新增

```
方式一：
models.Publisher.objects.create(name=pub_name,addr=pub_addr)  # 对象
方式二：
pub_obj = models.Publisher(name=pub_name,addr=pub_addr)  # 内存中的对象 和数据库没关系
pub_obj.save()  # 插入到数据库中
```

删除

```
models.Publisher.objects.filter(pid=pid).delete()  # 对象列表 删除
models.Publisher.objects.get(pid=pid).delete()  # 对象 删除
```



编辑

```
 pub_obj.name = pub_name
 pub_obj.addr = pub_addr
 pub_obj.save()  # 将修改提交的数据库
```











