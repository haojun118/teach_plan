## Ajax

js的技术，就是发请求接受响应的技术，传输少量的数据。

### 发请求的途径：

1. a标签     get   
2. 地址栏输入地址   get   
3. form表单    get / post 

### 特点：

1. 局部刷新
2. 传输的数据量少

3. 异步

### 简单使用：

```
$.ajax({
            'url':'/calc/',
            'type':'post',
            'data':{
                'k1':$('[name="i1"]').val(),
                'k2':$('[name="i2"]').val(),
            },
            success:function (ret) {
               $('[name="i3"]').val(ret)
            }
        })
```

### 参数

```
$.ajax({
            url: '/test/',   	#  提交的地址
            type: 'post',		#  请求方式
            data: {				#  提交的数据
                name: 'alex',
                age: 73,
                hobby: JSON.stringify(['装逼', '画饼', '上过北大'])
            },
            success: function (ret) {		# 响应成功的回调函数

            },
            error:function (ret) {			# 响应失败的回调函数
                console.log(ret)
            }
        })
```

### 上传文件

```
$('button').click(function () {

        var form_data = new FormData();
        form_data.append('k1','v1');
        form_data.append('f1',$('#f1')[0].files[0]);

        $.ajax({
            url :'/file_upload/',
            type:'post',
            data:form_data,  // { k1:v1 }
            processData: false,  //   不需要处理数据
            contentType: false,  //   不需要contentType请求头
            success:function (ret) {
                console.log(ret)
            }

        })

    })
```

### ajax能通过django的csrf的校验：

前提必须有cookie：

1. {% csrf_token %}

2. 给视图加装饰器

   ```
   from django.views.decorators.csrf import ensure_csrf_cookie
   
   @ensure_csrf_cookie
   def index(request):
   ```

   

方式一：data中添加键值对   csrfmiddlewaretoken

```
$.ajax({
            'url': '/calc/',
            'type': 'post',
            'data': {
                'csrfmiddlewaretoken': $('[name="csrfmiddlewaretoken"]').val(),
                'k1': $('[name="i1"]').val(),
                'k2': $('[name="i2"]').val(),
            },
            success: function (ret) {
                $('[name="i3"]').val(ret)
            }
        })
```

方式二：加请求头  x-csrftoken

```
$('#b2').click(function () {
        // 点击事件触发后的逻辑
        $.ajax({
            'url': '/calc2/',
            'type': 'post',
            headers:{'x-csrftoken':$('[name="csrfmiddlewaretoken"]').val(),},
            'data': {
                'k1': $('[name="ii1"]').val(),
                'k2': $('[name="ii2"]').val(),
            },
            success: function (ret) {
                $('[name="ii3"]').val(ret)
            }
        })

    })
```

方式三：

导入文件



