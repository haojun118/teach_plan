from django.shortcuts import render, HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie

# Create your views here.
@ensure_csrf_cookie
def index(request):
    if request.method == 'POST':
        i1 = request.POST.get('i1')
        i2 = request.POST.get('i2')
        i3 = int(i1) + int(i2)
        return render(request, 'index.html', {'i1': i1, 'i2': i2, 'i3': i3})

    return render(request, 'index.html')


import time


def calc(request):
    # time.sleep(5)
    i1 = request.POST.get('k1')
    i2 = request.POST.get('k2')
    i3 = int(i1) + int(i2)
    return HttpResponse(i3)


def calc2(request):
    i1 = request.POST.get('k1')
    i2 = request.POST.get('k2')
    i3 = int(i1) + int(i2)
    return HttpResponse(i3)


import json


def test(request):
    print(request.POST)
    name = request.POST.get('name')
    age = request.POST.get('age')
    # hobby= request.POST.getlist('hobby[]')
    hobby = json.loads(request.POST.get('hobby'))
    print(name)
    print(age)
    print(hobby, type(hobby))


def file_upload(request):
    if request.method == 'POST':
        f1 = request.FILES.get('f1')
        with open(f1.name,'wb') as f:
            for i in f1.chunks():
                f.write(i)

        return HttpResponse('ok')

    return render(request, 'file_upload.html')
