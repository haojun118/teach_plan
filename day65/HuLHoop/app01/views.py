from django.shortcuts import render, redirect,HttpResponse,reverse
from app01 import models
from django.db.models import F, Q


# Create your views here.
def index1(request):
    ret = render(request, 'index1.html')
    return ret

def comment(request):
    text = request.POST.get('text')
    a_id = request.POST.get('a_id')
    models.Comment.objects.create(content=text,user_id=request.session.get('pk'),article_id=a_id)
    return redirect(reverse('article',args=(a_id,)))

def index(request):
    class_label = models.Class_title.objects.all()
    pk = request.GET.get("id")

    text_all = models.Text.objects.filter(Class_title__id=pk) if pk and pk != '1' else models.Text.objects.all()

    return render(request, "index.html", {"text_all": text_all, "class_label": class_label})


def login(request):
    if request.method == 'POST':
        user = request.POST.get('user')
        pwd = request.POST.get('pwd')
        user_obj =models.Author.objects.filter(name=user, password=pwd).first()
        if user_obj:
            # 保存登录状态  设置cookie
            return_url = request.GET.get('returnUrl')

            ret = redirect(return_url if return_url else 'index')
            # 保存登录状态
            request.session['is_login'] = 'true'
            request.session['username'] = user
            request.session['pk'] = user_obj.pk

            return ret
        return render(request, 'login.html', {'error': '用户名或密码错误'})

    return render(request, 'login.html')


def article(request, pk):
    obj = models.Text.objects.filter(pk=pk).first()

    if obj:
        obj.browse_count += 1
        obj.save()

        return render(request, "article.html", {"obj": obj,'comments':obj.comment_set.all()})
    else:
        return HttpResponse('文章不存在')