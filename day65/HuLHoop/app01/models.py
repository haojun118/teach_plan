from django.db import models


# Create your models here.


class Author(models.Model):
    name = models.CharField(max_length=32)
    password = models.CharField(max_length=32)
    avatar = models.URLField(null=True)


class Class_title(models.Model):
    name = models.CharField(max_length=32)


class Text(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    time = models.DateTimeField(auto_now_add=True)
    browse_count = models.IntegerField()
    review_count = models.IntegerField()
    picture = models.CharField(max_length=255)
    author = models.ForeignKey("Author", on_delete=models.CASCADE)
    Class_title = models.ManyToManyField("Class_title")


class Comment(models.Model):
    content = models.TextField()
    time = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey('Author',on_delete=models.CASCADE)
    article = models.ForeignKey('Text',on_delete=models.CASCADE)

    # parent = models.ForeignKey('Comment',on_delete=models.CASCADE,null=True,blank=True)


