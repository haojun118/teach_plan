"""
11.选做题：写代码，完成下列需求：
用户可持续输入（用while循环），用户使用的情况：
输入A，则显示走大路回家，然后在让用户进一步选择：
是选择公交车，还是步行？
选择公交车，显示10分钟到家，并退出整个程序。
选择步行，显示20分钟到家，并退出整个程序。
输入B，则显示走小路回家，并退出整个程序。
输入C，则显示绕道回家，然后在让用户进一步选择：
是选择游戏厅玩会，还是网吧？
选择游戏厅，则显示 ‘一个半小时到家，爸爸在家，拿棍等你。’并让其重新输入A，B,C选项。
选择网吧，则显示‘两个小时到家，妈妈已做好了战斗准备。’并让其重新输入A，B,C选项。
"""

# while True:
#     choose = input("请输入(A/B/C)进行选择:").strip().upper()
#     if "A" == choose:
#         print("走大路回家")
#         car = input("公交车还是步行啊:")
#         if car == "公交车":
#             print("10分钟到家")
#             break
#         elif car == "步行":
#             print("20分钟到家")
#             break
#     elif "B" == choose:
#         print("走小路回家")
#         break
#     elif "C" == choose:
#         print("绕道回家")
#         play = input("游戏厅还是网吧:")
#         if play == "游戏厅":
#             print("一个半小时到家，爸爸在家，拿棍等你。")
#         elif play == "网吧":
#             print("两个小时到家，妈妈已做好了战斗准备。")


"""
lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
将列表lis中的"tt"变成大写（用两种方式）。
将列表中的数字3变成字符串"100"（用两种方式）。
将列表中的字符串"1"变成数字101（用两种方式）。
"""
lis = [2, "3", "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
# lis[3][2][1][0] = lis[3][2][1][0].upper()
# lis[3][2][1][0] = lis[3][2][1][0].replace("t","T")

# lis[3][2][1][1] = str(lis[3][2][1][1] + 97)
# lis[3][2][1][1] = "100"

# 将列表中的字符串"1"变成数字101（用两种方式）。
# lis[3][2][1][2] = 101
# lis[3][2][1][2] = int(lis[3][2][1][2] + "01")
# print(lis)

"""
4.请用代码实现：
li = ["alex", "wusir", "taibai"]
利用下划线将列表的每一个元素拼接成字符串"alex_wusir_taibai"
"""
# s = "" # alex_wusir_taibai_
# li = ["alex", "wusir", "taibai"]
# for i in li:
#     s = s + i + "_"
# print(s.strip("_"))
# print(s[:-1])

# li = ["alex", "wusir", "taibai"]
# print("_".join(li))

# name = "alex"
# name.upper()

"""
5.利用for循环和range打印出下面列表的索引。
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
"""
# li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]

# count = 0
# for i in li:
#     print(count)
#     count += 1

# for i in range(len(li)):
#     print(i)

"""
11.查找列表li中的元素，移除每个元素的空格，并找出以"A"或者"a"开头，并以"c"结尾的所有元素，
并添加到一个新列表中,最后循环打印这个新列表。
li = ["TaiBai ", "alexC", "AbC ", "egon", " riTiAn", "WuSir", " aqc"]
"""
# new_lst = []
# li = ["TaiBai ", "alexC", "A  bC ", "egon", " riTiAn", "WuSir", " aqc"]
# for i in li:
    # i = i.strip()
    # i = i.replace(" ","")
#     if (i.startswith("A") or i.startswith("a")) and i.endswith('c'):
#         new_lst.append(i)
# print(new_lst)

"""
12.开发敏感词语过滤程序，提示用户输入评论内容，如果用户输入的内容中包含特殊的字符：
敏感词列表 li = ["老师苍", "东京比较热", "武兰", "波多"]
则将用户输入的内容中的敏感词汇替换成等长度的*（老师苍就替换***），
并添加到一个列表中；如果用户输入的内容没有敏感词汇，则直接添加到上述的列表中。
"""

# li = ["老师苍", "东京比较热", "武兰", "波多"]
# msg = input("请输入您的评论:")

# msg = "昨天我去的东京比较热找武兰喝了点酒"
# msg = "昨天我去的*****找武兰喝了点酒"
# aaa = "昨天我去的东京比较热找**喝了点酒"

# lst = []
# for i in li:
#     if i in msg:
#         msg = msg.replace(i,len(i) * "*")
# lst.append(msg)
# print(lst)

# 13.有如下列表（选做题）
# li = [1, 3, 4, "alex", [3, 7, 8, "TaiBai"], 5, "RiTiAn"]
# 循环打印列表中的每个元素，遇到列表则再循环打印出它里面的元素。
# 我想要的结果是：
# 1
# 3
# 4
# alex
# 3
# 7
# 8
# taibai
# 5
# ritian

# li = [1, 3, 4, "alex", [3, 7, 8, "TaiBai"], 5, "RiTiAn"]
# for i in li:
#     if type(i) == list:
#         for em in i:
#             if type(em) == str:
#                 print(em.lower())
#             else:
#                 print(em)
#     else:
#         if type(i) == str:
#             print(i.lower())
#         else:
#             print(i)
