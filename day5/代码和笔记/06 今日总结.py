"""
1.字典
        dict, 键值对,
        键:唯一,不可变数据类型(可哈希)
        值:任意
        可变,无序  --  Python36版本以上 感官是有序的
    增:
        字典名[键] = 值
        字典名.setdefault(键,值)    # 键在字典中存在就不添加,不存在就添加
    删:
        字典名.pop(键)    # 通过键进行删除,具有返回值
        字典名.clear()    # 清空
        del 字典名[键]    # 通过键进行删除
    改:
        字典名[键] = 值
        字典名.update(新字典)
    查:
        字典名.get(键,找不到的返回值)
        字典名.setdefault(键)
        keys()   所以的键
        values() 所以的值
        items()  键值
        for循环 -- 字典的键

    解构:
        面试题: a,b = b,a
        * -- 聚合
        按照位置进行结构, *可以接受任意多个

        for a,b in dic.items()
            print(a) # 键
            print(b) # 值

    字典的嵌套:

        dic = {
            101:{1:{"日魔":"对象"},
                 2:{"隔壁老王":"王炸"},
                 3:{"乔碧萝":("日魔","炮手","宝元")},
                 },
            102:{1:{"汪峰":{"国际章":["小苹果","大鸭梨"]}},
                 2:{"邓紫棋":["泡沫","信仰","天堂","光年之外"]},
                 3:{"腾格尔":["隐形的翅膀","卡路里","日不落"]}
                 },
            103:{1:{"蔡徐坤":{"唱":["鸡你太美"],
                           "跳":["钢管舞"],
                           "rap":["大碗面"],
                           "篮球":("NBA形象大使")}},
                 2:{"JJ":{"行走的CD":["江南","曹操","背对背拥抱","小酒窝","不潮不花钱"]}},
                 3:{"Jay":{"周董":["菊花台","双节棍","霍元甲"]}}},
            201:{
                1:{"韦小宝":{"双儿":"刺客","建宁":{"公主":{"吴三桂":"熊"}},"龙儿":{"教主老婆":"教主"}}}
            }
        }

        字典嵌套查找的时候一定是按照键一层一层进行查
        # 温馨提示:千万不要高估自己

        常用的数据类型:

            1.字典
            2.列表
            3.字符串
            4.数字
"""