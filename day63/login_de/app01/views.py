from django.shortcuts import render, redirect, HttpResponse
from functools import wraps


# Create your views here.

def login_required(func):
    @wraps(func)
    def inner(request, *args, **kwargs):
        # is_login = request.COOKIES.get('is_login')
        # is_login = request.get_signed_cookie('is_login',salt='.',default=None)
        # 获取session
        print(request.session)
        is_login = request.session.get('is_login')

        print(is_login)
        if is_login != 'true':
            return redirect('/login/?returnUrl={}'.format(request.path_info))

        ret = func(request, *args, **kwargs)
        return ret

    return inner


def login(request):
    if request.method == 'POST':
        user = request.POST.get('user')
        pwd = request.POST.get('pwd')
        if user == 'alex' and pwd == 'alexdsb':
            # 保存登录状态  设置cookie
            return_url = request.GET.get('returnUrl')

            ret = redirect(return_url if return_url else '/home/')

            # ret['Set-Cookie'] = 'is_login=true; Path=/'
            # ret.set_cookie('is_login', 'true',path='/home/')  # Set-Cookie: is_login=true; Path=/
            # ret.set_signed_cookie('is_login', 'true', '.')  # Set-Cookie: is_login=true; Path=/
            # 设置session
            request.session['is_login'] = 'true'
            # request.session.set_expiry(20)

            # ret.set_cookie('user','alex')  # Set-Cookie: is_login=true; Path=/
            return ret
        return render(request, 'login.html', {'error': '用户名或密码错误'})

    return render(request, 'login.html')


@login_required
def home(request):
    print(request.session.session_key)
    request.session.clear_expired()  # 清空已经失效的session数据
    # print(request.COOKIES, type(request.COOKIES))

    # is_login= request.COOKIES.get('is_login')
    # if is_login != 'true':
    #     return redirect('/login/')

    return HttpResponse('home页面')


@login_required
def center(request):
    return HttpResponse('center页面')

def logout(request):
    ret = redirect('/login/')
    # ret.delete_cookie('is_login')
    # ret.set_cookie('is_login','',max_age=0)
    # request.session.pop('is_login')
    # request.session.delete()
    request.session.flush()
    return ret
