

### cookie

#### 定义：

​		保存在浏览器本地上的一组组键值对

#### 特性：

		1. 服务器让浏览器进行设置的 
  		2. 下次访问时自动携带相应的cookie

#### 在django的操作：

1. 获取

   request.COOKIES   {}  

   request.COOKIES[]   .get

   request.get_signed_cookie(key,salt='...',default='xxx')

2. 设置

   response.set_cookie(key,value)

   response.set_signed_cookie(key,value,salt='...')

3. 删除cookie

   response.delete_cookie(key) 

### session

#### 定义：

​		保存在服务器上的一组组键值对，必须依赖cookie

#### Django的使用：

1. 设置

​	  request.session[key] = value

2. 获取

   request.session[key]

   request.session.get(key)

3. 其他

   ```
   del request.session['k1']
   # 会话session的key
   request.session.session_key
   
   # 将所有Session失效日期小于当前日期的数据删除
   request.session.clear_expired()
   
   # 删除当前会话的所有Session数据
   request.session.delete()
   　　
   # 删除当前的会话数据并删除会话的Cookie。
   request.session.flush() 
   
   # 设置会话Session和Cookie的超时时间
   request.session.set_expiry(value)
       * 如果value是个整数，session会在些秒数后失效。
       * 如果value是个datatime或timedelta，session就会在这个时间后失效。
       * 如果value是0,用户关闭浏览器session就会失效。
       * 如果value是None,session会依赖全局session失效策略。
   ```

   

#### session的配置

from django.conf import global_settings

```
cookie名字
SESSION_COOKIE_NAME = 'sessionid'
超时时间
SESSION_COOKIE_AGE = 60 * 60 * 24 * 7 * 2
每次请求保存session
SESSION_SAVE_EVERY_REQUEST = False
浏览器关闭session数据失效
SESSION_EXPIRE_AT_BROWSER_CLOSE = False
存放session的位置
SESSION_ENGINE = 'django.contrib.sessions.backends.db'


```



