## 内容回顾

### js

#### 引入

```
在HTML中写标签
<script>
	js代码
</script>

引入文件

<script src='js文件' ></script>

//  link  href
```

#### 变量

```
var 声明变量
数字 下划线 字母  $  
	不能数字开通  不能使用关键
```

#### 基本的数据类型

##### number 

1      1.222  

##### string 

```
''   ""  
属性： length 
方法：trim()  charAt()  indexOf()  split()   slice()  concat()  
```

##### boolean

true   []  {} 

false     ''   null  undefined  NaN  

null     定义的空

undefined    未定义 

#### 内置的对象

##### Array

```
var a  =  [1,2,3,4,5]
var a = new Array()
a[1]  // 2 
a[1] = 3  
属性： length 
方法: push()  pop()  unshfit()  shift() concat()  join(',,')  slice() sort() reverse() 
splice(1,2,4,4,5,6,7) 
```

##### 自定义的对象

```
var  a = { 'name':'alex','age':84 }
a['name']  //  'alex'
a['name']  ='alexdsb'  //  'alex'
```

#### 数据类型的转换

```
数字转字符串
num.toString()
String(num)
字符串转数字
parseInt(s)   //  NaN  
parseFloat(s) 
Number（s）   //   null  _> 0   undefined NaN

转化成布尔值
Boolean()
```

#### 运算符

##### 赋值运算符 

= += -+ *= /=

##### 算数运算符

```
+ - * /  %  ++ --
var a = 1;
var b = a++    // b 1 a 2  
var a = 1;
var b = ++a    // b 2 a 2  
```

##### 比较运算

```
> < >= <= 
==  != 
===  !==
```

##### 逻辑运算 

```
或  ||    // or 
与  &&    // and 	
非  !     // not
```

##### 流程控制

if判断

```
if (条件){
 // 条件满足时执行的代码
}

if (条件){
    // 条件满足时执行的代码
}else {
	// 条件不满足时执行的代码
}


if (条件){
    // 条件满足时执行的代码
else  if (条件){
    // 条件满足时执行的代码
}  
}else {
	// 条件不满足时执行的代码
}

```

switch case 

```
switch (值){
	case 1 :
		// 要执行的代码
		break;
	case 2 :
		// 要执行的代码
		break;
	default:
		// 值不匹配的时候执行的代码
}
```

while

```
while (条件) {

	// 条件满足时执行的代码
}
```

do  - while

```
do {
	// 不管条件是佛满足先执行一次，如果满足条件继续执行的代码
}while (条件) 
```

for 

```
for ( i in arr ) {

	// i 获取到索引

}

for (var i=0;i<arr.length;i++) {

	// i 是自己声明的索引
	//  执行完代码后 i自增
}
```

三元运算：

```
var a =  条件 ？   条件成立时的值：条件不成立时的值
//   a =  条件成立时的值  if  条件 else 条件不成立时的值
```

#### 函数

```
function 函数名（参数）{

	// 函数体
	return 返回值  //  [1,2,3,4]
}
函数名（参数）

匿名函数
function （参数）{

	// 函数体
	return 返回值  //  [1,2,3,4]
}

自执行函数

（function 函数名（参数）{

	// 函数体
	return 返回值  //  [1,2,3,4]
}）（参数，参数）

//  argumenets  接受了所有的参数
```

#### 正则表达式

```
var reg = RegExp('[0-9]')
var reg = RegExp('\\d')
var reg = /\d/
reg.test('字符串')  //  判断字符中是否包含正则表达式的内容 

字符串的正则的应用
var  a ='alex is a big sb'
a.match(/a/)   // a.match(/a/g)    a.match(/a/gi)      g查找所有  i忽略大小写
a.search(/a/) // 匹配元素的索引
a.replace(/a/,'777') // 替换掉匹配了的元素

问题1：
var reg = /\d/g
reg.test('a1b2c3')
true
reg.test('a1b2c3')
true
reg.test('a1b2c3')
true
reg.test('a1b2c3')
false


问题2：
var reg = /\w{5,10}/
undefined
reg.test()  //  不写内容 就是'undefined'
true
```

## 今日内容

### DOM

document object model 文档对象模型

#### 查找元素

##### 直接查找

```
document.getElementById('div1')   // 通过ID查找  返回一个节点
document.getElementsByClassName('son')  // 通过类名查找  返回一个对象 对象中包含多个节点
document.getElementsByTagName('div')  /// 通过标签名查找 
```

##### 间接查找

```
节点.parentNode   // 找到父节点
节点.children    // 找子节点  多个节点 
节点.firstElementChild  // 找第一个子节点 
节点.lastElementChild // 找最后一个子节点 
节点.nextElementSibling  //找下一个兄弟节点
节点.previousElementSibling  //找上一个兄弟节点
```

##### 节点的属性操作

```
节点.getAttribute('属性')  //获取属性
节点.setAttribute('属性','值')  //设置属性
节点.removeAttribute('属性')  //删除属性
```

##### 节点的文本操作

```
节点.innerText  // 标签的文本内容
节点.innerHTML  // 标签内部的HTML文本

节点.innerText='设置的文本'    // 标签的文本内容
节点.innerHTML='设置HTML的文本'  // 标签内部的html的文本
```

##### 节点的值操作

```
//  input  select textarea
节点.value  // 获取节点的值 
节点.value = '值'  // 设置节点的值 

```

##### 节点样式的操作

```
节点.style // 所有的样式   只有行内样式才能拿多
节点.style.样式名称  
节点.style.样式名称   = '值'  // 设置样式
```

##### 节点类的操作

```
节点.classList                   // 节点所有的class 
节点.classList.add（'类名'）      // 给节点添加一个类
节点.classList.remove（'类名'）   // 给节点删除一个类
```

##### 节点的操作

```
document.createElement('标签的名字')   //  创建一个标签节点   div  a  span 

添加节点到文档中
父节点.appendChild(子节点)   // 添加一个子节点到父节点的后面
父节点.insertBefore(新节点,父节点的子节点)   // 添加一个子节点到父节中的指定子节点前

删除节点
父节点.removeChild(子节点) // 通过父节点删除子节点


替换节点
节点.replaceChild(新节点,节点的子节点)

复制节点
节点.cloneNode()   //  不写数字或者0 只拷贝节点
节点.cloneNode(1)   // 拷贝节点和它的子孙节点
```

#### 事件

##### 事件的绑定

```
// 方式一
<div onclick="alert('你点我了')">
    事件示例
</div>

// 方式二
<div onclick="func()">
    事件示例2
</div>


<script>
    function func() {
        alert('你点我了')
        alert('你还点')
    }

</script>

// 方式三
<div id="div1">
    事件示例3
</div>

<script>

    var div1 = document.getElementById('div1')


    div1.onclick = function () {
        alert('你点我了');
        alert('你还点');
        alert('你还点!!!');
    }


</script>
```

##### 事件的种类

| **属性**    | **当以下情况发生时，出现此事件** |
| ----------- | -------------------------------- |
| onblur      | 元素失去焦点                     |
| onchange    | 用户改变域的内容                 |
| onclick     | 鼠标点击某个对象                 |
| ondblclick  | 鼠标双击某个对象                 |
| onerror     | 当加载文档或图像时发生某个错误   |
| onfocus     | 元素获得焦点                     |
| onkeydown   | 某个键盘的键被按下               |
| onkeyup     | 某个键盘的键被松开               |
| onload      | 某个页面或图像被完成加载         |
| onmousemove | 鼠标被移动                       |
| onmouseout  | 鼠标从某元素移开                 |
| onmouseover | 鼠标被移到某元素之上             |
| onmouseup   | 某个鼠标按键被松开               |
| onreset     | 重置按钮被点击                   |
| onresize    | 窗口或框架被调整尺寸             |
| onselect    | 文本被选定                       |
| onsubmit    | 提交按钮被点击                   |

滚动事件：

```
<script>

    //实施监听滚动事件
    window.onscroll = function () {
        console.log(1111)
        console.log('上' + document.documentElement.scrollTop)
        console.log('左' + document.documentElement.scrollLeft)
        console.log('宽' + document.documentElement.scrollWidth)
        console.log('高' + document.documentElement.scrollHeight)
    }
</script>

```





### BOM

browser object  model 

窗口

```
打开新窗口
window.open(url,target)
关闭窗口 
window.close()  // 只能关闭用open打开的窗口

窗口的宽高
window.innerHeight  高度
window.innerWidth   宽度

```

定时器 **

```
定时器 ** 
// setTimeout  某段时间结束后触发执行
 function func(){
        alert('是否已满18岁')
    }
    
 setTimeout(func,2000)

// setInterval  设置时间间隔的定时器 每隔一段时间要触发执行函数
var ret= setInterval(func, 500);  // 开启定时器
clearInterval(ret)	// 关闭定时器

```

location **

```
location.href // 当前的地址
location.href = '地址'  // 当前页面会跳转到新页面
```

















