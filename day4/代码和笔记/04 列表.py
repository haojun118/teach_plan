# 啥是列表？
# 数据类型之一，存储数据，大量的，存储不同类型的数据
# list
# 定义一个列表
# lst = [1,2,"alex",True,["钥匙","门禁卡",["银行卡"]]]
# print(lst)

# 别的语言称为数组的就是Python中的列表
# 列表 -- 容器

# 列表是一种有序的容器    支持索引
# 列表是一种可变数据类型  原地修改

# lst = [1,2,3,4,3]

# 列表的增加：
    # lst.append(13)   # 追加  在最末尾的地方进行添加
    # print(lst)

    # lst.insert(2,"rimo")       # 插入
    # print(lst)
    #
    # lst.insert(0,"炮手")
    # print(lst)

    # lst.extend([1,2,3,4])       # 迭代添加
    # print(lst)


    # for i in [1,2,3,4]:   (了解)
    #     lst.append(i)
    # print(lst)        # [1, 2, 3, 4, 3, 1, 2, 3, 4]


# 列表的删除：

    # print(repr(lst.pop(2)))    # repr()查看当前数据的原生态
    # print(lst)

    # lst.clear()             # 清空
    # print(lst)

    # del lst[4]    # 通过索引删除
    # del lst[2:5]    # 通过切片删除
    # del lst[1:5:2]      # 通过步长删除
    # print(lst)

# 列表的修改：

    # lst = [1,2,3,4,5]
    # lst[2] = 80    # 通过索引进行修改
    # print(lst)

    # lst[1:3] = "20"  # 通过切片进行修改，默认步长为1，修改的内容必须是可迭代的对象，修改的内容可多可少
    # print(lst)

    # lst[1:5:2] = 100,100    # 步长不为1的时候，必须一一对应
    # print(lst)


# 列表的查：
    # for 循环
    # 索引
    # lst = [1,2,3,4,5]
    # for i in lst:
    #     print(i)

# （） 小括号 [] 中括号 {} 大括号
# a = "meet"  # 字符串中只要是占一个位置的就是一个元素
# lst = [11,2,2,3,4]  # 列表中只要用逗号隔开的就是一个元素

# 列表的嵌套：
# lst = [1,2,[3,4,5,["alex[]",True,[[1,2,]],90],"wusir"],"taibai"]
# lst1 = lst[2]   # [3, 4, 5, ['alex[]', True, [[1, 2]], 90], 'wusir']
# lst2 = lst1[3]  # ['alex[]', True, [[1, 2]], 90]
# str_1 = lst2[0]
# print(str_1[-1])
#
# print(lst[2][3][0][-1])

lst = ["国际章","阿娇",
       ['汪峰',"国际章","小苹果","小姨"],
       ["蔡徐坤",["篮球","姚明","林书豪"],
        ["唱","邓丽君","蔡国庆","腾格尔"],
        ["跳","蔡依林","罗志祥","赵四","蔡徐坤"],
        ["cnb","alex","rimo"]
        ],
       ["葫芦娃","黑猫警长","哆啦A梦","路飞"]
       ]

# print(lst[3][1][1])
# name = lst[3][-1][1].upper()
# # print(name)
# # lst[3][-1][1] = name
# # print(lst)

# lst[3][-1].append("ritian")
# print(lst)

