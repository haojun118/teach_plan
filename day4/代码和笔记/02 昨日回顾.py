# 1.整型
    # 10 - 2 print(bin())
    # 2 - 10 print(int("1110",2))

    # 布尔值 bool
        # bool(3)
        # bool("")
        # 数字中非零就是True
        # 字符串非空就是True
        # int(True)
        # int(False)
        # str(True)

# 2.字符串
#     索引
#         从左向右，从0开始数
#         从右向左，从-1开始数
#         索引时不能超出最大索引值
#     切片
#         print(变量名[起始位置：终止位置])   顾头不顾尾
#         print(变量名[:]) # 默认是从头到尾
#
#     步长
#         决定查找的方向
#         决定查找时查到的步子
#         步长默认为 1
#         【起始位置：终止位置：步长】

      # 切片和步长的时候可以超出界限
      # 切片的时候返回的结果，就是原数据本身
      

# 3.字符串方法
    # upper 全部大写
    # lower 全部小写
    # startswith 以什么开头
    # endswith 以什么结尾
    # count    计数，统计
    # split    分割 默认空格，换行符，制表符，分割次数
    # strip    脱 默认头尾两端的空格，换行符，制表符
    # replace  替换 第一个参数 旧的值，第二个参数 新的值，替换次数
    # format   格式化 1.按照顺序 2.按照索引 3.按照名字

    # is系列
    #     isdigit  判断是不是阿拉伯数字
    #     isdecimal 判断是不是十进制
    #     isalnum   判断是不是字母，数字，中文
    #     isalpha   判断是不是字母，中文



# 4.for循环
#     for 变量名 in 可迭代对象

# msg = "元宝dsb"
# for i in msg:
#     print(i)

# i = "alex"
# for i in msg:
#     pass
# print(i)

# 除了整型，布尔值其余的python数据类型都是可迭代对象