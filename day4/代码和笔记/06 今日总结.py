# 1.列表 -- list
# lst = [1,2,3,4,5,"alex"]
# 有序，可变
# 增：
#    append追加
#    insert 插入
#    extend 迭代添加

# 删：
#    remove通过元素名称删除
#    pop() 默认删除最后一个，并且有返回值，返回值就是被删除的元素
#    pop(2) 通过索引进行删除
#    clear 清理列表
#    del 切片删除

# 改：
#     lst[0] = 90 #通过索引进行修改
#     lst[1:3] = "alex" # 修改的值必须是可迭代对象，可多可少
#     lst[1:3:2] = "a" # 修改的时候，步长不为 1时 必须一一对应

# 查：
#     for i in list:
#         print(i)

# 列表的嵌套：
#     一层一层的查找，[...] 视为一个元素

# 2.元组 -- tuple
#     有序，不可变
    # 元组就是一个不可变的列表

    # for i in tuple:
    #     print(i)


# 3.range -- 范围
#     python2和python3的区别
#     range解决循环数字
#     range(起始位置，终值位置，步长)
#     range（终止位置） 默认起始位置是 0