"""
6.使用for循环对s="321"进行循环，打印的内容依次是：
"倒计时3秒"，"倒计时2秒"，"倒计时1秒"，"出发！"。
"""
# s="321"
# for i in s:
#     print(f"倒计时{i}秒")
# else:
#     print("出发")

"""
8.选做题：实现一个整数加法计算器（多个数相加）：
如：content = input("请输入内容:") 
用户输入：5+9+6 +12+ 13，然后进行分割再进行计算。
"""
# content = input("请输入内容:")
# lst = content.split("+")
#
# num = 0
# for i in lst:
#     num = num + int(i)
# print(num)

"""
9.计算用户输入的内容中有几个整数（以个位数为单位）。
如：content = input("请输入内容：") # 如fhdal234slfh98769fjdla
"""

# content = input("请输入内容：")
# # print(content,type(content))
# count = 0
# for i in content:
#     if i.isdecimal():
#         count += 1
# print(count)

# 10.写代码：计算 1 - 2 + 3 ... + 99 中除了88以外所有数的总和？

# num_sum = 0
# count = 1
# while count < 100:
#     if count % 2 == 1:
#         num_sum += count
#     else:
#         num_sum -= count
#     count += 1
# print(num_sum + 88)

"""
12.选做题：判断⼀句话是否是回⽂. 回⽂: 正着念和反着念是⼀样的. 例如, 上海⾃来⽔来⾃海上
"""
# msg = input("请输入内容：")
# if msg == msg[::-1]:
#     print("回文")
# else:
#     print("不是回文")


"""
13.制作趣味模板程序需求：等待⽤户输⼊名字、地点、爱好，
根据⽤户的名字和爱好进行任意现实 如：敬爱可亲的xxx，最喜欢在xxx地⽅⼲xxx
"""
# msg = "敬爱可亲的{0}，最喜欢在{2} 地⽅⼲{1} ".format('alex',"苞米地里","源码")
# print(msg)