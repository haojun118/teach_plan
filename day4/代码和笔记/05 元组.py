# python数据类型之一
# tuple
# 有序
# 不可变

# 定义方式：
# tu = (1,2,3)
# lst = [1,2,3]

# tu = (1,2,"alex",[1,3,4])
# print(tu)

# lst = [1,23,4,]
# tu = (1,2,3,4)
# print(lst[1:3])
# print(tu[1:3])

# tu = (1,2,3)
# for i in tu:
#     print(i)

# 元祖支持查询
# 元祖就是一个不可变的列表

# 元祖的方法
# 统计
# 获取索引


# tu = (1,2,3,4,5,1,2,1)
# print(tu.count(1))
# print(tu.index(2))  # 通过元素查询索引

# 元祖的用途：
# tu = (1,2,3)
# tu[2] = 80
# print(tu)

# 配置文件中

# 元祖的嵌套:
# tu = (1,2,3,4,(5,6,7,8,("alex","wusir",[1,23,4])))
# print(tu[4][4][0])

# range 范围
    # print(range(1,10))   # Python3中打印range是自己range自己本身
    # print range(1,10)    # Python2中打印range获取的是一个列表，列表的元素是1-9

# range(1,10)  # [起始位置：终止位置]  顾头不顾尾
# range(1,10,2)  # [起始位置：终止位置：步长] 默认为 1
# range(10) 　 # 10代表的是终止位置，起始位置默认为 0
# range是一个可迭代对象

# range的诞生是为了解决不能循环数字
# for i in range(2,10,2):
#     print(i)

# for i in range(0,100,2):
#     print(i)

# for i in range(1,100,2):
#     print(i)

# for i in range(100):
#     print(i)

# for i in range(100,-1,-1):
#     print(i)

# for i in range(100,-11,-1):
#     print(i)