# 索引 ： index / unique / primary key
    # 操作索引 ：创建和删除
        # 创建 ：create index 索引名 on 表名(字段名);
            # create index ind_id on s1(id);
            # create index ind_id2 on s1(id);
        # 删除 ：drop index 索引名  on 表名;
            # drop index ind_id2  on 表名;
    # 正确的使用索引（******）
        # 1.只有对创建了索引的列进行条件筛选的时候效率才能提高
        # 2.索引对应的列做条件不能参与运算、不能使用函数
        # 3.当某一列的区分度非常小(重复率高)，不适合创建索引
        # 4.当范围作为条件的时候，查询结果的范围越大越慢，越小越快
        # 5.like关键字 ： 如果使用%/_开头都无法命中索引
        # 6.多个条件 : 如果只有一部分创建了索引，条件用and相连，那么可以提高查询效率
                      # 如果用or相连，不能提高查询效率
            # and
                # select count(*) from s1 where id=1000000  and email = 'eva1000000@oldboy';
            # or
                # select count(*) from s1 where id=1000000  or email = 'eva1000000@oldboy';
        # 7.联合索引
            # creat index ind_mix on s1(id,name,email);
            # select count(*) from s1 where id=1000000  and email = 'eva1000000@oldboy';  快
            # select count(*) from s1 where id=1000000  or email = 'eva1000000@oldboy';   慢   条件不能用or
            # select count(*) from s1 where id=1000000;                                   快
            # select count(*) from s1 where email = 'eva1000000@oldboy';                  慢   要服从最左前缀原则
            # select count(*) from s1 where id>1000000  and email = 'eva1000000@oldboy';  慢   从使用了范围的条件开始之后的索引都失效
    # 基础概念（介绍）
        # explain 执行计划
            # explain select count(*) from s1 where id=1000000  and email = 'eva1000000@oldboy';
        # 覆盖索引
            # 在查询的过程中不需要回表 -- 覆盖索引 using index
            # explain select count(*) from s1 where id < 1000;
        # 索引合并
            # explain select count(*) from s1 where id=1000000  or email = 'eva1000000@oldboy';

















