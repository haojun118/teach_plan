$('.datetime').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:ss',
    autoclose: true,
    todayBtn: true,
    language: 'zh-CN'
});

$('.date').datetimepicker({
    format: 'yyyy-mm-dd',
    minView: 'month',
    autoclose: true,
    todayBtn: true,
    language: 'zh-CN'
})