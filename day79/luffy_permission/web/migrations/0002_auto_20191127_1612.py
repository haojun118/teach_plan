# -*- coding: utf-8 -*-
# Generated by Django 1.11.25 on 2019-11-27 08:12
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='date',
            field=models.DateField(default=datetime.datetime(2019, 11, 27, 8, 12, 25, 933796, tzinfo=utc), verbose_name='日期'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='payment',
            name='create_time',
            field=models.DateTimeField(verbose_name='付费时间'),
        ),
    ]
