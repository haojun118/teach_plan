from django.shortcuts import render, redirect, reverse, HttpResponse
from rbac import models
from rbac.service.permission_init import permission_init

def login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user_obj = models.User.objects.filter(username=username, password=password).first()
        if not user_obj:
            return render(request, 'login.html', {'error': '用户名或密码错误'})
        # 登录成功后 权限信息初始化
        permission_init(request,user_obj)

        return redirect(reverse('index'))

    return render(request, 'login.html')


def index(request):
    return render(request, 'index.html')
