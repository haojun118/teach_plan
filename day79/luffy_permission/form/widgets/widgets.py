from django import forms


class DateTimePickerInput(forms.DateTimeInput):
    template_name = 'forms/widgets/datetimepicker.html'
