data = [
    {'url': '/customer/list/', 'title': '客户列表', 'id': 1, 'pid': None},
    {'url': '/customer/add/', 'title': '添加客户', 'id': 2, 'pid': 1},
    {'url': '/customer/edit/(\\d+)/', 'title': '编辑客户', 'id': 3, 'pid': 1},
    {'url': '/customer/del/(\\d+)/', 'title': '删除客户', 'id': 4, 'pid': 1},
    {'url': '/payment/list/', 'title': '缴费列表', 'id': 5, 'pid': None},
    {'url': '/payment/add/', 'title': '添加缴费', 'id': 6, 'pid': 5},
    {'url': '/payment/edit/(\\d+)/', 'title': '编辑缴费', 'id': 7, 'pid': 5},
    {'url': '/payment/del/(\\d+)/', 'title': '删除缴费', 'id': 8, 'pid': 5}
]

dict = {
    1: {'url': '/customer/list/', 'title': '客户列表', 'id': 1, 'pid': None},
    2: {'url': '/customer/add/', 'title': '添加客户', 'id': 2, 'pid': 1},
    3: {'url': '/customer/edit/(\\d+)/', 'title': '编辑客户', 'id': 3, 'pid': 1},
    4: {'url': '/customer/del/(\\d+)/', 'title': '删除客户', 'id': 4, 'pid': 1},
    5: {'url': '/payment/list/', 'title': '缴费列表', 'id': 5, 'pid': None},
    6: {'url': '/payment/add/', 'title': '添加缴费', 'id': 6, 'pid': 5},
    7: {'url': '/payment/edit/(\\d+)/', 'title': '编辑缴费', 'id': 7, 'pid': 5},
    8: {'url': '/payment/del/(\\d+)/', 'title': '删除缴费', 'id': 8, 'pid': 5}
}


import json

ret = json.dumps(dict)
ret = json.loads(ret)
print(ret)