## 内容回顾

### css

#### overflow

```
溢出：
visible 可见
hidden  隐藏
auto  不超出时正常  超出了有滚动条
scroll 滚动条
```

#### 浮动

```
float ： left  right 
浮动起来脱离文档流  不占位置  
清除浮动：clear:both
伪元素清除法：
	.clearfix {
		content:'';
		clear:both;
		display:block;
	}
 overflow: hidden;  给父标签加
```

#### 定位

```
position: relative absolute fixed 
top 
left
rigt
bottom

相对定位: 相对于原来的位置的定位  占用原来的位置 
绝对定位： 相对于已经定位的父标签/html页面的位置的定位  不占原来的位置  
固定定位： 相对于窗口的位置  
```

#### z-index

```
显示层级的优先级  
1. 数值越大越在上 
2. 没有单位
3. 从父现象：父亲的z-index值小，自己的z-index值再大也没有用 
4. 浮动的元素没有z-index
```

### javascript 

​	 ECMAScript  标准化的规则    ES5  ES6 

​	 DOM   

​	 BOM 

#### 引入

```
方式一：
<script> 
   js代码
</script>

方式二：
xxxx.js
<script src='xxxx.js'></script>
```

#### js的标准

```
结束符  ; 
注释  单行注释 //
     多行注释  /* 多行   */
```

#### 变量

```
变量名   数字 字母 下划线 $    数字不能开头 区分大小写
定义一个变量  var 
var a ;  声明变量  undefined 
a= 1;
var a = 1;
```

#### 输入输出

```
alert('弹出的窗口')
console.log（‘xxxx’）

var  a = prompt('提示')
```

#### 基本数据类型

##### number

```
var a = 1;
var b = 1.1;
typeof(a)  number
NaN 
```

##### string

```
var a = '单引号'
var a = "双引号"
a[0]  
属性：  a.length 长度
方法： 
	trim()       左右去空白
	a.indexOf('a')   根据字符查找索引    
    a.concat('xxxxxx')   字符串的拼接
    a.slice(0,3)    切片
    a.split('',2)   分割   数字代表取前几个元素
    a.toUpperCase()  变大写
    a.toLowerCase()  变小写
```

##### boolean

```
true 
false
```

##### null

```
定义一个空 
```

##### undefined

```
声明了 但是没有定义
```

#### 内置对象类型

##### Array 数组

```
var a = [1,2,3,4,5];
var a = new Array();
a[0] =1;

属性： length 长度
方法：
	push  从后面插入
	unshfit  从前面插入
	pop   从后面删除  支持索引
	shfit  从前面删除
	a.concat('1','2') a.concat([1,2,3,4])  拼接列表
	a.slice(0,3) 切片
	a.join(',')  拼接元素
	a.sort() 排序
	a.reverse()  翻转
	a.splice(0,0,items)    删除的位置 删除的个数  替换的元素
	
```

##### 自定义对象

```
var a  = {}
var a = { name:'alex'  }
取值  a['name']
赋值  a['age'] = 73
```

## 今日内容

### 数据类型的转换

#### 字符串转数字

```
parseInt   字符串转整形
parseFloat   字符串转浮点型
Number(null)   0   
Number(undefined)  NaN
```

#### 数字转字符串

String(111.1111)

var a =  11
var b = a.toString()

#### 转布尔值

Boolean('')

```
true   []  {}
false  0 ‘’  null  undefined
```

### 运算符

#### 赋值运算符 

```
= += -= *= /=
```

#### 算数运算符

```
+ - * /  % 
a++  ++a  自加1 
```

#### 比较运算符

```
>  <  >=  <=
==  !=
===  !==
// != NaN 值为true
```

字符串 + 数字   字符串  

字符串 - 数字  =  数字

#### 逻辑运算

或 与 非  

与 1 && 2

或 false || true 

非 !true

#### 流程控制

```
条件 （）
代码块 {}
```

##### if

```
    if (2>1){
        console.log(1)
    }

    if (2 < 1) {
        console.log(1)
    } else {
        console.log(2)
    }
    
    
    if (2 < 1) {
        console.log(1)
    } else if (2 > 1) {
        console.log(3)
    } else {
        console.log(2)
    }
```

##### case switch

```
    var error_num = 1;

    switch (error_num) {
        
        case 1:
            console.log(1);
            break;
        case 2:
            console.log(2);
            break;
        default:
            console.log('xxxx');
    }
```

##### while 

```
while(i<=9){ //判断循环条件
    console.log(i);
    i++; //更新循环条件
}
```

##### do - while  至少执行一次

```
do{

    console.log(i)
    i++;//更新循环条件

}while (i<10) 
```

##### for 

```
var a = [1,2]

for (i in a ){
	//  i 索引
	//  a[i]
}

for (var i=0;i<a.length;i++){
   //  a[i]	
}
```

##### 三元运算

```
var c = a>b ? a:b  //如果a>b成立返回a，否则返回b
```

#### 函数

```
python 

def 函数名():
	函数体
	return 1
函数名()

js 
function  函数名(参数){
	函数体
    return 返回值

}

arguments 伪数组   接受所有的参数

匿名函数

function (参数){
	函数体
    return 返回值
}

自执行函数
(函数)(参数，参数)

函数
函数（参数）
var c = (function (a,b){
	console.log(arguments)
	console.log(a,b)
	return 1
})(1,2)


    function add(a, b) {
        console.log(arguments);
        console.log(a, b);
        return 1
    }
    add(1.2)
            
    (function (a, b) {
        console.log(arguments);
        console.log(a, b);
        return 1
    })(1, 2)
```

#### 正则表达式

```
var reg = RegExp('\\d')   
var reg = /\d/
reg.test('sss1')   能匹配成功返回true 不能返回false


字符串中使用正则
var a = 'alex is a dsb'
var reg = /a/

a.match(reg)  从左到右匹配一次 
a.match(/a/g) g 代表获取所有
a.match(/a/ig)  i 忽略大小写
a.search(/a/)   获取索引值 只找一个  匹配到就是索引 匹配不到就是-1
a.replace(/a/ig,'123')   替换  ig替换所有并且忽略大小写


问题1：
var reg =  /\d/g
reg.test('a1b2c3')
true
reg.test('a1b2c3')
true
reg.test('a1b2c3')
true
reg.test('a1b2c3')
false

问题2 
var reg =  /\d/
reg.test()   #不写内容相当于写的undefined
```





















































​	