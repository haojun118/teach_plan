from django.shortcuts import render,HttpResponse,redirect
from app01.models import User

# Create your views here.

def login(request):
    print(request.method,type(request.method))
    if request.method == 'GET':
        return render(request,'login.html')
    else:
        # post请求
        # 获取用户提交的数据
        # print(request.POST,type(request.POST))
        username = request.POST.get('username')
        password = request.POST.get('password')
        # print(username,type(username))
        # 验证用户名和密码
        # if username == 'alex' and password == 'alex3714':
        if User.objects.filter(username=username,password=password):

            # 验证成功 跳转到首页（重定向）
            return redirect('/index/') # 自己网站的地址
            # return redirect('http://www.baidu.com')
        else:
             # 验证失败 返回登录页面
            return render(request,'login.html')


def index(request):
    ret = User.objects.all()  # 对象列表  【  对象 】
    # print(ret,type(ret))
    # for i in ret:
    #     print(i.username,i.password)
    # ret = User.objects.get(username='alexs',password='alexdsb')   # 对象  get  只能获取数据库唯一存在的数据  有且唯一 （不存在或者多条数据就报错）
    # print(ret,type(ret))
    # print(ret.username,ret.password)

    ret  = User.objects.filter(password='dsb')  # 获取满足条件的所有对象  对象列表
    ret  = User.objects.filter(password='sb')  # 获取满足条件的所有对象
    print(ret)


    return HttpResponse('这就是首页')
