## 内容回顾

### HTTP

规定请求和响应的格式。

### 请求方法

8种  GET POST  (  PUT  DELETE  HEAD OPTIONS TRACE  CONNECT )

### 状态码

1xx   请求已接受，进一步进行处理

2xx   请求已经接受，也正常处理

3xx   重定向

4xx   请求的错误  403   404  

5xx   服务器的错误 

### URL 

https://www.sogou.com/web?query=苍老师&_asf=www.sogou.com

协议 ：  https 

域名： www.sogou.com   ip 

端口：  https （:443）   http （:80）

路径： /web     

查询的参数：  ?后面   k1=v1&k2=v2

### 请求和响应的格式

请求（浏览器给服务端发的消息） request 

 		‘请求方式 路径 HTTP/1.1

​		  k1：v1

​		  k2：v2

​		  

​		  请求数据（请求体）’

响应（服务器返回给浏览器的消息） response

​		'HTTP/1.1 状态码 状态描述

​		  k1：v1

​		  k2：v2

​		  

​		  响应数据（响应体）’'

#### 请求和响应的步骤

1. 在浏览器的地址栏种输入URL地址，回车，发个一个GET请求；
2. Django接受到请求，根据URL的路径找到对应的函数，执行函数拿到结果
3. Django将结果封装成http响应的报文返回浏览器
4. 浏览器接受响应，断开连接，解析数据。

#### 框架的功能

1. socket收发消息
 	2. 根据不同路径返回不同的结果（返回HTML页面）
 	3. 返回动态的页面（ 模板的渲染  字符串的替换 ）

#### Django部分

 1. #### 下载安装

    命令行：

    ​	pip install django==1.11.25  -i  源

    pycharm:

	2. 创建项目

    命令行：

    ​	django-admin startproject 项目名

    pycharm

    区别： pycharm创建时帮忙创建一个templates的文件夹，配置了模板的路径

3. #### 启动项目

   命令行：

   ​	cd 项目的根目录下

   ​	python  manage.py runserver    #  127.0.0.1:8000

   ​    python  manage.py runserver 80    #  127.0.0.1:80

   ​    python  manage.py runserver 0.0.0.0:80    #  0.0.0.0:80

   pycharm：

   ​	点绿三角   相关的配置

4. #### 使用：

   ```
   1. 设计URL 和 函数的对应关系  urls.py
       urlpatterns = [
          url(r'^index/', index),
   	]
   2. 写函数
   	from django.shortcuts import HttpResponse,render  
   	def 函数名（request）:
   		# 逻辑 
   		# 查询数据库 插入
   		# 返回信息给浏览器
   		return HttpResponse('字符串')
   		return render(request，‘html文件名’)
   
   ```

   

## 今日内容

### 1.静态文件的配置

settings：

```
STATIC_URL = '/static/'   # 静态文件的别名  以它做开头

STATICFILES_DIRS = [   #  静态文件存放的具体路径
    os.path.join(BASE_DIR,'static',)
]
```
http://www.jq22.com

### 2.登录示例

### form表单提交数据

1. 
action=""   向当前的地址进行提交   method="post"   请求方式
2. input需要有name属性  value值
3. button按钮或者input 类型是submit

在settings中注释一行代码，注释后可以提交post请求

```
MIDDLEWARE
'django.middleware.csrf.CsrfViewMiddleware' 
```

```  
request.method  请求方式 GET POST
request.POST    POST请求的数据  {} 
redirect（‘要重定向的地址’）   重定向 
```

### 3.app

#### 创建app

python manage.py startapp app名称

#### 注册app

```
INSTALLED_APPS = [
  	... 
    'app01',
    'app01.apps.App01Config'    # 推荐写法
]
```

#### app文件目录

````
admin.py   admin管理后台  增删改查数据库
apps.py    app相关
models.py  数据库表相关
views.py   写函数（逻辑）
````

### 4.ORM

#### 对应关系

类          ——》         表

对象       ——》       数据行（记录）

属性      ——》          字段

#### django要使用mysql数据库流程

1. 创建一个mysql数据库；

2. 在settings中配置数据库信息：

   ```python
   DATABASES = {
       'default': {
           'ENGINE': 'django.db.backends.mysql',   #  引擎  数据库类型
           'NAME': 'day53',				# 数据库名称
           'HOST': '127.0.0.1',			# ip
           'PORT': 3306,					# 端口
           'USER': 'root',					# 用户名
           'PASSWORD': '123'				# 密码
       }
   }
   ```

3. 告诉django使用pymysql模块连接数据库

   ```python
   import pymysql   
   pymysql.install_as_MySQLdb()   # 能执行就可以
   ```

4. 在app下写了model

   ```python
   class User(models.Model):
       username = models.CharField(max_length=32)  # username   varchar(32)
       password = models.CharField(max_length=32)  # username   varchar(32)
   
   ```

5. 执行命令  让数据有对应的结构的变化

   python  manage.py   makemigrations     #  检查所有已经注册的app下的models.py的变化 记录成一个文件

   python  manage.py    migrate   

#### ORM操作

```python
from app01.models import User
User.objects.all()  #  查询所有的数据   QuerySet 对象列表  【  对象 】
User.objects.get(username='alexs',password='alexdsb')  
# 对象  get  只能获取数据库唯一存在的数据  有且唯一 （不存在或者多条数据就报错）

User.objects.filter(password='dsb')  # 获取满足条件的所有对象  对象列表
```









​	



