##  内容回顾

### 1.MVC和MTV

MVC:

​	M: model  模型  和数据库交互

​	V:  view   视图  展示数据  HTML

​	C:  controller  控制器 业务流程 传递指令

MTV：

​	M:  model 模型  ORM

 	T：template 模板  HTML 

​	 V :  view  视图   业务逻辑  

### 2.模板

####  变量

render（request，‘模板的文件名’，{ k1:v1 }）

{{ 变量 }}    

{{ 变量.0 }}    

{{ 变量.key }}    

{{ 变量.keys }}     {{ 变量.values }}     {{ 变量.items }}    

{{  变量.属性  }}  {{  变量.方法  }}

优先级：

  字典的key   >   方法或者属性  >  数字索引

#### 过滤器

修改变量的显示结果

语法：

{{  变量|filter_name  }}   {{  变量|filter_name:参数 }}

内置的过滤器

default   变量不存在或者为空的时候使用默认值  {{  变量|default   ：‘默认值’ }}

add    +    数字的加法  字符串和列表的拼接

length     返回变量的长度  

filesizeformat  文件大小格式化  byte PB

upper  lower  join 

slice  切片    {{  list|slice:'1:2:2'  } } 

date 日期时间格式化  {{  日期时间类型|date:'Y-m-d H:i:s'  }}     datetime:   Y-m-d  H:M:S

​		settings:

​		USE_L10N =  False

​		DATETIME_FORMAT = 'Y-m-d H:i:s'

​		DATE_FORMAT = 'Y-m-d'

safe   前面的内容不用转义  在模板中使用

```
from django.utils.safestring import mark_safe  # py文件中用
```

#### 自定义过滤器

 1. 在app下创建一个名为templatetags的python包;

 2. 在包内创建py文件 (任意指定，my_tags.py) 

	3. 在py文件中写代码

    ```
    from  django import template
    register = template.Library()   #  register名字不能错
    ```

	4. 写函数 +  加装饰器

    ```
    @register.filter
    def str_upper(value,arg):
    	return  ''  
    ```

使用：

​		模板中使用：

```
{% load my_tags  %}
{{ 变量|str_upper：‘参数’ }}
```

## 今日内容

{% %}    tag

### for

```

{%  for i in list %}
	{{ forloop.counter }}
	{{ i }}
{% endfor  %}

{{ forloop.counter }}      循环的索引 从1开始
{{ forloop.counter0 }}     循环的索引 从0开始
{{ forloop.revcounter }}   循环的索引(倒叙) 到1结束
{{ forloop.revcounter0 }}   循环的索引(倒叙) 到0结束
{{ forloop.first }}        判断是否是第一次循环  是TRUE
{{ forloop.last }}         判断是否是最后一次循环  是TRUE
{{ forloop.parentloop }}   当前循环的外层循环的相关参数
```

```
 {% for tr in table %}
            <tr>
                {% for td in tr %}
                    <td > {{ td }} </td>
                {% endfor %}
            </tr>
        {% empty %}
            <tr> <td colspan="4" >没有数据</td> </tr>
{% endfor %}
```

### if 

```
{% if alex.age == 84 %}
    当前已经到了第二个坎了
{% elif alex.age == 73 %}
    当前已经到了第一个坎了
{% else %}
    不在坎上，就在去坎上的路上
{% endif %}

```

注意：

	1. 不支持算数运算  + - * / % 
 	2. 不支持连续判断    10 > 5 > 1  false

### with

```
{% with p_list.0.name as alex_name %}
    {{ alex_name }}
    {{ alex_name }}
    {{ alex_name }}
    {{ alex_name }}
{% endwith %}

{% with alex_name=p_list.0.name   %}
    {{ alex_name }}
    {{ alex_name }}
    {{ alex_name }}
    {{ alex_name }}
{% endwith %}
```

### csrf  跨站请求伪造

```
{% csrf_token %}  form表单中有一个隐藏的input标签  name='csrfmiddlewaretoken' value 随机字符串
可以提交post请求
```

### 母版和继承

母版：

	1. 就是一个HTML页面，提取到多个页面的公共部分；
 	2. 定义block块，留下位置，让子页面进行填充

继承：

	1. 写{% extends  ‘母版的名字’ %}
 	2. 重写block块

注意点：

​	1. {% extends 'base.html' %}   母版的名字有引号的  不带引号会当做变量

2.  {% extends 'base.html' %} 上不要写内容，想显示的内容要写在block块中
3. 多定义点block块，有css  js 

### 组件

一小段HTML代码   ——》 nav.html

```
{% include 'nav.html' %}
```

### 静态文件

```
{% load static %}
{% static '相对路径' %}

{% get_static_prefix %}    获取静态文件的别名
```

### simple_tag  和  inclusion_tag

自定义simple_tag：

1. 在app下创建一个名为templatetags的python包;

2. 在包内创建py文件 (任意指定，my_tags.py) 

3. 在py文件中写代码

   ```
   from  django import template
   register = template.Library()   #  register名字不能错
   ```

4. 写函数 +  加装饰器

   ```
   @register.simple_tag
   def str_join(*args, **kwargs):
       return '*'.join(args) + '_'.join(kwargs.values())  
       
       
   @register.inclusion_tag('page.html')
   def page(num):
       return {'num':range(1,num+1)}
       
   # 在templates写page.html
   ```

使用：

​		模板中使用：

```
{% load my_tags  %}
{{ 变量|str_upper：‘参数’ }}

{% str_join '1' '2' '3' k1='4' k2='5'  %}

{% page 10 %}
```