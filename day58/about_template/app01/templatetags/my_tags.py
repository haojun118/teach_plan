from django import template

register = template.Library()


@register.filter
def str_upper(value, arg='dsb'):
    ret = value + arg
    return ret.upper()


@register.filter
def cheng(a, b):
    try:
        a = int(a)
        b = int(b)
        return a * b
    except Exception:
        return 'xxx'


from django.utils.safestring import mark_safe


@register.filter
def show_a(url, name):
    return mark_safe('<a href="http://{}">{}</a>'.format(url, name))


@register.simple_tag
def str_join(*args, **kwargs):
    return '*'.join(args) + '_'.join(kwargs.values())
