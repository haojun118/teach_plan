from django.shortcuts import render
import datetime

# Create your views here.
def index(request):
    class Person():
        def __init__(self, name, age):
            self.name = name
            self.age = age

        def __str__(self):
            return '< Person obj: {} >'.format(self.name)

        __repr__ = __str__

        def sing(self):
            return '骑上我心爱的小摩托，它永远不会堵车~'

    num = 1
    string = 'yinmo'
    boolean = True
    name_list = ['yinmo羊', '炮手', '枪手', '操羊']
    dic = {
        'name': 'yinmo',
        'age': 40,
        'hobby': ['抠脚', '装样', '嫂子', '弟妹'],
        # 'keys':'xxxxxxxxx'
    }
    name_set = {'yinmo羊', '炮手', '枪手', '操羊'}

    alex = Person('alex', 38)
    peiqi = Person('peiqi', 84)
    p_list = [alex,peiqi]
    now = datetime.datetime.now()
    context = {
        'num': num,
        'string': string,
        'boolean': boolean,
        'name_list': name_list,
        'dic': dic,
        'name_set': name_set,
        'alex': alex,
        'p_list':p_list,
        'filesize':1*1024*1024*1024*1024*1024*1024,
        'now':now,
        'a':'<a href="http://www.baidu.com">baidu</a>',
        'baidu':'www.baidu.com',
        # "table": [name_list,name_list,name_list]
        "table": []

    # 'yinmo':False, # []  {}  ''  False

    }

    return render(request, 'index2.html', context)


def form(request):
    return render(request,'form.html')