# 存储引擎
    # innodb ： 持久化存储，数据和索引存在一个文件，表结构存一个文件
        # 支持事务 行级锁 表锁 外键
    # myisam ：持久化存储 数据和索引、表结构分三个文件存
        # 支持表锁
    # memory ：只能做缓存 表结构存一个文件 数据存在内存中
# 约束
    # unsigned 无符号的数字
    # not null
    # default
    # unique
        # 联合唯一
    # primary key
        # 联合主键
    # auto_increment
    # foreign key
        # on update cascade

# truncate table 表名;

