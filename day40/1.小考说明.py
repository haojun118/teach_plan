# 6\9 10   5分   60满  36分及格
# 1.create database day40;
# 2.show databases;
# 3.use day40;
# 4.show tables;
# 5.select database();
# 6.建表
# create table student(
# sid int primary key auto_increment,
# sname char(16) not null
# gender enum('male','female') default 'male',
# age tinyint unsigned,
# hobby set('喝酒','烫头'),
# class char(12),
# start_date date,
# teacher char(16),
# salary float(7,2)
# );
# 7.insert into student values(1,'日魔','male',28,'喝酒,烫头','py26','2019-7-26','宝元',10000);
# 8.update student set salary = 10500 where id=1;
    # update student set salary = salary + 500;
# 9.三张表
# 学生表
# create table stu(
# sid int primary key auto_increment,
# sname char(16) not null
# gender enum('male','female') default 'male',
# age tinyint unsigned,
# hobby set('喝酒','烫头')，
# class_id int,
# foreign key(class_id) references class(cid)
# )
# 班级表
# create table class(
# cid int primary key auto_increment,
# cname char(12),
# start_date date
# teacher_id int,
# foreign key(teacher_id) references teacher(tid)
# );
# 老师表
# create table teacher(
# tid int primary key auto_increment,
# teacher char(16),
# salary float(7,2)
# );

# 10.mysql函数(15个)
# avg sum min max count now year month day
# concat concat_ws group_concat
# database user password

