# create table
# drop table
# desc 表名;
# alter table

# alter table 表名 rename 表名;
# alter table 表名 charset 编码;
# alter table 表名 auto_increment 自增的位置;

# alter table 表名 add 字段名 类型(长度) 约束;
# alter table 表名 drop 字段名;

# alter table 表名 change 字段名 新名字 类型(长度) 约束;
# alter table 表名 modify 字段名 新类型(新长度) 约束;

# name id age
# alter table 表名 change id id 类型(长度) 约束 first;
# alter table 表名 change id id 类型(长度) 约束 after age;
# alter table 表名 add 字段名 类型(长度) 约束 first;
# alter table 表名 add 字段名 类型(长度) 约束 after name;