# 磁盘预读性原理
    # 1个block块 4096个字节/9ms

# 树
    # 树 根节点 分支节点 叶子节点
    # 平衡树 balance tree - B树

# 聚集索引/聚簇索引 ： 叶子节点会存储整行数据 —— innodb的主键
# 辅助索引/非聚集索引 ：除了主键之外的普通索引都是辅助索引，一个索引没办法查到整行数据，需要回聚集索引再查一次(回表)

# b+树 是为了更好的处理范围问题在b树的基础上有所优化
# mysql中innodb存储引擎的所有的索引树都是b+树
