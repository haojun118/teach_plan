# class People:
#
#     def __init__(self,name):
#         self.name = name
#
#     def open(self,bx):
#         bx.open_door(self)
#
#     def close(self,bx):
#         bx.close_door(self)
#
#
# class Refrigerator:
#
#     def __init__(self,name):
#         self.name = name
#
#     def open_door(self,p):
#         print(f"{p.name} 打开冰箱")
#
#     def close_door(self,p):
#         print(f"{p.name} 关闭冰箱")
#
#
# r = People("日魔")
# aux = Refrigerator("奥克斯")
# r.open(aux)
# r.close(aux)


# class People:
#
#     def __init__(self,name):
#         self.name = name
#
#     def eat(self,food,flag):
#         food.eat(self,flag)
#
#
# class Food:
#
#     def __init__(self,name):
#         self.name = name
#
#     def eat(self,p,f):
#         if f:
#             print(f"{p.name} 吃了 {self.name}")
#         else:
#             print(f"{p.name} 消化了 {self.name}")
#
#
# p = People("日魔")
# f = Food("大煎饼")
# p.eat(f,False)


# 总结:将一个类的对象当做参数传递到另一个类中使用 -- 依赖关系

# 2.组合关系

# class Boy:
#
#     def __init__(self,name):
#         self.name = name
#
#     def eat(self):
#         print(f"{self.name}和{self.girl} 一起吃了个烛光晚餐!")
#
#     def make_keep(self):
#         print(f"{self.name}带着{self.girl}去做俯卧撑!")
#
# b = Boy("日魔")
# b.girl = "乔bi萝"
# # b.eat()
# b.make_keep()


# class Boy:
#
#     def __init__(self,name,g):
#         self.name = name    # self = b
#         self.g = g          # g就是girl类实例化的一个对象内存地址
#
#     def eat(self):
#         print(f"{self.name}和{self.g.age}岁,且{self.g.weight}公斤的{self.g.name}py朋友.一起吃了个烛光晚餐!")
#
#     def make_keep(self):
#         self.g.live(f"{self.g.weight}公斤的{self.g.name}给{self.name}踩背")
#
#
# class Girl:
#
#     def __init__(self,name,age,sex,weight,*args):
#         self.name = name
#         self.age = age
#         self.sex = sex
#         self.weight = weight
#         self.args = args
#
#     def live(self,argv):
#         print(f"直播内容:{argv}")
#
#
# g = Girl("乔毕萝",54,"女",220)
# b = Boy("太正博",g)
# b.make_keep()


#总结: 将一个类的对象封装到另一个类的对象属性中