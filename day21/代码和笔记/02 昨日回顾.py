# 面向对象:
    # 类: 某一种东西的统称或概述
    # 对象:对一个东西的具体描述

# 面向对象的优点:
    # 1.结构清晰,可读性高
    # 2.上帝思维

# 类的结构:

    # class 类名(驼峰体):
        # 静态属性（静态字段，类变量）
        # 方法    （动态字段，动态属性）


# 查看类中所有内容
    # 类名.__dict__

# 查看对象中所有内容
    # 对象.__dict__

# 万能的点
    # 增,
    # 类名.变量名 = "值"
    # 删,
    # del 类名.变量名
    # 改,
    # 类名.变量名 = "值"
    # 查
    # 类名.变量名

# 类名() # 实例化一个对象

# 实例化一个对象,给对象开辟一个空间
# 实例化对象时自动执行__init__方法
# 将对象的参数地址传递给self 隐性传递,给对象封装属性

# self 是什么？
# self是一个方法中的位置参数
# self的名字给进行更改,但是不建议更改
# self 就是实例化对象的本身(self和对象指向的是同一个内存地址)

# 一个类可以实例化多个对象
# 对象空间之间是独立的
# 对象只能使用类中的属性和方法,不能进行修改

# 不建议使用类名操作方法
# 查找顺序:

# 先找对象空间,空间中没有找实例化这个对象的类空间(对象中记录实例化这个对象的类地址)