# 1.给对象空间添加属性
# class A:
#
#     def __init__(self,name):
#         # 类里边给对象添加属性
#         self.name = name
#
#     def func(self,sex):
#         self.sex = sex
#
# a = A("meet")
# a.func("男")
# # 类外边给对象添加属性
# a.age = 18
# print(a.__dict__)

# 总结:给对象空间添加属性可以在类的内部,类的外部,类中的方法

# 2.给类空间添加属性

# class A:
#
#     def __init__(self,name):
#         # 类内部给类空间添加属性
#         A.name = name
#
#     def func(self,age):
#         # 类中的方法给类空间添加属性
#         A.age = age

# 类外部给类空间添加属性
# A.name = "alex"
# a = A('meet')
# a.func(19)
# print(A.__dict__)

# 总结:给类空间添加属性可以在类的内部,类的外部,类中的方法

# class B:
#
#     def __init__(self,name):
#         self.name = name
#
#     def index(self):
#         print(self.name,"is index")
#
# b = B("alex")
# b.index()