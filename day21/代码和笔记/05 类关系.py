# 1.依赖关系
# 主 -- 人
# 次 -- 冰箱

# class People:
#
#     def __init__(self,name):
#         self.name = name
#
#     def open(self,bx):
#         bx.open_door(self)
#
#     def close(self,bx):
#         bx.close_door(self)
#
#
# class Refrigerator:
#
#     def __init__(self,name):
#         self.name = name
#
#     def open_door(self,p):
#         print(f"{p.name} 打开冰箱")
#
#     def close_door(self,p):
#         print(f"{p.name} 关闭冰箱")
#
#
# r = People("日魔")
# aux = Refrigerator("奥克斯")
# r.open(aux)
# r.close(aux)


