# 模拟英雄联盟写一个游戏人物的类（升级题).
# 要求:
#
# 创建一个 Game_role的类.
# 构造方法中给对象封装name,ad(攻击力),hp(血量).三个属性.
# 创建一个attack方法,此方法是实例化两个对象,互相攻击的功能:
# 例: 实例化一个对象 盖伦,ad为10, hp为100
# 实例化另个一个对象 剑豪 ad为20, hp为80
# 盖伦通过attack方法攻击剑豪,此方法要完成 '谁攻击谁,谁掉了多少血, 还剩多少血'的提示功能.


# class GameRole:

#     def __init__(self,name,ad,hp):
#         self.name = name
#         self.ad = ad
#         self.hp = hp
#
#     def attack(self,obj1):
#         obj1.hp = obj1.hp - self.ad  # obj1.hp = 70
#         print(f"{self.name} 攻击力 {obj1.name} {obj1.name}掉了{self.ad}滴血 还剩{obj1.hp}血")

# top = GameRole("盖伦",10,100)
# mid = GameRole("剑豪",20,80)

# top.attack(mid)
# top.attack(mid)
# top.attack(mid)

