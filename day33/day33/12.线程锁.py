import threading
import time


num = 0
# 线程锁
lock = threading.Lock()


def task():
    global num
    # # 申请锁
    # lock.acquire()
    # num += 1
    # time.sleep(0.2)
    # print(num)
    # # 释放锁
    # lock.release()

    with lock:
        num += 1
        time.sleep(0.2)
        print(num)

for i in range(10):
    t = threading.Thread(target=task)
    t.start()