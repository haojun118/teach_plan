class Foo:
    instance = None

    def __init__(self, name):
        self.name = name
    def __new__(cls, *args, **kwargs):
        # 返回空对象
        if cls.instance:
            return cls.instance
        cls.instance = object.__new__(cls)
        return cls.instance

obj1 = Foo('日魔')
obj2 = Foo('SB')

print(obj1,obj2)