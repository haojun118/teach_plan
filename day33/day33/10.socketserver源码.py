import socketserver


class MyRequestHandler(socketserver.BaseRequestHandler):

    def handle(self):
        data = self.request.recv(1024)
        print(data)


obj = socketserver.ThreadingTCPServer(('127.0.0.1',9000),MyRequestHandler)
obj.serve_forever()

