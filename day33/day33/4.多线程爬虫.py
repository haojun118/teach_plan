import requests

url_list = [
	'https://www3.autoimg.cn/newsdfs/g26/M02/35/A9/120x90_0_autohomecar__ChsEe12AXQ6AOOH_AAFocMs8nzU621.jpg',
	'https://www2.autoimg.cn/newsdfs/g30/M01/3C/E2/120x90_0_autohomecar__ChcCSV2BBICAUntfAADjJFd6800429.jpg',
	'https://www3.autoimg.cn/newsdfs/g26/M0B/3C/65/120x90_0_autohomecar__ChcCP12BFCmAIO83AAGq7vK0sGY193.jpg'
]

# 串行示例
# for url in url_list:
# 	ret = requests.get(url)
# 	file_name = url.rsplit('/',maxsplit=1)[-1]
# 	with open(file_name,mode='wb') as f:
# 		f.write(ret.content)

# 基于多线程
import threading
def task(arg):
	ret = requests.get(arg)
	file_name = arg.rsplit('/', maxsplit=1)[-1]
	with open(file_name, mode='wb') as f:
		f.write(ret.content)

for url in url_list:
	# 实例化一个线程对象
	t = threading.Thread(target=task,args=(url,))
	# 将线程提交给cpu
	t.start()