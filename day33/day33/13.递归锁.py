import threading
import time


num = 0
# 线程锁
lock = threading.RLock()


def task():
    global num
    # 申请锁
    lock.acquire()
    num += 1
    lock.acquire()
    time.sleep(0.2)
    print(num)
    # 释放锁
    lock.release()
    lock.release()


for i in range(10):
    t = threading.Thread(target=task)
    t.start()