## 内容回顾

### DOM

#### dom树

```
整个HTML就是一个树   多个节点
节点含的内容：本身的内容 属性 关系
```

#### 查找节点

##### 直接查找

```
var 节点 = document.getElementById('id')  //通过id查找  返回一个
var 节点 = document.getElementsByClassName('类型')  // 通过类名查找   返回多个 按照索引拿一个
var 节点 = document.getElementsByTagName('div')   //通过标签名查找   返回多个 按照索引拿一个
```

##### 间接查找

```
节点.children    // 多个子节点
节点.firstElementChild // 父节点下的第一个子节点
节点.lastElementChild // 父节点下的最一个子节点

节点.parentNode  //  父节点

节点.nextElementSibling  // 下一个兄弟节点
节点.previousElementSibling  // 下一个兄弟节点
```

#### 属性的操作

```
节点.getAttribute('属性名')  //  获取属性
节点.setAttribute('属性名','属性值')  //  设置属性
节点.removeAttribute('属性名')  //  删除属性
```

##### 文本的操作

```
节点.innerText   // 节点中的内容   
节点.innerText = '文本'   // 节点中的内容   不识别标签 

节点.innerHTML  // 节点中的HTML文本  
节点.innerHTML = '文本'   // 节点中的HTML文本   识别标签 
```

##### 值的操作

```
input   select  textarea 
节点.value  // 获取节点的值
节点.value=‘值’  // 设置节点的值
```

##### 样式的操作

```
节点.style.样式属性   // 获取样式的值  只能获取行内样式
节点.style.样式属性 = '值'// 设置样式的值 
```

##### 类的操作

```
节点.classList  //  查找类的名称 
节点.classList.add（'类名'）  // 添加
节点.classList.remove（'类名'）  // 删除
节点.classList.toggle（'类名'）  //  转换  存在的时候删除，没有的时候添加
节点.classList.contains（'类名'）  // 包含
```

#### 节点的操作

```
document.createElement('标签名')  // 创建标签节点
父节点.appendChild(子节点)  //  添加节点到父节点后面
父节点.insertBefore(子节点，父节点中的子节点  )  //  添加节点到某个子节点的前面
父节点.removeChild（子节点）  // 通过某个父节点删除子节点
父节点.replaceChild(新节点，替换的节点)   // 替换节点
节点.cloneNode(值) //  复制节点   值0 只复制该节点  值1  复制所有的子孙节点
```

#### 事件

##### 绑定方式

```
<button id='b1' ><button/>
方式一：
<script>
	var b1 = document.getElementById('b1')
	b1.onclick = function (){	
     	// 点击事件后要执行的代码	
	}
</script>
方式二：
<button id='b1' onclick='fn()'><button/>

<script>
	function fn(){
	
		// 点击事件后要执行的代码	
	}
</script>
```

##### 常用的事件 

```
onclick      鼠标单击
onfocus      获取焦点
onblur       失去焦点
onmouseover  鼠标悬浮
onmouseout   鼠标移出
onsubmit     提交表单触发
onslecet     下拉框选中
onchange     文本下拉框改变时
onkeyup      键松开
onload       文档图片加载完成
onscroll     滚动条滚动
```

### BOM

#### 窗口

```
window.open('url地址',打开方式)
window.close()   //  只能关闭用open打开的窗口
```

#### 定时器 **

```
//  每隔一段时间要执行一次操作
var 定时器的id = setInterval(函数，毫秒数)   // 开启定时器  每隔一段时间执行函数
clearInterval（定时器的id）   // 关闭定时器

//  一段时间后再执行一次操作
var 定时器的id = setTimeout(函数，毫秒数)   // 开启定时器  时间到了再执行函数
clearTimeout（定时器的id）   // 关闭定时器
```

#### location **

```
location.href    // 当前的地址
location.href ='新的地址'   // 跳转到新地址

location.reload()  // 刷新页面
```

#### history

```
history.back() // 后退 
history.forward() // 前进 
history.go()  // 0刷新  -1 后退  1 前进
```

## 今日内容

https://www.cnblogs.com/maple-shaw/articles/6972913.html

### jQuery的优点：

1. 浏览器的兼容性好

2. 隐式循环
3. 链式编程 

### Jquery对象和DOM对象的关系 、转换

```
Jquery对象内部封装了DOM对象、方法

DOM对象 ——》 Jquery对象
Jquery（DOM对象）     $（DOM对象）  
Jquery对象 ——》 DOM对象
Jquery对象[索引]      $（DOM对象）[] 

```

Jquery 和 $ 是一个东西

### jQuery的选择器

#### 基本选择器

```
id选择器  类选择器  标签选择器 通用选择器

$('#id')
$('.类名')
$('标签名')
$('*')  

交集选择器 
$('li.city#l1')
并集选择器
$('div,li,a')
```

#### 层级选择器

```
后代选择器 空格  子代选择器 >   毗邻选择 +  弟弟选择器 ~
$('ul li')     // 选择ul 下的所有li标签
$('ul>a')      //选择ul 下的子代中的a标签
$('#l1+li')	   //选择id为l1的标签后的一个li标签
$('#l1~li')	   //选择id为l1的标签后的所有的li标签
```

#### 属性选择器

```
$('[属性]')
$("标签[属性]")   // 选择某个包含某个属性的标签
$('[属性=“值”]')
$('[属性$=“值”]')  // 属性结尾为某个值的标签
$('[属性^=“值”]')  // 属性开头为某个值的标签
$('[属性*=“值”]')  // 属性包含某个值的标签
```

### jQuery的筛选器

#### 基本筛选器

````
$('选择器:筛选器') 
$('选择器:first') 
$('选择器:last') 
eq(索引)  等于某索引的标签
gt(索引)  大于某索引的标签
lt(索引)  小于某索引的标签
odd    奇数
even   偶数
not(选择器)  // 在当前的标签内排除某些标签
````

#### type筛选器

```
$(':text')
$(':radio')
$(':checkbox')
$(':password')
$(':submit')
$(':button')
$(':file')

注意： date 不支持
```

#### 状态选择器

```
$(':disabled')   // 禁用的标签
$(':enabled')   //  可使用的标签
$(':checked')    // radio、checkbox、select（option） 选中的标签
$(':selected')   // select 选中的option标签
```

### jQuery的筛选器方法

```
.children()  // 子代
.parent()   //  父代
.parents()   // 父辈们
.parentsUntil('选择器')   // 找父辈们，直达某个标签，且不包含该标签

.siblings()  // 所有兄弟标签
.prev()   // 上一个
.prevAll()   // 前面所有的兄弟
.prevUntil()   // 前面的兄弟到某个标签位置


.next()   // 下一个
.nextAll()   // 后面所有的兄弟
.nextUntil()   // 后面的兄弟到某个标签位置
```

```
first()
last()
eq()
has()  //  $('li').has('a')    选择一个包含a标签的li标签
find()  //  $('li').find('a')  在li标签下找所有的a标签
not()  //  在所有的标签内排除某些标签
filter()  // 获取一些满足条件的标签  交集选择器
```













