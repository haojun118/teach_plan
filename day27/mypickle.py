# import  pickle
class MyPickle:
    def __init__(self,path,mode='load'):
        self.path = path
        self.mode = 'ab' if mode=='dump' else 'rb'

    def __enter__(self):
        self.f = open(self.path, mode=self.mode)
        return self

    def dump(self,content):
        pickle.dump(content,self.f)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.f.close()

    def __iter__(self):
        while True:
            try:
                yield  pickle.load(self.f)
            except EOFError:
                break
#
# with MyPickle('file','dump') as f:
#      f.dump({1,2,3,4})
#
# with MyPickle('file') as f:
#     for item in f:
#         print(item)
import pickle

class Course:
    def __init__(self,name,price,period):
        self.name = name
        self.price = price
        self.period = period
python = Course('python',19800,'6 months')
linux = Course('linux',19800,'6 months')


with MyPickle('course_file') as p:
    for obj in p:
        print(obj.__dict__)
# with MyPickle('course_file','dump') as p:
#     p.dump(python)
#     p.dump(linux)

# with open('course_file','ab') as f:
#     pickle.dump(linux,f)

# with open('course_file','rb') as f:
#     while True:
#         try:
#             obj = pickle.load(f)
#             print(obj.__dict__)
#         except EOFError:
#             break
