# 面向对象
# 类
    # 类变量
    # 实例方法
    # 类方法  @classmethod    默认传一个cls表示本类 可以被类\对象调用
    # 静态方法 @staticmethod  没有默认的参数,可以被类\对象调用
    # 属性 @property

# 实例化
    # 首先使用new开辟一块空间,并返回
    # 再执行init方法,self参数就是new的返回值
# 对象
    # 实例变量 对象属性
# 三大特性
    # 依赖 :放到方法里
# class A:
#     def __eq__(self, other):
#         pass
    # 组合 :放到属性里
    # 继承 :
        # 单继承 : 自己有就用自己的 自己没有就用父类的
                # 自己有但还想用父类的 super调用 也可以直接用类名调用
        # 多继承 :
            # 经典类 : 2.x不继承object
                # 继承 :深度优先算法
                # 没有mro算法
                # super的用法和新式类不同
                    # super(子类名,子类对象).方法()
            # 新式类 : 继承object
                # 继承 :广度优先算法 C3算法
                # 有mro和super简便用法
            # 私有的成员不能被继承
    # 多态
        # 一个类描述出的多种形态
        # class Foo:pass
        # class User(Foo):pass
        # class VipUser(Foo):pass
        # alex = User()
        # wusir = VipUser()
        # # print(type(alex))
        # def pay(user):pass
        # pay(wusir)
    # 封装
        # 广义封装 : 对某一类事物的抽象,保护这些方法
        # 私有成员 : __名字
                # 不能被继承 也不能在类的外部使用
# 反射 :写作业的时候要用上
    # hasattr
    # getattr
    # setattr
    # delattr
# 双下方法
    # __new__  构造方法 单例模式
    # __del__  析构方法 删对象的之前会自动执行
# class A:
#     def __init__(self):
#         self.f = open('文件')
#     def __del__(self):
#         self.f.close()
# a = A()
# del a
    # __call__ : 对象()  # flask框架
        # 类()()
    # __str__  __repr__
        # print str %s   实现了str走__repr__,没实现走__repr__
        # repr %r 只走__repr__
# class A:
#     def __repr__(self):
#         return 'aaaa'
# a = A()
# print(repr(a))
    # __enter__ __exit__
        # https://www.cnblogs.com/Eva-J/articles/11447907.html
    # __len__
        # len()
    # __eq__
        # ==
    # __hash__
        # 一个算法 经过这个算法对一个对象进行计算会得出一个(在本次执行中的)唯一值
        # 这个唯一值是一个数值,标识这个对象所在的内存地址
class Person:
    def __init__(self,name,age,sex):
        self.name = name
        self.age = age
        self.sex = sex
    def __hash__(self):
        return hash(self.name+self.sex)
    def __eq__(self, other):
        if self.name == other.name and self.sex == other.sex:return True

p_lst = []
for i in range(84):
    p_lst.append(Person('egon',i,'male'))
print(p_lst)
print(set(p_lst))
    # __getitem__ __setitem__ __delitem__
        # []
    # __iter__
        # 是否可迭代
# class 类:
#     类变量 = 123
#     变量2 = []
#     def __init__(self):
#         pass
#     def wahaha(self):
#         pass
#
# 对象 = 类()
# print(对象.类变量)
# 对象.类变量 = 456
# 对象.变量2.append(456)
# print(对象.__dict__)
# print(类.__dict__)
# 尝试在作业中使用类方法 静态方法
