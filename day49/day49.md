## 补充内容

1. 循环多个元素时；

   ```
   var ul_li =  $('li')
   for (let i=0;i<ul_li.length;i++){  //  let 声明的变量只在代码块中生效
         console.log('li',ul_li[i])
    }
   ```

2. 局部变量 全局变量 

   ```
   function f() {
       var b = 2;   // 局部变量
       let c =3;    //  在当前代码块生效
       d = 4;
   }
   
   ```

## 内容回顾

jquery

```
jquery是一个高度封装了js的库
jq 封装 dom对象\方法
$ == jQuery   
```

### 引入方式

```
<script src="jquery.3.4.1.js"> </script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"> </script>
```

```
$('选择器') jQuery('选择器')

dom对象 —— 》  jq对象
$(dom对象)


jq对象 —— 》 dom对象
jq对象[index]
```

### jquery选择器

$('选择器')

#### 基本选择器

```
通用选择器  $('*')
标签选择器  $('div')  
id选择器   $('#id')
类选择器   $('.类名')

交集选择器  $('div.类名') $('div#id')
并集选择器  $('div,.类名') 
```

#### 层级选择器

```
后代选择器  $('div p')
子代选择器  $('div>p')
毗邻选择器  $('div+p')  
弟弟选择器  $('div~p')  
```

#### 属性选择器

```
$('[属性]')
$('[属性="值"]')
$('[属性!="值"]')
$('[属性$="值"]')
$('[属性^="值"]')
$('[属性*="值"]')
```

### jquery筛选器

#### $('选择器:筛选器')

```
:first  第一个
:last   最后一个
:eq(index)  按照索引取
:gt(index)  索引大于index的对象
:lt(index)  索引小于index的对象
:odd   奇数
:even  偶数
:not(‘选择器’)   排除选择器的其他的对象 排除某些
:has（'选择器'）  获取一个包含子代对象的父对象
```

#### 表单

```
:text  password radio checkbox submit button file reset date(用不了)
:disabled  enabled   checked(radio\checkbox\select(option)) selected(option) 

$(':checkbox:checked')  // 选择已选中的checkbox
$(':selected')
```

### jquery筛选器方法

```
子代: .children()
父亲： .parent（）  .parents()  .parentsUntil('选择器') 
兄弟： siblings()
弟弟： next()  nextAll()  nextUntil('选择器')
哥哥： prev()  prevAll()  prevUntil('选择器')
 
first
last
eq
has     获取一个包含子代对象的父对象
not     排除选择器的其他的对象 排除某些
filter  交集选择器
find    找子代
```

## 今日内容

### 事件的绑定

```
<button>按钮1</button>
<button>按钮2</button>

<script>
    $('button').click(function () {
        alert('你点我干啥')
        alert('点你咋地')
        alert('再点一个试试')
    })

</script>

```

### jQuery对象的操作

#### 文本的操作

```
.text()     // 获取标签的内容
.text(内容)  // 设置标签的内容


.html()       // 获取标签的内容
.html(HTML标签文本)  // 设置标签的内容
.html(DOM对象)  // 设置标签的内容
.html(jq对象)  // 设置标签的内容
```

#### 标签的操作

```
添加   //  js  父节点.appendChild(新的节点)  父节点.insertBefore(新的节点,参考节点) 
父子关系：
	添加到后面
    a.append（b）     在a节点孩子中添加一个b
    // a 父节点  b 新添加的子节点
    // a 必须是jq对象 b  （标签字符串、dom对象、jq对象 ）

    a.appendTo(b)    把a节点添加到b的孩子中
    // b 父节点  a 新添加的子节点
    // a 必须是jq对象  b  （选择器、dom对象、jq对象 ）
    添加到前面
    a.prepend(b)   b.prependTo(a)
兄弟关系：
	a.after(b)    在a的后面添加一个b   b.insertAfter(a)   把b添加到a的后面
	a.before(b)   在a的前面添加一个b   b.insertBefore(a)  把b添加到a的前面
 
	
// 操作同一个对象时，操作多次，相当于是移动的效果。
```

```
删除 remove detach empty

remove  // 删除标签并且删除事件  返回值是标签对象
detach  // 删除标签，但是保留事件  返回值是标签对象
empty   // 清空标签内容，但是保留标签本身

替换

a.replaceWith(b)   //  用b替换a标签
a.replaceAll(b)    //  用a替换所有的b  (选择器、jq对象、dom对象)

拷贝
clone(false)  // 只拷贝标签
clone(true)  // 拷贝标签也拷贝事件
```

#### 属性的操作

##### 普通属性

```
.attr('属性')  // 获取属性的值
.attr('属性','值')  // 设置单个属性的值
.attr({'属性'：'值'，'属性2':'值'})  // 设置多个属性的值

removeAttr('属性')   // 删除单一属性
```

##### 值

```
input  select textarea 

val（）  // 获取值
val（'值'）  // 设置值


//  radio  checkbox  select  是否选中使用prop 不见识attr
.prop('属性')  // 获取属性的值
.prop('checked',true)  // checked 变为选中状态
.prop('checked',false)  // checked 变为未选中状态

```

#####   类  

```
addClass()     //  添加类 
removeClass()  //  删除类
toggleClass()  // 切换类
```





