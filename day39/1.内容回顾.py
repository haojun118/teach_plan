# pymysql
# 建立连接
    # conn = pymysql.connect(host='ip',user = '用户名',password='密码'，database='库')
# 获取游标
    # cur = conn.cursor()
# 增删改查
    # cur.execute(sql语句,参数)
    # 查
    # cur.fetchone()/cur.fetchmany(n)/cur.fetchall()
    # 增删改
    # conn.commit() 提交让数据的修改生效
    # conn.rollback() 回滚 让之前执行的【未提交】的sql回归到没执行的状态
# 关闭连接和游标,归还操作系统资源
    # cur.close
    # conn.close

# sql注入 ： -- 注释之后的代码

# 多表查询
    # 连表查询
        # 内连接 inner join
            # select * from 表1,表2 where 表1.字段1 = 表2.字段2;
            # select * from 表1 inner join 表2 on 表1.字段1 = 表2.字段2;
        # 外连接
            # 左外连接 left join
                # select * from 表1 left join 表2 on 表1.字段1 = 表2.字段2;
            # 右外连接 right join
                # select * from 表1 right join 表2 on 表1.字段1 = 表2.字段2;
            # 全外连接 full join
                # select * from 表1 left join 表2 on 表1.字段1 = 表2.字段2
                # union
                # select * from 表1 right join 表2 on 表1.字段1 = 表2.字段2
    # 子查询
        # select * from 表 where 字段 = (select ....);