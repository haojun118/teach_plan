# not null  非空约束
# create table t1(id int not null,name char(12));
    # 默认插入0
# create table t2(id int,name char(12)  not null);
    # 默认插入空字符串

# default 默认值 default
# create table t3(id int,name char(12),sex enum('male','female') default 'male');

# 非空约束 和 默认值
# create table t3(id int not null,name char(12) not null,sex enum('male','female') not null default 'male');

# unique唯一约束 (不能重复)
# create table t4(id int unique,name char(12));

# 联合唯一约束
# create table t5(family char(12),name char(12),unique(family,name));
# create table t5(family char(12) not null,name char(12) not null,unique(family,name));  # 约束各自不能为空 且联合唯一


# 唯一+非空 id name
# create table t6(id int not null unique, name char(12) not null unique);
# pri 是怎么产生的？ 第一个被设置了非空+唯一约束会被定义成主键 primary key
# 主键在整张表中只能有一个

# 主键
# create table t6(id int primary key, name char(12) not null unique);
# create table t5(family char(12) ,name char(12),primary key(family,name));  # 约束各自不能为空 且联合唯一 还占用了整张表的主键

# 对某一列设置自增 auto_increment(自动增加,not null约束)
# create table t6(id int auto_increment, name char(12));   # 报错
# create table t7(id int unique auto_increment, name char(12) primary key) ;
# create table t8(id int primary key auto_increment, name char(12)) ;
# create table t9(id int unique auto_increment, name char(12)) auto_increment=100000;

# delete from t7; 清空表数据但不能重置auto_increment
# truncate table t7;  # 清空表并且重置auto_increment

# 所有的操作都无法改变auto_increment的自动计数。但是我们也没有必要去改变它。
    # 1.至少要看到自增的效果
    # 2.至少写3条数据 4，5，6
    # 3.删掉第5条，再看结果
    # 4.再insert一条数据
    # 5.删掉第5条，再看结果
    # 6.再insert一条数据
    # 7.清空整张表
    # 8.再insert一条数据，再看结果

# 修改auto_increment
    # alter table 表名 auto_increment = n; 修改表的auto_increment
    # alter table t7 auto_increment = 1000; 修改表的auto_increment

# 外键
    # 没有建立外键：
    # create table stu(id int,name char(12),class_id int);
    # create table class(cid int,cname char(12));
    # insert into stu values (1,'日魔',1),(2,'炮手',1)
    # insert into class values(1,'py27');
    # insert into class values(2,'py28');
    # select * from stu,class where class_id = cid;
    # delete from stu where id = 1；
    # delete from class where cid = 1;

    # stu2 class2
    # create table class2(cid int unique,cname char(12));
    # create table stu2(id int,name char(12),class_id int,foreign key(class_id) references class2(cid));
    # insert into class2 values(1,'py27');
    # insert into stu2 values (1,'日魔',1),(2,'炮手',1)
    # delete from class2 where cid = 1;
    # insert into class2 values(2,'py28');
    # update class2 set cid = 1 where cid = 2;  不能修改

    # stu3 class3 级联更新
    # create table class3(cid int primary key,cname char(12));
    # create table stu3(id int,name char(12),class_id int,foreign key(class_id) references class3(cid) on update cascade);
    # insert into class3 values(1,'py27');
    # insert into stu3 values (1,'日魔',1),(2,'炮手',1)
    # update class3 set cid = 2; 修改了class3中的cid，stu3中相关的数据也会跟着变化，是on update cascade设置导致的


