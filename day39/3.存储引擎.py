# show create table books;

# show engines;

# 什么是存储方式、存储机制（存储引擎）
    # 表结构 存在一个文件中  : 硬盘上
    # 表数据 存在另一个文件中、内存中
    # 索引(目录) 为了方便查找设计的一个机制 ：

# 存储引擎的种类
    # innodb ： 索引+数据 表结构  数据的持久化存储
        # 事务 ：一致性 n条语句的执行状态是一致的
            # begin；   # 开启事务
            # select id from innot where id =1 for update;
            # update innot set id = 2 where id = 1；
            # commit；  # 提交事务 解锁被锁住的数据，让他们能够被修改
        # 行级锁 ：只对涉及到修改的行加锁，利于并发的修改，但是对于一次性大量修改效率低下
        # 表级锁 ：一次性加一把锁就锁住了整张表，不利于并发的修改，但是加锁速度比行锁的效率要高
        # 外键约束 ：被约束表中的数据不能随意的修改/删除 约束字段据要根据被约束表来使用数据
    # myisam ： 索引 数据 表结构  数据的持久化存储
        # 表级锁
    # memory ： 表结构
        # 数据断电消失

# create table innot(id int) engine = innodb;
# create table myist(id int) engine = myisam;
# create table memot(id int) engine = memory;









