import os
import json
import socket

LOCAL_DIR = r'D:\Python_s25\day30\2 作业讲解\client\local'

sk = socket.socket()
sk.connect(('127.0.0.1',9001))

# 选择 要做的操作 是上传 还是下载
opt_lst = ['上传','下载']
for index,opt in enumerate(opt_lst,1):
    print(index,opt)
inp = int(input('>>>'))
if inp == 1:  # 选上传
    path = input('请输入要上传的文件路径 : ')# 用户输入要上传的文件
    if os.path.isfile(path) :# 检测这个文件是否存在
        filename = os.path.basename(path) # 获取文件名
        filesize = os.path.getsize(path)  # 获取文件大小
        opt_dic = {'filename':filename,'filesize':filesize,'operate':'upload'} # 把请求发过去
        json_opt = json.dumps(opt_dic)
        sk.send(json_opt.encode('utf-8'))
        # filesize = 16926596596198
        with open(path,'rb') as f:
            while filesize>0:
                content = f.read(4096)
                sk.send(content)
                filesize -= 4096
elif inp == 2: # 选下载
    # remote文件夹中的所有文件你都可以下载
    filename = input('请输入要下载的文件名 : ')  # 用户输入一个文件名
    opt_dic = {'filename': filename,'operate': 'download'}
    opt_bytes = json.dumps(opt_dic).encode('utf-8')
    sk.send(opt_bytes)# 把文件名发送到server端
    str_dic = sk.recv(1024).decode('utf-8')# 接收文件的大小
    size_dic = json.loads(str_dic)
    filepath = os.path.join(LOCAL_DIR,filename)
    with open(filepath,'wb') as f:
        while size_dic['filesize'] > 0:
            content = sk.recv(1024)
            f.write(content)
            size_dic['filesize'] -= len(content)





