import time
import socket

sk = socket.socket()
sk.bind(('127.0.0.1',9001))
sk.listen()

while True:
    conn,addr = sk.accept()
    n = 0
    while True:
        conn.send(str(n).encode('utf-8'))
        n+=1
        time.sleep(0.5)