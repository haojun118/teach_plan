import time
from socketserver import BaseRequestHandler,ThreadingTCPServer
# BaseRequestHandler 基础请求操作符
# ThreadingTCPServer 线程实现的基于tcp协议的server
class Myserver(BaseRequestHandler):
    def handle(self):
        n = 0
        while True:
            self.request.send(str(n).encode('utf-8'))  # self.request == conn
            n += 1
            time.sleep(0.5)

server = ThreadingTCPServer(('127.0.0.1',9001),Myserver)
server.serve_forever()