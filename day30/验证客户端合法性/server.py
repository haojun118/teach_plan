import os
import socket
import hashlib
SECRET_KEY = b'alexbigsb'

def check_client(conn):
    randbytes = os.urandom(32)
    conn.send(randbytes)

    md5 = hashlib.md5(SECRET_KEY)
    md5.update(randbytes)
    code = md5.hexdigest()
    code_cli = conn.recv(32).decode('utf-8')
    return code == code_cli

sk = socket.socket()
sk.bind(('127.0.0.1',9001))
sk.listen()
while True:
    conn,addr = sk.accept()
    if not check_client(conn):continue
    print('进程正常的通信了')

# import os
# import hmac  # hashlib
# randbytes = os.urandom(32)
# mac = hmac.new(SECRET_KEY,randbytes)
# ret = mac.digest()
# print(ret)
