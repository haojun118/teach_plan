# name = "alexdsb"
# print(name.find("dsb"))

import re
# findall 返回的是列表
# print(re.findall("\w","宝元-alex_dsb123日魔吃D烧饼"))   \w 字母.数字.下划线.中文            ***
# print(re.findall("\W","宝元-alex_dsb123日魔吃D烧饼"))   \w 不是字母.数字.下划线.中文     ***

# print(re.findall("\d","十10⑩"))                       # \d 匹配数字                        ***
# print(re.findall("\D","十10⑩"))                       #\D 匹配非数字                       ***


# print(re.findall("^a","alex"))                        # 以什么开头                         ***
# print(re.findall("x$","alex"))                        # 匹配什么结尾                       ***

# print(re.findall("a.c","abc,aec,a\nc,a,c"))           # 匹配任意一个字符串(\n除外)           ***
# print(re.findall("a.c","abc,aec,a\nc,a,c",re.DOTALL))


# print(re.findall('[0-9]',"alex123,日魔dsb,小黄人_229"))                                     ***
# print(re.findall('[a-z]',"alex123,日魔DSB,小黄人_229"))
# print(re.findall('[A-Z]',"alex123,日魔DSB,小黄人_229"))

# [0-9]  # 取0-9之前的数字
# [^0-9] # 取非 0-9之间的数字
# print(re.findall("[^0-9a-z]","123alex456"))                                                 ***


# print(re.findall("a*","alex,aa,aaaa,bbbbaaa,aaabbbaaa"))    # 匹配*左侧字符串0次或多次  贪婪匹配  ***
# print(re.findall("a*","alex,aa,aaaa,bbbbaaa,aaabbbaaa"))

# print(re.findall("a+","alex,aa,aaaa,bbbbaaa,aaabbbaaa"))  匹配左侧字符串一次或多次  贪婪匹配      ***

# print(re.findall("a?","alex,aa,aaaa,bbbbaaa,aaabbbaaa"))  # 匹配?号左侧0个或1个 非贪婪匹配        ***

# print(re.findall("[0-9]{11}","18612239999,18612239998,136133333323")) # 指定查找的元素个数        ***

# print(re.findall("a{3,8}","alex,aaaabbbaaaaabbbbbbaaa,aaaaaaaaabb,ccccddddaaaaaaaa"))            ***


# print(re.findall("<a>(.+)</a>","<a>alex</a> <a>wusir</a>"))              分组                     ***
# print(re.findall("<a>(.+?)</a>","<a>alex</a> <a>wusir</a>"))             控制贪婪匹配              ***

# print(re.findall("\n","alex\nwusir"))
# print(re.findall("\t","alex\twusir"))


# print(re.findall("\s","alex\tdsbrimocjb"))            # \s 匹配空格
# print(re.findall("\S","alex\tdsbrimocjb"))            # \s 匹配非空格

# print(re.findall("\Aa","asfdasdfasdfalex"))
# print(re.findall("d\Z","asfdasdfasdfalex"))

# print(re.findall("alex","alex\twusiralex"))

# print(re.findall("a.c","abc,aec,a d,a,c"))
# print(re.findall("^a.............c$","abc,aec,a d,a,c"))

# print(re.findall('[0-9a-zA-Z]',"alex123,日魔DSB,小黄人_229"))
# print(re.findall('[-0-9]',"alex-123,日魔DSB,小黄人_229"))

# a|b  或

# print(re.findall("a|b","alexdsb"))

# print(re.findall("a(.?)c","alc,abc,adc,a c"))

# print(re.findall("<a>(.)</a>","<a>alex</a> <a>wusir</a>"))
# print(re.findall("<a>(.+)</a>","<a>alex</a> <a>wusir</a>"))

# print(re.findall("<a>(.+)</a>","<a>alex</a> <a>wusir</a>"))
# print(re.findall("<a>(.+?)</a>","<a>alex</a> <a>wusir</a>"))
# print(re.findall("<a>(?:.+?)</a>","<a>alex</a> <a>wusir</a>"))

# 有如下字符串:'alex_sb ale123_sb wu12sir_sb wusir_sb ritian_sb' 的 alex wusir '
# 找到所有带_sb的内容

# s = 'alex_sb ale123_sb wu12sir_sb wusir_sb ritian_sb'
# print(re.findall("(.+?)_sb",s))
# print(re.findall("(.+)_sb",s))

# 面试题:
# search 和 match 区别
# search 从任意位置开始查找
# match 从头开始查看,如果不符合就不继续查找了
# group()进行查看

# print(re.search("a.+","lexaaaa,bssssaaaasa,saaasaasa").group())
# print(re.match("a.+","alexalexaaa,bssssaaaasa,saaasaasa").group())

# split  --  分割

# print(re.split("[:;,.!#]","alex:dsb#wusir.djb"))

# sub -- 替换
# s = "alex:dsb#wusir.djb"
# print(re.sub("d","e",s,count=1))

# compile 定义匹配规则
# s = re.compile("\w")
# print(s.findall("alex:dsb#wusir.djb"))


# s = re.finditer("\w","alex:dsb#wusir.djb")   # 返回的就是一个迭代器
# print(next(s).group())
# print(next(s).group())

# for i in s:
#     print(i.group())

import re   # (给分组命名)
# ret = re.search("<(?P<tag_name>\w+)>\w+</\w+>","<h1>hello</h1>")
# ret = re.search("<(?P<tag_name>\w+)>(?P<content>\w+)</\w+>","<h1>hello</h1>")
# print(ret.group("tag_name"))
# print(ret.group("content"))

# print(ret.group("tag_name"))
# print(ret.group())

# \. 没有任意的功能了

# s = "1-2*(60+(-40.35/5)-(-4*3))"
# print(re.findall("\d+",s))
# print(re.findall("\d+\.\d+|\d+",s))
# print(re.findall("-\d+\.\d+|-[0-9]|\d+",s))

s1  = """
<div id="cnblogs_post_body" class="blogpost-body ">
    <h3><span style="font-family: 楷体;">python基础篇</span></h3>
<p><span style="font-family: 楷体;">&nbsp; &nbsp;<strong><a href="http://www.cnblogs.com/guobaoyuan/p/6847032.html" target="_blank">python 基础知识</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/p/6627631.html" target="_blank">python 初始python</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<strong><a href="http://www.cnblogs.com/guobaoyuan/articles/7087609.html" target="_blank">python 字符编码</a></strong></strong></span></p>
<p><span style="font-family: 楷体;"><strong><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/6752157.html" target="_blank">python 类型及变量</a></strong></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/p/6847663.html" target="_blank">python 字符串详解</a></strong></span></p>
<p><span style="font-family: 楷体;">&nbsp; &nbsp;<strong><a href="http://www.cnblogs.com/guobaoyuan/p/6850347.html" target="_blank">python 列表详解</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/p/6850496.html" target="_blank">python 数字元祖</a></strong></span></p>
<p><span style="font-family: 楷体;">&nbsp; &nbsp;<strong><a href="http://www.cnblogs.com/guobaoyuan/p/6851820.html" target="_blank">python 字典详解</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<strong><a href="http://www.cnblogs.com/guobaoyuan/p/6852131.html" target="_blank">python 集合详解</a></strong></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/7087614.html" target="_blank">python 数据类型</a>&nbsp;</strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/p/6752169.html" target="_blank">python文件操作</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/p/8149209.html" target="_blank">python 闭包</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/6705714.html" target="_blank">python 函数详解</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/7087616.html" target="_blank">python 函数、装饰器、内置函数</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/7087629.html" target="_blank">python 迭代器 生成器</a>&nbsp;&nbsp;</strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/6757215.html" target="_blank">python匿名函数、内置函数</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/7087635.html" target="_blank">python 异常 反射</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/6796766.html" target="_blank">python 异常详解</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/7087637.html" target="_blank">python 模块</a>一</strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/6769197.html" target="_blank">python 模块二</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/6776134.html" target="_blank">python 模块三</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/p/8351052.html" target="_blank">python 正则详解</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/6798013.html" target="_blank">python 模块与包</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/7087643.html" target="_blank">python 类和对象</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/7087649.html" target="_blank">python 类的进阶</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/p/8340114.html" target="_blank">python 类的总结</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/6802776.html" target="_blank">python socket</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/6809447.html" target="_blank">python socket粘包</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/6814254.html" target="_blank">python socketserver</a></strong></span></p>
<p><span style="font-family: 楷体;"><strong>&nbsp; &nbsp;<a href="http://www.cnblogs.com/guobaoyuan/articles/7087658.html" target="_blank">python 进程线程</a></strong></span></p>
</div>
"""

# print(re.findall("((https|http|ftp|rtsp|mms)?:\/\/)[^\s]+",s1))
# lst1 = re.findall("href=(.+?) target",s1)
# lst2 = re.findall("python (.+?)</a>",s1)
# print(dict(zip(lst2,lst1)))