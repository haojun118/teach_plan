# 1.软件开发规范

# bin
#     starts
# conf
#     settings
# core
#     src
# db
#     用户数据
# lib
#     common
# log
#     日志


# 2.sys
    # import sys
    # sys.path -- 模块导入的顺序
    # sys.path.append()
    # sys.argv -- 只在终端执行,可以携带参数
    # sys.modules  -- 查看内存中已加载的模块
    # sys.platform  -- 查看当前操作平台
    # sys.version   -- 查看python解释器的版本

# 3.序列化
# json:
#     dumps: 将数据类型转换为字符串
#     loads: 将字符串转换成原数据类型
#     dump:  将数据类型转换为字符串,写入文件中
#     load:  将文件中的字符串读取出来,转换成原数据类型

# pickle:
#     dumps: 将数据类型转换为类似字节
#     loads: 将类似字节转换成原数据类型
#     dump:  将数据类型转换为类似字节,写入文件中
#     load:  将文件中的类似字节的内容读取出来,转换成原数据类型

# pickle 里不能用 lambda
# json序列元组时返回列表

# load 和 loads 只能读取一个数据类型,一行中只有一个数据类型就没有问题
# dump写文件时不能换行
# 当要序列化时,出现中文填加参数, ensure_ascii= False
# sort_keys=False 按照排序


# 如果想换行写入内容,就需要使用dumps


# 4.os
# import os
# os.getcwd() --  获取当前工作路径
# os.mkdir()  -- 创建文件夹
# os.rmdir()  -- 删除文件夹
# os.makedirs()  -- 递归创建文件夹
# os.removedirs()  -- 递归删除文件夹
# os.listdir()  -- 以列表形式显示,文件夹下所有的内容
# os.chdir()   -- 切换目录
# os.curdir     .
# os.pardir     ..


# os.remove()    删除
# os.rename()    重命名


# print(os.path.isfile())     # 判断是不是文件
# print(os.path.isdir())      # 判断是不是文件夹
# print(os.path.basename())   # 获取的是当前文件的名字
# print(os.path.getsize())    # 获取文件大小
# print(os.path.abspath())    # 获取绝对路径
# print(os.path.isabs())      # 判断是不是绝对路径
# print(os.path.join())       # 路径拼接
# print(os.path.exists())     # 判断路劲是否存在
# os.path.dirname()           # 获取指定路劲的父目录
# os.path.split()             # 获取一个列表,列表中第一个元素的路径,第二个元素是文件名


