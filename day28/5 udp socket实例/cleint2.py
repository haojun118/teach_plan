
from socket import socket,SOCK_DGRAM

sk = socket(type=SOCK_DGRAM)
server_addr = ('127.0.0.1',9001)

while True:
    msg = input('>>>')
    if msg.upper() == 'Q':
        break
    sk.sendto(msg.encode('utf-8'),server_addr)
    msg = sk.recv(1024)
    print(msg.decode('utf-8'))


