##  内容回顾

ORM

对象关系映射 面向对象和关系型数据库

对应关系

类  ——》  表

对象  ——》 数据行（记录）

属性  ——》  字段

### 常用字段

```
models.AutoField(primary_key=True)
models.CharField(max_length) # 字符串 varchar(32)
models.TextField  
models.BooleanField  # 布尔值
models.DateTimeField(auto_now=True)
models.IntegerField  # 10  -21亿  + 21亿
models.FloatField  
models.DecimalField(max_digits=6,decimal_places=3)  # 10进制  999.999
models.ForeignKey  
models.ManyToManyField
```

### 字段的参数

```
null=True    数据库中该字段可以为空
blank = True   form表单输入时允许为空
default    默认值
unique     唯一
choices    可选的参数
verbose_name  可显示的名字
db_index 数据库索引
db_column     列名
editable  是否可编辑
```

### 必知必会13条

返回querySet

```
all（）      所有的对象
filter()    获取满足条件的所有对象
exclude()   获取不满足条件的所有对象
order_by()   排序   - 
reverse（）   反转
values（）    [ {}  ]
values_list（）    [ （）  ]
distinct()   去重 
```

返回对象

```
get（）    获取有且唯一的对象
first（）
last（）
```

返回数字

```
count（）
```

返回布尔值

```
exists()
```

单表的双下划线

```
字段__条件
__gt   大于
__gte   大于等于
__lt  小于
__lte   小于等于
__in= []    成员判断
__range = [3,6]  范围  3-6

__contains =''    包含   like
__icontains =''   忽略大小写

__startswith =''   以什么开头
__istartswith =''   以什么开头


__endswith =''   以什么结尾
__endswith =''   以什么结尾


__year ='2019'  

__isnull = True   字段值为null 
```



### 外键

```python
# 基于对象的查询
# 正向查询
book_obj = models.Book.objects.get(pk=4)
print(book_obj.name)
print(book_obj.publisher)
print(book_obj.publisher_id)

# 反向查询
pub_obj = models.Publisher.objects.get(pk=1)
# 不指定related_name  表名小写_set
print(pub_obj)
print(pub_obj.book_set)   # 表名小写_set   关系管理对象
print(pub_obj.book_set.all())

# 指定related_name=‘books’  表名小写_set
print(pub_obj.books)
print(pub_obj.books.all())


# 基于字段的查询
# 查询“李小璐出版社”出版社的书
ret = models.Book.objects.filter(publisher__name='李小璐出版社')

# 查询“绿光”书的出版社
# 没有指定related_name  表名小写
ret = models.Publisher.objects.filter(book__name='绿光')

# 指定related_name='books'
ret = models.Publisher.objects.filter(books__name='绿光')

# 指定related_query_name='book'
ret = models.Publisher.objects.filter(book__name='绿光')

print(ret)

```

### 多对多

```python 

# 基于对象的
author_obj = models.Author.objects.get(name='yinmo')
# print(author_obj.books)     # 关系管理对象
# print(author_obj.books.all())  # 所关联的书

book_obj = models.Book.objects.get(name='原谅你')
# print(book_obj.author_set)
# print(book_obj.author_set.all())

# print(book_obj.authors)
# print(book_obj.authors.all())

# 基于字段的查询
# print(models.Author.objects.filter(books__name='原谅你'))
# print(models.Book.objects.filter(author__name='yinmo'))

# all 查询所有的对象

# set（）  设置多对多的关系   [id，id]   [对象，对象]
# author_obj.books.set([1,3])
# author_obj.books.set(models.Book.objects.filter(pk__in=[2,4]))

# add  新增多对多的关系   id，id   对象，对象
# author_obj.books.add(1,3)
# author_obj.books.add(*models.Book.objects.filter(pk__in=[1,3]))

# remove  删除多对多的关系   id，id   对象，对象
# author_obj.books.remove(1,3)
# author_obj.books.remove(*models.Book.objects.filter(pk__in=[2,4]))

# clear  删除所有多对多的关系
# author_obj.books.clear()
# author_obj.books.set([])

# create  创建一个所关联的对象并且和当前对象绑定关系
# ret = author_obj.books.create(name='yinmo的春天',publisher_id=1)
# print(ret)

pub_obj = models.Publisher.objects.get(pk=1)
print(pub_obj.books.all())
# print(pub_obj.books.set(models.Book.objects.filter(pk__in=[3,4])))
# print(pub_obj.books.add(*models.Book.objects.filter(pk__in=[1])))

# 外键的关系管理对象需要有remove和clear 外键字段必须可以为空
# print(pub_obj.books.remove(*models.Book.objects.filter(pk__in=[1])))
# print(pub_obj.books.clear())

# print(pub_obj.books.create(name="你喜欢的绿色"))

```



### 聚合和分组

```python 
# 所有的价格

# ret = models.Book.objects.all().aggregate(Sum('price'))
# ret = models.Book.objects.all().aggregate(sum=Sum('price'),avg=Avg('price'),count=Count('pk'))
# ret = models.Book.objects.filter(pk__in=[1,2]).aggregate(sum=Sum('price'),avg=Avg('price'),count=Count('pk'))

# print(ret,type(ret))

# 分组
# 每一本书的作者个数
ret = models.Book.objects.annotate(count=Count('author'))

# 统计出每个出版社买的最便宜的书的价格
# 方式一
ret = models.Publisher.objects.annotate(Min('book__price')).values()
# for i in ret:
#     print(i)
# 方拾二
# ret = models.Book.objects.values('publisher__name').annotate(min=Min('price'))


# 统计不止一个作者的图书

ret = models.Book.objects.annotate(count=Count('author')).filter(count__gt=1)


# 查询各个作者出的书的总价格

ret = models.Author.objects.annotate(Sum('books__price')).values()

print(ret)


```

### F和Q

```python 


from django.db.models import F, Q

ret = models.Book.objects.filter(sale__gt=F('kucun'))

ret = models.Book.objects.all().update(publisher_id=3)

ret = models.Book.objects.filter(pk=1).update(sale=F('sale')*2+43)

# & 与  and
# | 或  or
# ~ 非  not
# Q(pk__gt=5)
ret = models.Book.objects.filter(Q(~Q(pk__gt=5) | Q(pk__lt=3)) & Q(publisher_id__in=[1, 3]))

print(ret)


```

### 事务

```python
from django.db import transaction


try:
    with transaction.atomic():
        # 一系列的操作
        models.Book.objects.update(publisher_id=4)
        models.Book.objects.update(publisher_id=3)
        int('ss')
        models.Book.objects.update(publisher_id=2)
        models.Book.objects.update(publisher_id=5)

except Exception as e:
    print(e)
```

