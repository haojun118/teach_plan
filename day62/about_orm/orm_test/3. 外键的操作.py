import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "about_orm.settings")
import django

django.setup()

from app01 import models
# 基于对象的查询
# 正向查询
book_obj = models.Book.objects.get(pk=4)
print(book_obj.name)
print(book_obj.publisher)
print(book_obj.publisher_id)

# 反向查询
pub_obj = models.Publisher.objects.get(pk=1)
# 不指定related_name  表名小写_set
print(pub_obj)
print(pub_obj.book_set)   # 表名小写_set   关系管理对象
print(pub_obj.book_set.all())

# 指定related_name=‘books’  表名小写_set
print(pub_obj.books)
print(pub_obj.books.all())


# 基于字段的查询
# 查询“李小璐出版社”出版社的书
ret = models.Book.objects.filter(publisher__name='李小璐出版社')

# 查询“绿光”书的出版社
# 没有指定related_name  表名小写
ret = models.Publisher.objects.filter(book__name='绿光')

# 指定related_name='books'
ret = models.Publisher.objects.filter(books__name='绿光')

# 指定related_query_name='book'
ret = models.Publisher.objects.filter(book__name='绿光')

print(ret)


