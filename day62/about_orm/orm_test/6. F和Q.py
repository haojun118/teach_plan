import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "about_orm.settings")
import django

django.setup()
from app01 import models

from django.db.models import F, Q

ret = models.Book.objects.filter(sale__gt=F('kucun'))

ret = models.Book.objects.all().update(publisher_id=3)

ret = models.Book.objects.filter(pk=1).update(sale=F('sale')*2+43)

# & 与  and
# | 或  or
# ~ 非  not
# Q(pk__gt=5)
ret = models.Book.objects.filter(Q(~Q(pk__gt=5) | Q(pk__lt=3)) & Q(publisher_id__in=[1, 3]))

print(ret)
