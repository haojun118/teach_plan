import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "about_orm.settings")
import django

django.setup()

from app01 import models

# 基于对象的
author_obj = models.Author.objects.get(name='yinmo')
# print(author_obj.books)     # 关系管理对象
# print(author_obj.books.all())  # 所关联的书

book_obj = models.Book.objects.get(name='原谅你')
# print(book_obj.author_set)
# print(book_obj.author_set.all())

# print(book_obj.authors)
# print(book_obj.authors.all())

# 基于字段的查询
# print(models.Author.objects.filter(books__name='原谅你'))
# print(models.Book.objects.filter(author__name='yinmo'))

# all 查询所有的对象

# set（）  设置多对多的关系   [id，id]   [对象，对象]
# author_obj.books.set([1,3])
# author_obj.books.set(models.Book.objects.filter(pk__in=[2,4]))

# add  新增多对多的关系   id，id   对象，对象
# author_obj.books.add(1,3)
# author_obj.books.add(*models.Book.objects.filter(pk__in=[1,3]))

# remove  删除多对多的关系   id，id   对象，对象
# author_obj.books.remove(1,3)
# author_obj.books.remove(*models.Book.objects.filter(pk__in=[2,4]))

# clear  删除所有多对多的关系
# author_obj.books.clear()
# author_obj.books.set([])

# create  创建一个所关联的对象并且和当前对象绑定关系
# ret = author_obj.books.create(name='yinmo的春天',publisher_id=1)
# print(ret)

pub_obj = models.Publisher.objects.get(pk=1)
print(pub_obj.books.all())
# print(pub_obj.books.set(models.Book.objects.filter(pk__in=[3,4])))
# print(pub_obj.books.add(*models.Book.objects.filter(pk__in=[1])))

# 外键的关系管理对象需要有remove和clear 外键字段必须可以为空
# print(pub_obj.books.remove(*models.Book.objects.filter(pk__in=[1])))
# print(pub_obj.books.clear())

# print(pub_obj.books.create(name="你喜欢的绿色"))
