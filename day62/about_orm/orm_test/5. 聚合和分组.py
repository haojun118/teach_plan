import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "about_orm.settings")
import django

django.setup()
from app01 import models
from django.db.models import Max, Min, Avg, Count, Sum

# 所有的价格

# ret = models.Book.objects.all().aggregate(Sum('price'))
# ret = models.Book.objects.all().aggregate(sum=Sum('price'),avg=Avg('price'),count=Count('pk'))
# ret = models.Book.objects.filter(pk__in=[1,2]).aggregate(sum=Sum('price'),avg=Avg('price'),count=Count('pk'))

# print(ret,type(ret))

# 分组
# 每一本书的作者个数
ret = models.Book.objects.annotate(count=Count('author'))

# 统计出每个出版社买的最便宜的书的价格
# 方式一
ret = models.Publisher.objects.annotate(Min('book__price')).values()
# for i in ret:
#     print(i)
# 方拾二
# ret = models.Book.objects.values('publisher__name').annotate(min=Min('price'))


# 统计不止一个作者的图书

ret = models.Book.objects.annotate(count=Count('author')).filter(count__gt=1)


# 查询各个作者出的书的总价格

ret = models.Author.objects.annotate(Sum('books__price')).values()

print(ret)

