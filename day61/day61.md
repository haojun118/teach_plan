##  内容回顾

### 路由系统

urlconf

```python
from django.conf.urls import url

from app01 import views
urlpatterns = [

    url(r'^publisher_list/', views.publisher_list, name='publisher_list'),
    # url(r'^publisher_add/', views.publisher_add),
]
```

### 正则表达式

从上到下进行匹配，匹配到一个就不再往下匹配

r  $  ^  \d  \w [a-z]   [azs]{4}  .  ?  + * 

### 分组和命名分组

```
url(r'^(publisher)_list/', views.publisher_list, name='publisher_list'),
```

从URL地址上捕获的参数会按照 位置参数 传递给视图函数

```
url(r'^(?P<xxx>publisher)_list/', views.publisher_list, name='publisher_list'),
```

从URL地址上捕获的参数会按照 关键字参数 传递给视图函数

### include 路由分发

```
from django.conf.urls import url, include

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^app01', include('app01.urls')),
    url(r'^app02/', include('app02.urls')),
]

```

### URL的命名和反向解析

静态路由

```
url(r'^publisher_list/', views.publisher_list, name='publisher'),
```

反向解析

模板

```
{% url 'publisher' %}   ——》 /app01/publisher_list/
```

py文件

```
from django.urls import reverse
reverse('publisher')    ——》  /app01/publisher_list/
```

分组

```
url(r'^(publisher)_list/', views.publisher_list, name='publisher'),
```

反向解析

模板

```
{% url 'publisher' 'publisher' %}   ——》 /app01/publisher_list/
```

py文件

```
from django.urls import reverse
reverse('publisher',args=('publisher',))    ——》  /app01/publisher_list/
```

命名分组

```
url(r'^(？P<xx>publisher)_list/', views.publisher_list, name='publisher'),
```

反向解析

模板

```
{% url 'publisher' 'publisher' %}   ——》 /app01/publisher_list/
{% url 'publisher' xx='publisher' %}   ——》 /app01/publisher_list/
```

py文件

```
from django.urls import reverse
reverse('publisher',args=('publisher',))    ——》  /app01/publisher_list/
reverse('publisher',kwargs={'xx':'publisher'})    ——》  /app01/publisher_list/
```

namespace

```
urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^app01', include('app01.urls',namespce='app01')),
    url(r'^app02/', include('app02.urls')),
]
```

{% url  namespace:name %}

reverse('namespace:name ')

## 今日内容

### 常用的字段

```
AutoField         自增  primary_key=True主键
IntegerField	  整形 -21亿  - 21亿 
BooleanField   NullBooleanField     布尔值  0 1
CharField       字符串   
TextField       文本类型
DateTimeField  DateField TimeField
FloatField      浮点型
DecimalField	十进制小数
```

### 字段参数

```
null=True     数据库中该字段可以为空
blank=True    用户输入可以为空
default       默认值
db_index=True  索引
unique         唯一约束
verbose_name   显示的名称
choices        可选择的参数
```

### 使用admin的步骤：

1. 创建一个超级用户

   python manage.py createsuperuser

   输入用户名  和 秘密

2. 在app下的admin.py中注册model

   ```
   from django.contrib import admin
   from app01 import models
   # Register your models here.
   admin.site.register(models.Person)
   
   ```

3. 地址栏输入/admin/



### 必知必会13条

```python 
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "about_orm.settings")
import django

django.setup()

from app01 import models

# all()   获取所有的数据  QuerySet  对象列表
ret = models.Person.objects.all()

# get()   获取一个对象  对象  不存在或者多个就报错
# ret = models.Person.objects.get(name='alexdsb')

# filter()   获取满足条件的所有对象  QuerySet  对象列表
ret = models.Person.objects.filter(name='alexdsb')

# exclude()  获取不满足条件的所有对象   QuerySet  对象列表
ret = models.Person.objects.exclude(name='alexdsb')

# values  QuerySet   [ {} ]
# values()  不写参数  获取所有字段的字段名和值
# values('name','age')  指定字段 获取指定字段的字段名和值
ret = models.Person.objects.values('name', 'age')
# for i in ret:
#     print(i,type(i))

# values_list  QuerySet   [ () ]
# values_list()  不写参数  获取所有字段的值
# values_list('name','age')  指定字段 获取指定字段的值

ret = models.Person.objects.values_list('age', 'name')

# for i in ret:
#     print(i, type(i))

# order_by   排序  默认升序   降序 字段名前加-   支持多个字段
ret = models.Person.objects.all().order_by('-age', 'pk')


# reverse  对已经排序的结果进行反转
ret = models.Person.objects.all().order_by('pk')
ret = models.Person.objects.all().order_by('pk').reverse()

# distinct mysql不支持按字段去重
ret = models.Person.objects.all().distinct()

ret = models.Person.objects.values('name','age').distinct()

# count()  计数
ret = models.Person.objects.all().count()
ret = models.Person.objects.filter(name='alexdsb').count()

# first  取第一个元素  取不到是None
ret = models.Person.objects.filter(name='xxxx').first()

# last  取最后一个元素
ret = models.Person.objects.values().first()

# exists  是否存在  存在是True
ret = models.Person.objects.filter(name='xx').exists()
print(ret)


"""
返回对象列表  QuerySet
all()  
filter() 
exclude()
values()    [ {},{} ]
values_list()    [ (),() ]
order_by()
reverse() 
distinct()


返回对象
get
first
last

返回数字
count

返回布尔值
exists() 
"""

```

### 单表的双下划线

```python
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "about_orm.settings")
import django

django.setup()

from app01 import models

ret = models.Person.objects.filter(pk__gt=3)  # gt  greater than 大于
ret = models.Person.objects.filter(pk__lt=3)  # lt  less than   小于

ret = models.Person.objects.filter(pk__gte=3)  # gt  greater than equal  大于等于
ret = models.Person.objects.filter(pk__lte=3)  # lt  less than equal    小于等于

ret = models.Person.objects.filter(pk__range=[1, 4])  # 范围
ret = models.Person.objects.filter(pk__in=[1, 4])   # 成员判断
ret = models.Person.objects.filter(name__in=['alexdsb','xx'])

ret = models.Person.objects.filter(name__contains='x')   # contains 包含  like
ret = models.Person.objects.filter(name__icontains='X')   # ignore contains 忽略大小写

ret = models.Person.objects.filter(name__startswith='X')   # 以什么开头
ret = models.Person.objects.filter(name__istartswith='X')   # 以什么开头

ret = models.Person.objects.filter(name__endswith='dsb')   # 以什么开头
ret = models.Person.objects.filter(name__iendswith='DSB')   # 以什么开头


ret = models.Person.objects.filter(phone__isnull=False)   # __isnull=False  不为null

ret = models.Person.objects.filter(birth__year='2020')
ret = models.Person.objects.filter(birth__contains='2020-11-02')
print(ret)

```











