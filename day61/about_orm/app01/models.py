from django.db import models


class MyCharField(models.Field):
    """
    自定义的char类型的字段类
    """

    def __init__(self, max_length, *args, **kwargs):
        self.max_length = max_length
        super(MyCharField, self).__init__(max_length=max_length, *args, **kwargs)

    def db_type(self, connection):
        """
        限定生成数据库表的字段类型为char，长度为max_length指定的值
        """
        return 'char(%s)' % self.max_length


# Create your models here.
class Person(models.Model):
    pid = models.AutoField(primary_key=True)  # 主键  pid  pk
    name = models.CharField('用户名', max_length=32, db_column='username', help_text='不能是纯数字')  # varcher(32)
    age = models.IntegerField(default=18, )  # -21亿  -  21亿
    birth = models.DateTimeField(auto_now=True)
    phone = MyCharField(max_length=11, null=True, blank=True)
    gender = models.BooleanField(choices=((True, '男'), (False, '女')))  # 1 男

    # price = models.DecimalField(max_digits=5,decimal_places=2)  # 999.99

    def __str__(self):
        return  "{} {} {}".format(self.pk,self.name,self.age)

    class Meta:
        # 数据库中生成的表名称 默认 app名称 + 下划线 + 类名
        db_table = "person"

        # admin中显示的表名称
        verbose_name = '个人信息'

        # verbose_name加s
        verbose_name_plural = '所有用户信息'

        # 联合索引
        # index_together = [
        #     ("name", "age"),  # 应为两个存在的字段
        # ]
        #
        # 联合唯一索引
        # unique_together = (("name", "age"),)  # 应为两个存在的字段