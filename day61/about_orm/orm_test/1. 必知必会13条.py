import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "about_orm.settings")
import django

django.setup()

from app01 import models

# all()   获取所有的数据  QuerySet  对象列表
ret = models.Person.objects.all()

# get()   获取一个对象  对象  不存在或者多个就报错
# ret = models.Person.objects.get(name='alexdsb')

# filter()   获取满足条件的所有对象  QuerySet  对象列表
ret = models.Person.objects.filter(name='alexdsb')

# exclude()  获取不满足条件的所有对象   QuerySet  对象列表
ret = models.Person.objects.exclude(name='alexdsb')

# values  QuerySet   [ {} ]
# values()  不写参数  获取所有字段的字段名和值
# values('name','age')  指定字段 获取指定字段的字段名和值
ret = models.Person.objects.values('name', 'age')
# for i in ret:
#     print(i,type(i))

# values_list  QuerySet   [ () ]
# values_list()  不写参数  获取所有字段的值
# values_list('name','age')  指定字段 获取指定字段的值

ret = models.Person.objects.values_list('age', 'name')

# for i in ret:
#     print(i, type(i))

# order_by   排序  默认升序   降序 字段名前加-   支持多个字段
ret = models.Person.objects.all().order_by('-age', 'pk')


# reverse  对已经排序的结果进行反转
ret = models.Person.objects.all().order_by('pk')
ret = models.Person.objects.all().order_by('pk').reverse()

# distinct mysql不支持按字段去重
ret = models.Person.objects.all().distinct()

ret = models.Person.objects.values('name','age').distinct()

# count()  计数
ret = models.Person.objects.all().count()
ret = models.Person.objects.filter(name='alexdsb').count()

# first  取第一个元素  取不到是None
ret = models.Person.objects.filter(name='xxxx').first()

# last  取最后一个元素
ret = models.Person.objects.values().first()

# exists  是否存在  存在是True
ret = models.Person.objects.filter(name='xx').exists()
print(ret)


"""
返回对象列表  QuerySet
all()  
filter() 
exclude()
values()    [ {},{} ]
values_list()    [ (),() ]
order_by()
reverse() 
distinct()


返回对象
get
first
last

返回数字
count

返回布尔值
exists() 
"""









