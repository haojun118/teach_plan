import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "about_orm.settings")
import django

django.setup()

from app01 import models

ret = models.Person.objects.filter(pk__gt=3)  # gt  greater than 大于
ret = models.Person.objects.filter(pk__lt=3)  # lt  less than   小于

ret = models.Person.objects.filter(pk__gte=3)  # gt  greater than equal  大于等于
ret = models.Person.objects.filter(pk__lte=3)  # lt  less than equal    小于等于

ret = models.Person.objects.filter(pk__range=[1, 4])  # 范围
ret = models.Person.objects.filter(pk__in=[1, 4])   # 成员判断
ret = models.Person.objects.filter(name__in=['alexdsb','xx'])

ret = models.Person.objects.filter(name__contains='x')   # contains 包含  like
ret = models.Person.objects.filter(name__icontains='X')   # ignore contains 忽略大小写

ret = models.Person.objects.filter(name__startswith='X')   # 以什么开头
ret = models.Person.objects.filter(name__istartswith='X')   # 以什么开头

ret = models.Person.objects.filter(name__endswith='dsb')   # 以什么开头
ret = models.Person.objects.filter(name__iendswith='DSB')   # 以什么开头


ret = models.Person.objects.filter(phone__isnull=False)   # __isnull=False  不为null

ret = models.Person.objects.filter(birth__year='2020')
ret = models.Person.objects.filter(birth__contains='2020-11-02')
print(ret)
