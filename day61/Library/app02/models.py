from django.db import models


class Publisher(models.Model):
    pid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32)  # varchar(32)


class Book(models.Model):
    title = models.CharField(max_length=32)  # varchar(32)
    pub = models.ForeignKey('Publisher',on_delete=models.CASCADE)


class Author(models.Model):
    name = models.CharField(max_length=32)  # varchar(32)
    books = models.ManyToManyField(Book)  # 生成第三张表  不会在Author表中生成字段




