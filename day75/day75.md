### crm

客户关系管理系统

### 使用系统的人员

销售  班主任 财务 老师

### 功能+技术点：

1. 登录+注册

   注册 modelform

   ```python
   form django import forms 
   form app01 import models
   from django.core.exceptions import ValidationError
   class RegForm(forms.ModelForm):
       
       
       def clean_字段名（self）：
       	# 通过校验 返回当前字段的值
           # 不通过校验  抛出异常  ValidationError
           
       def clean（self）:
           self._validate_unique = True  # 在数据库校验唯一性
   	
   	class Meta:
   		model = models.User
   		fields = '__all__'   # ['xxx','xx']
           exclude = ['is_active'] 
           
           labels = {
               'school': '校区'
           }
           
           widgets = {
               'username': forms.EmailInput(attrs={'placeholder': '用户名'}),
               'password': forms.PasswordInput(attrs={'placeholder': '密码'}),
           }
           
          
   
   form_obj = RegForm()
   
   form_obj = RegForm(data=request.POST)
   form_obj.is_valid() 
   form_obj.save()
   
   form_obj = RegForm(data=request.POST,instance=obj)
   
   模板：
   	{{ form_obj.as_p }}
       {{ form_obj.username }}   _>  input框
       {{ form_obj.username.errors }}   _>  该字段的所有的错误
       {{ form_obj.username.label }}   _>  提示信息
       {{ form_obj.username.id_for_label }}   _>  input框的id
       
       {{ form_obj.errors }}  ——》   所有的错误
       {{ form_obj.non_filed_errors }}  ——》   __all__的错误
      
   ```

2. 公户和私户的展示

   展示数据:

   	1. 普通字段  obj.字段名
    	2. choices参数   obj.字段名   ` obj.get_字段名_display()`

   3.  自定义方法   

   分页：

   ​	url地址上有?page = 2

   ​	per_num  = 15 

   ​    start = (page  - 1) *per_num

   ​	end  = page  *per_num

   分页保留搜索条件

   ​	QueryDict    request.GET     {      }       QueryDict.copy()

   ​	QueryDict.urlencode()     page=1&query=xxx    

   模糊搜索

   ​	Q  

   ​	Q(qq__contains=query)   

   ​	Q(('qq__contains',query))

   ​	q =  Q()    q.connector = 'OR'    q.children.append()

   编辑后跳转至源页面

   ​	simple_tag     返回一个地址

   ​	url反向解析  QueryDict

   ​	path =  request.get_full_path()

   ​	QueryDict[path]  =request.get_full_path()

   公户和私户的转换

   ​	反射 + 具体的操作

   ​	事务+行级锁

3. 登录成功

   保存登录状态   用户的pk   ——》  session 

   中间件  process_request

   ​	白名单

   ​	校验登录状态

   ​		登录：  保存用户对象  request.user_obj

   ​		没有登录：  跳转到登录页面

4. 跟进记录  报名记录  

   展示 

   新增和编辑 

   2个url   +  1个视图  + form.html    modelform

   

   



modelformset