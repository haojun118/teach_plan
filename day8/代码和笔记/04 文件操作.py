# 文件操作
"萝莉小姐姐电话号"

# 1.找到文件的位置
# 2.双击打开
# 3.进行一些操作  # r - read(读) w - write(写) a - 追加
#                rb -- (读字节) wb ab r+ w+ a+
# 4.关闭文件

# open()  # 打开  open通过python控制操作系统进行打开文件
# file 文件的位置
# mode 默认不写就是r
# encoding 文件的编码
# f 文件句柄

# 文件只能读取一次

# 读操作:
# f = open("D:\Python_s25\day08\小姐姐电话号",mode="r",encoding="utf-8")
# print(f.read())   # 全部读取
# print(f.read(3))  # 按照字符读取

# print(f.readline())  # 默认尾部有一个\n
# print(f.readline().strip())  # 读取一行
# print(f.readline().strip())  # 将\n去除

# print(f.readlines()) #一行一行读取,全部存储在列表中

# 绝对路径
# f = open(r"D:\Python_s25\day08\t1",mode="r",encoding="utf-8")
# print(f.read())

# 相对路径
# f = open("t1",mode="r",encoding="utf-8")
# print(f.read())


# 路径转义:
#     1."D:\\Python_s25\\day08\\t1"
#     2.r"D:\Python_s25\day08\t1"   -- 推荐使用

# 路径:
#     1.绝对路径 : 从磁盘(C盘)开始查找
#     2.相对路径 : 相对于某个文件进行查找

# 字节操作 不能指定encoding编码

# f = open("timg.jpg",mode="rb")
# print(f.read())      # 全部读取
# print(f.read(3))     # 按照字节读取
# print(f.readline())  # 按照行进行读取
# print(f.readlines())

# r和 rb的区别:
#     1.r需要指定encoding,rb不需要
#     2.r模式中的read(3) 按照字符读取, rb模式中的read(3) 按照字节读取

# read 和 readlines 如果文件较大时,会出现内存溢出
# 解决方案:

# 面试题:
# 当文件交大时,使用for循环进行读取
# f = open('t1',mode="r",encoding="utf-8")
# for i in f:
#     print(i.strip())

# 写操作:
# w操作: -- 清空写(写的是文本)
    # 1.先清空文件(打开文件时清空)
    # 2.写入内容

# 当模式为 w 和 a 时,有文件就是用当前文件,没有文件就创建一个文件

# ff = open("a1",mode="w",encoding="utf-8")
# ff.write("123")  # 写的内容必须是字符串

# ff = open("a1",mode="w",encoding="utf-8")
# ff.write("我是一个字符串串")  # 写的内容必须是字符串

# ff = open("a1",mode="w",encoding="utf-8")
# ff.write("[1,2,3,4]\n")  # 写的内容必须是字符串
# ff.write('1111\n')  # 写的内容必须是字符串
# ff.write('2222\n')  # 写的内容必须是字符串

# ff = open("a1",mode="w",encoding="utf-8")
# ff.write("[1,2,3,4]\n")  # 写的内容必须是字符串
# ff.write('1111\n')  # 写的内容必须是字符串
# ff.write('2222\n')  # 写的内容必须是字符串

# wb -- 清空写(写字节)
# f = open('timg.jpg',mode="rb")
# f1 = open("g1.jpg",mode="wb")
# content = f.read()
# f1.write(content)


# a -- 追加写(文本)
# f = open("b1",mode="a",encoding="utf-8")
# f.write("你好啊\n")
# f.write("我好啊\n")
# f.write("他好啊\n")
# f.write("大家好啊\n")

# ab  -- 追加写(字节)

