# r+ 读写  (有点用)  ****

# 坑 -- 使用方式是错误
# f = open("b1",mode="r+",encoding="utf-8")
# f.write("今天是周一")
# print(f.read())

# 正确的操作:
# f = open("b1",mode="r+",encoding="utf-8")
# print(f.read())
# f.write("今天是周一")

# w+ 写读 (有点用)
# f = open("b1",mode="w+",encoding="utf-8")
# f.write("今天是周一")
# f.seek(0)  # 移动光标
# print(f.read())

# f = open("b1",mode="w+",encoding="utf-8")
# f.write("今天是周一")
# f.seek(0)  # 移动光标
# f.write("啊啊啊啊啊啊")
# f.seek(0)
# print(f.read())


# a+ 追加读  # 坑
# f = open("b1",mode="a+",encoding="utf-8")
# f.write("今天是周一")
# f.seek(0)  # 移动光标
# f.write("啊啊啊啊")
# print(f.read())


# 其他操作:
# seek() 移动光标
# f.seek(0,0)  # 移动光标到文件的头部
# f.seek(0,1)  # 移动光标到当前位置
# f.seek(0,2)  # 移动光标到文件末尾
# f.seek(6)   # 光标是按照字节移动


# f = open("a1","r",encoding="utf-8")
# print(f.read(5))
# f.seek(0,0)  # 移动光标到文件的头部
# f.seek(0,1)  # 移动光标到当前位置
# f.seek(0,2)  # 移动光标到文件末尾
# print(f.read())


# f = open("c1","r",encoding="gbk")
# f.seek(6)   # 光标是按照字节移动
# print(f.read(3))


# 查看光标:
# tell 查光标
# f = open("c1","r",encoding="gbk")
# print(f.read(3))
# print(f.tell())  # 按照字节进行计算

# 刷新:  自己感兴趣自己去了解一下
# 截取:  自己感兴趣自己去了解一下


# 修改文件:
# import os  # 操作系统交互的接口
#
# f = open('a2',"r",encoding="utf-8")
# f1 = open("a1","w",encoding="utf-8")
# for i in f:
#     i = i.replace("日","天")
#     f1.write(i)
#
# f.close()
# f1.close()
# os.remove("a2")   # 删除不能找回
# os.rename("a1","a2")




# import os  # 操作系统交互的接口
# f = open('a2',"r",encoding="utf-8")
# f1 = open("a1","w",encoding="utf-8")
# for i in f:
#     i = i.replace("天","日")
#     f1.write(i)
#
# f.close()
# f1.close()
# os.rename("a2","a3")
# os.rename("a1","a2")


# 考点:
# import os  # 操作系统交互的接口
# f = open('a2',"r",encoding="utf-8")
# f1 = open("a1","w",encoding="utf-8")
# i = f1.read().replace("天","日")    # 将文件中全部内容读取 容易导致内存溢出
# f1.write(i)
#
# f.close()
# f1.close()
# os.rename("a2","a3")
# os.rename("a1","a2")

# with open("a3","r",encoding="utf-8")as f,\
#         open('a2',"r",encoding="utf-8")as f1:
#     print(f.read())
#     print(f1.read())

# 1.自动关闭文件
# 2.同一时间操作多个文件

# 文件操作的目的:
#     1.持久化: 永久存储