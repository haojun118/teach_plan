# 1.文件操作:
# 1.r w a
# 2.rb wb ab
# 3.r+ w+ a+

# 打开文件:
# 字符串:
# 1.文件位置(路径)
# 2.文件的操作模式
# 3.文件的编码
# open("t1","r",encoding="utf-8")

# 读(r/rb):
# read() 全部读取
# read(3) r 读字符 rb 读字节
# readline() 读一行
# readlines() 一行一行读取，存放在列表

# 写(w/wb) 清空写 - 会创建文件
# 清空写:
#     1.打开文件时清空文件内容
#     2.写内容
# write("必须是字符串")

# 追加写(a/ab)  - 会创建文件
# 永远在文件的末尾进行添加

# r+ 读写
# 1.先读后写

# w+ 清空写 读
# 1.想要读取 必须移动光标

# a+ 追加写 读
# 1.想要读取 必须移动光标
# 坑: 移动关标,写内容还是在文件的末尾

# 其他操作:
# seek() 移动光标
# seek(0,0)  移动到光标的头部
# seek(0,1)  移动到关标的当前位置
# seek(0,2)  移动到光标的末尾

# tell() 查看关标
# 移动光标和查看光标都是按照字节来进行的

# 文件的修改:
    # 1.创建一个新的文件
    # 2.读取旧的文件内容
    # 3.进行替换修改
    # 4.将修改的内容写入到新的文件中
    # 5.将文件进行关闭
    # 6.将文件名进行修改

# with open("文件的位置","操作文件的模式","文件的编码集")as f:   面向对象 -- 上下文管理
#     f.read()

# with open好处:
# 1.自动关闭文件
# 2.同一时间操作多个文件

# 文件操作目的: 持久化,永久存储  (数据库之前 -- 文件操作就是代替数据库)