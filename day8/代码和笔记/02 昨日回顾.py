# 1.数据类型补充
#     str:
        # str.capitalize() 首字母大写
        # str.title() 每个单词首字母大写
        # str.swapcase() 大小写转换
        # str.find()   查找,查找不到就返回-1
        # str.index()  查找,查找不到就报错
        # str.center()   居中 -- 填充(20,"*")
        # str.join()     字符串拼接  将列表转换成字符串

    # list:
    #     list.sort()   # 默认是升序
    #     list.sort(reverse=True)  这就是降序
    #     list.reverse()    # 反转

        # lst = [1,[]] * 5
        # lst[-1].append(10)
        # 元素是共用的,执行效果:5份1,[10]

        # 列表在+ * 都会新开辟空间

    # tuple:

        # tu = ()    T
        # tu = (1)   F
        # tu = (1,2) T
        # tu = ([1,2]) F


    # dict:

    #     popitem   # 随机删除 返回键值对
    #     formkeys  # 批量创建键值对  第一个参数是可迭代对象  第二个参数是值
            #注意: 值是共用的,值如果是可变数据类型会有坑
            # 坑是? 有个键值对进行修改,其他键值对都进行改变

    # 数据类型转换:

        # list -- str
        # join()

        # str -- list
        # split()


# 2.坑
#     删除列表中的所有元素
#     1.从后先前删除

        # lst = [1,2,3,4,5,6]
        # for i in range(len(lst)):
        #     lst.pop()
        # print(lst)

    # 2.创建一个新的列表,通过新列表删除旧列表

        # lst = [1,2,3,4,5,6]
        # lst1 = lst.copy()

        # for i in lst1:
        #     # lst.pop()
        #     lst.remove(i)
        # print(lst)

    # 字典和集合:
    # 在进行遍历(循环)的时,不能修改源数据的长度,但是能够修改字典的值

    # dic = {"key1" : 1}
    # dic["key2"] = 2
    # print(dic)

# 3.二次编码

    # 编码 encode()
    # 解码 decode()
    # 用什么编码就用什么解码

    # python3 和 python2的区别
    # python3: unicode
    # python2: ascii