# v1 = [1,2,3,4,5]
# v2 = [v1,v1,v1] # [[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5]]
# v2[1][0] = 111
# v2[2][0] = 222
# print(v1)
# print(v2)


"""
17.dic = {'k1':'太白','k2':'barry','k3': '白白', 'age': 18}
请将字典中所有键带k元素的键值对删除.
"""

dic = {'k1':'太白','k2':'barry','k3': '白白', 'age': 18}
# lst = []
# for i in dic:
#     if "k" in i:
#         lst.append(i)
# for em in lst:
#     dic.pop(em)
# print(dic)

# dic1 = dic.copy()
# for i in dic1:
#     if "k" in i:
#         dic.pop(i)
# print(dic)

"""
用户输入一个数字，判断一个数是否是水仙花数。
水仙花数是一个三位数, 三位数的每一位的三次方的和还等于这个数. 那这个数就是一个水仙花数,
例如: 153 = 1**3 + 5**3 + 3**3
"""
# num_sum = 0 #
# num = input("请输入一个三位数:")  #153
# for i in num:
#     num_sum += int(i) ** 3  # 1 ** 3
# if num_sum == int(num):
#     print("是")
# else:
#     print("不是")

"""
把列表中所有姓周的⼈的信息删掉(此题有坑, 请慎重):
lst = ['周⽼⼆', '周星星', '麻花藤', '周扒⽪']
结果: lst = ['麻花藤']
"""
# lst = ['周⽼⼆', '周星星', '麻花周',"高圆圆","谢周","小米粥", '周扒⽪']
# lst1 = lst[:]
# for i in lst1:
#     if "周" in i:
#         lst.remove(i)
# print(lst)

"""
21.车牌区域划分, 现给出以下车牌. 根据车牌的信息, 分析出各省的车牌持有量. (选做题)
cars = ['鲁A32444','鲁B12333','京B8989M','⿊C49678','⿊C46555','沪 B25041']
locals = {'沪':'上海', '⿊':'⿊⻰江', '鲁':'⼭东', '鄂':'湖北', '湘':'湖南'}
结果: {'⿊⻰江':2, '⼭东': 2, '上海': 1}
"""
# cars = ['鲁A32444','鲁B12333','京B8989M','⿊C49678','⿊C46555','沪 B25041']
# locals = {'沪':'上海', '⿊':'⿊⻰江', '鲁':'⼭东', '鄂':'湖北', '湘':'湖南'}
#
# dic = {}  #{"山东":1}
# for i in cars:
#     key = i[0]  # 地域分布的键
#     if key in locals:
#         locals_key = locals[key]  # ⼭东
#         dic[locals_key] = dic.get(locals_key,0) + 1   # 1
# print(dic)

# dic = {}
# print(dic.get("山东",0) + 1)
# dic["山东"] = 1
# print(dic.get("山东",0) + 1)



# dic = {}
# for i in cars:
#     if i[0] in locals:
#         dic[locals[i[0]]] = dic.get(locals[i[0]],0) + 1
# print(dic)


