from datetime import datetime,timedelta
# print(datetime.now())  # 获取当前时间

# print(datetime(2018,10,1,10,11,12) - datetime(2011,11,1,20,10,10))
# 指定时间

# 将对象转换成时间戳
# d = datetime.now()
# print(d.timestamp())

# 将时间戳转换成对象
# import time
# f_t = time.time()
# print(datetime.fromtimestamp(f_t))

#将对象转换成字符串
# d = datetime.now()
# print(d.strftime("%Y-%m-%d %H:%M:%S"))

# 将字符串转换成对象
# s = "2018-12-31 10:11:12"
# print(datetime.strptime(s,"%Y-%m-%d %H:%M:%S"))

# 可以进行加减运算

# from datetime import datetime,timedelta
# print(datetime.now() - timedelta(days=1))
# print(datetime.now() - timedelta(hours=1))