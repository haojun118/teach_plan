# random -- 随机数
import random

# print(random.randint(1,50))
# 选择1-50之间随机的整数
# print(random.random())
#  0-1 之间随机小数,不包含1
# print(random.uniform(1,10))
# 1- 10 之间随机小数,不包含10

# print(random.choice((1,2,3,4,5,7)))
# #从容器中随机选择一个
# print(random.choices((1,2,3,4,5,7),k=3))
# 从容器中随机选择3个元素,以列表的形式方式,会出现重复元素
# print(random.sample((1,2,3,4,5,7),k=3))
# 从容器中随机选择3个元素,以列表的形式方式,不会出现重复元素
# print(random.randrange(1,10,2))  # 随机的奇数或随机的偶数

# lst = [1,2,3,4,5,6,7]
# random.shuffle(lst)
# # 洗牌 将有序的数据打散
# print(lst)