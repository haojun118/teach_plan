import time  # 内置模块 -- 标准库

# print(time.time())  # 时间戳  浮点数  秒
# time.sleep(3)  # 秒

# 将时间戳转换成结构化时间
# print(time.localtime(time.time()))   # 命名元组
# print(time.localtime(time.time())[0])
# print(time.localtime(time.time()).tm_year)

# 将结构化时间转换成字符串
# time_g = time.localtime()
# print(time.strftime("%Y-%m-%d %H:%M:%S",time_g))

# 将字符串转换成结构化时间
# str_time = "2018-10-1 10:11:12"
# time_g = time.strptime(str_time,"%Y-%m-%d %H:%M:%S")

# 将结构化时间转换成时间戳
# print(time.mktime(time_g))

# str_time = "2016-10-1 10:11:12"
# time_int = time.time() - time.mktime(time.strptime(str_time,"%Y-%m-%d %H:%M:%S"))
# print(time.localtime(time_int))

# 总结:
# time.time() 时间戳
# time.sleep() 睡眠
# time.localtime() 时间戳转结构化
# time.strftime() 结构化转字符串
# time.strptime() 字符串转结构化
# time.mktime() 结构化转时间戳
# %Y 年
# %m 月
# %d 日
# %H 时间
# %M 分钟
# %S 秒