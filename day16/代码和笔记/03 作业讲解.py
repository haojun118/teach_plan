# 1.请实现一个装饰器，限制该函数被调用的频率，如10秒一次（面试题）
# 答
# import time
# def sleeps(func_addr):
#     t = 0 # 150120213.1123
#     def inner(*args,**kwargs):
#         nonlocal t
#         if time.time() - t > 2:
#             t = time.time()
#             func_addr(*args,**kwargs)
#         else:
#             print("被限制!")
#     return inner
# @sleeps
# def foo():
#     print("我是被装饰de!")
#
# while True:
#     time.sleep(1)
#     foo()

# 3.编写装饰器完成下列需求:
# 用户有两套账号密码,一套为京东账号密码，一套为淘宝账号密码分别保存在两个文件中。
# 设置四个函数，分别代表 京东首页，京东超市，淘宝首页，淘宝超市。
# 启动程序后,呈现用户的选项为:
# 1,京东首页
# 2,京东超市
# 3,淘宝首页
# 4,淘宝超市
# 5,退出程序
# 四个函数都加上认证功能，用户可任意选择,用户选择京东超市或者京东首页,只要输入一次京东账号和密码并成功,则这两个函数都可以任意访问;用户选择淘宝超市或者淘宝首页,只要输入一次淘宝账号和密码并成功,则这两个函数都可以任意访问.
# 相关提示：用带参数的装饰器。装饰器内部加入判断，验证不同的账户密码。

# msg = """
# 1,京东首页
# 2,京东超市
# 3,淘宝首页
# 4,淘宝超市
# 5,退出程序
# >>>
# """
#
# login_dic = {
#     "jd_flag":False,
#     "tb_flag":False
# }
#
# def auth(argv):
#     def wrapper(func_addr):
#         def inner(*args,**kwargs):
#             if argv == "tb":
#                 f = open("tb",'r',encoding="utf-8")
#                 if login_dic["tb_flag"] == False:
#                     login("tb_flag", f, func_addr, *args, **kwargs)
#                 else:
#                     func_addr(*args, **kwargs)
#             elif argv == "jd":
#                 f = open("jd", 'r', encoding="utf-8")
#                 if login_dic["jd_flag"] == False:
#                     login("jd_flag",f,func_addr,*args,**kwargs)
#                 else:
#                     func_addr(*args, **kwargs)
#         return inner
#     return wrapper
#
#
# def login(flag,file,func,*args,**kwargs):
#     user = input("username:")
#     pwd = input("password:")
#     for i in file:
#         file_user, file_pwd = i.strip().split(":")
#         if user == file_user and pwd == file_pwd:
#             login_dic[flag] = True
#             func(*args, **kwargs)
#             break
#     else:
#         print("输入有误!")
#
# @auth("jd")
# def jd():
#     print("京东首页")
#
# @auth("jd")
# def jd_shopping():
#     print("京东超市")
#
# @auth("tb")
# def tb():
#     print("淘宝首页")
#
# @auth("tb")
# def tb_shopping():
#     print("淘宝超市")
#
#
# func_dic = {
#     "1":jd,
#     "2":jd_shopping,
#     "3":tb,
#     "4":tb_shopping,
#     "5":exit
# }
#
# while True:
#     choose = input(msg)
#     if choose in func_dic:
#         func_dic[choose]()
#     else:
#         print("请重新输入!")


# 5.用递归函数完成斐波那契数列（面试题）：
# 斐波那契数列：1，1，2，3，5，8，13，21..........(第三个数为前两个数的和，但是最开始的1，1是特殊情况，可以单独讨论)

# def func(n):
#     if n == 1 or n == 2:
#         return 1
#     return func(n-1) + func(n-2)
#
# n =  int(input("请输入数字:"))
# for i in range(1,n):
#     print(func(i))


# 6.用户输入序号获取对应的斐波那契数字：比如输入6，返回的结果为8.

# def func(n):
#     if n == 1 or n == 2:
#         return 1
#     return func(n-1) + func(n-2)
# print(func(6))


# lst = [1,1]
# for i in range(10):
#     lst.append(lst[-1]+lst[-2])
# print(lst)