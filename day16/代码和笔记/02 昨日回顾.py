# 1.装饰器的进阶
# 有参装饰器,在标准版装饰器外层添加一层,额外需要多调用一层

# 语法糖:
# def auth(argv):
#     def wrapper(func):
#         def inner(*args,**kwargs):
#             func(*args,**kwargs)
#         return inner
#     return wrapper
#
# a = auth("微信")
# inner = a(foo)
# inner()

# 多个装饰装饰一个函数时,先装饰离被装饰函数最近的装饰器
# 小技巧:V

# 递归:
# 递: 传参
# 归: 返回

# 一递一归, 归的过程不能间断
# 递归最大深度: 官方1000 实际测试 998/997
# 不断调用自己函数 就是死递归
# 有明确的终止条件