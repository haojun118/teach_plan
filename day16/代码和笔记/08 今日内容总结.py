# 1.自定义模块
# import 模块名
# from 模块名 import 函数名
# from 模块名 import *  __all__ = ["func"]

# 起别名: as  支持import 和 from
    # 名字过长,避免覆盖之前的内容

# import 和 from 的区别
# import 导入所有
# from   导入指定的功能
# from 比 import 灵活
# import 后边不能有点操作

# 导入多次时只执行一次
# 不建议一行导入多个

# if __name__ == "__main__":
    # 当文件被当做模块被调用的时候__name__返回的当前模块名
    # 当文件当做脚本执行时__name__返回的__main__

# sys.path.append 添加一个模块查找的路劲, python解释器中的环境变量中
# 查找顺序:
# append
# 内存 > 内置 > 第三方 > 自定义

# insert
# 内存 > 自定义 > 内置 > 第三方

# 注意事项:(了解)
# 字节码
# 不能交叉导入,导入
# 自定义模块不能和内置模块重复
# 自定义模块最重要就是 import导入 和 from导入

# 2.time
    # import time
    # time.time()
    # time.sleep()
    # time.localtime()
    # time.strftime()
    # time.strptime()
    # time.mktime()
    # Y m d H M S

# 3.datetime
from datetime import datetime,timedelta
# datetime.now()

# import datetime
# datetime.datetime.now()

# datetime(2018,10,1,10,11,12)
# datetime(2018,10,1,10,11,12) - datetime.now()

# datetime.timestamp()   # 将对象转换成时间戳
# datetime.fromtimestamp() # 将时间戳转换成对象

# datetime.strftime()  # 将对象转换字符串
# datetime.strptime()  # 将字符串转换成对象

# datetime.now() - timedelta(days=1) # 时间加减

# 4.random
import random

# print(random.randint(1,50))
# 选择1-50之间随机的整数
# print(random.random())
#  0-1 之间随机小数,不包含1
# print(random.uniform(1,10))
# 1- 10 之间随机小数,不包含10

# print(random.choice((1,2,3,4,5,7)))
# #从容器中随机选择一个
# print(random.choices((1,2,3,4,5,7),k=3))
# 从容器中随机选择3个元素,以列表的形式方式,会出现重复元素
# print(random.sample((1,2,3,4,5,7),k=3))
# 从容器中随机选择3个元素,以列表的形式方式,不会出现重复元素
# print(random.randrange(1,10,2))  # 随机的奇数或随机的偶数

# lst = [1,2,3,4,5,6,7]
# random.shuffle(lst)
# # 洗牌 将有序的数据打散
# print(lst)


# import sys
# for i in sys.path:
#     print(i)

# from day15 import aaaa
# import aaaa


# import sys
# sys.path.append(r"C:\Users\oldboy\Desktop")
# sys.path.insert(2,r"C:\Users\oldboy\Desktop")
# print(sys.path)


# import ttt