__all__ = ['b',"t1","t2"]

print("这是一个test工具箱")
# from c import ac
b = 20
# print(ac)

def t1():
    """
    十字螺丝刀
    :return:
    """
    print("我是十字螺丝刀")


def t2():
    """
    一字螺丝刀
    :return:
    """
    print("我是一字螺丝刀")


tt = "螺丝刀使用说明书"

if __name__ == '__main__':
    t1()
    t2()
