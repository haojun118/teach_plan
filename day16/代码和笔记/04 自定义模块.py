# 什么是模块?
# 分类  认证 ,视频 ,评论

# 模块能干什么?

# 1.文件化管理 提高可读性,避免重复代码
# 2.拿来就用 (避免重复造轮子) python中类库特别多

# 定义一个模块
# 一个文件就是一个模块  (模块就是一个工具箱  工具(函数))




# import时会做三件事:
# 1.将test.py文件中所有代码读取到当前文件
# 2.当前文件开辟空间
# 3.等待被调用

# import 导入同一个模块名是,只执行一次
# import test # 导入  拿test工具箱
# import test # 导入  拿test工具箱
# import test # 导入  拿test工具箱

# def t1():
#     print("高级工程师")

# import test
# test.t1()
# test.t2()
# print(test.tt)

# import test  # 只能将整个工具箱拿来
# a = test.t1
# b = test.t2
#
# a()
# b()

# import test as t  # 工具箱名字过长可以起别名
# t.t1()

# import test # 导入模块不能加后缀名

# 飘红不代表报错

# from test import t1 as t # 从test工具箱中将t1这个工具拿过来
#
# def t1():
#     print("高级工程师")
# t1()
# t()

# as 支持 import 和 from
# from 和 import 推荐使用from
# from test import t1
# t1()

# import 和 from 的区别:
# 1.from只能执行导入的工具
# 2.import 能够执行整个模块中所有的功能
# 3.from容易将当前文件中定义的工能覆盖
# 4.from比import灵活

# import 只能导入当前文件夹下的模块

# import 后边不能加点操作

# from day15 import ttt
# from day15.ttt import func
# func()

# import 和 from 使用的都是相对路径

# import sys  # 和python解释交互接口
# print(sys.path)
# sys.path.append(r"C:\Users\oldboy\Desktop")
# print(sys.path)

# 模块导入顺序:
#sys.path.append(r"C:\Users\oldboy\Desktop")
# 内存 > 内置 > 第三方> 自定义
#sys.path.insert(0,r"C:\Users\oldboy\Desktop")
# 内存 > 自定义 > 内置 > 第三方

# 模块的两种用法: if __name__ == "__main__"
# 1.当做模块被导入   import from
# 2.当做脚本被被执行

# import test

# 只有py文件当做模块被导入时,字节码才会进行保留

# 以后你们导入模块会产生的坑

# 1.注意自己定义模块的名字
# import abcd
# abcd.func()

# 2.注意自己的思路 -- 循环导入时建议 导入模式放在需要的地方

# from test import b
# a = 10
# print(b)

# import test,c,abcd   不建议这样导入
# test.t1()
# print(c.ac)
# abcd.func()

# import test
# import c
# import abcd        # 建议这样导入

# import from

from test import *   # 拿整个工具箱

# t1()
# t2()
# print(tt)

# 通过__all__ 控制要导入的内容