实现简单的权限控制

表结构   RBAC

```
权限表
	url   url正则表达式    不带^$
    title  标题
    
角色表  
	name   名称
	permissions   多对多  关联权限
	

用户表
	username
	password 
	
	roles   多对多  关联角色

```

数据结构

```
permission = [ {'permission__url':'xxxx'  } ]
```

一级菜单

表结构   RBAC

```
权限表
	url   url正则表达式    不带^$
    title  标题
    is_menu  布尔值
    icon   
    
角色表  
	name   名称
	permissions   多对多  关联权限
	

用户表
	username
	password 
	
	roles   多对多  关联角色

```

数据结构

```
permission_list = [ {'url':'xxxx'  } ]
menu_list =  [  { url   title   icon    } ] 

```

二级菜单

表结构   RBAC

```
菜单表
	title  标题
	icon 

权限表
	url   url正则表达式    不带^$
    title  标题
    menu  外键关联 菜单表  null=True 
      
    
角色表  
	name   名称
	permissions   多对多  关联权限
	

用户表
	username
	password 
	
	roles   多对多  关联角色

```

数据结构

```
permission_list = [ {'url':'xxxx'  } ]
menu_dict = {
	一级菜单的id  :  {
			title  : 一级菜单的标题
			icon  : 图标
			children : [  
            	{ url  title   }
            ]	
	}
}

```

一级菜单排序

表结构   RBAC

```
菜单表
	title  标题
	icon 
	weight  1   权重大的靠上 

权限表
	url   url正则表达式    不带^$
    title  标题
    menu  外键关联 菜单表  null=True 
      
    
角色表  
	name   名称
	permissions   多对多  关联权限
	

用户表
	username
	password 
	
	roles   多对多  关联角色

```

数据结构

```
permission_list = [ {'url':'xxxx'  } ]
menu_dict = {
	一级菜单的id  :  {
			title  : 一级菜单的标题
			icon  : 图标
			weight ： 权重
			children : [  
            	{ url  title   }
            ]	
	}
}


permission  

id   url       title     menu_id   parent_id
1    /list/    缴费列表      1          null  
2    /add/     添加缴费      null        1
2    /edit/    编辑缴费      null        1
```

 