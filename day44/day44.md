## 内容回顾

### 块级标签

#### 排版标签

```
p div  hr pre
p 前后有点空白
div 没有任何样式的块  span
hr  分割线   br 换行
pre 保留里面的空格
```

#### 列表

```
ul  ol  dl
ul li
	type: disc  square circle none  
ol li
	type: 1 a A I i  start: 2
dl dt dd
	dt 标题
	dd 内容
```

#### 表格

 ```
table
thead:tr
	th  
tbody:tr 
	td
border 1  cellpading  内容和单元格的距离  cellspacing 单元格和边框的距离
rowspan 合并行   colspan 合并列
align 水平排列  left center right
valign 垂直排列 top  middle bottom
 ```

#### 表单

```
form action:提交的地址
input 
	type:text\password\radio\checkbox\submit\button\file\date\reset\hidden
	name\value
	text： placeholder 提示 
		  readonly  只读  不能改值但是可以提交
		  disabled  禁用  不能改值 不可以提交
	radio\checkbox : checked 选中
select ： name  size=3 multiple（多选） 
	option ： value   selected 选中

label 
	<label for="input的id">用户名</label>  input：id 

<textarea name="" cols="10" rows="30"></textarea>
文本框

注意：
	1. 要提交数据  必须有一个input的类型为submit或者button
	2. 上传文件 file ，改编码类型form：enctype="multipart/form-data"
```

### CSS

#### 引入的方式

```
行内引入
<div style="color: red"></div>

内联引入
  <style>
        div {
            color: red;
        }
        
  </style>
  
  
外联引入  链接式
	<link rel="stylesheet" href="home.css">
外联引入  导入式
    <style>
        @import url('home.css');
    </style>

```

#### 简单样式

color 字体颜色  

width 宽度

height 高度

background-color 背景颜色

#### 选择器

#### 基本选择器

```
标签选择器  div  p  span a 
id选择器  #id
类选择器  .类
通用选择器  *
```

#### 高级选择器

后代\子代

```
后代
    <style>
        div span {
            color: red;
        }
    </style>

</head>
<body>
<div>
    <span>div下的span</span>
    <p>
        <span>p下的span</span>

    </p>
</div>
<span>普遍的span</span>

子代
    <style>
        div>span {
            color: red;
        }
    </style>

</head>
<body>
<div>
    <span>div下的span</span>
    <p>
        <span >p下的span</span>
    </p>
</div>
<span>普遍的span</span>


```

毗邻\弟弟

```
毗邻
<style>
        p+a {
            color: red;
        }

    </style>

</head>
<body>


<div>
    <a>这是一个a0 </a>
    <p>这是一个p </p>
    <a >这是一个a1 </a>
    <a >这是一个a2 </a>
    <a>这是一个a3 </a>

</div>


弟弟
    <style>
        p~a {
            color: red;
        }

    </style>

</head>
<body>
<!--<div>-->
<!--    <span>div下的span</span>-->
<!--    <p>-->
<!--        <span >p下的span</span>-->
<!--    </p>-->
<!--</div>-->
<!--<span>普遍的span</span>-->

<div>
    <a>这是一个a0 </a>
    <p>这是一个p </p>
    <a>这是一个a1 </a>
    <a>这是一个a2 </a>
    <a>这是一个a3 </a>

</div>
```

属性

```
   <style>
      
        a[href] {
            color: red;
        }

         a[href='www.taobao.com'] {
            color: red;
        }
    </style>

</head>
<body>

<a href="www.taobao.com">xxxxx</a>
<a href="" >xxxxx</a>
<a href="">xxxxx</a>
<a >xxxxx</a>
```

并集、交集

```
    <style>
        a,p {
            color: red;
        }

    </style>

</head>
<body>

<a>这是一个a0 </a>
<p>这是一个p </p>
<a>这是一个a1 </a>
<a>这是一个a2 </a>
<a>这是一个a3 </a>
```

```
交集
<style>
        a.x1 {
            color: red;
        }


    </style>

</head>
<body>

<a class="x1">这是一个a0 </a>
<p class="x1">这是一个p </p>
<a>这是一个a1 </a>
<a>这是一个a2 </a>
<a>这是一个a3 </a>

```

伪类

```
    <style>
        /*a {*/
        /*    color: red;*/
        /*}*/

        a:visited {
            color: green;
        }

        a:link {
            color: gray;
        }
        a:active {
            color: red;
        }

    </style>

</head>
<body>
<a href="http://www.xiaohuar.com">这是一个a1 </a>
<a href="">这是一个a0 </a>
```

伪元素

```
  <style>

        p:before {
            content: 'p前';
            color: red;
        }

        p:after {
            content: 'p后';
            color: red;
        }

        p:first-letter {

            color: green;
        }

    </style>

</head>
<body>

<p>这是一个p</p>

```

## 今日内容

### css选择器的优先级

```
行内样式>id选择器>类选择器>标签选择器>继承
1000    100     10      1        0
优先级可以累加，但是不进位
优先级相同时，后面的样式会应用
!important  提升选择器的优先级到最高
```

### 颜色的表示

```
rgb 三原色 红 绿 蓝
rab(255,255,255)   白色
rab(0,0,0)   黑色

16进制
#000000 - #ffffff
#000 - #fff

字母：green red 

rgba(255,103,0,1);
a： 透明度  0-1   0 完全透明 1 完全不透明
```

### 字体

```
 font-family: '华文楷体','微软雅黑 Light','宋体';  字体
 font-size:16px;    字体大小
 font-weight:400;   字体的粗细
```

### 文本

```
 text-align: right;  文本水平排列
 text-decoration：  装饰线  underline下划线 line-through 删除线 overline 上划线
 text-indent:2em;    文本缩进  em 相对长度单位  1em=当前字体的一个大小
 line-height:  行高  调节文字上下居中 行高=块的高度
 
 文本显示在一行超出部分显示为省略号
/*强制在一行内显示*/
white-space: nowrap;
/*超出部分隐藏*/
overflow: hidden;
/*显示省略符号来代表被修剪的文本*/
text-overflow: ellipsis;

```

### 背景图片

```
    div {
            /*background-color: red;*/      背景颜色
            /*background-image: url("1.png");*/     背景图片
            /*background-repeat: no-repeat;*/       
            	图片的重复方式  no-repeat 不重复 repeat 重复  repeat-x  x轴方向重复 repeat-y  y轴方向重复 
            /*background-position:right bottom;*/
            width: 900px;
            height: 900px;
            background: red url("1.png") no-repeat left center;   综合的写法
        }
```

### 边框的设置

```
border: 2px;
border-color: black;
border-style: solid(实线);   dashed（虚线）   dotted（实心圆） double（双）
/*  4个 方向  上 右 下 左
            2个   上下  左右
            1个  所有方法
 */
4个方向单独设置
border-left:2px ;
border-left-color:yellow ;
border-left-style:solid ;
统一设置
border: #3bff00 2px solid;
```

### 块和行内解释和转换

```
行内元素不能设置宽高
块元素可以设置宽高
行内 ——》  块   display:block；
块 ——》  行内块  display:inline-block；
display: none;  不显示 并且不占位置
```

### 盒模型

```
padding   内边距  
border  边框
margin  外边距

宽度 = width+2*padding + 2*border + 2*margin 
塌陷的现象：上下的盒子外边距取最大外边距 
```













