from django.conf.urls import url
from app01 import views

urlpatterns = [
    url(r'^login/$', views.login, name='login'),
    url(r'^register/$', views.register, name='register'),
    url(r'^customer/$', views.Customer.as_view(), name='customer'),
    url(r'^my_customer/$', views.Customer.as_view(), name='my_customer'),
    # url(r'^customer_add/$', views.customer_add, name='customer_add'),
    # url(r'^customer_edit/(\d+)/$', views.customer_edit, name='customer_edit'),
    url(r'^customer_add/$', views.customer_change, name='customer_add'),
    url(r'^customer_edit/(\d+)/$', views.customer_change, name='customer_edit'),
    #  展示某个销售的所有的跟进记录
    url(r'^consult_record/$', views.ConsultRecordList.as_view(), name='consult_record'),
    #  展示某个客户的所有的跟进记录
    url(r'^consult_record/(?P<customer_id>\d+)/$', views.ConsultRecordList.as_view(), name='one_consult_record'),

    url(r'^consult_record_add/$', views.consult_record_change, name='consult_record_add'),
    url(r'^consult_record_edit/(\d+)/$', views.consult_record_change, name='consult_record_edit'),

    url(r'^enrollment_list/$', views.EnrollmentList.as_view(), name='enrollment_list'),
    url(r'^enrollment_list/(?P<customer_id>\d+)/$', views.EnrollmentList.as_view(), name='one_enrollment_list'),

    url(r'^enrollment_add/(?P<customer_id>\d+)$', views.enrollment_change, name='enrollment_add'),
    url(r'^enrollment_edit/(?P<pk>\d+)$', views.enrollment_change, name='enrollment_edit'),

]
