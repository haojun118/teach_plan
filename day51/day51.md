## 内容回顾

前端

​	HTML

​	css

​    js

js /jquery       $  ==   jQuery 

jquery封装js的一个库

dom对象  jQuery对象

dom _>  jq   $(dom对象)

jq _> dom    jq对象【index】

js  操作标签   标签的属性  文本  事件

```
document.getEmelentById
document.getEmelentsByClassName
document.getEmelentsByTagName
document.createEmelent

添加  删除 替换  克隆

getAttribute()
setAttribute()
removeAttribute()

节点.innerText
节点.innerText = 

节点.innerHTML
节点.innerHTML = 

节点.onclick = fn 
windows.onload = fn   // 页面 视频 图片 音频 
body.onload = fn   // 页面 
```

jq

```
$('选择器：筛选器').筛选器方法()
#  .  * div     div.s1   div,span
div p
div>p
div+p
div~p

[href]   "[href='xxx']"   "[href!='xxx']"    "[href$='xxx']"  "[href^='xxx']" "[href*='xxx']"   

:first
:last 
:eq(index)
:has(选择器)
:not(选择器)

first（） 
last（）
parent()   children()
siblings()   
prev()   next()
has()  not()
```

```
文本
text()    html()
属性
attr('属性')  
值
val（） 
属性
prop（‘selected’） prop（‘checked’）
事件
jq.bind('事件名'，参数，函数) 
函数名（e）{
	e.data    // 插入的参数
}


jq.事件名(参数，函数) 
函数名（e）{
	e.data    // 插入的参数
}

jq.unbind('事件名')

常用的事件
click
focus blur 
change 
mouseenter  mouseleave      hover
mouseover  mouseout

动画
show  hide  toggle
slideDown(时间，函数)  slideUp()  slideToggle()
fadeIn  fadeOut  fadeToggle 

stop()  停止动画
```

事件冒泡

触发子代的事件，父辈的事件也依次执行。

阻止事件冒泡：

​	return false

​	e.stopPropagation()

事件委托

利用事件冒泡的原理，将子代的事件委托给父代执行。

```
 $('tbody').on('click','button',function () {
     console.log(this)
})

```

bootstrap

https://v3.bootcss.com/

http://www.fontawesome.com.cn/









