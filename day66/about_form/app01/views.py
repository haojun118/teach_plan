from django.shortcuts import render, HttpResponse


# Create your views here.
def register(request):
    if request.method == 'POST':
        # 获取用户提交的数据
        username = request.POST.get('username')
        password = request.POST.get('password')
        # 校验
        if len(username) >= 6:
            # 校验成功给注册
            return HttpResponse('注册成功')
        # 没有校验成功 返回注册页面 + 错误提示
        return render(request, 'register.html', {'error': '用户名太短了'})

    return render(request, 'register.html')


from django import forms
from app01 import models
from django.forms import models as form_model

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator

def checkusername(value):
    if models.User.objects.filter(username=value):
        raise ValidationError('用户名已存在')



def xxxx(value):
    if models.User.objects.filter(username=value):
        raise ValidationError('用户名已存在')

class RegForm(forms.Form):

    def clean(self):
        # 全局钩子
        # 通过校验   返回self.cleaned_data
        # 不通过校验   抛出异常  _>   __all__
        password = self.cleaned_data.get('password')
        re_password = self.cleaned_data.get('re_password')
        if password == re_password:
            return self.cleaned_data
        else:
            self.add_error('password','两次密码不一致!!')
            raise ValidationError('两次密码不一致')

    def clean_username(self):
        # 局部钩子
        # 通过校验   返回当前字段的值
        # 不通过校验   抛出异常
        value = self.cleaned_data.get('username')
        if models.User.objects.filter(username=value):
            raise ValidationError('用户名已存在')
        return value

    username = forms.CharField(
        label='用户名',
        # disabled=True,
        initial="张三",
        widget=forms.TextInput,
        # min_length=6,
        validators=[],
        required=True,
        error_messages={'required': '该字段不能为空', 'min_length': '该字段至少为6位'}
    )
    password = forms.CharField(label='密码', widget=forms.PasswordInput,validators=[])
    re_password = forms.CharField(label='确认密码', widget=forms.PasswordInput,validators=[])
    # gender = forms.CharField(label='性别', widget=forms.SelectMultiple(choices=((1, '男'), (2, '女'))))
    gender = forms.ChoiceField(label='性别', choices=((1, '男'), (2, '女')), widget=forms.RadioSelect)
    # gender = forms.MultipleChoiceField(label='性别', choices=((1, '男'), (2, '女')))
    hobby = forms.MultipleChoiceField(choices=models.Hobby.objects.values_list('id', 'name'),
                                      widget=forms.CheckboxSelectMultiple)

    # hobby = form_model.ModelMultipleChoiceField(queryset=models.Hobby.objects.all())

    keep = forms.fields.ChoiceField(
        label="是否记住密码",
        initial="checked",
        widget=forms.widgets.CheckboxInput()
    )

    email = forms.EmailField(error_messages={'required': '不能为空', 'invalid': '格式错误'})
    phone = forms.CharField(min_length=11,max_length=11,validators=[RegexValidator(r'^1[3-9]\d{9}$','手机号格式不正确')])

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     self.fields['hobby'].choices = models.Hobby.objects.values_list('id', 'name')



def register2(request):
    if request.method == 'POST':
        form_obj = RegForm(request.POST)

        if form_obj.is_valid():  # 校验数据
            # 插入数据库
            print(form_obj.cleaned_data)
            print(request.POST)

            return HttpResponse('注册成功')
    else:
        form_obj = RegForm()
    return render(request, 'register2.html', {'form_obj': form_obj})
