## 内容回顾

ajax

js技术，发请求。

特点：

	1. 异步 
 	2. 局部刷新
 	3. 数据量小 

发请求的方式:

1. 地址栏输入地址  get
2. a标签    get
3. form  get /post
   1. action  提交的地址   method='post'   上传的文件的 enctype="multipart/form-data"
   2. 有button或者 input类型submit 
   3. input 标签有name属性 有的需要有value
4. ajax

简单使用：

使用jq发送ajax请求

```js
$.ajax({
    url:'' ,  //  请求发送的地址
    type：'post',   //  请求方式
    data: {} ,  //  数据
    success:function (res){}      //  响应成功之后执行的回调函数  res返回的响应体 
    
})
```

上传文件

```js
var form_data  =  new FormData()
form_data.append('k1','v1')
form_data.append('f1',$('#f1')[0].files[0])

$.ajax({
    url:'' ,  //  请求发送的地址
    type：'post',   //  请求方式
    data:form_data ,  //  数据
    processData:false,   //  不需要处理数据的编码 multipart/form-data
    contentType:false,   //  不需要处理contentType的请求头
    success:function (res){}      //  响应成功之后执行的回调函数  res返回的响应体 
    
})

```

ajax通过django的csrf的校验

前提：必须要有csrftoken的cookie

方式一：

给data添加csrfmiddlewaretoken键值对

方式二：

给headers 添加  x-csrftoken的键值对 

方式三：

导入文件

csrf相关装饰器

```
from django.views.decorators.csrf import ensure_csrf_cookie,csrf_exempt,csrf_protect

# ensure_csrf_cookie  确保有cookie
# csrf_exempt   不需要进行csrf的校验
# csrf_protect  需要进行csrf的校验

注意：  csrf_exempt CBV的时候只能加载dispatch方法上
```

## 今日内容

form组件

form

1. 有input让用户输入
2. 提交数据

form组件

1. 提供input框
2. 能对数据做校验
3. 返回错误提示

定义：

```
from django import forms
class RegForm(forms.Form):
    username = forms.CharField(label='用户名')
    password = forms.CharField(label='密码')
```

使用：

```
函数
def register2(request):
    if request.method == 'POST':
        form_obj = RegForm(request.POST)

        if form_obj.is_valid():  # 校验数据
            # 插入数据库
            print(form_obj.cleaned_data)
            print(request.POST)

            return HttpResponse('注册成功')
    else:
        form_obj = RegForm()
    return render(request, 'register2.html', {'form_obj': form_obj})

```

```
模板
{{ form_obj.as_p }}   # 展示所有的字段

{{ form_obj.username }}           #  生成input框
{{ form_obj.username.label }}     #  中文提示
{{ form_obj.username.id_for_label }}    # input框的id
{{ form_obj.username.errors }}     # 该字段的所有的错误
{{ form_obj.username.errors.0 }}    # 该字段的第一个的错误

{{ form_obj.errors }}    # 该form表单中所有的错误

```

常用字段：

```
CharField      # 文本输入框
ChoiceField    # 单选框
MultipleChoiceField  # 多选框
```

字段参数：

```
    required=True,               是否必填
    widget=None,                 HTML插件
    label=None,                  用于生成Label标签或显示内容
    initial=None,                初始值
    error_messages=None,         错误信息 {'required': '不能为空', 'invalid': '格式错误'} 
    disabled=False,              是否可以编辑
    validators=[],               自定义验证规则


```

校验

自定义校验规则

1. 写函数

   ```
   from django.core.exceptions import ValidationError
   
   def checkusername(value):
   	# 通过校验规则  什么事都不用干
   	# bu通过校验规则  抛出异常ValidationError
       if models.User.objects.filter(username=value):
           raise ValidationError('用户名已存在')
           
   username = forms.CharField(
   
           validators=[checkusername,]）
   
   ```

2. 用内置的校验器

   ```
   from django.core.validators import RegexValidator
   
   phone = forms.CharField(min_length=11,max_length=11,validators=[RegexValidator(r'^1[3-9]\d{9}$','手机号格式不正确')])
   
   
   ```

   

局部钩子和全局钩子

```python
    def clean(self):
        # 全局钩子
        # 通过校验   返回self.cleaned_data
        # 不通过校验   抛出异常
        password = self.cleaned_data.get('password')
        re_password = self.cleaned_data.get('re_password')
        if password == re_password:
            return self.cleaned_data
        else:
            self.add_error('password','两次密码不一致!!')
            raise ValidationError('两次密码不一致')

    def clean_username(self):
        # 局部钩子
        # 通过校验   返回当前字段的值
        # 不通过校验   抛出异常
        value = self.cleaned_data.get('username')
        if models.User.objects.filter(username=value):
            raise ValidationError('用户名已存在')
        return value

```







