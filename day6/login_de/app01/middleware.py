from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import redirect


class LoginRequired(MiddlewareMixin):

    def process_request(self, request):
        if request.path_info == '/login/':
            return

        is_login = request.session.get('is_login')
        if is_login == 'true':
            return
        else:
            return redirect('/login/')
