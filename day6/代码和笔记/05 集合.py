# python中数据类型之一
# 集合 -- set
# 定义方式:
# dic = {"key":1,"key":2}
# s = {1,2,3,4,}

# 集合就是一个没有值得字典
# 唯一,不可变
# 无序,可变

# 集合天然去重
# s = {1,23,4,2,1,2,3,1}
# print(s)

# 面试题:
# lst = [1,223,1,1,2,31,231,22,12,3,14,12,3]
# print(list(set(lst)))


# s = {}       # 空字典
# s1 = set()   # 空集合
# print(type(s1))
# print(type(s))

# 增:
    # s = set()
    # s.add("alex")
    # s.update("wusir")  # 迭代添加
    # print(s)

    # set("wusir")  # 迭代添加
    # print(s)

# 删:
    # s = {100,0.1,0.5,1,2,23,5,4}
    # s.remove(4)    # 通过元素删除
    # print(s)
    # s.clear()  # 清空
    # s.pop()      # 随机删除 (最小的)
    # print(s)

# 改:
    # s = {1,2,3,4,5}
    # 先删后加

# 查:
    # for循环

# 其他操作:
# s = {1,23,9,4,5,7}
# s1 = {1,2,3,4,5}

# 差集 -
# print(s - s1)
# print(s1 - s)

# 交集  &
# print(s & s1)

# 并集  |(管道符)
# print(s | s1)

# 反交集  shift + 6 ==  ^
# print(s ^ s1)

# s = {1,23,9,4,5,7}
# s1 = {1,4,5}

# 子集  返回的一个布尔值
# print(s > s1)

# 父集(超集)
# print(s1 < s)


# print(frozenset({1,23,4,5}))   # 冻结集合


# 没啥用
# dic = {frozenset({1,23,4,5}):123}
# print(dic)

