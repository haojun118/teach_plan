# == is id

# == 判断两个值是否相等

# a = 10
# b = 10
# print(a == b)

# a = "宝元"
# b = "宝元"
# print(a == b)

# lst = [1,2,3,4]
# lst1 = [1,2,3,4]
# print(lst == lst1)

# is -- 是

# a = 10
# b = 10
# print(a is b)

# 代码块：一个py文件，一个函数，一个模块，终端中每一行都是代码块
# int，str，bool

# int：-5 ~ 正无穷
    # a = -6
    # b = -6
    # print(a is b)

    # a = 1000
    # b = 1000
    # print(id(a),id(b))
    # print(a is b)

# str：
#     字符串:
#       1.定义字符串的时候可以是任意的
#       2.字符串(字母,数字)进行乘法时总长度不超过20
#       3.特殊符号(中文,符号)进行乘法的时候乘以 0 或 1

        # a = "asdfasdfa22143df21af啊发生的vuwqtavuiqf！54人24215124"
        # b = "asdfasdfa22143df21af啊发生的vuwqtavuiqf！54人24215124"
        # print(a == b)
        # print(a is b)

        # a = "alex" * 6
        # b = "alex" * 6
        # print(a == b)
        # print(a is b)

        # a = "你好啊" * 1
        # b = "你好啊" * 1
        # print(a == b)
        # print(a is b)

# bool:
#     True,False

# is 判断的是两边的内存地址是否相同

# 小数据池,代码块同在的情况下先执行代码块

# 小数据池 -- int,str,bool
    # int：-5 ~ 256
    # a = 1000
    # b = 1000
    # print(a is b)

    # str:
    #     1.字母,数字长度任意符合驻留机制
    #     2.字符串进行乘法的时候总长度不能超过20
    #     3.特殊符号进行乘法的时候只能乘 0

        # a = "asdfasdfasdfsad12341123123121wef214afsda"
        # b = "asdfasdfasdfsad12341123123121wef214afsda"
        # print(a is b)

        # a = "你好啊" * 0
        # b = "你好啊" * 0
        # print(a is b)

# 驻留机制: 节省内存空间,提升效率(减少了开辟空间和销毁空间的耗时)
