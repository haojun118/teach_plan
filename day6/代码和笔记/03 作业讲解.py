"""
9.有字符串"k: 1|k1:2|k2:3 |k3 :4" 处理成字典 {'k':1,'k1':2,'k3':4}
写代码
"""
# s = "k: 1|k1:2|k2:3 |k3 :4"
# str_lst = s.replace(" ","").split("|")
# # print(str_lst)
#
# dic = {}
# for i in str_lst:
#     # dic = {"k":1}  dic = {"k1":2}
#     k,v = i.split(":")
#     dic[k] = int(v)
#
# print(dic)

"""
10.有如下值 li= [11,22,33,44,55,77,88,99,90],将所有大于 66 的值保存至字典的第一个key对应的列表中，
将小于 66 的值保存至第二个key对应的列表中。
"""
# li= [11,22,33,44,55,77,88,99,90]
# result = {'k1':[],'k2':[]}
#
# for i in li:
#     if i > 66:
#         result["k1"].append(i)
#     else:
#         result["k2"].append(i)
# print(result)

"""
11.看代码写结果
"""

# v = {}  # "users":9
# for index in range(10):
#     v['users'] = index
# print(v)

"""
12.输出商品列表，用户输入序号，显示用户选中的商品
商品列表：
goods = [
{"name": "电脑", "price": 1999},
{"name": "鼠标", "price": 10},
{"name": "游艇", "price": 20},
{"name": "美女", "price": 998}
]
要求:
1：页面显示 序号 + 商品名称 + 商品价格，如：
1 电脑 1999
2 鼠标 10
...
2：用户输入选择的商品序号，然后打印商品名称及商品价格
3：如果用户输入的商品序号有误，则提示输入有误，并重新输入。
4：用户输入Q或者q，退出程序。
"""

goods = [
    {"name": "电脑", "price": 1999},
    {"name": "鼠标", "price": 10},
    {"name": "游艇", "price": 20},
    {"name": "美女", "price": 998}
]

# PEP8
# while True:
#     for i in range(len(goods)):
#         print(i + 1, goods[i]["name"], goods[i]["price"])
#
#     num = input("请输入商品序号(Q/退出):")  # 1
#     if num.isdecimal() and len(goods) >= int(num) > 0:
#         num = int(num) - 1
#         print(goods[num]["name"], goods[num]["price"])
#
#     elif num.upper() == "Q":
#         break
#
#     else:
#         print("输入有误,请重新输入!")
