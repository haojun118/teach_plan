class People:
    mind = "有思想"  # 静态属性

    def eat(self):   # 方法
        print("在吃饭")

    def work(self):
        print("在工作")

# 查看类下所有内容
# print(People.__dict__)

# 万能的点 查看单个属性或方法
# print(People.mind)

# 增:
# People.emotion = "有情感"

# 删:
# del People.mind

# 改:
# People.mind = "无脑"
# print(People.__dict__)

# 查:
# print(People.mind)  # 单独查一个

# People.eat(1)
# 一般情况下我们不使用类名去操作方法 (类方法除外)
