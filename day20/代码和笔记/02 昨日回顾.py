# 1.包
# 具有__init__.py的文件就是一个包

# 导入:

    # 启动文件要和包文件是同级
    # 绝对路径: import 包.模块  form 包.包 import 模块
    # 相对路径: from ..包 import 模块,都会触发 __init__.py
    # 在启动文件启动包,包里导入了包中同级模块,需要填加到 sys.path中

    # __init__.py是每个包的秘书,所有操作都交于秘书干
    # __file__ : 获取当前文件的路径, __name__ : 获取当前文件的名称

    # .当前目录
    # ..上一级
    # ...上上级

# 2.日志
    # 自动挡
    # import logging
    # # 初始化一个空日志
    # logger = logging.getLogger()   # -- 创建了一个对象
    # # 创建一个文件,用于记录日志信息
    # fh = logging.FileHandler('test.log',encoding='utf-8')
    # # 创建一个文件,用于记录日志信息
    # fh1 = logging.FileHandler('test1.log',encoding='utf-8')
    # # 创建一个可以在屏幕输出的东西
    # ch = logging.StreamHandler()
    # # 对要记录的信息定义格式
    # msg = logging.Formatter('%(asctime)s - [line:%(lineno)d] %(filename)s - %(levelname)s - %(message)s')
    # # 对要记录的信息定义格式
    # msg1 = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    # # 设置记录等级
    # logger.setLevel(10) or logger.setLevel(logging.DEBUG)
    # # 等级对应表
    # '''
    # DEBUG - 10
    # INFO - 20
    # WARNING - 30
    # ERROR - 40
    # CRITICAL - 50
    # '''
    # 将咱们设置好的格式绑定到文件上
    # fh.setFormatter(msg)
    # fh1.setFormatter(msg)
    # # 将咱们设置好的格式绑定到屏幕上
    # ch.setFormatter(msg1)
    # 将设置存储日志信息的文件绑定到logger日志上
    # logger.addHandler(fh) #logger对象可以添加多个fh和ch对象
    # logger.addHandler(fh1)
    # logger.addHandler(ch)
    # # 记录日志
    # logger.debug([1,2,3,4,])
    # logger.info('logger info message')
    # logger.warning('logger warning message')
    # logger.error('logger error message')
    # logger.critical('logger critical message')

# 3.hashlib
    # 加密,校验
    # md5,sha1,sha256,sha512
    # md5 速度快,安全系数低
    # sha512 速度慢,安全系数高
    # 加密的内容一样时,密文一定一致,内容不一致时,密文不一定一样
    # 加密是不可逆的

    # 加盐
    # import hashlib
    # md5 = hashlib.md5("盐".encode("utf-8"))
    # md5.update("你要加密的内容".encode("utf-8"))
    # md5.hexdigest()

    # 动态加盐

    # 只有英文时 b"alex123"就是字节
    # update() 可以写多次
    # 中文时,内容相同编码不同时,密文不同
    # 英文是相同的


# 4.collections

    # Counter()  计数  统计元素出现的次数,返回的一个字典
    # deque()    双端队列
    # namedtuple() 命名元组
    # OrderedDict() 有序字典  (python2中使用)
    # defaultdict() 默认字典


    # from collections import Iterator,Iterable
    # isinstance  -- 判断数据类型,返回布尔值
    # lst = [1,2,3,4,5]
    # print(isinstance(lst,list))
    # print(isinstance(lst,Iterable))
    # print(isinstance(lst,Iterator))