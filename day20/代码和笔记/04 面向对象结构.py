# class -- def 都是关键字

# def 函数名(使用下划线):
#     函数体

# class 类名(使用驼峰体):
#     静态属性 (类变量,静态字段)
#     方法 (类方法,动态属性,动态字段)

class People:

    mind = "有思想"  # 静态属性

    def eat(self):   # 方法
        print("在吃饭")

    def work(self):
        print("在工作")


class Dog:

    hair = "有毛"   # 静态属性

    def eat(self):
        print("吃大bb")

    def lick(self):
        print("会舔")

