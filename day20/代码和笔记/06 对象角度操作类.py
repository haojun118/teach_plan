# class People:
#     mind = "有思想"  # 静态属性
#
#     def eat(self):   # 方法
#         print("self --->",self)
#         print("在吃饭")
#
#     def work(self):
#         print("在工作")

# 创建对象 -- 类名()
# p = People()  # 实例化对象
# print(p.__dict__)  # 对象的空间
# print("p---->",p)
# print(p.mind)
# p.eat()
# p.work()



class People:

    mind = "有思想"  # 静态属性

    def __init__(self,name,age,sex):   # 初始化
        # self == p
        self.name = name
        self.age = age
        self.sex = sex

    def eat(self):   # 方法
        print(self.name)
        print("在吃饭")

    def work(self):
        print("在工作")

p = People("alex",19,"男") # 实例化一个对象
p.eat()


# 类外部给对象创建属性 不建议这样使用
# p.mind = "无脑"  # 给对象创了一个空间
# print(People.__dict__)
# print(p.__dict__)




# 对象只能使用类中的属性和方法,不能进行修改

# 1. 实例化一个对象,给对象开辟空间
# 2. 自动执行__init__方法
# 3. 自动将对象的地址隐性传递给了self