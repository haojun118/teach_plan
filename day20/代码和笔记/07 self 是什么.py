class People:

    mind = "有思想"  # 静态属性

    def __init__(self,name,age,sex,high=None):   # 初始化(给创建的对象封装独有属性)
        # self == p
        self.name = name
        self.age = age
        self.sex = sex
        if high:
            self.high = high

    def eat(self):   # 方法
        print(f"{self.name}在吃饭")

    def work(self):
        print("在工作")

p1 = People("alex",19,"未知",170) # 实例化一个对象
p2 = People("wusir",19,"未知",170) # 实例化一个对象
p3 = People("rimo",19,"未知",170) # 实例化一个对象
p4 = People("李俊玲",19,"未知",170) # 实例化一个对象
p5 = People("狗哥",19,"未知",170) # 实例化一个对象

p1.eat()
p2.eat()


# p2 = People("日魔",15,"人x",40) # 实例化一个对象
# p3 = People("狗哥",21,"男",175) # 实例化一个对象
# p4 = People("豹哥",18,"男",100) # 实例化一个对象



# self :
# 1.就是函数的位置参数
# 2.实例化对象的本身(p和self指向的同一个内存地址)