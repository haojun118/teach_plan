# 面向  对象(核心)   编程

# 1.面向过程 vs 函数式编程

# s = "alexdsb"
# count = 0
# for i in s:
#     count += 1
# print(count)
#
# s = [1,2,7,3,4,5,]
# count = 0
# for i in s:
#     count += 1
# print(count)


# def my_len(s):
#     count = 0
#     for i in s:
#         count += 1
#     print(count)
# my_len([1,2,3,4,5])

# 1.减少重复代码
# 2.可读性高

# 函数式编程 vs 面向对象

def login():
    pass

def check_buy_goods():
    pass

def change_pwd():
    pass

def shopping():
    pass

def register():
    pass

def check_unbuy_goods():
    pass


class Auth:

    def login(self):
        pass

    def register(self):
        pass

    def change_pwd(self):
        pass

class Shopping:

    def shopping(self):
        pass

    def check_buy_goods(self):
        pass

    def check_unbuy_goods(self):
        pass

# 1.结构清晰,可读性高
# 2.上帝思维(慢慢体会)

# 面向对象:
# 类 : 对一事物的统称和概况
# 对象 : 实实在在存在的东西,具有特征和功能
