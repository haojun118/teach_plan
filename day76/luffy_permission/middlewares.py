from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import redirect, reverse, HttpResponse
from django.conf import settings
import re


class AuthMiddleWare(MiddlewareMixin):

    def process_request(self, request):
        url = request.path_info
        # 白名单
        for i in settings.WHITE_LIST:
            if re.match(i, url):
                return
        # 登录状态的校验
        is_login = request.session.get('is_login')
        if not is_login:
            return redirect(reverse('login'))

        # 免认证的地址校验
        for i in settings.PASS_LIST:
            if re.match(i, url):
                return
        # 权限的校验
        permissions = request.session.get('permission')
        for i in permissions:
            if re.match(r'^{}$'.format(i['permissions__url']), url):
                return

        return HttpResponse('没有访问的权限，请联系管理员')
