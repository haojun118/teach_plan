from django.db import models


class Permission(models.Model):
    url = models.CharField(max_length=200, verbose_name='权限')
    title = models.CharField(max_length=32, verbose_name='标题')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "权限表"
        verbose_name_plural = verbose_name


class Role(models.Model):
    name = models.CharField(max_length=32, verbose_name="角色名")
    permissions = models.ManyToManyField('Permission',blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "角色表"
        verbose_name_plural = verbose_name


class User(models.Model):
    username = models.CharField(max_length=32, verbose_name="用户名")
    password = models.CharField(max_length=32, verbose_name="密码")
    roles = models.ManyToManyField('Role',blank=True)

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = "用户表"
        verbose_name_plural = verbose_name
