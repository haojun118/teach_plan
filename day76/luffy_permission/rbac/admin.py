from django.contrib import admin
from rbac import models

# Register your models here.
admin.site.register(models.User)


class PermissionAdmin(admin.ModelAdmin):
    list_display = ['id', 'url', 'title']
    list_editable = ['url', 'title']
    ordering = ('id',)


admin.site.register(models.Permission, PermissionAdmin)

admin.site.register(models.Role)
