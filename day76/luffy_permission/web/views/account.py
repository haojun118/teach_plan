from django.shortcuts import render, redirect, reverse, HttpResponse
from rbac import models


def login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user_obj =models.User.objects.filter(username=username, password=password).first()
        if not user_obj:
            return render(request, 'login.html', {'error': '用户名或密码错误'})
        # 用户名和密码校验成功
        # 获取权限的信息  保存到session中
        # ret = user_obj.roles.exclude(permissions__url=None).values('permissions__url').distinct()
        permissions  = user_obj.roles.filter(permissions__url__isnull=False).values('permissions__url').distinct()

        request.session['is_login'] = True
        request.session['permission'] = list(permissions)

        return redirect(reverse('index'))


    return render(request, 'login.html')


def index(request):
    return render(request,'index.html')