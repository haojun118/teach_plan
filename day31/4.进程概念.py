# 程序与进程(计算机中最小的资源分配单位)
    # 运行中的程序 就是 进程
    # 进程与进程之间的数据是隔离的
# 线程(计算机中能被操作系统调度的最小单位)
    # 每个程序执行到哪个位置是被记录下来的
    # 在进程中 有一条线程是负责具体的执行程序的

# 进程的调度(由操作系统完成的) :
    # 被操作系统调度的,每个进程中至少有一个线程
    # 短作业优先算法
    # 先来先服务算法
    # 时间片轮转算法
    # 多级反馈算法
# 进程的启动 销毁
    # 进程的启动 : 交互(双击) 在一个进程中启动另一个 开机自启动
        # 负责启动一个进程的程序 被称为一个父进程
        # 被启动的进程 被成为一个子进程
    # 销毁 : 交互  被其他进程杀死(在父进程结束子进程)  出错进程结束
# 父子进程
    # 父进程开启子进程
    # 父进程还要负责对结束的子进程进行资源的回收
# 进程id --> processid --> pid
    # 在同一台机器上 同一个时刻 不可能有两个重复的进程id
    # 进程id不能设置 是操作系统随机分配的
    # 进程id随着多次运行一个程序可能会被多次分配 每一次都不一样
# 进程的三状态图
    # 就绪ready 运行run  阻塞block


# import os
# import time
#
# print(os.getpid())
# print(os.getppid())  # parent process id
# time.sleep(100)

# 2.模块multiprocessing模块 :内置模块
# multiple 多元化的
# processing 进程
# 把所有和进程相关的机制都封装在multiprocessing模块中了

# 3.学习这个模块
import os
import time
from multiprocessing import Process

def func():
    '''
    在子进程中执行的func
    :return:
    '''
    print('子进程 :',os.getpid(),os.getppid())
    time.sleep(3)
if __name__ == '__main__':
    p = Process(target=func)
    p.start()
    print('主进程 :',os.getpid())

# 并行 : 多个程序同时被CPU执行
# 并发 : 多个程序看起来在同时运行
# 同步 : 一个程序执行完了再调用另一个 并且在调用的过程中还要等待这个程序执行完毕
# 异步 : 一个程序在执行中调用了另一个 但是不等待这个任务完毕 就继续执行 start
# 阻塞 : CPU不工作
# 非阻塞 : CPU工作