# 穿孔卡片
# 高速磁带
# -- 操作系统
    # 多道操作系统
        # 第一次提出了多个程序可以同时在计算机中被计算
        # 1.遇到IO就让出CPU
        # 2.把CPU让给其他程序,让其他程序能够使用CPU
        # 3.CPU的让出这件事 占用时间
        # 4.两个程序来回在CPU上切换,不会
            # 每个程序有独立的内存空间
            # 每个程序在切换的前后会把当前程序的状态记录下来

# CPU计算和不计算(IO)操作
# IO操作(网络操作\文件操作) : 输入输出:相对内存
    # 阻塞: sleep\input\recv\accept\recvfrom是不需要cpu参与的
    # 对文件的读取 : 对硬盘的操作一次读取相当于90w条代码
    # Input : 向内存输入数据
        # 读\load\input\recv\recvfrom\accept\connect\close
    # Output : 从内存输出数据
        # 写\dump\print\send\sendto\accept\connect\close
    # 所有的IO操作本质都是文件操作
        # input\print input是写入文件,然后通过读取文件把输入的内容加载到内存
        #               print是直接写入文件,然后通过文件展示给用户看
        # socket中的交互方法 : 都是文件操作
            # send 是向缓存文件中写
            # recv 是从缓存文件中读
        # 也就是说只要涉及到IO操作 至少就是一个0.009s=就是CPU执行90w条python代码的时间

# 0.009s
# 500000000条指令/s  /5 = 100000000条python代码/s
# 0.009s * 100000000 = 900000条python代码

# import dis
# a = 1
# def func():
#     global a
#     a+=1
# dis.dis(func)

# 1.老教授和研究生
    # 研究生 5min 没有IO操作 先来先服务(FIFS)
    # 老教授  24h 没有IO操作
    # 研究生 3min 没有IO操作 短作业优先算法

# 2.时间片轮转算法 -- 分时操作系统
    # 1w = 0.00005s
    # 1.时间片到了才让出CPU
    # 2.CPU的让出这件事 占用时间
    # 3.减低工作效率,提高了用户体验











