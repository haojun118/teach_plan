## 内容回顾

前后端怎么交互

浏览器发送一个请求到后端，后端处理逻辑，把结果（HTML文本）返回给浏览器。

前端

HTML   骨架 内容

css     样式

js      动态效果

HTML  超文本标记语言

标记 标签

分类

单边标记\双边标记 

head

```html
<meta>  编码 关键字 描述  
<title>标题的内容</title>  
link	 style   script  
```

body

内联(行内)标签

```span
字体
	加粗  b   strong
	斜体  i  em 
	上下标   sup  sub 
	中划线 下划线   del s  u 
<span></span> 
<br>
```

```html
<a href="链接" target="_blank" title >页面显示的内容</a>
href: 链接地址
	锚点： #id
target：打开方式
	_blank： 新建一个窗口
	_self:  当前窗口
title: 鼠标悬浮上显示的标题
```

```html
<img src="图片的地址" alt='' title width="20px"  height="100%" >
src:   网络地址、本地地址（相对路径、绝对路径）
alt ：  链接地址有问题时提示的内容 
title: 鼠标悬浮上显示的标题
width： 设置图片的宽度
height：设置图片的高度  
width和height选择一个
```

特殊字符：

&nbsp:  空格  

块级标签  

​	h1 - h6



## 今日内容

### 块级标签

#### 排版标签

p:一个文本级别的段落标签    前后有间距

P标签中不嵌套其他的块级标签

div  没有任何样式的块级标签

hr   分割线

#### 列表

##### 无序列表    

type="原点的样式"

```html
<!-- 样式 disc（实心圆、默认）、 circle（空心圆）、none（无）、square（方点）-->

<ul >
    <li>手机</li>
    <li>电脑</li>
    <li>iPad</li>
</ul>

<ul type="none">
    <li>手机</li>
    <li>电脑</li>
    <li>iPad</li>
</ul>
<ul type="circle">
    <li>手机</li>
    <li>电脑</li>
    <li>iPad</li>
</ul>
<ul type="square">
    <li>手机</li>
    <li>电脑</li>
    <li>iPad</li>
</ul>

```

##### 有序列表   

type="数字的样式"  start =“起始值”（数字）

```html
<!-- 样式 1（数字）、 a（小写字母）、A（大写）、I（罗马数字）-->

<ol>
    <li>手机</li>
    <li>电脑</li>
    <li>iPad</li>
</ol>

<ol type="1">
    <li>手机</li>
    <li>电脑</li>
    <li>iPad</li>
</ol>

<ol type="a" start="25">
    <li>手机</li>
    <li>电脑</li>
    <li>iPad</li>
</ol>

<ol type="A">
    <li>手机</li>
    <li>电脑</li>
    <li>iPad</li>
</ol>

<ol type="I">
    <li>手机</li>
    <li>电脑</li>
    <li>iPad</li>
</ol>

```

##### 定义列表 

dt 标题

dd 内容

```html
<dl>
    <dt>城市</dt>
    <dd>北京</dd>
    <dd>上海</dd>
    <dd>深圳</dd>

    <dt>城市</dt>
    <dd>北京</dd>
    <dd>上海</dd>
    <dd>深圳</dd>
</dl>

```

#### 表格

```html
<!--有表头的表格-->
<!--tbale 嵌套 thead tbody 
	thead和tbody嵌套tr
	tr嵌套 th  td 
-->

<!--tbale 属性 
border 边框
cellpadding   元素和单元格的边距
cellspacing   单元格和边框的间距
-->

<!--tr 属性 
align   内容的水平排列  left  center  right
valign  内容的垂直排列  top middle bottom
-->
<!--td  属性 
rowspan   占几行
colspan   占几列
-->

<table border="1" cellpadding="20px" cellspacing="20px">
    <thead>
    <tr align="left">
        <th> 序号</th>
        <th> 姓名</th>
        <th> 年龄</th>
    </tr>
    </thead>
    <tbody>
    <tr align="center" valign="bottom">
        <td>1</td>
        <td >alex</td>
        <td >84</td>


    </tr>
    <tr align="center" valign="top">
        <td>2</td>
        <td >alex</td>


    </tr>
    <tr>
        <td>2</td>
        <td>wusir</td>
        <td rowspan="2">2208</td>

    </tr>
    </tbody>

</table>

<!-- 无表头的表格-->
<table border="1" cellpadding="20px" cellspacing="20px">
    
    <tbody>
    <tr align="center" valign="bottom">
        <td>1</td>
        <td >alex</td>
        <td >84</td>


    </tr>
    <tr align="center" valign="top">
        <td>2</td>
        <td >alex</td>


    </tr>
    <tr>
        <td>2</td>
        <td>wusir</td>
        <td rowspan="2">2208</td>

    </tr>
    </tbody>

</table>

```

#### 表单

```html
<!-- form 标签
action： 提交的地址  
-->

<!-- input 标签
type： 类型   
	text：普通文本
	password：密码 密文
	radio：  单选框
	checkbox： 复选框
	submit： 提交按钮
能提交数据 input有name属性  有value值

-->

<button>提交</button>  
<!-- 
form表单中button和type=‘submit’的input的作用是一样的
-->


<form action="http://127.0.0.1:9999">
    <input type="text" name="user" placeholder="请输入用户名">   <!--name:提交数据的key   value:值   placeholder：占位的内容 -->
    <input type="password" name="pwd" value="3714">
    <input type="radio" name="sex" value="1"> 男
    <input type="radio" name="sex" value="2" checked  > 女  <!-- checked默认选中 -->
    <input type="checkbox" name="hobby" value="1"> 跳
    <input type="checkbox" name="hobby" value="2"> 唱
    <input type="checkbox" name="hobby" value="3"> rap
    <input type="checkbox" name="hobby" value="4"> 篮球
    <input type="submit">
    <button>提交</button  
    
</form>
```

label

```html
<!--给input标签定义id  给label标签的for填上id-->
<label for="i1">用户名：</label>
<input id="i1" type="text" name="user" placeholder="请输入用户名">

```

其他的input

```html
<input type="hidden" name="alex" value="alexdsb">   <!--  隐藏的input框  -->
<input type="reset"> <!--  重置按钮  -->
<input type="button" value="提交"> <!--  普通的按钮  -->
<button type="button">提交</button>  <!--  普通的按钮  -->
<input type="date">    <!--  日期格式  -->
```

select  option

```html
<!--下拉框 单选 -->
<!--size  框的大小 -->
<select name="city" id="" size="4" >
     <option value="1">北京</option>
     <option value="2"  selected >上海</option>  <!-- selected默认选中 -->
     <option value="3">深圳</option>
</select>

<!--下拉框 多选 -->
<!--size  框的大小 -->
<select name="city" id="" size="4"  multiple>
     <option value="1">北京</option>
     <option value="2">上海</option>
     <option value="3">深圳</option>
</select>

```

上传文件 

```html
<input type="file" name="f1">   
<form  enctype="multipart/form-data">  <!--编码指定为multipart/form-data-->
```

### CSS

#### 引入方式

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>

    <!--内联引入-->
    <style>
        div {
            color: #ffef6b
        }

    </style>

    <!--外联引入：链接  使用较多  -->
        <link rel="stylesheet" href="index.css">
    <!--外联引入：导入-->
    <style>
        @import url('index.css');
    </style>


</head>
<body>


<!--行内引入-->
<div style="color: red">黄焖鸡米饭</div>
<div>黄焖鸡排骨</div>

</body>
</html>
```

#### 简单的样式

```css

 color: #ffef6b;      /*字体颜色*/
 width: 200px;		  /*宽度*/
 height: 200px;		  /*高度*/	
 background: blue;   /*背景颜色*/

```

#### 选择器

##### 基本选择器

标签\id\类\通用选择器

```html
<style>
        div {      标签
            color: #ffef6b;
        }

        a {
            color: green;
        }


        span {
            color: #42ff68;
        }

        #sp1 {    id
            color: chartreuse;

        }

        .cai {    类
             color: #192aff;
        }

        .x1 {
             background: #3bff00;
        }
    
    	
		* {
             background: #3bff00;
        }

    </style>


```

```html
<body>

<!--<div >黄焖鸡米饭</div>-->
<!--<div>黄焖鸡排骨</div>-->

<div>黄焖鸡米饭
    <span class="cai">鸡</span>
    <span>米饭</span>
    <a href="xxxx">外卖连接</a>
</div>
<div>黄焖鸡排骨
    <span class="cai x1 x2">排骨</span>
    <span>米饭</span>
</div>
<span id="sp1">米饭</span>


</body>
```

















