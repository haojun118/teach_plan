
# logging -- 日志
# 1.记录用户的信息
# 2.记录个人流水
# 3.记录软件的运行状态
# 4.记录程序员发出的指令
# 5.用于程序员代码调试

# 日志中要记录的信息
# 默认从warning开始记录

# 手动挡
# import logging
# logging.basicConfig(
#     level=logging.DEBUG,
#     format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
#                     datefmt='%Y-%m-%d %H:%M:%S',
#                     filename="test.log",
#                     filemode="a",
# )
#
#
# logging.debug("你是疯儿,我是傻") # debug 调试
# logging.info("疯疯癫癫去我家")   # info 信息
# logging.warning("缠缠绵绵到天涯")   # info 警告
# logging.error("我下不床")           # error 错误
# logging.critical("你回不了家")        # critical 危险


# 自动挡
# import logging
# # 初始化一个空日志
# logger = logging.getLogger()   # -- 创建了一个对象
# # 创建一个文件,用于记录日志信息
# fh = logging.FileHandler('test.log',encoding='utf-8')
# # 创建一个文件,用于记录日志信息
# fh1 = logging.FileHandler('test1.log',encoding='utf-8')
# # 创建一个可以在屏幕输出的东西
# ch = logging.StreamHandler()
# # 对要记录的信息定义格式
# msg = logging.Formatter('%(asctime)s - [line:%(lineno)d] %(filename)s - %(levelname)s - %(message)s')
# # 对要记录的信息定义格式
# msg1 = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
# # 设置记录等级
# logger.setLevel(10) or logger.setLevel(logging.DEBUG)
# # 等级对应表
# '''
# DEBUG - 10
# INFO - 20
# WARNING - 30
# ERROR - 40
# CRITICAL - 50
# '''
# 将咱们设置好的格式绑定到文件上
# fh.setFormatter(msg)
# fh1.setFormatter(msg)
# # 将咱们设置好的格式绑定到屏幕上
# ch.setFormatter(msg1)
# 将设置存储日志信息的文件绑定到logger日志上
# logger.addHandler(fh) #logger对象可以添加多个fh和ch对象
# logger.addHandler(fh1)
# logger.addHandler(ch)
# # 记录日志
# logger.debug([1,2,3,4,])
# logger.info('logger info message')
# logger.warning('logger warning message')
# logger.error('logger error message')
# logger.critical('logger critical message')

