# collections  -- 基于python自带的数据类型之上额外增的几个数据类型

# 命名元组:
# from collections import namedtuple
# limit = namedtuple("limit",["x","y"])
# l = limit(1,2)
# print(l.x)
# print(l[0])


# 双端队列
from collections import deque
# lst = [1,2,3,4]

# deque = [1,2,3,4]
# deque.append(1)
# deque.remove(1)
# print(deque)

# l = deque([1,2])
# l.append(3)
# l.appendleft(0)
# l.pop()
# l.popleft()
# l.remove(2)
# print(l)

# 队列: 先进先出
# 栈: 先进后出

# from collections import OrderedDict
# 有序字典(python2版本)  -- python3.6 默认是显示有序

# dic = OrderedDict(k=1,v=11,k1=111)
# print(dic)
# print(dic.get("k"))
# dic.move_to_end("k")


from collections import defaultdict
# 默认字典

# dic = defaultdict(list)
# dic[1]
# print(dic)

# lst = [11,22,33,44,55,77,88,99]
# dic = defaultdict(list)
# for i in lst:
#     if i > 66:
#         dic['key1'].append(i)
#     else:
#         dic['key2'].append(i)
# print(dict(dic))


# from collections import Counter
# 计数 返回一个字典
# lst = [1,2,112,312,312,31,1,1,1231,23,123,1,1,1,12,32]
# d = Counter(lst)
# print(list(d.elements()))
# print(dict(d))

# 重要Counter