# 管理模块(文件化)
# 什么是包?
# 文件夹下具有__init__.py的就是一个包


# import bake     #现在不好使
# bake.api.es

# 指定功能导入
# import bake.api.es
# bake.api.es.func()

# import bake.api.es as f
# f.func()
# f.foo()

# from bake.api.es import func,foo
# func()
# foo()


# 导入模块中全部
# import bake
# bake.api.es.foo()       # 建筑师
# bake.api.es.func()      # 体验师   --  宾馆 -- 睡觉(裸睡) -- 体验(酒店设施,酒店的床)  -- 写报告
#
# bake.cmd.manage.rimo()  # 炊事班
# bake.db.models.alex()   # 保洁部

# 路径
# 绝对路径:从包的最外层进行查找,就是绝对路径
# from bake.api.es import *

# from ..
# func()

import sys
print(sys.path)