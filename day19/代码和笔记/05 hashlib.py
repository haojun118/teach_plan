# 摘要算法,加密算数 ...
# 1.加密
# 2.校验

# md5,sha1,sha256,sha512
# 1.md5,加密速度快,安全系数低
# 2.sha512 加密速度慢,安全系数高

# 明文(123adsa) -- 字节 -- 密文(bs2501153023ras32rf150q23r13ar)

# 1.当要加密的内容相同时,你的密文一定是一样的
# 2.当你的明文不一样时,密文不一定一样
# 3.不可逆

# import hashlib
# md5 = hashlib.md5()   # 初始化
# md5.update("alex".encode("utf-8"))   # 将明文转换成字节添加到新初始化的md5中
# print(md5.hexdigest())   # 进行加密

# dic = {"534b44a19bf18d20b71ecc4eb77c572f":"alex"}

# 534b44a19bf18d20b71ecc4eb77c572f
# 9b4c00b63b24c060abd31c6cb96b7bc8

# msg = input("请输入密码")
# print(msg)

# 加盐

# 加固定盐
# import hashlib
# md5 = hashlib.md5("rimo_dsb".encode("utf-8"))   # 初始化
# md5.update("alex".encode("utf-8"))   # 将明文转换成字节添加到新初始化的md5中
# print(md5.hexdigest())   # 进行加密

# 加动态盐

# import hashlib
# user = input("username:")
# pwd = input("password:")
#
# md5 = hashlib.md5(user.encode("utf-8"))   # 初始化
# md5.update(pwd.encode("utf-8"))   # 将明文转换成字节添加到新初始化的md5中
# print(md5.hexdigest())   # 进行加密


# 1f174367fa08bf51d789a5c988f8ff1e
# d599321766c76d5ce8b9e2b53ebd5764

# 60c6d277a8bd81de7fdde19201bf9c58a3df08f4
# 35f319ca1dfc9689f5a33631c8f93ed7c3120ee7afa05b1672c7df7b71f63a6753def5fd3ac9db2eaf90ccab6bff31a486b51c7095ff958d228102b84efd7736

# import hashlib
# sha1 = hashlib.sha1()
# sha1.update("alex".encode("utf-8"))
# print(sha1.hexdigest())

# import hashlib
# sha1 = hashlib.sha1()
# sha1.update("日魔就是一个大SB".encode("utf-8"))
# print(sha1.hexdigest())
#
# sha1 = hashlib.sha1()
# sha1.update("日魔就是一个大SB".encode("gbk"))
# print(sha1.hexdigest())

# 中文内容编码不同时密文是不一致,英文的密文都是一致的

# import hashlib
# md5 = hashlib.md5()
# md5.update(b"afdadfadfadsfafasdfasfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfd")
# md5.update(b"afdadfadfadsfafasdfasfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfd")
# md5.update(b"afdadfadfadsfafasdfasfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfd")
# print(md5.hexdigest())
# efe9bb9e7e090768597517019e5716b6
# efe9bb9e7e090768597517019e5716b6

# import hashlib
# md5 = hashlib.md5()
# md5.update(b"afdadfadfadsfafasd")
# print(md5.hexdigest())

# import hashlib
# def file_check(file_path):
#     with open(file_path,mode='rb') as f1:
#         md5= hashlib.md5()
#         while True:
#             content = f1.read(1024)   # 2049 1025 1
#             if content:
#                 md5.update(content)
#             else:
#                 return md5.hexdigest()
# print(file_check('python-3.6.6-amd64.exe'))

# ftp 