# 运算符:
# 1.算数运算符
#     +
#     -
#     *
#     /
#     %(取余)
#         print(8 % 2)
#     ** (幂/次方)
#       print(2 ** 16)
#    //(整除(地板除) -向下取整)
#       print(5//2)

# 2.比较运算符
#     > < >= <=
#     == 等于
#     != 不等于

# 3.赋值运算符
#     =
    # += 自加
    # a = 10
    # a += 1  # a = a + 1
    # print(a)

    # -= 自减  a = a - 1
    # *=  # a = a  * 1
    # /=  # a = a / 1
    # **= # a = a ** 1
    # %=  # a = a % 1
    # //= # a = a // 1

# 4.逻辑运算符
#     and or not
#     与  或  非

    # True and False
    # 数字非零的都是真

    # and 的运算,都为真才是真,有一个是假就是假
    # and 的运算,都为真的的时候选择and后边的内容
    # and 的运算,都为假的的时候选择and前边的内容

    # or 的运算,只要有一个真就是真
    # or 的运算,都为真的的时候选择or前边的内容
    # or 的运算,都为假的的时候选择or后边的内容

    # 非 -- 不是
    # print(not True)

    # print(11 > 2 and 8)

    # 优先级: () > not > and > or

# 9 or 8 and 4 and not 4 < 5 or 8
# 9 or 8 and 4 and False or 8
# 9 or 4 and False or 8
# 9 or False or 8
# 9 or 8
# 9

# 8 and 9 or not False and 15
# 8 and 9 or True and 15
# 8 and 9 or  15
# 9 or 15

# not 3<5 and 6>3 or 6 and not True or False and 8 or 90
# False and 6>3 or 6 and not True or False and 8 or 90
# False and 6>3 or 6 and False or False and 8 or 90
# False or 6 and False or False and 8 or 90
# False or  False or False and 8 or 90
# False or False or False or 90
# 90

# 5.成员运算符
# in 在
# not in 不在

# a = "alex"
# if "b" not in a:
#     print("不在")
# else:
#     print("在")


# a = "alex"
# if "b" in a:
#     print("在")
# else:
#     print("不在")

