# while 循环
# while -- 关键字
# if 条件:
#     结果

# while 条件:
#     循环体

# while True:
#     print("爱情买卖")
#     print("痒")
#     print("年少有为")
#     print("浮夸")
#     print("威风堂堂")
#     print("大悲咒")
#     print("情锁")

# num = 2
# while num > 0:
#     print("爱情买卖")
#     print("痒")
#     print("年少有为")
#     num = num - 1

# while True:
#     print("爱情买卖")
#     break


# while True:
#     print("爱情买卖")
#     print("痒")
#     continue
#     print("年少有为")


# 使用while输出10 - 57的数字(包含10和57)
# num = 10
# while num <= 57:
#     print(num)
#     num = num + 1

# 使用while 输出 100-10 的数字(包含100和10)

# num = 100
# while num > 9:
#     print(num)
#     num = num - 1

# while else
# print(11)
# while True:
#     print(123)
#     break
# print(234)

# flag = True
# while flag:
#     print(123)
#     flag = False
# print(234)

# flag = True
# while flag:
#     print(123)
#     flag = False
# else:
#     print(234)


# while True:
#     print(123)
#     break
# else:
#     print(234)

# flag = True
# while flag:
#     print(123)
#     flag = False
# else:
#     print(input(">>>"))


# while循环 -- 死循环
# while循环通过条件和break能够终止循环
# break -- 必须在循环中使用
# break -- 终止当前循环,并且break下方代码不会执行
# continue -- 跳出本次,继续下次循环(伪装成循环体中最后一个代码)
# continue -- 必须在循环中使用,且下方的代码不会被执行

