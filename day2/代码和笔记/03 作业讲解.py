"""
4.用print打印出下面内容：

文能提笔安天下,
武能上马定乾坤.
心存谋略何人胜,
古今英雄唯世君.
"""
# msg = """
# 文能提笔安天下,
# 武能上马定乾坤.
# 心存谋略何人胜,
# 古今英雄唯世君.
# """
# print(msg)


# a = "文能提笔安天下,"
# b = "武能上马定乾坤."
# c = "心存谋略何人胜,"
# d = "古今英雄唯世君."
# print(a)
# print(b)
# print(c)
# print(d)

"""
10.用户输入一个月份.然后判断月份是多少月.根据不同的月份,
打印出不用的饮食(根据个人习惯和老家习惯随意编写)
"""
# input()
# if
# print()

# num = int(input("请输入一个月份:"))
# if num == 1:
#     print("饺子")
# elif num == 2:
#     print("锅包肉")
# elif num == 3:
#     print("小龙虾")
# elif num == 4:
#     print("羊肉串")
# elif num == 5:
#     print("小米粥")
# elif num == 6:
#     print("兰州拉面")
# elif num == 7:
#     print("浆水面")
# elif num == 8:
#     print("大烧饼")
# elif num == 9:
#     print("煎饼")
# elif num == 10:
#     print("凉皮+日膜")
# elif num == 11:
#     print("海底捞")
# elif num == 12:
#     print("唇辣号")
# else:
#     print("输入错误!")