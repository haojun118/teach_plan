"""
-----------info----------
姓名:
年龄:
公司:
电话:
------------end-----------
"""


# name = input("name:")
# age = input("age:")
# addr = input("addr:")
# phone = input("phone:")

# a = "-----------info----------"
# b = "姓名:" + name
# c = "年龄:" + age
# d = "地址:" + addr
# e = "电话:" + phone
# f = "------------end-----------"
#
# print(a)
# print(b)
# print(c)
# print(d)
# print(e)
# print(f)


# name = input("name:")
# age = input("age:")
# addr = input("addr:")
# phone = input("phone:")

# info = """
# -----------info----------
# 姓名:%s
# 年龄:%s
# 公司:%s
# 电话:%s
# ------------end-----------
# """%(name,age,addr,phone)

# info = """
# -----------info----------
# 姓名:%s
# 年龄:%d
# 公司:%s
# 电话:%d
# ------------end-----------
# """%(name,int(age),addr,int(phone))

# print(info)

# msg = "%s的学习进度是2%%"%(1.5)
# print(msg)


# python3.6版本及以上才能使用
# name = "日魔"
# print(f"{name}的学习进度2%")

# print(f"{input('>>>')},{23},{34},{45}")
# print(f"{input('>>>')},{23},{34},{45}")
# print(f"{'meet'},{15},{'女'}")



# info = """
# -----------info----------
# 姓名:%s
# 年龄:%d
# 公司:%s
# 电话:%d
# ------------end-----------
# """%(name,int(age),addr,int(phone))

# msg = f"""-----------info----------
# 姓名:{input('name')}
# 年龄:{input('age')}
# 公司:{input('addr')}
# 电话:{input('phone')}
# ------------end-----------"""
# print(msg)

# %s -- 占字符串的位置(%s数字,字符串都能够进行填充)
# %d|%i -- 占数字的位置
# %% 转义 -- 转换成普通的百分号
# 占的位置和填充时必须要一一对应
# 填充的时候按照顺序填充


# f"" python3.6版本及以上才能使用