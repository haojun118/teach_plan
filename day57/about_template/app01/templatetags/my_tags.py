from django import template

register = template.Library()


@register.filter
def str_upper(value, arg='dsb'):
    ret = value + arg
    return ret.upper()
