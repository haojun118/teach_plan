## 内容回顾

### 1.django的命令

 1. 下载

    pip  install django==1.11.25 -i 源

	2. 创建项目

    django-admin startproject  项目名

3. 启动项目

   cd 项目的根目录

   python  manage.py runserver   # 127.0.0.1:8000

   python  manage.py runserver 80   # 127.0.0.1:80

   python  manage.py runserver 0.0.0.0:80   # 0.0.0.0:80

4. 创建APP

   python  manage.py  startapp  app名称

5. 数据库迁移的命令

   python  manage.py  makemigrations   #  检查已经注册的APP下的models.py的变更记录

   python  manage.py  migrate   # 将变更记同步到数据库中

### 2.django的配置

​	TEMPLATES   模板  DIRS  [ os.path.join(BASE_DIR,'templates') ]

​	 静态文件

​			STATIC_URL = '/static/'  # 静态文件的别名

​			STATICFILES_DIRS = [

​					os.path.join(BASE_DIR,'static')

​			]

​		注册的app

​		INSTALLED_APPS = [

​				'app01'

​				'app01.apps.App01Config'

​	   ]

​	 中间件

​			注释掉csrf中间件   可以提交POST请求

​	 数据库的配置

​			ENGINE  :    mysql

​			NAME:   数据库的名称

​			HOST:   ip 

​			PORT :   3306

​			USER :  用户名

​			PASSWORD :  密码

​    

### 3.django使用mysql数据库的流程

 1. 创建一个mysql数据库

 2. 数据库settings中的配置

    ​	 	   ENGINE  :    mysql

    ​			NAME:   数据库的名称

    ​			HOST:   ip 

    ​			PORT :   3306

    ​			USER :  用户名

    ​			PASSWORD :  密码

  3. 告诉django使用pymysql连接数据库

     写在与settingst同级目录下的`__init__.py`中：

     import pymysql

     pymysql.install_as_MySQLdb()

		4. 在models写类（继承models.Model）:

     ```python
     class Publisher(models.Model):
         pid = models.AutoField(primary_key=True)
         name = models.CharField(max_length=32)  # varchar(32)
         
         
     class Book(models.Model):
         title = models.CharField(max_length=32)  # varchar(32)
         pub = models.ForeignKey('Publisher',on_delete=models.CASCADE)
         
         
     class Author(models.Model):
         name = models.CharField(max_length=32)  # varchar(32)
         books = models.ManyToManyField(Book)  # 生成第三张表  不会在Author表中生成字段
     ```

		5. 执行数据库迁移的命令

     python  manage.py  makemigrations   #  检查已经注册的APP下的models.py的变更记录

     python  manage.py  migrate   # 将变更记同步到数据库中

### 4.request  请求相关的内容

​	request.GET     url上携带的参数   {}    get()

​    request.POST   POST请求提交的数据   {}    get()     getlist()

​	request.method  请求方法 GET POST 

### 5.响应

​	HttpResponse(‘字符串’)    返回的字符串‘

​	render(request,'模板的文件名',{})      返回的是 完整的页面

​	redirect（重定向的地址 ）  重定向

### 6.ORM

​	对象关系映射 

​	对应关系：

​	 类   ——》  表

​	对象  ——》  数据行（记录）

​	属性  ——》  字段

具体的操作：

  查询

```python
from app01 import  models
models.Publisher.objects.all()   #  查询所有的数据  QuerySet  对象列表
models.Publisher.objects.get(name='xxx')   #  查询满足条件的一条数据（唯一存在） 对象 不存在或者多个就报错
ret = models.Publisher.objects.filter(name='xxx')   #  查询满足条件所有的数据   对象列表
ret[0]
ret.first()   #  获取第一个对象  取不到就是None


for i in pubs：
	i.name
    i.pid   i.pk
    
book_obj.title
book_obj.pub    #  外键关联的对象
book_obj.pub_id    #  外键关联对象的id


author_obj.name
author_obj.books   # 关系管理对象
author_obj.books.all()   # 所关联的所有的对象   [book,book]
```

新增

```python
ret =  models.Publisher.objects.create(name='xxxx')  #  新增数据  返回一个对象
obj = models.Publisher（name='xxxx'）
obj.save()

models.Book.objects.create(title='xxxx',pub=出版社对象) 
models.Book.objects.create(title='xxxx',pub_id=出版社对象id) 

obj = models.Author.objects.create(name='xxx')
obj.books.set([book的id])
```

删除

```
 models.Publisher.objects.filter(pk=pk).delete()
 book_obj.delete()
```

编辑

```
book_obj.title = 'xxxx'
book_obj.pub_id =id
book_obj.save()

author_obj.name='xxx'
author_obj.save()
author_obj.books.set([book的id，book的id，book的id])
```

### 7.模板

```
render(request,'模板的文件名'，{k1:v1})

变量
{{  k1 }}

for循环
{% for i in list %}
	{{ forloop.counter }}
	{{ i }}
{% endfor %}

if判读

{% if 条件 %}
	满足条件后的结果
{% endif  %}


{% if 条件 %}
	满足条件后的结果
{% else %}	
	不满足条件后的结果
{% endif  %}


{% if 条件 %}
	满足条件后的结果
{% elif 条件1 %}
	满足条件后的结果	
{% else %}	
	不满足条件后的结果
{% endif  %}

```

## 内容回顾

### 1.MVC和MTV

MVC  

​	M:  model    模型 数据库交互

​	V：view  视图   展示给用户看的  HTML 

​    C ：controller   控制器  业务逻辑  传递指令

MTV:

​	M:  model  模型   ORM

​	T:  template  模板   

​	V:  view   视图  业务逻辑

### 2.变量

#### 1.点的使用

{{  变量 }}

  .索引  . key  .属性   .方法

优先级：

key   >   属性或者方法  >  数字索引

#### 过滤器

  {{ value|filter_name }}

  {{ value|filter_name:参数 }}

##### 内置的过滤器

https://docs.djangoproject.com/en/1.11/ref/templates/builtins/#built-in-filter-reference

default

```
{{  变量|default:'默认值'  }}   变量不存在或者为空 显示默认值
```

filesizeformat  

显示文件大小

add

```
{{ 2|add:'2'  }}   数字加法
{{ 'a'|add:'b'  }}   字符串的拼接
{{ [1,2]|add:[3,4]  }}   列表的拼接
```

length  

返回变量的长度

slice

切片

```
{{ string|slice:'-1::-1' }}
```

date  日期格式化

```
{{ now|date:'Y-m-d H:i:s' }}
```

safe

```
{{ a|safe }}  告诉django不需要进行转义 

```

##### 自定义过滤器

1. 在app下创建一个名叫templatetags的python包  （templatetags名字不能改）

2. 在包内创建py文件 （文件名可自定义 my_tags.py）

3. 在python文件中写代码：

   ```
   from django import template
   register = template.Library()  # register名字不能错
   ```

4. 写上一个函数 + 加装饰器

   ```
   @register.filter
   def str_upper(value,arg):  # 最多两个参数
       ret = value+arg
       return ret.upper()
   ```

使用：

​	在模板中

```
{% load my_tags  %}
{{ 'alex'|str_upper:'dsb'  }}
```









