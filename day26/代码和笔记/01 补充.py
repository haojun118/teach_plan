# 1.我个人理解: 在嵌套函数内,使用非本层且非全局的变量就是闭包

# 官方:
# 1.在嵌套函数内部使用非全局变量
# 2.并将嵌套函数返回


# a = 10
# def func(a):
#     def foo():
#         # a = 19
#         print(a)
#     print(foo.__closure__)
# func(1)


# 修改递归的最大深度
# import sys
# sys.setrecursionlimit(1000000000)
#
# def func():
#     print(1)
#     func()
# func()


# def argv(d):
#     def func(func):
#         def foo(*args,**kwargs):
#             func(*args,**kwargs)
#         return foo
#     return func


# def func(a):
#     return a

# def func(a):
#     return a
#
# def foo():
#     func(1)
# foo()

# def func(a):
#     return a
#
# @func   # foo = func(foo)
# def foo():
#     print(1)
# foo()
#
#
# def f(a):
#     return a
#
# def foo():
#     f(f)
# foo()

# 只要能写糖的都是装饰器