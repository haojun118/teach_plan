# 1.双下方法:
    # ***
        # __str__  : str()  print
        # __call__ : 对象()
        # __new__  : 实例化时自动触发,先执行__new__在执行__init__ (面试手写单例模式)

    # __len__ : len()
    # __hash__: hash()
    # __repr__: %r
    # __eq__ : ==
    # __del__ : del
    # __item__ 系列 和字典操作一样
        # __getitem__ : dic["键"]
        # __setitem__ : dic["键"] = 值
        # __delitem__ : del dic["键"]

# 2.异常处理:

# 错误:
    # 1.语法错误
    # 2.逻辑错误

# 常用异常:
    # SyntaxError
    # IndexError
    # KeyError
    # ValueError
    # NameError
    # TypeError
    # StopIteration
    # ImportError
    # Exception

# 异常的编写结构
    # try: #尝试
    #     int("alex")  # 尝试运行的代码
    #
    # except ValueError as e:  # 捕捉异常
    #     print(e) # 显示异常

# 异常处理:
    # 1.防止程序中断运行
    # 2.出现异常,程序终止用户体验不良好

# 异常的处理方式:
    # 1.if (避免出现报错)  if可以进行异常处理,但是只能针对某一段代码.
    # 2.try(捕捉错误)      可以捕获异常,保证程序不中断运行

# 分支:
    # try:
    #     num = int(input("请输入数字:"))
    #     lst = [1,2,3]
    #     # dic = {"name":"meet",1:"rimo"}
    #     # print(dic[num])
    #     print(lst[num])
    #
    # except ValueError as e:
    #     print(e)
    #
    # except KeyError as e:
    #     print(f"没有{e}这个键")
    #
    # except IndexError as e:
    #     print(e)
    #
    # except Exception as e:
    #     print(e)

# 分支 + else

    # try:
    #     num = int(input("请输入数字:"))
    #     lst = [1,2,3]
    #     # dic = {"name":"meet",1:"rimo"}
    #     # print(dic[num])
    #     print(lst[num])
    #
    # except ValueError as e:
    #     print(e)
    #
    # except KeyError as e:
    #     print(f"没有{e}这个键")
    #
    # except IndexError as e:
    #     print(e)
    #
    # except Exception as e:
    #     print(e)
    #
    # else:
        # print("没有异常走我")

# 分支 + else + finally

    # try:
    #     num = int(input("请输入数字:"))
    #     lst = [1,2,3]
    #     # dic = {"name":"meet",1:"rimo"}
    #     # print(dic[num])
    #     print(lst[num])
    #
    # except ValueError as e:
    #     print(e)
    #
    # except KeyError as e:
    #     print(f"没有{e}这个键")
    #
    # except IndexError as e:
    #     print(e)
    #
    # except Exception as e:
    #     print(e)
    #
    # else:
    #     print("没有异常走我")
    # finally:
    #      print("清理工作")

    # assert 断言

# 推荐:使用try,但是不建议在任意位置添加try