# 双下方法:给源码程序员使用,咱们慎用 (以便你们更好的阅读源码)

# __len__

# class A(object):
#
#     def __init__(self,name):
#         self.name = name
#         print("触发了__init__")
#
#     def __len__(self):  # len() 触发
#         print("走这里")
#         return len(self.name)    # return len("alex")  str中__len__
#         # 必须有返回值,返回值的类型必须是整型
#
# a = A("alexqrqwr")
# print(len(a))

# str
# a = "12345" # str这个类的实例
# lst = [1,2,3,4,4,5,5,5,5] # list这个类的实例
# print(len(a))
# print(len(lst))

# __hash__  hash()触发

# class A(object):
#
#     def __init__(self,name,age):
#         self.name = name
#         self.age = age
#
#     def __hash__(self):  # hash()
#         hash({1,2,345})  # 可变数据类,不可数据类型
#         # 必须有返回值,返回值的类型必须是整型
#
# a = A("meet",25)
# print(hash(a))


# __str__   ***  Django

# class A:
#
#     def __init__(self,name,age,sex):
#         self.name = name
#         self.age = age
#         self.sex = sex
#
#     def __str__(self):   # print 触发 str()
#         print(111)
#         return f"姓名:{self.name} 年龄:{self.age} 性别:{self.sex}"
#         # 必须有返回值,返回值的类型必须是字符串
#
#
# a = A("meet",20,"男")
# a1 = A("meet11",200,"男11")
# str(a)
# print(a)
# print(a1)

# a = A("meet",20,"男")
# a1 = A("meet2",200,"女")
#
# print(f"姓名:{a.name} 年龄:{a.age} 性别:{a.sex}")   # "姓名:meet 年龄:20 性别:男"
# print(f"姓名:{a1.name} 年龄:{a1.age} 性别:{a1.sex}")  # "姓名:meet2 年龄:200 性别:女"

# __repr__

# class A:
#
#     def __init__(self):
#         pass
#
#
#     def __repr__(self):   # print触发  %r
#         print(1111)
#         return "日魔"
#
#     def __str__(self):   # str 优先级高于 repr  两个都存在就只执行str
#         return "宝元"
#
# a = A()
# print("%r"%(a))

# __call__   *** 面试题

# class A:
#
#     def __init__(self):
#         pass
#
#     def __call__(self, *args, **kwargs):  # 对象()时调用的__call__
#         print("走我")
#         print(*args, **kwargs)
#
# a = A()
# a()


# __eq__ # 等于

# class A(object):
#
#     def __init__(self,name,age):
#         self.name = name
#         self.age = age
#
#     def __eq__(self, other):     #  a == a1
#         if self.name == other.name:
#             return True
#         else:
#             return False
#
# a = A("meet",56)
# a1 = A("meet",108)
#
# print(a == a1)

# __del__  析构方法

# class A:
#     def __init__(self):
#         pass
#
#     def __del__(self):    del 触发
#         print("删除时执行我")
#
# a = A()
#
# import time
# time.sleep(5)
# del a

# a = 1
# b = a
# a = b

# 垃圾回收
    # 80  5/s
    # 引用计数
    # 标记清除
    # 分袋回收 袋一:10 2/h  袋二: 5/3 4h  袋三: 3 20h


# __new__  单例模式(面试必问)  # 工厂模式 等等

# class A(object):
#
#     def __init__(self,name):  # 初始化
#         self.name = name
#         print("我是__init__,先走我")
#
#     def __new__(cls, *args, **kwargs):
#         obj = object.__new__(A)
#         print("我在哪:",obj)
#         return obj                   # obj == __init__()
#
#         # print("我是__new__,先走我")
#         # return "啦啦啦"
#
# a = A("meet")
# print("我是谁:",a)


# class A:
#
#     def __init__(self):
#         print(1111)
#
# a = A()
# print(a)

# class A(object):
#
#     def __init__(self,name): # 初识化
#         self.name = name


    # def __new__(cls, *args, **kwargs):
    #     obj = object.__new__(A)   # 调用的是object类中的__new__ ,只有object类中的__new__能够创建空间
    #     return obj   #本质: obj == __init__()     return __init__()  # 触发了__init__方法
    #     # print("先执行的是我")
    #     # return "宝元"


# a = A("meet")  # a是对象的内存地址
# a1 = A("日魔")  # a是对象的内存地址
# a2 = A("朝阳")  # a是对象的内存地址
# print(a.name)
# print(a1.name)
# print(a2.name)


# 先执行__new__方法在执行__init__方法

# class A:
#     __a = None  #__a =  0x000001F346079FD0
#
#     def __init__(self,name,age):
#         self.name = name
#         self.age = age
#
#     def __new__(cls, *args, **kwargs):
#         if cls.__a is None:
#             obj = object.__new__(cls)
#             cls.__a = obj
#         return cls.__a
#
# a = A("meet",123)  # 0x000001F346079FD0
# a1 = A("日魔",11)
# print(a.name)
# print(a1.name)


# 单例模式:不管你创建多少次,使用的都是同一个内存空间
# 模块的导入,手写的单例模式
# 实例化对象时发生的事
# 1.创建对象,并开辟对象空间 __new__
# 2.自动执行__init__方法


# __item__  可以向操作字典一样操作实例方法
# dic["键"] = 值
# del dic["键"]
# dic["键"]

# class A:
#
#     def __init__(self,name,age):
#         self.name = name
#         self.age = age
#
#     def __getitem__(self, item):
#         print(self.__dict__)
#         print(self.__dict__[item])  # self.__dict__ 获取的就是字典
#
#     def __setitem__(self, key, value):
#         self.__dict__[key] = value
#
#     def __delitem__(self, key):
#         del self.__dict__[key]
#
# a = A("meet",58)
# a["sex"] = "男"
# a["sex"]
# del a["sex"]
# print(a.__dict__)



# 了解一下上下文
# __enter__
# __exit__

# class my_open:
#
#     def __init__(self,file,mode="r",encoding="utf-8"):
#         self.file = file
#         self.mode = mode
#         self.encoding = encoding
#
#     def __enter__(self):
#         self.f = open(self.file,self.mode,encoding=self.encoding)
#         return self.f
#
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         print(exc_type,exc_val,exc_tb) # 没有错误时就是None
#         self.f.close()
#
#
# with my_open("a.txt") as ffff:
#     for i in ffff:
#         print(i)
# print(ffff.read(1111))
# print(ffff.read())
# with open("a") as f: 使用了上下文