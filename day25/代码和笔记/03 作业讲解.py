# class Base1:
#     def f1(self):
#         print('base1.f1')
#
#     def f2(self):
#         print('base1.f2')
#
#     def f3(self):
#         print('base1.f3')
#         self.f1()
#
#
# class Base2:
#     def f1(self):
#         print('base2.f1')
#
#
# class Foo(Base1, Base2):
#     def fo(self):
#         print('foo.fo')
#         self.f3()
#
#
# obj = Foo()
# obj.fo()


# class A:
#     pass
#
# class B(A):
#     pass
#
# class C(A):
#     pass
#
# class D(A):
#     pass
#
# class E(B,C):
#     pass
#
# class F(C,D):
#     pass
#
# class G(D):
#     pass
#
# class H(E,F):
#     pass
#
# class I(F,G):
#     pass
#
# class K(H,I):
#     # name = "111"
#     pass

# k = K()
# print(k.name)