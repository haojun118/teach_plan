# 属性: 组合(伪装)

# class A:
#
#     live = "有思想"
#
#     def func(self):
#         print("is A func")
#
# A.live
# A.func()

# class A:
#
#     @property
#     def func(self):
#         print("is A func")
#
# a = A()
# a.func

# BMI  人体体质指标
# BMI = 体重(kg) / (身高(m) ** 2)

# class Bmi:
#
#     live = "有思想"
#
#     def __init__(self, height, weight):
#         self.height = height
#         self.weight = weight
#
#     @property
#     def bmi(self):
#         return self.weight / self.height ** 2
#
#     @bmi.setter
#     def bmi(self,value):
#         print(value)
#         print("设置属性时执行我")
#
#     @bmi.getter
#     def bmi(self):
#         print("查看属性时执行我")
#
#     @bmi.deleter
#     def bmi(self):
#         print("删除属性时执行我")
#
# rimo = Bmi(1.8,100)
# print(rimo.live) # 真属性


# rimo.live = "无脑"
# del Bmi.live
# print(rimo.live)
# print(rimo.bmi)
# print(Bmi.__dict__)

# rimo.bmi = 20    # 伪装成属性进行设置
# print(rimo.bmi)  # 伪装成属性
# del rimo.bmi


# class Bmi:
#
#     live = "有思想"
#
#     def __init__(self, height, weight):
#         self.height = height
#         self.weight = weight
#
#     @property
#     def bmi(self):
#         return self.weight / self.height ** 2
#
#     @bmi.setter
#     def bmi(self,value):
#         print(value)
#         print("设置属性时执行我")
#
#     @bmi.getter
#     def bmi(self):
#         print("查看属性时执行我")
#
#     @bmi.deleter
#     def bmi(self):
#         print("删除属性时执行我")
#
# rimo = Bmi(1.8,100)
# print(rimo.live) # 真属性


# class Foo:
#
#     def get_AAA(self):
#         print('get的时候运行我啊')
#
#     def set_AAA(self,value):
#         print('set的时候运行我啊')
#
#     def delete_AAA(self):
#         print('delete的时候运行我啊')
#
#     AAA = property(get_AAA,set_AAA,delete_AAA) #内置property三个参数与get,set,delete一一对应

# f1=Foo()
# f1.AAA
# f1.AAA = 'aaa'
# del f1.AAA