# 1.类方法

# class A:
#
#     def func(self):         #实例方法
#         print("is A func")
#
# # a = A()  # 实例化 一个对象(实例)
# A.func()  # 添加参数
# a.func()

# import time
# class Times:
#
#     t = time.time()   # 类属性
#
#     @classmethod
#     def func(cls):
#         print(cls)   # 类的地址
#         print("is A func")
#         # object = cls()
#         # object = cls()
#         cls.t = 1010101

# a = A()
# a.func()

# Times.func()
# print(Times.t)


# class Human:
#     count = 0
#
#     def __init__(self, name, age):
#         self.name = name
#         self.age = age
#
#     @classmethod
#     def add_count(cls):
#         cls.count += 1
#
#     @classmethod
#     def get(cls):
#         return cls.count

# Human("日魔",56)
# Human("日魔",56)
# Human("日魔",56)
# Human("日魔",56)
# Human("日魔",56)
# Human("日魔",56)
# Human("日魔",56)
# Human("日魔",56)
# Human("日魔",56)
# Human("日魔",56)
# print(Human.get())

# 类方法可以自动变换类名 (最大的作用)
# 1.使用类方法可以获取到类地址进行实例化
# 2.可以通过类名修改类属性
# 类方法偶然会使用,使用最多的还是实例方法

# 2.静态方法 (使用更少)

# class A:
#
#     def func(self):        # 实例方法
#         print("is A func")
#
#     @classmethod
#     def cls_func(cls):     # 类方法
#         print("is A cls_func")

    # @staticmethod
    # def static_func():
    #     print("他就是普通的函数了")

# 静态方法不依赖于对象和类 (静态方法就是一个普通的函数)

# def static_func():
#     print("他就是普通的函数了")

# A.static_func()
# a = A()
# a.static_func()

# 3.属性(组合)