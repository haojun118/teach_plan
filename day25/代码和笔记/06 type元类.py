# type  查看数据类型 (能够查看出当前内容从属于那个类)

# print(type("alex"))
# print(type([1,2,3,4]))
# print(type((1,2,3,40)))

# python自带的类

# class A:
#     pass
#
# a = A()

# print(type(a))

# print(type(A))
# print(type(str))
# print(type(list))
# print(type(tuple))
# print(type(object))

# python自带的 str,list,tuple 也是某一个类的对象 (python中一切皆对象)


# isinstance 判断参数1是不是参数2的对象
# print(isinstance([1,2,3],list))
# print(isinstance(str,type))
# print(isinstance(list,type))
# print(isinstance(object,type))

# issubclass 判断参数1是不是参数2的子类

# class A:
#     pass
#
# class B(A):
#     pass
#
# print(issubclass(B,A))

# 元类 -- 构建类
# object与type的关系 (了解)

# 1.object是type的对象
# 2.新式类都默认继承object

# print(issubclass(type,object))  # type是object的子类
# print(isinstance(object,type))  # object是type的对象