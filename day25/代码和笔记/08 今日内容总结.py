# 1.类的其他成员
    # 实例方法
    #     依赖对象,不需要装饰器装饰,方法的参数是self
    # 类方法
    #     @classmethod :  依赖类 被classmethod装饰器装饰的方法参数是cls
    # 静态方法
    #     @staticmethod : 不依赖对象和类  就是一个普通的函数

    # 属性(组合)

        # @property
        # def bmi():
        #     pass
        # @bmi.setter
        # @bmi.getter
        # @bmi.deleter


# 2.元类

    # python大部分自带的类和自己定义的类都是由type类实例化出来的
    # python中一切皆对象
    # issubclass()   # 判断参数1是不是参数2的子类
    # isinstance()   # 判断参数1是不是参数2的对象

    # object 和 type 的关系
    # object 是 type 的对象
    # type 是 object 的子类


# 3.反射
# 通过字符串操作对象的属性和方法

# 1.对象的角度使用反射
# 2.类的角度使用反射
# 3.当前模块使用反射
# 4.其他模块使用反射

# 模块部分使用反射的方式:
    # 方法一:
    #     import sys
    #     sys.modules[__name__]

    # 方法二:
    #     globals()[__name__]

# hasattr(对象,字符串)
# getattr(对象,字符串,查找不到的返回值)
# setattr(对象,字符串,值)
# delattr(对象,字符串)

# hasattr() 及 getattr() 配合使用

# if hasattr():
    # getattr()