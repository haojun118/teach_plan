# 1.鸭子类型
# 看的像鸭子就是鸭子
# class A:
#
#     def f1(self):
#         print("这是f1")
#
# class B:
#
#     def f1(self):
#         print("这是f2")

# 统一接口

# def _(object):
#     object().f1()
# _(B)

# python中 str,list,tuple ... 都使用的是鸭子类型

# 2.类的约束
# 控制子类定义某些方法
# 方法一:
from abc import ABCMeta,abstractmethod
# 方法二:
# raise Exception(万能异常) #主动抛出异常  (推荐使用)

# 3.super
# super的使用场景:
# 1.多继承
# 2.新式类
# 3.会根据不同的当前类,按照mro的顺序自动查找当前类的下一个类

# super 是按照mro的顺序进行查找
# super(类名,self) # super会依照mro的顺序获取类名的下一个类,self是指定mro的列表

# 4.类的私有成员
# 私有:只能自己拥有
# 以__开头的变量名就是私有的
# _变量名(程序员约定俗称的私有)

# 私有的类的属性
# 私有的类的方法
# 私有的对象属性

# 私有属性和方法可以在当前类中使用,类外部不能正常访问
# 子类不能继承父类的私有方法
# 私有方法的作用是保护数据的安全性
# 子类可以强制继承父类的方法(非常不建议使用)

# python2和python3的区别
# python2 中 中文存储在容器中是以字节的形式显示
# python2 中 字符串 -- unicode   字节 -- str

