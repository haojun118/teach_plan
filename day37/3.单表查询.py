# select
    # select * from 表;
    # 指定列查询
    # select emp_name,salary from employee;
    # 在列中使用四则运算
    # select emp_name,salary*12 from employee;
    # 重命名
    # select emp_name,salary*12 as annul_salary from employee;
    # select emp_name,salary*12 annul_salary from employee;
    # 去重
    # select distinct post from employee;
    # select distinct sex,post from employee;


    # 函数 concat() 拼接
    # select concat('姓名 ：',emp_name),concat('年薪:',salary*12) from employee;
    # select concat_ws('|','a','b','c')

    # case when语句 == if条件判断句


# where语句 根据条件筛选行
    # 比较运算 = > < >= <= !=/<>
        # select * from employee where age>18;
        # select * from employee where salary<10000;
        # select * from employee where salary=20000;
    # between a and b    #[a,b]
        # select * from employee where salary between 10000 and 20000;
    # in
        # select * from employee where salary in (17000,19000);
    # like 模糊查询
        # _ 通配符  表示一个字符长度的任意内容
        # select * from employee where emp_name like 'jin___'
        # % 通配符  表示任意字符长度的任意内容
        # select * from employee where emp_name like 'jin%'
        # select * from employee where emp_name like '%g'
        # select * from employee where emp_name like '%n%'
    # regexp 正则匹配
        # select * from employee where emp_name regexp '^jin'

# 逻辑运算
    # and
        # select * from employee where age>18 and post='teacher';
    # or
        # select * from employee where salary<10000 or salary>30000;
    # not
        # select * from employee where salary not in (10000,17000,18000);

# 关于null
    # 查看岗位描述为NULL的员工信息
    # select * from employee where post_comment is null;
    # 查看岗位描述不为NULL的员工信息
    # select * from employee where post_comment is not null;

# 5个聚合函数
    # count
    # max
    # min
    # avg
    # sum

# 分组聚合 group by
    # 查询岗位名以及岗位包含的所有员工名字
    # select post,group_concat(emp_name) from employee group by post;

    # 查询各部门年龄在20岁以上的人的平均薪资
    # select post,avg(salary) from employee where age>20 group by post;

    # select * from 表 where 条件 group by 分组

# 过滤 having (group by + 聚合函数)
    # 查询平均薪资大于1w的部门
    # select avg(salary) from employee group by post having avg(salary) > 10000

    # 1. 查询各岗位内包含的员工个数小于2的岗位名、岗位内包含员工名字、个数
    # select post,emp_name,count(id) from employee group by post having count(id)<2

    # 2. 查询各岗位平均薪资大于10000的岗位名、平均工资
    # select post,avg(salary) from employee group by post having avg(salary) > 10000

    # 3. 查询各岗位平均薪资大于10000且小于20000的岗位名、平均工资
    # select post,avg(salary) from employee group by post having avg(salary) between 10000 and 20000;

# order by 排序
    # 升序
    #  select * from employee order by salary;
    #  select * from employee order by salary asc;
    # 降序
    #  select * from employee order by salary desc;

    # select * from employee order by age,salary;
    # select * from employee order by age,salary desc;
    # select * from employee order by age desc,salary;

# limit
    # select * from 表 order by 列 limit n; 取前n条
    # select * from 表 order by 列 limit m,n; 从m+1开始,取n条
    # select * from 表 order by 列 limit n offset m; 从m+1开始,取n条


# select * from 表 where 条件 group by 分组 having 过滤 order by 排序 limit n;