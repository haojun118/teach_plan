# 数据操作
# create table emp2(
# id int,
# name char(12),
# age  tinyint unsigned,
# gender enum('male','female'),
# salary float(7,2),
# hobbey set('linux','python','go')
# );

# 增 insert
# insert into emp values (3,'alex',84,'female',2.2,'linux,python'),
#                        (4,'alex',84,'female',2.2,'linux,python');
# insert into emp(id,name) values (5,'wusir'), (6,'wusir');
# insert into emp2 select * from emp;
# insert into emp2(id,name) select id,name from emp;

# 删 delete

# 改 update
# update 表 set 字段1=值1,字段2=值2 where 条件;

# 单表查询

# \c 取消当前操作
# '>' \c

# 编码问题
# 1.临时解决问题 在客户端执行 set xxxx = utf8;
# 2.永久解决问题 在my.ini添加 set xxxx = utf8;
# 3.实时解决问题 create table 表名() charset=utf8;
