# day37
# create database day37;

# 查看当前有哪些库
# show databases;

# 切换到库
# use day37

# 创建表
# create table emp(
# id int,
# name char(12),
# age  tinyint unsigned,
# gender enum('male','female'),
# salary float(7,2),
# hobbey set('linux','python','go')
# );
# id name age gender salary hobby('linux','python','go')

# 写数据
# insert into emp values (1,'alex',84,'male',1000000,'linux,python');

# 修改数据
# update emp set salary = 2 where id = 1;


# server端 192.168.12.44
# 第一次安装成功之后 启动它 net start mysql
# 之后它会开启自动启动的

# client端
# mysql -u用户名 -h192.168.12.44 -p
# password:密码

# mysql> create database 库名;
# mysql> create table 表(字段1 类型(长度) 约束,字段2 类型(长度) 约束,...);

# mysql> show databases; 查看所有的数据库
# mysql> show tables;    查看当前数据库下的所有表

# mysql> use 数据库

# mysql> drop table 表名;

# mysql> desc 表结构;
# mysql> describe 表结构;        # 结构更清晰，但是能够显示的信息有限
# mysql> show create table 表名; # 查看建表语句，显示的信息更全面，可读性差

# 数据的操作
# mysql> insert into 表名 values (值1,值2,...)
# mysql> delete from 表名 where 条件;
# mysql> update 表名 set 字段=新的值 where 条件
# mysql> select * from 表

# select user();
# select database();

# 基础数据类型
# 数字类型
    # 整数 tinyint int unsigned
    # 小数 float
# 字符串类型
    # char ：定长的 节省时间 浪费空间     手机号码、身份证号
    # varchar ： 变长的 节省空间 浪费时间 评论
# 时间类型
    # now()
    # datetime 日志,消费记录,上下班打卡
    # time     计时软件
    # date     入职日期 出账日期 还款日期
    # year     酒的产期
    # timestamp 不能为空，自动写当前时间，级联更新，范围小
# 枚举和集合
    # enum 单选
    # set  多选，自动去重




















