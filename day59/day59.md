##  内容回顾

模板

### 变量

{{  变量 }}

.key    .属性   .方法  .索引 

### 过滤器

{{  变量|过滤器 }}  {{  变量|过滤器:参数 }}

default add filesizeformart  date safe join truncatechars 

### for 循环

{%  for i in list %}

​		{{ i }}    {{ forloop }}

{%  endfor %}



forloop .counter 当前循环的序号  从1开始

forloop .counter0 当前循环的序号  从0开始

forloop .revcounter 当前循环的序号  到1结束

forloop .revcounter0 当前循环的序号  到0结束

forloop .first    当前循环是否是第一次循环 布尔值  

forloop . last    当前循环是否是最后一次循环 布尔值

forloop .parentloop  当前循环的外层循环的参数



for ... empty 

{%  for i in list %}

​		{{ i }}    {{ forloop }}

{% empty  %}

​    list 为空的时候 

{%  endfor %}



### if判断

{% if 条件 %}

​	显示的内容

{%  endif %}



{% if 条件 %}

​	显示的内容

{% elif 条件1 %}

​	显示的内容

{%  endif %}



{% if 条件 %}

​	显示的内容

{% elif 条件1 %}

​	显示的内容

{% else%}

​	显示的内容

{%  endif %}

if 要注意的内容：

	1. 不支持算数运算 （过滤器） 
 	2. 不支持连续判断  （10>5>1）

### with

{%  with  alex.name as alex_name %}

​	{{  alex_name }}

{% endwith %}



{%  with  alex_name=alex.name %}

​	{{  alex_name }}

{% endwith %}



### csrf_token

{% csrf_token %}写在form标签内，form标签有一个隐藏的input标签，name=‘csrfmiddlewaretoken’ 

### 母版和继承

母版：

​	本身就是一个HTML页面，提取到多个页面的公共部分，页面中定义多block块。

子页面继承：

	1. {% extends  ‘母版文件名’ %}
 	2. 重写block块

注意点：

	1. {% extends  ‘母版文件名’ %}   文件名带引号   不带引号当做变量
 	2. {% extends  ‘母版文件名’ %}  上面不要写内容   想要显示的内容写在block块中
 	3. 母版中多定义写block块  css  js

组件

​	一小段HTML代码段  ——》  HTML文件中

​	{% include 'nav.html' %}

### 静态文件

​	{% load static %}

​	{%  static  '相对路径'  %}

​	{%  get_static_prefix %}   STATIC_URL 

### 模板自定义方法

filter   simple_tag  inclusion_tag

定义：

 1. 在已注册的app下创建templatetags的python包；

 2. 在包中创建py文件 （文件名任意指定  my_tags.py）

 3. 在py文件中写固定的代码：

    ```
    from  django  import template
    register = template.Library()   #  register的名字也不能错
    ```

	4. 写函数 +  加装饰器

    ```python 
    # filter
    @register.filter
    def str_upper(value,arg)  
    	return 'xxxxx'
    
    #simple_tag
    @register.simple_tag
    def str_join(*args,**kwargs)：
    	return 'xxxxx'
    
    #inclusion_tag
    @register.inclusion_tag('li.html')
    def show_li(num,*args,**kwargs):
        return {'num':range(1,num+1)}
    
    # li.html
    <ul>
    	{% for i in num %}
        	<li>{{ i }}</li>
        {% endfor %}
    </ul>
    
    ```

使用：

```
{% load my_tags  %}

{{ 'xxx'|str_upper:'参数'  }}   

{% str_join 1 2 3 k1=4 k2=5 %}

{% show_li 3 %}
' 
<ul>
	<li>1</li>
	<li>2</li>
	<li>3</li>
</ul>
'
```

## 今日内容

视图

MVC MTV

### FBV  和 CBV

FBV   function based view

CBV   class based view

#### 定义CBV

```
from django.views import View
class PublisherAdd(View):
    
    def get(self,request):
        # 处理get请求的逻辑
        return response
    
    def post(self,request):
        # 处理post请求的逻辑
        return response

```

#### 写对应关系

```
 url(r'^publisher_add/', views.PublisherAdd.as_view()),
```

### as_view的流程

1. 程序加载时，执行View中as_view的方法 ，返回一个view函数。

2. 请求到来的时候执行view函数：

   1. 实例化类 ——》 self

   2. self.request = request 

   3. 执行self.dispatch(request, *args, **kwargs)

      1. 判断请求方式是否被允许：

         1. 允许

            通过反射获取当前对象中的请求方式所对应的方法

            handler = getattr(self, request.method.lower(), self.http_method_not_allowed)

          2. 不允许

             handler = self.http_method_not_allowed

         	3. 执行handler  获取到结果，最终返回给django



### 装饰器补充

```
from functools import wraps


def wrapper(func):
    @wraps(func)
    def inner(*args, **kwargs):
        # 之前
        ret = func(*args, **kwargs)
        # 之后
        return ret

    return inner
```

### django中给视图加装饰器

FBV 直接给函数添加装饰器

CBV

from django.utils.decorators import method_decorator

1. 加在方法上

   ```
   @method_decorator(wrapper)
   def get(self,request):
   ```

2. 加在dispatch方法上

   ```
   @method_decorator(wrapper)
       def dispatch(self, request, *args, **kwargs):
           ret = super().dispatch(request, *args, **kwargs)
           return ret
   
   
   @method_decorator(wrapper, name='dispatch')
   class PublisherAdd(View):
   ```

3. 加在类上

   ```
   @method_decorator(wrapper, name='post')
   @method_decorator(wrapper, name='get')
   class PublisherAdd(View):
   ```

### request

``` python 
request.method    # 请求方式
request.GET		#  URL上携带的参数
request.POST	# POST请求提交的数据   enctype="application/x-www-form-urlencoded"
request.FILES	# 上传的文件   enctype="multipart/form-data"
request.path_info   # 路径信息  不包含IP和端口  也不包含查询参数
request.body   # 请求体 原始数据
request.COOKIES  # cookies
request.session  # session
request.META   # 请求头的信息

request.get_full_path() # 路径信息  不包含IP和端口 包含查询参数
request.is_ajax()   # 是否是ajax请求 布尔值
```

### response

```
HttpResponse('字符串')    # 返回字符串
render(request,'模板的文件名'，{})   # 返回一个页面
redirect（地址） # 重定向   响应头 Location： 地址
```

