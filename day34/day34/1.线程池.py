"""
import threading
import time
def task():
    time.sleep(2)
    print('任务')


num = int(input('请输入要执行的任务个数:'))
for i in range(num):
    t = threading.Thread(target=task)
    t.start()
"""


# ####################### 示例2 ############################
"""
import time
from concurrent.futures import ThreadPoolExecutor
def task(n1, n2):
    time.sleep(2)
    print('任务')
# 创建线程池
pool = ThreadPoolExecutor(10)
for i in range(100):
    pool.submit(task, i, 1)
print('END')
# 等线程池中的任务执行完毕之后,再继续往下走
pool.shutdown(True)
print('其他操作,依赖线程池执行的结果')
"""

# ####################### 示例3 ############################
"""
import time
from concurrent.futures import ThreadPoolExecutor

def task(arg):
    time.sleep(2)
    print('任务')
    return 666
# 创建线程池
pool = ThreadPoolExecutor(10)
ret = pool.map(task,range(1,20))
print('END',ret)
pool.shutdown(True)
for item in ret:
    print(item)
"""

# ####################### 示例4 ############################
"""
import time
from concurrent.futures import ThreadPoolExecutor
def task(n1, n2):
    time.sleep(2)
    print('任务')
    return n1+n2
# 创建线程池
pool = ThreadPoolExecutor(10)
future_list = []
for i in range(20):
    fu = pool.submit(task, i, 1)
    future_list.append(fu)

pool.shutdown(True)
for fu in future_list:
    print(fu.result())
"""
