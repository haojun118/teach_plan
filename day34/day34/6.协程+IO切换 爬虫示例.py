from gevent import monkey
monkey.patch_all()


import gevent
import requests

def f1(url):
    print('GET: %s' % url)
    data = requests.get(url)
    print('%d bytes received from %s.' % (len(data.content), url))

def f2(url):
    print('GET: %s' % url)
    data = requests.get(url)
    print('%d bytes received from %s.' % (len(data.content), url))

def f3(url):
    print('GET: %s' % url)
    data = requests.get(url)
    print('%d bytes received from %s.' % (len(data.content), url))

gevent.joinall([
        gevent.spawn(f1, 'https://www.python.org/'),
        gevent.spawn(f2, 'https://www.yahoo.com/'),
        gevent.spawn(f3, 'https://github.com/'),
])