import threading
from queue import Queue
import time

q = Queue()

def send(to,subject,text):
    import smtplib
    from email.mime.text import MIMEText
    from email.utils import formataddr

    # 写邮件的内容
    msg = MIMEText(text, 'plain', 'utf-8')
    msg['From'] = formataddr(["炮手", 'zh309603@163.com'])
    msg['To'] = formataddr(["老板", to])
    msg['Subject'] = subject

    server = smtplib.SMTP_SSL("smtp.163.com", 465)
    server.login("zh309603", "zhzzhz123")  # 授权码
    server.sendmail('zh309603@163.com', [to, ], msg.as_string())
    server.quit()

def producer(i):
    """
    生产者
    :return:
    """
    print('生产者往队列中放了10个任务',i)
    info = {'to':'424662508@qq.com', 'text':'你好','subject':'好友请求'}
    q.put(info)


def consumer():
    """
    消费者
    :return:
    """
    while True:
        print('消费者去队列中取了任务')
        info = q.get()
        print(info)
        send(info['to'],info['subject'],info['text'])

for i in range(10):
    t = threading.Thread(target=producer,args=(i,))
    t.start()

for j in range(5):
    t = threading.Thread(target=consumer)
    t.start()