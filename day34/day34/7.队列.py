"""
队列:
    Queue
    redis中的列表
    rabbitMQ


"""
from queue import Queue
q = Queue()

"""
q.put('123')
q.put(456)

v1 = q.get()
v2 = q.get()
print(v1,v2)
"""

# 默认阻塞
v1 = q.get()
print(v1)
